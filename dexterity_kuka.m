%% path helpers
% import data based on kukaIiwa14_default_FLIP_202211021531(12+)
addpath(genpath('./log'));

%% constants
targetedFlipSolution = 8;
maxFailRepeat = 2;
maxRepeatLimit = 10;
iLocalRotation = 1;

% prepare for fileName
robotTag=robotMapsClass.commonDataSlot.robotParameters.tag;
startTime=datestr(datetime('now'),'yyyymmddHHMM');
dirName = ['DEX_',startTime,'_',robotTag];
logPath = ['./log/',dirName,'/'];

status = mkdir('./log',dirName);
addpath(genpath('./log'));
if status == 0
    ME = MException('robotMapsClass:fileFailed', ...
            'mkdir failed.', ...
            gridMark);
    throw(ME)
end

%% 1
% constants
iLocalPosition = 1;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,984,[6 2 1 7 4 5 8 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,348,[2 3 7 8 6 5 4 1])

gikAndNSWithDifferentStage;

% save
savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 2
% constants
iLocalPosition = 2;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,318,[2 4 8 5 7 6 1 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,954,[4 8 7 5 3 1 2 6])

gikAndNSWithDifferentStage;

%% 3
iLocalPosition = 3;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,859,[3 5 1 7 4 2 8 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,218,[8 5 2 3 4 7 6 1])

gikAndNSWithDifferentStage;


%% 4
iLocalPosition = 4;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,122,[1 5 6 2 4 7 8 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,893,[8 1 4 5 2 3 7 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,975,[2 6 3 7 5 4 NaN 1])

gikAndNSWithDifferentStage;

%% 5
iLocalPosition = 5;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,1019,[5 8 7 2 1 6 3 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,486,[6 7 1 5 3 8 4 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,269,[1 2 7 3 5 6 NaN 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,974,[6 7 5 4 3 1 NaN 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,431,[4 2 1 6 7 3 NaN 5])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 6
iLocalPosition = 6;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,311,[2 7 4 NaN 1 5 3 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,895,[1 3 NaN 6 2 7 5 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,1096,[2 5 8 4 6 7 3 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,429,[8 7 5 4 3 6 2 1])

gikAndNSWithDifferentStage;

%% 7
iLocalPosition = 7;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,1017,[7 5 1 8 4 6 3 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,380,[5 7 4 6 2 3 8 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,300,[5 3 2 6 8 1 4 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,429,[6 3 4 1 2 7 NaN 5])

gikAndNSWithDifferentStage;

%% 8
iLocalPosition = 8;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,922,[4 2 7 8 5 6 3 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,214,[1 6 4 8 3 5 2 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,296,[2 1 3 NaN 7 6 4 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,976,[5 NaN 4 3 6 1 7 2])

gikAndNSWithDifferentStage;


%% 9
iLocalPosition = 9;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,740,[2 7 5 1 8 4 6 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,825,[3 NaN 6 1 7 5 4 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,245,[4 1 7 2 8 5 3 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,202,[2 5 7 4 3 1 6 NaN])

gikAndNSWithDifferentStage;

%% 10
iLocalPosition = 10;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,139,[3 4 6 2 NaN 1 7 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,188,[1 4 6 7 3 5 8 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,792,[3 1 7 NaN 2 5 6 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,786,[1 NaN 6 5 7 4 3 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,740,[6 4 2 7 5 3 1 NaN])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,742,[6 NaN 3 1 4 NaN 2 5])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 11
iLocalPosition = 11;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,313,[4 7 5 1 8 3 6 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,138,[4 6 8 7 2 5 1 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,811,[3 1 NaN 6 5 4 2 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,789,[3 6 7 8 1 4 5 2])

gikAndNSWithDifferentStage;

%% 12
iLocalPosition = 12;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,855,[1 4 3 8 7 5 2 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,277,[6 1 7 4 2 3 8 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,264,[5 3 7 4 8 2 6 1])

gikAndNSWithDifferentStage;


%% 13
iLocalPosition = 13;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,335,[4 7 2 5 3 1 6 8])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,1037,[2 NaN 3 6 1 4 5 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,792,[2 7 6 4 1 NaN 3 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,394,[1 3 NaN 2 NaN 5 4 6])

gikAndNSWithDifferentStage;

%% 14
iLocalPosition = 14;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,1011,[5 2 8 3 7 4 1 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,920,[7 6 3 4 1 5 2 8])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,462,[1 4 8 6 5 3 2 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,479,[2 1 NaN 7 4 5 3 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,515,[5 1 4 2 NaN 6 7 3])

gikAndNSWithDifferentStage;

%% 15
iLocalPosition = 15;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,806,[8 5 2 1 6 3 4 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,922,[6 5 7 4 8 3 2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,411,[2 3 5 7 NaN 4 1 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,296,[3 1 NaN 4 6 2 7 5])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 16
iLocalPosition = 16;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,263,[1 8 5 3 6 4 7 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,808,[8 6 7 2 3 1 4 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,653,[1 NaN 7 6 2 3 4 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,687,[5 4 NaN 7 6 2 1 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,214,[1 3 4 7 2 6 5 8])

gikAndNSWithDifferentStage;

%% 17
iLocalPosition = 17;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,169,[4 7 5 3 2 6 1 NaN])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,181,[3 1 2 6 NaN 7 4 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,721,[4 2 3 7 NaN 5 6 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,910,[7 4 6 1 3 5 NaN 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,688,[6 5 7 1 4 3 2 NaN])

gikAndNSWithDifferentStage;

%% 18
iLocalPosition = 18;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,173,[2 3 8 4 6 1 7 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,1066,[5 8 2 7 6 4 1 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,425,[5 1 NaN 3 7 6 2 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,547,[3 2 5 1 NaN 6 4 NaN])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,512,[5 2 6 4 NaN NaN 3 1])

gikAndNSWithDifferentStage;


%% 19
iLocalPosition = 19;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,173,[6 8 3 2 4 1 5 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,916,[5 1 7 4 2 6 3 8])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,910,[8 7 5 2 6 3 4 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,377,[4 1 6 5 2 8 7 3])

gikAndNSWithDifferentStage;

%% 20
iLocalPosition = 20;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,176,[7 5 3 6 8 2 1 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,1007,[3 6 7 1 2 4 8 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,292,[6 8 1 5 4 7 NaN 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,871,[7 NaN 1 5 3 6 2 4])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% flip swap and delete
% for iLocalPosition=1:20
%     robotMapsClass.generateMapSlot.dataList.testDexterity.removeRepeatFlip(iLocalPosition,iLocalRotation);
% end


%% final rotation
for iLocalPosition=1:20
    for iFlipSolution=1:4
    disp(['[Dexterity Main]: start eeRotation for (',num2str(iLocalPosition),',', ...
                                                        num2str(iLocalRotation),',:,:,', ...
                                                        num2str(iFlipSolution),').']);
    robotMapsClass.generateMapSlot.dataList.testDexterity.eeRotation(iLocalPosition,iLocalRotation,iFlipSolution);
    end
end

savePoint(robotMapsClass,'DEX','20EE',logPath)