%% path helpers
% import data based on frankaEmikaPanda_dof7_reachabilityTest_flippedDexterityTest_202210260927
addpath(genpath('./log'));

%% constants
targetedFlipSolution = 4;
maxFailRepeat = 2;
maxRepeatLimit = 10;
iLocalRotation = 1;

% prepare for fileName
robotTag=robotMapsClass.commonDataSlot.robotParameters.tag;
startTime=datestr(datetime('now'),'yyyymmddHHMM');
dirName = ['DEX_',startTime,'_',robotTag];
logPath = ['./log/',dirName,'/'];

status = mkdir('./log',dirName);
addpath(genpath('./log'));
if status == 0
    ME = MException('robotMapsClass:fileFailed', ...
            'mkdir failed.', ...
            gridMark);
    throw(ME)
end

%% 1
% constants
iLocalPosition = 1;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,823,[2 4 5 8 7 1 3 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,285,[8 6 4 1 3 5 7 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,177,[5 1 3 2 4 8 7 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,843,[7 3 6 5 1 4 2 8])

gikAndNSWithDifferentStage;

% save
savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 2
% constants
iLocalPosition = 2;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,716,[3 8 6 1 2 5 7 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,726,[8 4 1 6 7 2 5 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,252,[7 3 1 2 6 5 4 8])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,121,[4 5 7 3 6 1 2 8])

gikAndNSWithDifferentStage;

%% 3
iLocalPosition = 3;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,238,[5 3 8 1 2 4 7 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,217,[4 3 2 1 6 8 5 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,755,[8 6 4 7 5 3 2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,680,[2 7 8 1 4 6 5 3])

gikAndNSWithDifferentStage;

%% 4
iLocalPosition = 4;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,843,[8 3 2 6 1 4 7 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,820,[8 2 3 6 5 7 4 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,248,[3 2 6 8 5 7 4 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,240,[3 5 2 6 4 1 8 7])

gikAndNSWithDifferentStage;

%% 5
iLocalPosition = 5;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,307,[2 4 1 5 3 8 6 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,455,[2 5 1 7 4 3 8 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,822,[4 8 2 7 5 1 3 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,898,[8 7 1 4 2 6 5 3])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 6
iLocalPosition = 6;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,895,[7 4 6 3 8 2 1 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,757,[5 2 3 6 4 1 8 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,451,[2 1 6 3 5 4 7 8])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,372,[3 2 5 1 7 6 8 4])

gikAndNSWithDifferentStage;

%% 7
iLocalPosition = 7;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,372,[8 1 2 6 5 7 3 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,587,[1 7 2 6 5 4 3 8])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,908,[4 8 3 7 5 6 1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,898,[8 2 5 6 4 1 7 3])

gikAndNSWithDifferentStage;

%% 8
iLocalPosition = 8;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,777,[2 5 8 1 4 7 6 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,752,[6 7 3 8 2 5 4 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,377,[5 3 4 1 7 2 8 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,269,[6 3 4 2 7 5 8 1])

gikAndNSWithDifferentStage;

%% 9
iLocalPosition = 9;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,144,[1 5 6 4 8 2 7 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,152,[5 1 3 4 7 6 2 8])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,718,[7 4 5 8 2 1 6 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,707,[2 7 3 8 1 5 6 4])

gikAndNSWithDifferentStage;

%% 10
iLocalPosition = 10;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,609,[3 5 4 7 2 6 8 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,517,[4 5 8 2 1 7 3 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,214,[5 3 1 7 6 4 8 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,175,[4 2 3 1 5 7 8 6])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 11
iLocalPosition = 11;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,113,[2 1 5 7 3 4 8 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,98,[2 4 1 6 7 5 3 8])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,551,[8 3 1 7 4 6 5 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,743,[5 7 1 8 2 6 3 4])

gikAndNSWithDifferentStage;

%% 12
iLocalPosition = 12;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,740,[7 3 5 4 2 1 8 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,719,[8 6 2 7 5 1 3 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,309,[3 5 8 4 7 2 6 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,232,[7 4 2 1 3 6 5 8])

gikAndNSWithDifferentStage;

%% 13
iLocalPosition = 13;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,300,[8 4 3 1 6 2 7 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,344,[2 7 1 3 6 5 4 8])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,877,[5 8 7 6 4 2 3 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,868,[8 7 4 1 2 3 5 6])

gikAndNSWithDifferentStage;

%% 14
iLocalPosition = 14;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,894,[6 1 4 7 8 2 3 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,849,[2 7 5 8 6 3 4 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,516,[5 7 1 4 8 3 2 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,368,[2 6 5 3 7 1 8 4])

gikAndNSWithDifferentStage;

%% 15
iLocalPosition = 15;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,300,[6 5 4 8 2 3 1 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,242,[4 7 8 2 1 6 5 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,781,[4 3 8 5 2 1 7 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,673,[1 7 8 5 2 4 6 3])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 16
iLocalPosition = 16;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,671,[4 6 8 1 5 7 2 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,677,[5 1 3 8 6 2 4 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,149,[2 6 3 4 5 7 1 8])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,39,[6 4 1 7 8 3 2 5])

gikAndNSWithDifferentStage;

%% 17
iLocalPosition = 17;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,112,[1 3 8 5 2 6 7 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,179,[1 7 4 5 3 8 2 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,647,[6 7 8 4 2 5 3 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,772,[7 4 6 8 3 2 5 1])

gikAndNSWithDifferentStage;

%% 18
iLocalPosition = 18;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,865,[6 8 2 1 4 7 3 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,875,[4 7 8 3 2 1 5 6])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,307,[7 1 6 2 3 4 8 5])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,297,[8 7 5 2 3 1 4 6])

gikAndNSWithDifferentStage;

%% 19
iLocalPosition = 19;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,138,[3 7 2 1 8 5 6 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,207,[3 2 5 4 8 6 1 7])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,746,[6 7 3 5 4 8 2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,770,[1 2 6 3 5 8 4 7])

gikAndNSWithDifferentStage;

%% 20
iLocalPosition = 20;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,770,[5 3 8 7 6 4 2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,779,[5 3 7 6 2 8 4 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,208,[2 3 6 5 7 8 4 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,198,[6 1 4 8 2 7 5 3])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% flip swap and delete
for iLocalPosition=1:20
    robotMapsClass.generateMapSlot.dataList.testDexterity.removeRepeatFlip(iLocalPosition,iLocalRotation);
end

%% final rotation
for iLocalPosition=1:20
    for iFlipSolution=1:8
    disp(['[Dexterity Main]: start eeRotation for (',num2str(iLocalPosition),',', ...
                                                        num2str(iLocalRotation),',:,:,', ...
                                                        num2str(iFlipSolution),').']);
    robotMapsClass.generateMapSlot.dataList.testDexterity.eeRotation(iLocalPosition,iLocalRotation,iFlipSolution);
    end
end

%% final calculation
robotMapsClass.generateMapSlot.dataList.testDexterity.dexterityStatistics()