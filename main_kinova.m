%% Preparation
addpath(genpath('./src'));
codePrepare;

%% Initialization
% define an empty robotMaps
robotMapsClass=robotMaps(); 

% prepare data
% robotColumnIn=loadrobot('kinovaGen3','DataFormat','column');

robotMapsClass.initialize('assignRobot');
robotMapsClass.assignRobotSlot.setRobotName('kinovaGen3','EndEffector_Link');
robotMapsClass.assignRobotSlot.setDexterityReference('base_link','HalfArm1_Link','ForeArm_Link'); 
robotMapsClass.finalize('assignRobot');

%% grid generation
robotMapsClass.initialize('generateGrid');
robotMapsClass.generateGridSlot.cartesian2(1.25,0.025);
robotMapsClass.finalize('generateGrid');

%% filter Grid
robotMapsClass.initialize('filterGrid');
robotMapsClass.filterGridSlot.rangeZ(-0.520,1.4);
robotMapsClass.filterGridSlot.sphereIn([0 0 0.2848],0.91);
% robotMapsClass.filterGridSlot.sphereIn([0 0 0.8],0.1);
robotMapsClass.finalize('filterGrid');

%% direction generation
robotMapsClass.initialize('generateDirection');
robotMapsClass.generateDirectionSlot.saffAndKuijlaars(0.025,20);
% robotMapsClass.generateDirectionSlot.nullRandomRotation(5);
robotMapsClass.generateDirectionSlot.alwaysNorthRotation(5);
robotMapsClass.finalize('generateDirection');

%% map Generation
robotMapsClass.initialize('generateMap');
robotMapsClass.generateMapSlot.assignAllTform();

%% reachability map
% robotMapsClass.generateMapSlot.reachability('numericMEX_parallel');
robotMapsClass.generateMapSlot.reachabilityTest('hardSkip',maxConstraintRepeatIn=15);