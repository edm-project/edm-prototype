function [configSol,solInfo] = ikMEX(ikHandle,endEffectorName,tform,weightsIn,initialguess)
%IKMEX inverseKinematics MEX function helper function
%     arguments
%         ikHandle(1,1) function_handle
%         endEffectorName(1,:) char
%         tform(4,4) double
%         weightsIn(1,6) double
%         initialguess(:,1) double
%     end
    [configSol,solInfo] = ikHandle(endEffectorName, ...
                                   tform, ...
                                   weightsIn, ...
                                   initialguess);
end

