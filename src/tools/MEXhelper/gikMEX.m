function [configSol,solInfo] = gikMEX(gikHandle,initialguess,endEffectorName,tformSlice,jointBounds)
%GIKMEX generalizedInverseKinematics helper MEX function
%     arguments
%         gikHandle(1,1) function_handle
%         initialguess(:,1) double
%         endEffectorName(1,:) char
%         tformSlice(4,4) double
%         jointBounds(:,2) double
%     end
[configSol,solInfo] = gikHandle(initialguess,endEffectorName,tformSlice,jointBounds);
end

