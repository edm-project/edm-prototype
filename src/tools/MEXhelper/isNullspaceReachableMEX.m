function isReachable = isNullspaceReachableMEX(isReachableHandle,sectionHandle,nSection,configSolColumn,configSol)
    [~,sectionNumber] = dexteritySectionMEX(sectionHandle,configSol,nSection);
    isReachable = isReachableHandle(configSolColumn(:,sectionNumber),configSol);
end

