function [configSolCol,haveSolutionCol] = nullspaceRoundMEX(robotParameters,nullspaceFixedHandle,nullspaceFixedReverseHandle,sectionHandle, ...
                                                            configSol,nSection,timeStep,increaseStep,maxStep)
%NULLSPACEROUND input the parameters needed and directly give out configSol
%that goes a round
% out:
% [configSolCol]: a 7*nSection array that represents the nullmotion of configSol with different nSection
% [haveSolutionCol]: a 1*nSection array that [true]
% [mode]: the mode that  
% [timeStep]: the each step length of nullmotion 
% [increaseStep]: the inceasting step when not all 24 steps are found
% [maxStep]: the maximum step numbers for attempting to get all 24 sections
%     arguments
%         robotParameters(1,1) struct
%         nullspaceFixedHandle(1,1) function_handle
%         configSol(:,1) double
%         nSection(1,1) double
%         timeStep(1,1) double
%         increaseStep(1,1) double
%         maxStep(1,1) double
%     end

    stepCount = 0;

    while true
        % the iteration counter
        stepCount = stepCount + increaseStep;
        if stepCount > maxStep
            break
        end

        % initialize the result
        configSolCol = zeros(length(homeConfiguration(robotParameters.robotColumn)),nSection);
        haveSolutionCol = zeros(nSection,1);

        % collect the information
        [configSolColFix,~,sectionColFix] = nullspaceFixedHandle(configSol, ...
                                                            timeStep, ...
                                                            nSection, ...
                                                            stepCount);
        
        [configSolColFixR,~,sectionColFixR] = nullspaceFixedReverseHandle(configSol, ...
                                                    timeStep, ...
                                                    nSection, ...
                                                    stepCount);

        % decide the section
        [~,sectionNumber] = dexteritySectionMEX(sectionHandle,configSol,nSection);

        % sort the information
        for iSection = mod((sectionNumber-1):(sectionNumber-1)+(nSection-1),nSection)+1
            
            % if the point already calculated, skip the point
            if haveSolutionCol(iSection) == 1
                continue;
            end

            if sum(sectionColFix==iSection,'all')~=0
                firstIndex = find(sectionColFix==iSection);
                configSolTemp = configSolColFix(:,firstIndex(1));
                % if the section cross the sign definition, stop the calculation
                if sign(configSolTemp(2))~=sign(configSol(2)) || sign(configSolTemp(4))~=sign(configSol(4)) || sign(configSolTemp(6))~=sign(configSol(6))
                    continue;
                end
                configSolCol(:,iSection) = configSolTemp;
                haveSolutionCol(iSection)=1;
            end
        end

        for iSection = mod((sectionNumber-1):-1:(sectionNumber-1)-(nSection-1),nSection)+1
            % if the point already calculated, skip the point
            if haveSolutionCol(iSection) == 1
                continue;
            end

            if sum(sectionColFixR==iSection,'all')~=0
                firstIndex = find(sectionColFixR==iSection);
                configSolTemp = configSolColFixR(:,firstIndex(1));
                % if the section cross the sign definition, stop the calculation
                if sign(configSolTemp(2))~=sign(configSol(2)) || sign(configSolTemp(4))~=sign(configSol(4)) || sign(configSolTemp(6))~=sign(configSol(6))
                    continue;
                end
                configSolCol(:,iSection) = configSolTemp;
                haveSolutionCol(iSection)=1;
            end
        end

%         for iSection = 1:nSection
%             if sectionNumber <= 12
%                 if iSection <= 12 && iSection >=sectionNumber  % choose the forward
%                     if sum(sectionColFix==iSection,'all')~=0
%                         firstIndex = find(sectionColFix==iSection);
%                         configSolCol(:,iSection)=configSolColFix(:,firstIndex(1));
%                         haveSolutionCol(iSection)=1;
%                     end
%                 else
%                     if sum(sectionColFixR==iSection,'all')~=0  % choose the reverse
%                         firstIndex = find(sectionColFixR==iSection);
%                         configSolCol(:,iSection)=configSolColFixR(:,firstIndex(1));
%                         haveSolutionCol(iSection)=1;
%                     end
%                 end
%             else
%                 if iSection >= 13 && iSection <= sectionNumber  % choose the forward
%                     if sum(sectionColFixR==iSection,'all')~=0
%                         firstIndex = find(sectionColFixR==iSection);
%                         configSolCol(:,iSection)=configSolColFixR(:,firstIndex(1));
%                         haveSolutionCol(iSection)=1;
%                     end
%                 else
%                     if sum(sectionColFix==iSection,'all')~=0  % choose the reverse
%                         firstIndex = find(sectionColFix==iSection);
%                         configSolCol(:,iSection)=configSolColFix(:,firstIndex(1));
%                         haveSolutionCol(iSection)=1;
%                     end
%                 end
%             end
%         end

        % determine the loop end
        if all(haveSolutionCol)
            break;
        end
    end
end

