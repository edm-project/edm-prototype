function stringOut = struct2strDisplay(structIn)
%STRUCT2STRDISPLAY input a struct and output a string
    arguments
        structIn(1,1) struct
    end

    fieldName = fieldnames(structIn);
    value = struct2cell(structIn);

    stringOut = '';

    for iLength=1:length(fieldName)
        stringOut=[stringOut,fieldName{iLength},':'];
        if isnumeric(value{iLength})
            stringOut=[stringOut,mat2str(value{iLength}),newline];
        elseif ischar(value{iLength})
            stringOut=[stringOut,value{iLength},newline];
        else
            ME = MException('robotMapsClass:struct2strDisplay', ...
                'type in the struct unsupported.');
            throw(ME)
        end
    end
end

