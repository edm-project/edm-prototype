function interpValue = plotInterpolation(originValue,targetNum)
    xOrigin = linspace(0,1,length(originValue));
    xInterp = linspace(0,1,targetNum);
    interpValue = interp1(xOrigin,originValue,xInterp);
end

