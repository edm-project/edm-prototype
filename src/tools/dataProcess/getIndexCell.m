function index = getIndexCell(cellIn,termName)
%GETINDEXCELL get the index of termName in cellIn
% return 0 if not found
    arguments
        cellIn(1,:) cell
        termName(1,:) char
    end
    
    index=0;
    for iName = 1:length(cellIn)
        if isequal(termName,cellIn{iName})
            index = iName;
            return
        end
    end
end

