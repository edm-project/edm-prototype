function lengthMap = lengthContainerMap(containerMap)
%LENGTHCONTAINERMAP Calculate the total length of all elements in the
%Container Map
    arguments
        containerMap containers.Map
    end

    lengthMap = 0;

    for iElement=1:containerMap.Count
        lengthMap=lengthMap+length(containerMap(iElement));
    end
end

