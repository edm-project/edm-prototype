function containerMap = mergeContainerMap(containerMap,keys,values)
%MERGECONTAINERMAP merge the keys and values to the containerMap
    arguments
        containerMap containers.Map
        keys(1,:) double
        values(1,:) double
    end

    % error when keys and values are not paired
    if length(keys)~=length(values)
        ME = MException('mergeContainerMap:lengthMismatch', ...
            'keys and values are not correctly paired [%d][%d]', ...
            length(keys),length(values));
        throw(ME)
    end

    % merge the containerMap
    for iKeys=1:length(keys)
        containerMap(keys(iKeys))=[containerMap(keys(iKeys)),values(iKeys)];
    end
end

