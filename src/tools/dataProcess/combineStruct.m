function structOut = combineStruct(structIn1,structIn2)
%  Combines structures "structIn1","structIn2" into single struct "structOut"
%  assuming no crossover, throw error when having fieldname
    arguments
        structIn1(1,1) struct
        structIn2(1,1) struct
    end

    structOut=struct();

    % Get fieldnames
    fieldName1 = fieldnames(structIn1);
    fieldName2 = fieldnames(structIn2);

    % Check for clashes
    for i = 1:length(fieldName1)
        iFieldName = fieldName1{i};

        % if Two struct name clashes, throw error
        if max(strcmp(iFieldName,fieldName2))==true
            ME = MException('combineStruct:collisionAttr', ...
                            'Two struct has same Attribute [%s]', ...
                            iFieldName);
            throw(ME)
        end
    end

    % Combine into a single output structure
    for i = 1:length(fieldName1)
        structOut.(fieldName1{i}) = structIn1.(fieldName1{i});
    end
    for i = 1:length(fieldName2)
        structOut.(fieldName2{i}) = structIn2.(fieldName2{i});
    end
end


