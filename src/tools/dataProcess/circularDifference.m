function difference = circularDifference(newValue,oldValue,maxValue)
%CIRCULARDIFFERENCE Caluclate the circular difference of two value. Only
% consider the closest value with lowest abs().
% e.g for circle of 24, difference of 3-2 1-24 are all 1
%                       difference of 2-3 24-1 are all -1
%     arguments
%         newValue(1,1) double
%         oldValue(1,1) double
%         maxValue(1,1) double
%     end

    candidateOne = newValue - oldValue;
    candidateTwo = newValue + maxValue - oldValue;
    candidateThree = newValue - maxValue - oldValue;

    if min([abs(candidateOne),abs(candidateTwo),abs(candidateThree)])==abs(candidateOne)
        difference=candidateOne;
    elseif min([abs(candidateOne),abs(candidateTwo),abs(candidateThree)])==abs(candidateTwo)
        difference=candidateTwo;
    else %min([abs(candidateOne),abs(candidateTwo),abs(candidateThree)])==abs(candidateThree)
        difference=candidateThree;
    end
end

