function output = circularMod(input,period,maxValue)
%CIRCULARMOD project the value into a interval. The result should be
%defined with period and maxValue which means:
%   for period=2pi and maxValue=2pi, it means value will be projected into [0,2pi)
%   for period=2pi and maxValue=pi,  it means value will be projected into [-pi,pi)
%   for period=2pi and maxValue=3/2 pi,  it means value will be projected into [-1/2 pi,3/2 pi)
%   it is for project large angle into a fixed interval, but can also be
%   used in other occations.
%     arguments
%         input(1,1) double
%         period(1,1) double {mustBePositive}
%         maxValue(1,1) double {mustBePositive}
%     end

    output = input;
    output = mod(output,period);
    if output >= maxValue
        output = output - period;
    end
end

