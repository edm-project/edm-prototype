function structOut = mergeUpdateStruct(structInOld,structInNew)
%  Combines structures "structInOld","structInNew" into single struct "structOut"
%  merge the struct, same fieldname will take the version in structInNew
    arguments
        structInOld(1,1) struct
        structInNew(1,1) struct
    end

    structOut=struct();

    % Get fieldnames
    fieldNameOld = fieldnames(structInOld);
    fieldNameNew = fieldnames(structInNew);

    % merge into a single output structure
    for i = 1:length(fieldNameOld)
        structOut.(fieldNameOld{i}) = structInOld.(fieldNameOld{i});
    end
    for i = 1:length(fieldNameNew)
        structOut.(fieldNameNew{i}) = structInNew.(fieldNameNew{i});
    end
end

