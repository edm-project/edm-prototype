maxFailRepeat = 2;
for iFlip = 1:2
    maxCounter = 0;
    % do gikMotion
    failCounter = 0;
    while failCounter < maxFailRepeat
        [calculationFinished,~] = robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotionEmpirical(iLocalPosition,iLocalRotation,iFlip,logPath=logPath, ...
               maxDifference=20);
        if calculationFinished==false
            failCounter=failCounter+1;
        else
            robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotion(iLocalPosition,iLocalRotation,iFlip);
            failCounter=0;
        end

        maxCounter = maxCounter+1;
        if maxCounter>=maxRepeatLimit
            break;
        end
    end
end

maxFailRepeat = 1;
for iFlip = 3:4
    maxCounter = 0;
    % do gikMotion
    failCounter = 0;
    while failCounter < maxFailRepeat
        [calculationFinished,~] = robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotionEmpirical(iLocalPosition,iLocalRotation,iFlip,logPath=logPath);
        if calculationFinished==false
            failCounter=failCounter+1;
        else
            robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotion(iLocalPosition,iLocalRotation,iFlip);
            failCounter=0;
        end

        maxCounter = maxCounter+1;
        if maxCounter>=maxRepeatLimit
            break;
        end
    end
end

maxFailRepeat = 2;
for iFlip = 3:4
    maxCounter = 0;
    for iRepeat = 1:maxFailRepeat
        [calculationFinished,~] = robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotionEmpirical(iLocalPosition,iLocalRotation,iFlip,logPath=logPath, ...
                forceChoosing=true);
        if calculationFinished==true
            robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotion(iLocalPosition,iLocalRotation,iFlip);
            failCounter=0;
        end
    end
end