function [robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,targetedFlipSolution,maxFlipRepeat)
%DEXTERGENERATEDATA import and calculate a single point of dexterity map
% into a specific flipSolution

    lenFlipSolution = 1;
    
    disp(['[Dexterity Main]: import data (',num2str(iLocalPosition),',', ...
                                           num2str(iLocalRotation),',', ...
                                           num2str(startGlobalPosition),')']);
    % import the data in iLocalPosition
    [sectionNumberList(lenFlipSolution),iFlipSolutionList(lenFlipSolution)] = robotMapsClass.generateMapSlot.dataList.testDexterity.pointImport(iLocalPosition,iLocalRotation,startGlobalPosition);
    
    % try for maxFlipRepeat times in order to get different flipSolutions
    for iTrial = 1:maxFlipRepeat
        if lenFlipSolution == targetedFlipSolution
            disp(['[Dexterity Main]: target [',num2str(targetedFlipSolution),'] for direction [',num2str(iLocalPosition),'] reached']);
            break;
        end
        
        disp(['[Dexterity Main]: trial [',num2str(iTrial),'] with lenFlip [',num2str(lenFlipSolution),'] for target [',num2str(targetedFlipSolution),'].']);
    
        robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotionSinglePoint(iLocalPosition,iLocalRotation,startGlobalPosition,iFlipSolutionList(lenFlipSolution));
        disp(['[Dexterity Main]: nullspace motion for (',num2str(iLocalPosition),',', ...
                                                        num2str(iLocalRotation),',', ...
                                                        num2str(startGlobalPosition),',:,', ...
                                                        num2str(iFlipSolutionList(lenFlipSolution)),') finished.']);
        
        [sectionNumberOut,iFlipSolutionOut] = robotMapsClass.generateMapSlot.dataList.testDexterity.pointCalculate(iLocalPosition,iLocalRotation,startGlobalPosition);        

        if iFlipSolutionOut==lenFlipSolution + 1
            lenFlipSolution = lenFlipSolution + 1;
            sectionNumberList(lenFlipSolution)=sectionNumberOut;
            iFlipSolutionList(lenFlipSolution)=iFlipSolutionOut;
            disp(['[Dexterity Main]: FlipSolution ',num2str(lenFlipSolution),' update success with S[',num2str(sectionNumberOut),'] F[',num2str(iFlipSolutionOut),']']);
        end
    end
end

