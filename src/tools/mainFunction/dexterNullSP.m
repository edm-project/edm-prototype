function robotMapsClass = dexterNullSP(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,toFlip)
    for iFlip=1:toFlip
        robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotionSinglePoint(iLocalPosition,iLocalRotation,startGlobalPosition,iFlip)
    end
end

