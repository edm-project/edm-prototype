%% Enviroment preparation

% check enviroments
if isMATLABReleaseOlderThan("R2021a")
    error('Matlab Older than R2021a. Some functions will not work')
end
v = ver;
if ~ any(strcmp(cellstr(char(v.Name)), 'Robotics System Toolbox'))
    error('Robotics System Toolbox missing!')
end

if ~ canUseParallelPool()
    warning('Default Parallel pool unable to be used! Some acceleration modes will not work.')
    disp('Either toolbox not installed or default parpool not configured.')
end
if ~ any(strcmp(cellstr(char(v.Name)), 'MATLAB Coder'))
    warning('Matlab coder missing! Some acceleration modes will not work.')
end

% clear the variables
clear
clc

% configure the current path to allow function call.
addpath(genpath('./data'));
addpath(genpath('./src'));