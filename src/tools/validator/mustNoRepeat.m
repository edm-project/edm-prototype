function mustNoRepeat(obj,callMarkIn)
%MUSTNOREPEAT Validator: [callMarkIn] must be not a repeat of marks [obj.marks]
    arguments
        obj(1,1)
        callMarkIn(1,:) char
    end

    mark=obj.mark;

    if getIndexCell(mark,callMarkIn)~=0
        ME = MException('robotMapsClass:validator:mustNoRepeat', ...
            'duplicate call of same method [%s] not allowed in this process', ...
            callMarkIn);
        throw(ME)
    end
end