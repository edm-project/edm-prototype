function mustHaveMark(obj,targetMark)
%MUSTHAVEMARK obj.mark must include one of the mark in targetMark
    arguments
        obj(1,1)
        targetMark(1,:) cell
    end

    mark=obj.mark;

    intersectResult=intersect(mark,targetMark);
    if isempty(intersectResult)
        ME = MException('robotMapsClass:validator:mustHaveMark', ...
            ['In this object, At least one of [%s] should be called.\n' ...
            'Now attempt finalize without any one in the above mentioned set.'], ...
            string(strjoin(targetMark)));
        throw(ME)
    end
end

