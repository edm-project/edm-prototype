function mustNoRepeatGroup(obj,callMarkIn,noRepeatGroup)
%MUSTNOREPEATGROUP if [callMarkIn] is in [noRepeatGroup] it must not be 
% a repeat in marks [obj.marks]
    arguments
        obj(1,1)
        callMarkIn(1,:) char
        noRepeatGroup(1,:) cell
    end

    mark=obj.mark;
    
    if getIndexCell(noRepeatGroup,callMarkIn)~=0
        if getIndexCell(mark,callMarkIn)~=0
            ME = MException('robotMapsClass:validator:noRepeatGroup', ...
                'duplicate call of same method [%s] not allowed in this process', ...
                callMarkIn);
            throw(ME)
        end
    end
end

