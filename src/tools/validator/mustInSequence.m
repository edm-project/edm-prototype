function mustInSequence(obj,callMarkIn,afterMarkList,beforeMarkList)
%MUSTINSEQUENCE marks in [afterMarkList] must only appear after one calling of [beforeMarkList]
% implementation: if [callMarkIn] is in [afterMarkList], check if any [beforeMarkList] 
% already appears once in [obj.marks].
    arguments
        obj(1,1)
        callMarkIn(1,:) char
        afterMarkList(1,:) cell
        beforeMarkList(1,:) cell
    end

    mark=obj.mark;
    
    % if callMarkIn is in afterMarkList
    if getIndexCell(afterMarkList,callMarkIn)~=0
        % check if any of beforeMarkList exists in obj.marks
        intersectResult=intersect(mark,beforeMarkList);
        if isempty(intersectResult)
            ME = MException('robotMapsClass:validator:mustInSequence', ...
            ['In this object, [%s] are only allow to be called after [%s].\n' ...
            'Now attempt calling [%s] without any one in the above mentioned set.'], ...
            string(strjoin(afterMarkList)),string(strjoin(beforeMarkList)),callMarkIn);
            throw(ME)
        end
    end    
end

