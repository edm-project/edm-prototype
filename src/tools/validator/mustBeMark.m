function mustBeMark(obj,targetMark,allowMode)
%MUSTISACTIVESTATE obj.isActive must be {targetmark} (includeMode=true);
% obj.isActive must not be {targetmark} (includeMode=false);
    arguments
        obj(1,1)
        targetMark(1,:) char
        allowMode(1,1) logical
    end

    mark=obj.mark;

    if allowMode==true && ~isequal(mark,{targetMark})
        ME = MException('robotMapsClass:validator:mustBeMark', ...
            'The mark of the object should be [%s] in the initialization. Now [%s].', ...
            targetMark,string(mark));
        throw(ME)
    end

    if allowMode==false && isequal(mark,{targetMark})
        ME = MException('robotMapsClass:validator:mustBeMark', ...
            'The mark of the object should not be [%s] in the finalization. Now [%s].', ...
            targetMark,string(mark));
        throw(ME)
    end
end

