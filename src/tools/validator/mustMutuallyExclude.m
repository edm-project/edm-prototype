function mustMutuallyExclude(obj,callMarkIn,excludeMarkList)
%MUSTMUTUALLYEXCLUDE marks in [excludeMarksList] must only appear once in [obj.marks]
% implementation: if [callMarkIn] is in [excludeMarksList], check if any [excludeMarksList] 
% already appears once in [obj.marks].
    arguments
        obj(1,1)
        callMarkIn(1,:) char
        excludeMarkList(1,:) cell
    end

    mark=obj.mark;

    if getIndexCell(excludeMarkList,callMarkIn)~=0
        for iMarkList=1:length(excludeMarkList)
            excludePosition = getIndexCell(mark,excludeMarkList{iMarkList});
            if excludePosition~=0
                ME = MException('robotMapsClass:validator:mustMutuallyExclude', ...
                    ['In this object, [%s] are not allow to be called togethor.\n' ...
                    'Now attempt calling [%s] after [%s].'], ...
                    string(strjoin(excludeMarkList)),callMarkIn,mark{excludePosition});
                throw(ME)
            end
        end
    end
end

