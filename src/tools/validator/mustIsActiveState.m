function mustIsActiveState(obj,shouldBeState)
%MUSTISACTIVESTATE obj.isActive must be in state of shouldBeState
    arguments
        obj(1,1)
        shouldBeState(1,1) logical
    end

    isActive=obj.isActive;

    if isActive~=shouldBeState
        ME = MException('robotMapsClass:validator:mustIsActiveState', ...
            'obj.isActive should be [%s], now in [%s]', ...
            string(shouldBeState),string(isActive));
        throw(ME)
    end
end

