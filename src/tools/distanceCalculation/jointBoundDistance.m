function jointDist = jointBoundDistance(robotColumn,configSol)
    jointConstraintRow = constraintJointBounds(robotColumn).Bounds;
    globalMinimum = Inf;
    for iAngle = 1:length(configSol)
        angleMinimum = min(configSol(iAngle)-jointConstraintRow(iAngle,1), ...
                          jointConstraintRow(iAngle,2)-configSol(iAngle));
        globalMinimum = min(globalMinimum,angleMinimum);
    end
    jointDist = globalMinimum;
end

