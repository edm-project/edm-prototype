function jointDist = jointSectionDistance(configSol,configSolColumn)
    if length(configSol)~=size(configSolColumn,1)
        error('Two configSol have different size!')
    end
    globalMinimum = Inf;
    for iConfigSol = 1:size(configSolColumn,2)
        angleDistance = jointJointDistance(configSol,configSolColumn(:,iConfigSol));
        globalMinimum = min(globalMinimum,angleDistance);
    end
    jointDist = globalMinimum;
end

