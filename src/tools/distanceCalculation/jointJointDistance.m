function jointDist = jointJointDistance(configSol1,configSol2)
    if length(configSol1)~=length(configSol2)
        error('Two configSol have different size!')
    end
    globalMaximum = -Inf;
    for iAngle = 1:length(configSol1)
        angleDistance = abs(circularDifference(configSol1(iAngle),configSol2(iAngle),2*pi));
        globalMaximum = max(globalMaximum,angleDistance);
    end
    jointDist = globalMaximum;
end

