function jointDist = sectionSectionDistance(configSolCol1,configSolCol2)
    if size(configSolCol1,1)~=size(configSolCol2,1)
        error('Two configSol have different size!')
    end
    globalMinimum = Inf;
    for iConfigSol = 1:size(configSolCol1,2)
        angleDistance = jointSectionDistance(configSolCol1(:,iConfigSol),configSolCol2);
        globalMinimum = min(globalMinimum,angleDistance);
    end
    jointDist = globalMinimum;
end

