function jointDist = jointBoundDistanceNorm2(robotColumn,configSol)
    jointConstraintRow = constraintJointBounds(robotColumn).Bounds;
    globalNorm = 0;
    for iAngle = 1:length(configSol)
        angleMinimum = min(configSol(iAngle)-jointConstraintRow(iAngle,1), ...
                          jointConstraintRow(iAngle,2)-configSol(iAngle));
        if angleMinimum<=0
           globalNorm = 0;
           break;
        else
           globalNorm = globalNorm +  angleMinimum ^2;
        end
    end
    jointDist = sqrt(globalNorm);
end

