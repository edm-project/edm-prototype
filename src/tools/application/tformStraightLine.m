function tformMatrix = tformStraightLine(endPosition,xRotation,tSamplesNum)
%TFORMSTRAIGHTLINE
    arguments
        endPosition(3,1) double
        xRotation(1,1) double
        tSamplesNum(1,1) double
    end

    % calculate the zAxis
    zAxisDirection = endPosition;
    zAxis = zAxisDirection / norm(zAxisDirection);

    % calculate the xAxis
    xAxisStart = axang2rotm([0 1 0 pi/2])*zAxis;
    xAxis = axang2rotm([zAxis',xRotation])*xAxisStart;

    % calculate the yAxis
    yAxis = cross(zAxis,xAxis);

    % get the rotation for both lines
    rotmAll = [xAxis,yAxis,zAxis]; 

    % get the the start and end direction
    startTform = rotm2tform(rotmAll);
    endTform = rotm2tform(rotmAll);
    endTform(1:3,4) = endPosition;

    % interpolate between to transformations
    [tformMatrix,~,~] = transformtraj(startTform,endTform,[0 1],linspace(0,1,tSamplesNum));

end

