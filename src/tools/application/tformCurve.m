function tformMatrix = tformCurve(centerPosition,directionPositive,rotateAngle,xRotation,tSamplesNum)
%TFORMCURVE
    arguments
        centerPosition(3,1) double
        directionPositive(1,1) logical
        rotateAngle(1,1) double
        xRotation(1,1) double
        tSamplesNum(1,1) double
    end
    
    % calculate the xAxis
    radiaus = - centerPosition;
    xAxisStart = radiaus / norm(radiaus);

    % decide the yAxis
    if directionPositive == true
        yAxisStart = [0 1 0]';
    else
        yAxisStart = [0 -1 0]';
    end

    % calculate zAxis
    zAxis = cross(xAxisStart,yAxisStart);

    % rotate the x and y Axis
    xAxis =  axang2rotm([zAxis',xRotation])*xAxisStart;
    yAxis =  axang2rotm([zAxis',xRotation])*yAxisStart;

    % get the rotation for both lines
    rotmAll = [xAxis,yAxis,zAxis];

    % get the the start and end direction
    startTform = rotm2tform(rotmAll);
    angleList = linspace(0,rotateAngle,tSamplesNum);
    
    tformMatrix = zeros(4,4,tSamplesNum);
    tformMatrix(:,:,1) = startTform;
    for iTform = 2:tSamplesNum
        translationBefore = trvec2tform(-centerPosition');
        rotationAngle = axang2tform([-yAxis',angleList(iTform)]); 
        translationAfter = trvec2tform(centerPosition');

        tmpTform = translationAfter*rotationAngle*translationBefore*startTform;
        tformMatrix(:,:,iTform) = tmpTform;
    end

end

