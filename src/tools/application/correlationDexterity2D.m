function correlationValue = correlationDexterity2D(robotMapsClass,solutionFlagMap)

    % input the data
    solutionFlag = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag;
    solutionFlagWithSolution = squeeze(sum(solutionFlag==1,4));
    positionRow = robotMapsClass.commonDataSlot.positionRow;

    % calculate the parameter 
    nGlobalPosition = size(positionRow,1);
    nFlip =  robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.flipCounter - 1;

    correlationValue = zeros(nGlobalPosition,nFlip);
    for iFlip = 1:nFlip
        solutionFlagFlip = squeeze(solutionFlagWithSolution(:,:,:,iFlip));
        for iGlobalPosition = 1:nGlobalPosition
            shiftVector = positionRow(iGlobalPosition,:);
            [successFlag,solutionFlagMapShift] = shiftSolutionFlagMask(positionRow,solutionFlagMap,shiftVector);
            if successFlag == true
                correlationValue(iGlobalPosition,iFlip) = sum(solutionFlagFlip(solutionFlagMapShift),'all');
            end
        end
    end

end

