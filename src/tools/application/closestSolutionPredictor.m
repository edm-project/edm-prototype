function [minGlobalPosition,minLocalPosition,minLocalRotation] = closestSolutionPredictor(robotMapsClass,tform,weights)
%CLOSESTSOLUTIONPREDICTOR
    arguments
        robotMapsClass(1,1) robotMaps
        tform(4,4) double
        weights(1,2) double = [1.0 0.1]
    end
    
    % process the input parameters
    position = tform(1:3,4);
    rotation = tform(1:3,1:3);

    % input the data
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    endEffectorTform = robotMapsClass.commonDataSlot.endEffectorTform;

    % input the patameters
    nGlobalPosition = size(positionRow,1);
    nLocalPosition = size(endEffectorTform,3);
    nLocalRotation = size(endEffectorTform,4);

    % calculate the distance between translation
    translationDistance = zeros(nGlobalPosition,1);
    for iGlobalPosition = 1:nGlobalPosition
        translationDistance(iGlobalPosition) = norm(position-positionRow(iGlobalPosition,:)');
    end
    
    % calculate the distance between the rotation
    rotationDistance = zeros(nLocalPosition,nLocalRotation);
    for iLocalPostion = 1:nLocalPosition
        for iLocalRotation = 1:nLocalRotation
            rotDistantTmp = rotm2axang(rotation*endEffectorTform(1:3,1:3,iLocalPostion,iLocalRotation)');
            rotationDistance(iLocalPostion,iLocalRotation) = abs(rotDistantTmp(4));
        end
    end

    % combine the distance between two data
    totalDistance = repmat(translationDistance,1,nLocalPosition,nLocalRotation)*weights(1) + ...
                    permute(repmat(rotationDistance,1,1,nGlobalPosition),[3 1 2])*weights(2);

    % find the minimum distance
    minValue = inf;
    minGlobalPosition = 0;
    minLocalPosition = 0;
    minLocalRotation = 0;
    for iGlobalPosition = 1:nGlobalPosition
        for iLocalPostion = 1:nLocalPosition
            for iLocalRotation = 1:nLocalRotation
                if totalDistance(iGlobalPosition,iLocalPostion,iLocalRotation) <minValue
                    minValue = totalDistance(iGlobalPosition,iLocalPostion,iLocalRotation);
                    minGlobalPosition = iGlobalPosition;
                    minLocalPosition = iLocalPostion;
                    minLocalRotation = iLocalRotation;
                end
            end
        end
    end
end

