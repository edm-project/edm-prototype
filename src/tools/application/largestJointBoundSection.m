function iSectionNumberList = largestJointBoundSection(testDexterity,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn, ...
                                                        iFlipSolutionIn,outputLength,options)
    arguments
        testDexterity
        iLocalPositionIn
        iLocalRotationIn
        iGlobalPositionIn
        iFlipSolutionIn
        outputLength
        options.weights double = [1 5]
    end
    robotParameters = testDexterity.generateMapHandle.commonDataHandle.robotParameters;
    robotColumn = testDexterity.generateMapHandle.commonDataHandle.robotParameters.robotColumn;
    solutionFlag = squeeze(testDexterity.dataList.solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlipSolutionIn));
    configSolColumn = squeeze(testDexterity.dataList.configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlipSolutionIn));
    nSection = testDexterity.dataList.nSection;
    
    % calculate the joint bound
    jointBoundDist = zeros(nSection,1);
    for iSection = 1:nSection
        if solutionFlag(iSection)==1
            jointBoundDist(iSection) = jointBoundDistance(robotColumn,configSolColumn(:,iSection));
        else
            jointBoundDist(iSection) = -Inf;
        end
    end

    jointBoundDist(jointBoundDist < 0) = -Inf;

    % calculate the manipulability
    minimumSigma = zeros(nSection,1);
    for iSection = 1:nSection
        if solutionFlag(iSection)==1
            minimumSigma(iSection) = calculateMinimunSigma(robotParameters,configSolColumn(:,iSection));
        end
    end

    % calculate the generalized manipulability
    generalizedDistance = jointBoundDist*options.weights(1) + minimumSigma*options.weights(1);

    % get the Global maximum and local maximum
    [~,globalMax] = sort(generalizedDistance,"descend");
    localMax = find(islocalmax(generalizedDistance));
    [~,localMaxIndex] = sort(generalizedDistance(localMax),"descend");
    localMax = localMax(localMaxIndex);

    % combine the output
    outputList = unique([localMax;globalMax],"stable");
    firstInfIndex = find(generalizedDistance(outputList)==-inf,1,"first");
    outputList = outputList(1:firstInfIndex-1);
    
    % output the suggested section List
    iSectionNumberList = outputList(1:min(length(outputList),outputLength));
end

function sigmaMin =calculateMinimunSigma(robotParameters,configSol)
    robotColumn= robotParameters.robotColumn;
    endEffectorName = robotParameters.endEffectorName;
    jacobian = geometricJacobian(robotColumn,configSol,endEffectorName);

    [~,S,~] = svd(jacobian);
    maximumSize = min(size(S,1),size(S,2));
    sigmaMin = S(maximumSize,maximumSize);
end