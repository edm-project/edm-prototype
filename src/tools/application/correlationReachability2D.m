function correlationValue = correlationReachability2D(robotMapsClass,solutionFlagMap)
    
    % input the data
    solutionFlag = robotMapsClass.generateMapSlot.dataList.testReachability.dataList.solutionFlag;
    solutionFlagWithSolution = solutionFlag == 1;
    positionRow = robotMapsClass.commonDataSlot.positionRow;

    % calculate the parameter 
    nGlobalPosition = size(positionRow,1);

    correlationValue = zeros(nGlobalPosition,1);
    for iGlobalPosition = 1:nGlobalPosition
        shiftVector = positionRow(iGlobalPosition,:);
        [successFlag,solutionFlagMapShift] = shiftSolutionFlagMask(positionRow,solutionFlagMap,shiftVector);
        if successFlag == true
            correlationValue(iGlobalPosition) = sum(solutionFlagMapShift == solutionFlagWithSolution & solutionFlagMapShift,'all');
        end
    end
end

