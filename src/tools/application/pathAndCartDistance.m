function [pathLength,cartLength] = pathAndCartDistance(robotParameters,pathCell)
    
    nCell = length(pathCell);
    pathLength = zeros(nCell,1);
    cartLength = zeros(nCell,1);

    robotColumn = robotParameters.robotColumn;
    baseName=robotParameters.baseName;
    endEffectorName=robotParameters.endEffectorName;

    for iCell=1:nCell
        path = pathCell{iCell};
        nPath = size(path,2);
        if nPath == 0
            continue;
        end
        tformPath = zeros(4,4,nPath);
        tformPath(:,:,1) = getTransform(robotColumn,path(:,1),endEffectorName,baseName);
        for iPoints = 2:nPath
            pathDifference = path(:,iPoints) - path(:,iPoints-1);
            pathNorm = norm(pathDifference);
            pathLength(iCell) = pathLength(iCell) + pathNorm;

            tformPath(:,:,iPoints) = getTransform(robotColumn,path(:,iPoints),endEffectorName,baseName);
            cartDifference = tformPath(1:3,4,iPoints) - tformPath(1:3,4,iPoints-1);
            cartNorm = norm(cartDifference);
            cartLength(iCell) = cartLength(iCell) + cartNorm;
        end
    end
end

