function [markSequence,solutionFlagMap] = batchPredictor(robotMapsClass,tformMatrix)
%BATCHPREDICTOR 
    % input the data
    solutionFlag = robotMapsClass.generateMapSlot.dataList.testReachability.dataList.solutionFlag;
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    successFlag = shiftCheck(robotMapsClass,positionRow,tformMatrix);

    if successFlag==false
        error('The transformation matrix contains data that possibly make projection. Please change parameter');
    end

    % initalize a empty
    solutionFlagMap = zeros(size(solutionFlag),'logical');
    markSequence = zeros(3,size(tformMatrix,3));
    for iTform = 1:size(tformMatrix,3)
        [minGlobalPosition,minLocalPosition,minLocalRotation] = closestSolutionPredictor(robotMapsClass,tformMatrix(:,:,iTform));
        solutionFlagMap(minLocalPosition,minLocalRotation,minGlobalPosition) = true;
        markSequence(:,iTform) = [minLocalPosition,minLocalRotation,minGlobalPosition];
    end
end

