function [successFlag,markSequenceShift] = shiftMarkSequence(positionRow,markSequence,shiftVector)
%SHIFTMARKSEQUENCE 
    arguments
        positionRow(:,3) double
        markSequence(3,:) double
        shiftVector(1,3) double
    end

    % calculate the parameters
    nMarkSequence = size(markSequence,2);

    % initialize for the loop
    successFlag = true;
    markSequenceShift = zeros(size(markSequence));

    for iMarkSequence = 1:nMarkSequence
        positionNow = positionRow(markSequence(3,iMarkSequence),:)+shiftVector;
        positionIndex = searchPositionIndex(positionRow,positionNow);
        if positionIndex == 0
            successFlag = false;
            return;
        end
        markSequenceShift(:,iMarkSequence) = [markSequence(1,iMarkSequence),markSequence(2,iMarkSequence),positionIndex];
    end

end


