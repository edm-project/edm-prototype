function [successFlag,solutionFlagMapShift] = shiftSolutionFlagMask(positionRow,solutionFlagMap,shiftVector)
    arguments
        positionRow(:,3) double
        solutionFlagMap(:,:,:) double
        shiftVector(1,3) double
    end

    % input parameter
    nLocalPosition = size(solutionFlagMap,1);
    nLocalRotation = size(solutionFlagMap,2);
    GlobalPositionList = find(squeeze(any(solutionFlagMap,[1 2]))); 
    
    % prepare information
    successFlag = true;
    solutionFlagMapShift = zeros(size(solutionFlagMap),'logical');
    for iGlobalPosition = GlobalPositionList'
        for iLocalPosition = 1:nLocalPosition
            for iLocalRotation = 1:nLocalRotation
                if solutionFlagMap(iLocalPosition,iLocalRotation,iGlobalPosition) == true
                    positionNow = positionRow(iGlobalPosition,:)+shiftVector;
                    positionIndex = searchPositionIndex(positionRow,positionNow);
                    if positionIndex == 0
                        successFlag = false;
                        return;
                    end
                    solutionFlagMapShift(iLocalPosition,iLocalRotation,positionIndex) = true;
                end
            end
        end
    end

end

