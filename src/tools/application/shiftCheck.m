function successFlag = shiftCheck(robotMapsClass,positionRow,tformMatrix)
%SHIFTVECTORDECIDE 

    % import the parameters
    shiftUnit=robotMapsClass.generateGridSlot.metaList{1}.sphereRadius;

    % prepare the output
    successFlag = true;

    % find the maximum of the distance
    distance = calculateDistance(positionRow,tformMatrix);
    maxDistance = max(distance,[],'all');

    if maxDistance >= sqrt(2)*shiftUnit
        successFlag=false;
    end
end

function distance = calculateDistance(positionRow,tformMatrix)
    % import the data
    nTform = size(tformMatrix,3);
    nGlobalPosition = size(positionRow,1);

    % calculate the distance
    distance = zeros(nTform,1);
    for iTform = 1:nTform
        position = tformMatrix(1:3,4,iTform);
        positionRowDistance = positionRow - repmat(position,[1,nGlobalPosition])';
        distanceTemp = zeros(nGlobalPosition,1);
        for iGlobalPosition = 1:nGlobalPosition
            distanceTemp(iGlobalPosition) = norm(positionRowDistance(iGlobalPosition,:));
        end
        minDistance = min(distanceTemp,[],'all');
        distance(iTform) = minDistance;
    end
end
