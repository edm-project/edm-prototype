function scoreList = outputGoodConfigAverageScore(testDexterity,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,options)
    arguments
        testDexterity
        iLocalPositionIn
        iLocalRotationIn
        iGlobalPositionIn
        options.weights double = [1 8]
    end
    robotParameters = testDexterity.generateMapHandle.commonDataHandle.robotParameters;
    robotColumn = testDexterity.generateMapHandle.commonDataHandle.robotParameters.robotColumn;
    solutionFlag = squeeze(testDexterity.dataList.solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,:));
    configSolColumn = squeeze(testDexterity.dataList.configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,:));
    nSection = testDexterity.dataList.nSection;
    nFlip = testDexterity.dataList.flipCounter - 1;
    
    % calculate the joint bound
    jointBoundDist = zeros(nSection,nFlip);
    for iSection = 1:nSection
        for iFlip = 1:nFlip
            if solutionFlag(iSection,iFlip)==1
                jointBoundDist(iSection,iFlip) = jointBoundDistance(robotColumn,squeeze(configSolColumn(:,iSection,iFlip)));
            else
                jointBoundDist(iSection,iFlip) = -Inf;
            end
        end
    end

    jointBoundDist(jointBoundDist < 0) = 0;

    % calculate the manipulability
    minimumSigma = zeros(nSection,nFlip);
    for iSection = 1:nSection
        for iFlip = 1:nFlip
            if solutionFlag(iSection,iFlip)==1
                minimumSigma(iSection,iFlip) = calculateMinimunSigma(robotParameters,squeeze(configSolColumn(:,iSection,iFlip)));
            end
        end
    end

    % calculate the generalized manipulability
    generalizedDistance = jointBoundDist*options.weights(1) + minimumSigma*options.weights(2);

    scoreList = sum(generalizedDistance,2) ./ sum(any(generalizedDistance~=0),2);
end

function sigmaMin =calculateMinimunSigma(robotParameters,configSol)
    robotColumn= robotParameters.robotColumn;
    endEffectorName = robotParameters.endEffectorName;
    jacobian = geometricJacobian(robotColumn,configSol,endEffectorName);

    [~,S,~] = svd(jacobian);
    maximumSize = min(size(S,1),size(S,2));
    sigmaMin = S(maximumSize,maximumSize);
end