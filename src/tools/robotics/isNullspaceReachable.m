function isReachable = isNullspaceReachable(robotParameters,configSolInput,configSolNew)
%ISNULLSPACEREACHABLE check if configSol1, configSol2 can reach each other with
%  the nullspace motion.
% This is an unstable algorithm. Fortunately this function will not be
% repeatedly called. It would better that this function does not apply on
% the strange configurations.
%     arguments
%         robotParameters(:,1) struct
%         configSolInput(:,1) double
%         configSolNew(:,1) double
%     end

    % collect the information
%     robotColumn=robotParameters.robotColumn;
%     baseName=robotParameters.baseName;
%     endEffectorName=robotParameters.endEffectorName;

%     % calculate the tform of two configSol
%     tform1 = getTransform(robotColumn,configSolInput,endEffectorName,baseName);
%     tform2 = getTransform(robotColumn,configSolNew,endEffectorName,baseName);

    % compensate the difference when it is larger than upper limit
%     if norm(tform1-tform2) > 0.01
%         disp('start norm different too large.')
% %         warning('norm difference too large [%d]: is this two configuration not the same end effector pose?')
% %         disp('Continue Anyway?');
% %         pause on;
% %         pause;
%     end
    configSolNew = compensateCartesianDifference(robotParameters,configSolNew,configSolInput);

    % initialization
    configSolNow = configSolNew;
    isReachable = false;

    % we want to align every angle, and see if the whole system still align with each other
    for iConfigSol = 1:length(configSolNow)
        step = 0.05;
        repeatCounter = 0;
        nowDifference = circularDifference(configSolNow(iConfigSol),configSolInput(iConfigSol),2*pi);
        while norm(nowDifference) > 1e-9
            configSolStepPositve = nullspaceStep(robotParameters,configSolNow,configSolNew,step);
            configSolStepNegative = nullspaceStep(robotParameters,configSolNow,configSolNew,-step);
        
            % watch two steps: a positive step and a negative step, then
            % compare with on is closest
            nowDifference = circularDifference(configSolNow(iConfigSol),configSolInput(iConfigSol),2*pi);
            positiveStepDifference = circularDifference(configSolStepPositve(iConfigSol),configSolInput(iConfigSol),2*pi);
            negativeStepDifference = circularDifference(configSolStepNegative(iConfigSol),configSolInput(iConfigSol),2*pi);
    
            % if the sign are all same: target is outside the both steps:
            if sign(nowDifference)==sign(positiveStepDifference) && sign(nowDifference)==sign(negativeStepDifference)
                % choose the closest one
                if abs(positiveStepDifference)<abs(negativeStepDifference)
                    configSolNow=configSolStepPositve;
                else
                    configSolNow=configSolStepNegative;
                end
            % if the sign are not same: one step marches over the target
            else
                step = step/2;
            end
    
            % compensate and normalize
            configSolNow = compensateCartesianDifference(robotParameters,configSolNow,configSolNew);
            configSolNow = jointNormalize(robotParameters,configSolNow);
            repeatCounter = repeatCounter + 1;
            if repeatCounter >= 500
%                 disp('isNullspaceReachable() exceeds the repeat time.');
                break;
            end
        end

        % check if the configSolNow is then same as the configSolInput
        % strange, seems that the align of one robot joint will always cause other 
        % joint a bit away form it's angle.
        if norm(configSolNow-configSolInput) < 0.02
            isReachable = true;
            break;
        end
    end
end

