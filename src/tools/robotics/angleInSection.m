function sectionNumber = angleInSection(nSection,angle)
%ANGLESECTION return the section Number within the
%     arguments
%         nSection(1,1) double {mustBePositive}
%         angle(1,1) double {mustBeGreaterThanOrEqual(angle,0),mustBeLessThan(angle,6.2832)}
%     end
    sectionLength=2*pi/nSection;
    sectionNumber=floor(angle/sectionLength)+1;
    
    if sectionNumber>nSection
%         ME = MException('angleInSection:outOfRange', ...
%                         'Angle out of [0 2pi) range [%d]', ...
%                         angle);
%         throw(ME)
        error('Angle out of [0 2pi) range');
    end
end

