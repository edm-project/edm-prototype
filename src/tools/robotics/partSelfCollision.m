function result = partSelfCollision(rigidBodyIn,columnIn)
%PARTSELFCOLLISION return the self-collision part
    if isMATLABReleaseOlderThan("R2022b")
        [~,selfSeparationDist,~] = checkCollision(rigidBodyIn,columnIn);
    else
        [~,selfSeparationDist,~] = checkCollision(rigidBodyIn,columnIn,SkippedSelfCollisions="parent");
    end
    [result,~] = find(isnan(selfSeparationDist));
    result = sort(result);

    if ~any(columnIn) || isempty(result)
        result=[];
    end
end

