function result = partJointLimit(rigidBodyIn,columnIn)
%partJointLimit return the angle index who's joint is over limit
    jointConstraintRow = constraintJointBounds(rigidBodyIn).Bounds;
    jointConstraintResult = columnIn >= jointConstraintRow(:,1) & columnIn <=jointConstraintRow(:,2);
    result = find(jointConstraintResult == false);

    if ~any(columnIn) || isempty(result)
        result=[];
    end
end

