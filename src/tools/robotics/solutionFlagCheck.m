function [solutionFlagOut,configSolOut] = solutionFlagCheck(robotParameters,configSol)
%SOLUTIONFLAGCHECK check the solutionFlag and return the normalized version
% of the data.
    robotColumn = robotParameters.robotColumn;
    
    % normalize the joint configSol
    configSolOut = jointNormalize(robotParameters,configSol);

    if checkCollision(robotColumn,configSolOut)==true
        solutionFlagOut=4;
    elseif checkJointLimits(robotColumn,configSolOut)==true
        solutionFlagOut=3;
    else
        solutionFlagOut=1;
    end

end

