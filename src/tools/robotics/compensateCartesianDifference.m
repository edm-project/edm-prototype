function configSolCompensate = compensateCartesianDifference(robotParameters,configSolNew,configSolInitial)
%     arguments
%         robotParameters(1,1) struct
%         configSolNew(:,1) double
%         configSolInitial(:,1) double
%     end

    % collect the information
    robotColumn=robotParameters.robotColumn;
    baseName=robotParameters.baseName;
    endEffectorName=robotParameters.endEffectorName;

    % calculate the tform of two configSol
    tform1 = getTransform(robotColumn,configSolNew,endEffectorName,baseName);
    tform2 = getTransform(robotColumn,configSolInitial,endEffectorName,baseName);

    % compensate the difference when it is larger than upper limit
    if norm(tform1-tform2) > 1e-3
%         warning('norm [%d]:Trigger extra cartesian error compensation. Probably step too long.',norm(tform1-tform2))
        while norm(tform1-tform2) > 1e-3
            % get Cartesian error form the previous step
            cartesianError = cartesianDifference(robotParameters,configSolInitial,configSolNew);

            % Compensate the cartesian error
            jacobianAfter = geometricJacobian(robotColumn,configSolNew,endEffectorName);
            configSolNew = configSolNew + 0.2*pinv(jacobianAfter)*cartesianError;

            % recalculate the error
            tform1 = getTransform(robotColumn,configSolNew,endEffectorName,baseName);
        end
    end

    configSolCompensate = configSolNew;
end

