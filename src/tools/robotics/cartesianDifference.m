function cartesianError = cartesianDifference(robotParameters,configSolInitial,configSolMoved)
%ALGOTEST_CARTESIANERROR get the cartesian difference of two configSol
%     arguments
%         robotParameters(1,1) struct
%         configSolInitial(:,1) double
%         configSolMoved(:,1) double
%     end

    % get information needed
    robotColumn=robotParameters.robotColumn;
    baseName=robotParameters.baseName;
    endEffectorName=robotParameters.endEffectorName;

    % get the cartesian tform for two poses
    tformInitial = getTransform(robotColumn,configSolInitial,endEffectorName,baseName);
    tformMoved = getTransform(robotColumn,configSolMoved,endEffectorName,baseName);

    % we want to move the arm back to the initial state, therefore the error is old-new
    positionDifference = tformInitial(1:3,4) - tformMoved(1:3,4);
    rotmDifference = tformInitial(1:3,1:3)*tformMoved(1:3,1:3)';
    eulXYZ = rotm2eul(rotmDifference,'XYZ');
    cartesianError = [eulXYZ';positionDifference];
end

