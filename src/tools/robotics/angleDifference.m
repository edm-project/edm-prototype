function angle = angleDifference(reference,target,normal)
%ANGLEDIFFERENCE Calculate the angle difference from [reference] to [target],
%with the right-hand rule of the [normal] angle.
%     arguments
%         reference(3,1) double
%         target(3,1) double
%         normal(3,1) double
%     end

    if dot(reference,normal)>= 1e-7 || dot(target,normal)>= 1e-7
%         ME = MException('angleDifference:notNormal', ...
%                         'Normalvector not orthogonal\n[r %s][t %s][n  %s]\n[rn %d][tn %d]', ...
%                         mat2str(reference),mat2str(target),mat2str(normal), ...
%                         dot(reference,normal),dot(target,normal));
%         throw(ME)
        error('Normalvector not orthogonal');
    end

    % This algorithm is better at numeric perfomance
    crossRefTar = cross(reference,target);
    signedNormCrossRefTar = sign(dot(crossRefTar,normal)) * norm(crossRefTar);
    angle = atan2(signedNormCrossRefTar,dot(reference,target));
    
    % shift the angle, in order to have a [0, 2pi] output
    if angle<0
        angle=angle+2*pi;
    end
end

