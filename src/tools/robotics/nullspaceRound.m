function [configSolCol,haveSolutionCol] = nullspaceRound(robotParameters,configSol,nSection,options)
%NULLSPACEROUND input the parameters needed and directly give out configSol
%that goes a round
% out:
% [configSolCol]: a 7*nSection array that represents the nullmotion of configSol with different nSection
% [haveSolutionCol]: a 1*nSection array that [true]
% [mode]: the mode that  
% [options.timeStep]: the each step length of nullmotion 
% [options.increaseStep]: the inceasting step when not all 24 steps are found
% [options.maxStep]: the maximum step numbers for attempting to get all 24 sections
    arguments
        robotParameters(1,1) struct
        configSol(:,1) double
        nSection(1,1) double
        options.timeStep(1,1) double
        options.increaseStep(1,1) double
        options.maxStep(1,1) double
    end

    stepCount = 0;

    while true
        % the iteration counter
        stepCount = stepCount + options.increaseStep;
        if stepCount > options.maxStep
            break
        end

        % initialize the result
        configSolCol = zeros(length(homeConfiguration(robotParameters.robotColumn)),nSection);
        haveSolutionCol = zeros(nSection,1);

        % collect the information
        [configSolColFix,~,sectionColFix] = nullspaceFixed(robotParameters, ...
                                                            configSol, ...
                                                            options.timeStep, ...
                                                            nSection, ...
                                                            stepCount);

        [configSolColFixR,~,sectionColFixR] = nullspaceFixedReverse(robotParameters, ...
                                                            configSol, ...
                                                            options.timeStep, ...
                                                            nSection, ...
                                                            stepCount);

        [~,sectionNumber] = dexteritySection(robotParameters,configSol,nSection);
        
        % sort the information
        for iSection = 1:nSection
            if sectionNumber <= 12
                if iSection <= 12 && iSection >=sectionNumber  % choose the forward
                    if sum(sectionColFix==iSection,'all')~=0
                        firstIndex = find(sectionColFix==iSection);
                        configSolCol(:,iSection)=configSolColFix(:,firstIndex(1));
                        haveSolutionCol(iSection)=1;
                    end
                else
                    if sum(sectionColFixR==iSection,'all')~=0  % choose the reverse
                        firstIndex = find(sectionColFixR==iSection);
                        configSolCol(:,iSection)=configSolColFixR(:,firstIndex(1));
                        haveSolutionCol(iSection)=1;
                    end
                end
            else
                if iSection >= 13 && iSection <= sectionNumber  % choose the forward
                    if sum(sectionColFixR==iSection,'all')~=0
                        firstIndex = find(sectionColFixR==iSection);
                        configSolCol(:,iSection)=configSolColFixR(:,firstIndex(1));
                        haveSolutionCol(iSection)=1;
                    end
                else
                    if sum(sectionColFix==iSection,'all')~=0  % choose the reverse
                        firstIndex = find(sectionColFix==iSection);
                        configSolCol(:,iSection)=configSolColFix(:,firstIndex(1));
                        haveSolutionCol(iSection)=1;
                    end
                end
            end
        end

        % determine the loop end
        if all(haveSolutionCol)
            break;
        end
    end
end

