function configSolNormalized = jointNormalize(robotParameters,configSolIn)
%JOINTNORMALIZE normalize the joint for the robot. 
% because some joint of the robot have a very special limit (like 3.7, more
% than 2 pi), therefore this function is for every step.
%     arguments
%         robotParameters(1,1) struct
%         configSolIn(:,1) double
%     end

    % get information needed
    robotName=robotParameters.robotName;

    % configure the normalization for different robots
    if isequal(robotName,'frankaEmikaPanda')
        configSolIn(1)=circularMod(configSolIn(1),2*pi,pi);
        configSolIn(2)=circularMod(configSolIn(2),2*pi,pi);
        configSolIn(3)=circularMod(configSolIn(3),2*pi,pi);
        configSolIn(4)=circularMod(configSolIn(4),2*pi,pi);
        configSolIn(5)=circularMod(configSolIn(5),2*pi,pi);
        configSolIn(6)=circularMod(configSolIn(6),2*pi,3/2*pi);
        configSolIn(7)=circularMod(configSolIn(7),2*pi,pi);
    elseif isequal(robotName,'universalUR5')
        configSolIn(1)=circularMod(configSolIn(1),2*pi,pi);
        configSolIn(2)=circularMod(configSolIn(2),2*pi,pi);
        configSolIn(3)=circularMod(configSolIn(3),2*pi,pi);
        configSolIn(4)=circularMod(configSolIn(4),2*pi,pi);
        configSolIn(5)=circularMod(configSolIn(5),2*pi,pi);
        configSolIn(6)=circularMod(configSolIn(6),2*pi,pi);
    elseif isequal(robotName,'kukaIiwa14')
        configSolIn(1)=circularMod(configSolIn(1),2*pi,pi);
        configSolIn(2)=circularMod(configSolIn(2),2*pi,pi);
        configSolIn(3)=circularMod(configSolIn(3),2*pi,pi);
        configSolIn(4)=circularMod(configSolIn(4),2*pi,pi);
        configSolIn(5)=circularMod(configSolIn(5),2*pi,pi);
        configSolIn(6)=circularMod(configSolIn(6),2*pi,pi);
        configSolIn(7)=circularMod(configSolIn(7),2*pi,pi);
    elseif isequal(robotName,'kinovaGen3')
        configSolIn(1)=circularMod(configSolIn(1),2*pi,pi);
        configSolIn(2)=circularMod(configSolIn(2),2*pi,pi);
        configSolIn(3)=circularMod(configSolIn(3),2*pi,pi);
        configSolIn(4)=circularMod(configSolIn(4),2*pi,pi);
        configSolIn(5)=circularMod(configSolIn(5),2*pi,pi);
        configSolIn(6)=circularMod(configSolIn(6),2*pi,pi);
        configSolIn(7)=circularMod(configSolIn(7),2*pi,pi);
    else
%         ME = MException('robotMapsClass:jointNormalize', ...
%             'The joint normalization for [%s] is not implemented.', ...
%             robotName);
%         throw(ME)
        error("joint normalization for the robot is not implemented");
    end

    configSolNormalized = configSolIn;
end

