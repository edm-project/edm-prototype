function [angle,sectionNumber] = dexteritySection(robotParameters,configSol,nSection)
%DEXTERITYSECTION Plot the dexterity
%     arguments
%         robotParameters(1,1) struct
%         configSol(:,1) double
%         nSection(1,1) double {mustBePositive}
%     end
    
    [reference,target,normal] = dexterityReferenceVectors(robotParameters,configSol);
    angle = angleDifference(reference,target,normal);
    sectionNumber = angleInSection(nSection,angle);
end

