function [reference,target,normal] = dexterityReferenceVectors(robotParameters,configSol)
%DEXTERITYREFERENCEVECTOR calculate 2 reference needed for dexterity
%according to the reference and robotParameters.
%     arguments
%         robotParameters(1,1) struct
%         configSol(:,1) double
%     end

    % get information needed
    robotColumn=robotParameters.robotColumn;
    baseName=robotParameters.baseName;
    shoulderName=robotParameters.shoulderName;
    elbowName=robotParameters.elbowName;
    endEffectorName=robotParameters.endEffectorName;
    
    % position vectors
    p_shoulder = getTransform(robotColumn,configSol,shoulderName,baseName);
    p_shoulder = p_shoulder(1:3,4);
    p_elbow = getTransform(robotColumn,configSol,elbowName,baseName);
    p_elbow = p_elbow(1:3,4);
    p_endEffetor = getTransform(robotColumn,configSol,endEffectorName,baseName);
    p_endEffetor = p_endEffetor(1:3,4);

    % common normal axis (shoulder-endEffetor)
    normal = p_endEffetor - p_shoulder;
    normal = normal / norm(normal);

    % normal reference Vector for planes
    normReference = dexterityNormalVector(robotParameters,configSol);

    % reference plane vectors
    reference = cross(normReference,normal);
    reference = reference / norm(reference);

    % moving (targeted) plane vectors
    target = cross(p_elbow-p_shoulder,normal);
    % numeric infomation warning
    if norm(target) <= 1e-4
%         warning("Output vector is numerically not fine for calculation [%s][%d]",mat2str(target),norm(target))
    end
    target = target/norm(target);
end

