function normReference = dexterityNormalVector(robotParameters,configSol)
%DEXTERITYNORMALVECTOR get normal vector according to the angle reference
%     arguments
%         robotParameters(1,1) struct
%         configSol(:,1) double
%     end

    % get information needed
    robotColumn=robotParameters.robotColumn;
    baseName=robotParameters.baseName;
    shoulderName=robotParameters.shoulderName;
    endEffectorName=robotParameters.endEffectorName;
    
    % position vectors
    p_shoulder = getTransform(robotColumn,configSol,shoulderName,baseName);
    p_shoulder = p_shoulder(1:3,4);
    p_endEffetor = getTransform(robotColumn,configSol,endEffectorName,baseName);
    p_endEffetor = p_endEffetor(1:3,4);

    % normal reference vectors
    normal = p_endEffetor - p_shoulder;
    normal = normal / norm(normal);

    % if the original normal vector is too close to the EE-shoulder axis,
    % then change it to the vectors.
    if norm(normal - [0,0,1]) < 1e-10
        normReference = [1;0;0];
    else
        normReference = [0;0;1];
    end
end

