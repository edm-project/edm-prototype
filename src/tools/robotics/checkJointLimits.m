function result = checkJointLimits(rigidBodyIn,columnIn)
%CHECKJOINTLIMITS check if joint limit is obeyed. true if limits violated
%     arguments
%         rigidBodyIn(1,1) rigidBodyTree
%         columnIn(:,1) double
%     end
    jointConstraintRow = constraintJointBounds(rigidBodyIn).Bounds;
    jointConstraintResult = columnIn >= jointConstraintRow(:,1) & columnIn <=jointConstraintRow(:,2);
    result = ~all(jointConstraintResult);
end

