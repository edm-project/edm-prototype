function jointConst = jointBoundIntersect(robotParameters,previousSolution,angleTolerance)
%JOINTBOUNDINTERSECT 此处显示有关此函数的摘要
%   此处显示详细说明
    arguments
        robotParameters(1,1) struct
        previousSolution(1,:) double
        angleTolerance(1,1) double
    end

    robotColumn=robotParameters.robotColumn;
    jointConst=constraintJointBounds(robotColumn);
    jointConstRow=jointConst.Bounds;

    for iRow=1:size(jointConstRow,1)
        jointConstRow(iRow,1)=previousSolution(iRow)-angleTolerance;
        jointConstRow(iRow,2)=previousSolution(iRow)+angleTolerance;
    end

    jointConst.Bounds=jointConstRow;
end

