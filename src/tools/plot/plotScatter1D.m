function plotScatter1D(axesHandle,value,yAxis,varargin)
    yHeight = repelem(yAxis,length(value),1);
    scatter(axesHandle,value(value~=0),yHeight(value~=0),'filled',varargin{:});
end

