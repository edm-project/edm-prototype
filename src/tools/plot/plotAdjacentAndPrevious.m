function plotAdjacentAndPrevious(adjacentArray,previousArray,positionRow,solutionFlagArray,displayRed)
%PLOTADJACENTANDPREVIOUS plot the adjacent and previous information for
%adjacent list
    if length(adjacentArray)~=size(previousArray,1) 
        ME = MException('robotMaps:plotAdjacentAndPrevious', ...
            'size of the input is not correct');
        throw(ME);
    end

    sizeMark = 20;
    linWidth = 1;

    positionRowFiltered=positionRow(solutionFlagArray==1,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'green','filled');
    if displayRed==true
    positionRowFiltered=positionRow(solutionFlagArray==2,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'red','filled');    
    end
    positionRowFiltered=positionRow(solutionFlagArray==3,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'yellow','filled');
    positionRowFiltered=positionRow(solutionFlagArray==4,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'magenta','filled');
 
    rootCalculation = length(find(previousArray(:,1) == 0));
    title(['Root count = ',num2str(rootCalculation)]);
    
    colorList = ["blue","cyan","red","magenta"];

    for iPrevious = 1:size(previousArray,2)
        previousMask = previousArray(:,iPrevious)~=0;
        currentPoints = adjacentArray(previousMask);
        previousPoints = previousArray(previousMask,iPrevious);

        quiver3(positionRow(previousPoints,1), ...
            positionRow(previousPoints,2), ...
            positionRow(previousPoints,3), ...
            positionRow(currentPoints,1)-positionRow(previousPoints,1), ...
            positionRow(currentPoints,2)-positionRow(previousPoints,2), ...
            positionRow(currentPoints,3)-positionRow(previousPoints,3), ...
            'off',colorList(iPrevious),LineWidth=linWidth)
    end
end

