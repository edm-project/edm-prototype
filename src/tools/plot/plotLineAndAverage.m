function p = plotLineAndAverage(axesHandle,yValueCell,mainStyle,color)
    maxLength = 0;
    nCell = length(yValueCell);
    axesHandle.NextPlot = 'add';
    for iCell = 1:nCell
        maxLength=max(maxLength,length(yValueCell{iCell}));
    end
    
    yValueMatrix = zeros(nCell,maxLength);
    for iCell = 1:nCell
        yValueMatrix(iCell,:) = plotInterpolation(yValueCell{iCell},maxLength);
    end

    yValueAverage = mean(yValueMatrix,1);
    xValue = linspace(0,1,maxLength);
    p = plot(axesHandle,xValue,yValueAverage,mainStyle,'LineWidth',2,'Color',color);

    for iCell = 1:nCell
        plotLine(axesHandle,[0 1],yValueMatrix(iCell,:),color,':','');
    end
end

