function colorbarMark = getJetColorbar()
%GETJETCOLORBAR 
    f = figure;
    colorbarMark=colormap("jet");
    colorbarMark=flipud(colorbarMark);
    matrixHeight=size(colorbarMark,1);
    colorbarMark=colorbarMark(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    delete(f);
end

