function p = plotLine(axesHandle,xSection,yValue,color,style,dispalyName)
    nData = length(yValue);
    xValue = linspace(xSection(1),xSection(2),nData);
    p = plot(axesHandle,xValue,yValue,style,'Color',color,'DisplayName',dispalyName);
end

