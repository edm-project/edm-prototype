function axesHandle = plotIndex(fig,plottedIndex,positionRow,options)
%PLOTDEXTERITYINDEX 
    arguments
        fig(1,1)
        plottedIndex(:,1) double
        positionRow(:,3) double
        options.previousAxesHandle = ''
        options.colorbar(:,3) double = ''
        options.markSize(1,1) double = 15
    end
    
    if size(plottedIndex,1)~=size(positionRow,1)
        error('The size of two inputs are not fit for plotting! [%d][%d]', ...
               size(plottedIndex,1), ...``
               size(positionRow,1))
    end

    % delete the previous handle
    if ~isempty(options.previousAxesHandle)
        delete(options.previousAxesHandle);
    end

    % plot graph on the figure
    positionRowX = positionRow(:,1);
    positionRowY = positionRow(:,2);
    positionRowZ = positionRow(:,3);
    axesHandle = axes(fig);
    scatter3(axesHandle,positionRowX,positionRowY,positionRowZ,options.markSize,plottedIndex,"filled");

    % change the colorbar
    if ~isempty(options.colorbar)
        colormap(axesHandle,options.colorbar)
    end
end

