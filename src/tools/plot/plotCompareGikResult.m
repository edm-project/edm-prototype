function plotCompareGikResult(solutionFlagSlice,difference,positionRow,randomList,iGlobalPositionOut)
%PLOTCOMPAREGIKRESULT 此处显示有关此函数的摘要
%   此处显示详细说明

    % set constants
    sizeMark = 18;
    linWidth = 0.75;

    % plot the basic graph
    positionRowFiltered=positionRow(solutionFlagSlice==1,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'green','filled');
    positionRowFiltered=positionRow(solutionFlagSlice==2,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'red','filled');
    positionRowFiltered=positionRow(solutionFlagSlice==3,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'yellow','filled');
    positionRowFiltered=positionRow(solutionFlagSlice==4,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'magenta','filled');

    % plot the difference graph
    scatter3(positionRow(difference,1),positionRow(difference,2),positionRow(difference,3),sizeMark+1,'black',LineWidth=linWidth);

    % plot the candidate start point
    scatter3(positionRow(randomList,1),positionRow(randomList,2),positionRow(randomList,3),sizeMark+1,'black',LineWidth=1.5*linWidth);
    
    % plot the chosen start point
    scatter3(positionRow(iGlobalPositionOut,1),positionRow(iGlobalPositionOut,2),positionRow(iGlobalPositionOut,3),sizeMark+1,'black',LineWidth=3*linWidth);

    % plot the title:
    title(['difference Num: ',num2str(length(find(difference))),'. Choose index: ',num2str(iGlobalPositionOut),' ',mat2str(positionRow(iGlobalPositionOut,:),3)])

end

