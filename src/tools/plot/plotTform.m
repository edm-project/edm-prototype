function plotTform(tformIn,scale)
%PLOTTFORM Plot the frame array of a homogen Transformation
    arguments
        tformIn(4,4) double
        scale(1,1) double
    end

    % extract rotm and tansvec
    rotMat=tformIn(1:3,1:3)*scale;
    transVec=tformIn(1:3,4);
%     stemWidth = 0.0002;
%     stemWidth = scale/40;
    stemWidth = scale/40;

    % draw the 3 axis of graph
    hold on
%     quiver3(transVec(1),transVec(2),transVec(3),rotMat(1,1),rotMat(2,1),rotMat(3,1),'off','r') %x
%     quiver3(transVec(1),transVec(2),transVec(3),rotMat(1,2),rotMat(2,2),rotMat(3,2),'off','g') %y
%     quiver3(transVec(1),transVec(2),transVec(3),rotMat(1,3),rotMat(2,3),rotMat(3,3),'off','b') %z
    mArrow3(transVec,transVec+rotMat(:,1),'color','r','stemWidth',stemWidth,'tipWidth',stemWidth*3); %x
    mArrow3(transVec,transVec+rotMat(:,2),'color','g','stemWidth',stemWidth,'tipWidth',stemWidth*3); %x
    mArrow3(transVec,transVec+rotMat(:,3),'color','b','stemWidth',stemWidth,'tipWidth',stemWidth*3); %x
    grid on;
%     view(0,0);
%     xlim([-1 1])
%     zlim([-0.5 1.5])
%     ylim([-1 1])
end

