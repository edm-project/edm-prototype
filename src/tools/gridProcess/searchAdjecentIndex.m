function adjacentIndex = searchAdjecentIndex(adjacentIndexMap,startIndex,endIndex)
%SEARCHADJECENTINDEX 此处显示有关此函数的摘要
%   此处显示详细说明
    arguments
        adjacentIndexMap containers.Map
        startIndex(1,1) double {mustBeInteger,mustBePositive}
        endIndex(1,1) double {mustBeInteger,mustBePositive}
    end

    % exchange the sequence of start and end
    if startIndex>endIndex
        temp = startIndex;
        startIndex = endIndex;
        endIndex = temp;
    end

    % search the point
    adjacentIndex=adjacentIndexMap([num2str(startIndex),',',num2str(endIndex)]);

end

