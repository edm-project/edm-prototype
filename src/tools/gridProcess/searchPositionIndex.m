function positionIndex = searchPositionIndex(positionRowIn,position)
%SEARCHPOSITIONINDEX search the index of the position
    arguments
        positionRowIn(:,3) double
        position(3,1) double
    end

    positionIndex=find((abs(positionRowIn(:,1)-position(1))<1e-10 & ...
                  abs(positionRowIn(:,2)-position(2))<1e-10 & ...
                  abs(positionRowIn(:,3)-position(3))<1e-10)==true);
    
    if isempty(positionIndex)
        positionIndex=0;
    elseif length(positionIndex)==1
        % do nothing
    else
        ME = MException('robotMapsClass:searchPositionIndex', ...
            'two or more potential points searched. Impossible data. [%d]', ...
            mat2str(positionIndex));
        throw(ME)
    end
end

