function [keys,values] = shiftPointsCartesian(positionRow,shiftArray)
%SHIFTPOINTSCARTESIAN shift the Points (in cartesian space) according to
%shift Array and record the result in [keys:point index] [values:adjacent point index]
    arguments
        positionRow(:,3) double
        shiftArray(1,3) double
    end

    % shift the positionRow
    positionRowShifted = positionRow+shiftArray;
    nGlobalPositions = size(positionRow,1);

    % prepare the empty array
    keys=1:nGlobalPositions;
    values=zeros(1,nGlobalPositions);

    % assign the positionIndex
    for iRow=1:size(positionRow,1)
        searchedIndex=searchPositionIndex(positionRow,positionRowShifted(iRow,:));
        if searchedIndex~=0
            values(iRow) = searchedIndex;
        end
    end

    % remove the zero terms;
    nonzeroMask = (values~=0);
    keys=keys(nonzeroMask);
    values=values(nonzeroMask);
end

