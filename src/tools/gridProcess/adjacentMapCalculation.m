function adjacentMap = adjacentMapCalculation(gridMark,gridMetaList,positionRow,nGlobalPosition,mode)
%ADJACENTMAPCALCULATION 
    arguments
        gridMark
        gridMetaList
        positionRow(:,3) double
        nGlobalPosition(1,1) double
        mode char {mustBeMember(mode,{'face','faceEdge','faceEdgeCorner'})}
    end

    % initialize the map
    adjacentMap=containers.Map(1:nGlobalPosition,cell(1,nGlobalPosition));

    if isequal(gridMark,'cartesian2') 
        % change the mode for cartesian2
        if isequal(mode,'faceEdgeCorner')
            mode = 'faceEdge';
        end

        % get the adjacent infomation
        shiftUnit=gridMetaList.sphereRadius*2;
        [shiftMatrix,~]=shiftMatrixCartesian(gridMark,mode,shiftUnit);

        % merge the adjacent pairs
        for iShift=1:size(shiftMatrix,1)
            % full adjacent List
            [keys,values] = shiftPointsCartesian(positionRow,shiftMatrix(iShift,:));
            adjacentMap = mergeContainerMap(adjacentMap,keys,values);
        end

    elseif isequal(gridMark,'cartesian3')
        % get the adjacent infomation
        shiftUnit=gridMetaList.sphereRadius*2;
        [shiftMatrix,~]=shiftMatrixCartesian(gridMark,mode,shiftUnit);

        % merge the adjacent pairs
        for iShift=1:size(shiftMatrix,1)
            % full adjacent List
            [keys,values] = shiftPointsCartesian(positionRow,shiftMatrix(iShift,:));
            adjacentMap = mergeContainerMap(adjacentMap,keys,values);
        end
    else
        ME = MException('testDexterity:assignAdjacent:noImplementation', ...
            'The assignAdjacent is not implemented for [%s] grid', ...
            gridMark);
        throw(ME)
    end
end

