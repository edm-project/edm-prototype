function [shiftMatrix,maxAdjacent] = shiftMatrixCartesian(gridMark,adjacentRange,shiftUnit)
%shiftMatrixCartesian generate a Matrix that give hints about how to detect the
%adjacent points
    arguments
        gridMark(1,:) char {mustBeMember(gridMark,{'cartesian2','cartesian3'})}
        adjacentRange(1,:) char
        shiftUnit(1,1) double {mustBePositive}
    end

    shiftMatrix=[];
    maxAdjacent=0;

    if isequal(gridMark,'cartesian2')
        if contains(adjacentRange,'face')
            shiftMatrix=[shiftMatrix;
                         shiftUnit,0,0;-shiftUnit,0,0;
                         0,0,shiftUnit;0,0,-shiftUnit];
            maxAdjacent=maxAdjacent+4;
        end
        
        if contains(adjacentRange,'Edge')
            shiftMatrix=[shiftMatrix;
                shiftUnit,0,shiftUnit;-shiftUnit,0,shiftUnit;
                shiftUnit,0,-shiftUnit;-shiftUnit,0,-shiftUnit];
            maxAdjacent=maxAdjacent+4;
        end

        if contains(adjacentRange,'Corner')
            warning('For cartesian2 mode there are no corner as adjacent')
        end
    elseif isequal(gridMark,'cartesian3')
        if contains(adjacentRange,'face')
            shiftMatrix=[shiftMatrix;
                         shiftUnit,0,0;-shiftUnit,0,0;
                         0,shiftUnit,0;0,-shiftUnit,0;
                         0,0,shiftUnit;0,0,-shiftUnit];
            maxAdjacent=maxAdjacent+6;
        end

        if contains(adjacentRange,'Edge')
            shiftMatrix=[shiftMatrix;
                shiftUnit,0,shiftUnit;-shiftUnit,0,shiftUnit;
                shiftUnit,0,-shiftUnit;-shiftUnit,0,-shiftUnit;
                shiftUnit,shiftUnit,0;-shiftUnit,shiftUnit,0;
                shiftUnit,-shiftUnit,0;-shiftUnit,-shiftUnit,0;
                0,shiftUnit,shiftUnit;0,-shiftUnit,shiftUnit;
                0,shiftUnit,-shiftUnit;0,-shiftUnit,-shiftUnit];
            maxAdjacent=maxAdjacent+12;
        end

        if contains(adjacentRange,'Corner')
            shiftMatrix=[shiftMatrix;
                shiftUnit,shiftUnit,shiftUnit;-shiftUnit,shiftUnit,shiftUnit;
                shiftUnit,shiftUnit,-shiftUnit;-shiftUnit,shiftUnit,-shiftUnit;
                shiftUnit,-shiftUnit,shiftUnit;-shiftUnit,-shiftUnit,shiftUnit;
                shiftUnit,-shiftUnit,-shiftUnit;-shiftUnit,-shiftUnit,-shiftUnit];
            maxAdjacent=maxAdjacent+8;
        end
    else
    end
end

