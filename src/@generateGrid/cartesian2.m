function cartesian2(obj,armLength,sphereRadius)
%CARTESIAN2 generate cartesian points in x-z plane
% generate a discretized square in x-z plane. Centered at (0,0,0)
% 
% armLength: robot longest reachable length, half of the square side length
% sphereRadius: radius of discretized points, half of the grid step length 
    arguments
        obj(1,1) generateGrid
        armLength(1,1) double {mustBePositive}
        sphereRadius(1,1) double {mustBePositive}
    end
    
    % check if allow callable for efficiency
    obj.mustAllowCall('cartesian2');

    % check for armLength and sphereRadius relationship
    if mod(armLength,sphereRadius)~=0
        warning('mod(armLength,sphereRadius) is not zero! This cause potential problems.')
        armLength=armLength-mod(armLength,sphereRadius);
        warning('armLength now reset to [%d]',armLength);
    end

    % generate 2D cartesian grid
    scalar = -armLength:2*sphereRadius:armLength;                % cut the scalar unit
    [X_mesh,Z_mesh] = meshgrid(scalar);                          % generate 2D grid
    grid_temp=[X_mesh(:),zeros(length(X_mesh(:)),1),Z_mesh(:)];  % collect points in X-Z plane

    % prepare for update
    markIn='cartesian2';
    dataListIn.positionRow=grid_temp;
    metaListIn.armLength=armLength;
    metaListIn.sphereRadius=sphereRadius;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

