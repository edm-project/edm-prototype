function cartesian3(obj,armLength,sphereRadius)
%CARTESIAN2 generate cartesian points in a cube
% generate a discretized cube in space. Centered at (0,0,0)
% 
% armLength: robot longest reachable length, half of the cube side length
% sphereRadius: radius of discretized points, half of the grid step length 
    arguments
        obj(1,1) generateGrid
        armLength(1,1) double {mustBePositive}
        sphereRadius(1,1) double {mustBePositive}
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('cartesian3');

    % check for armLength and sphereRadius relationship
    if mod(armLength,sphereRadius)~=0
        warning('mod(armLength,sphereRadius) is not zero! This cause potential problems.')
        armLength=armLength-mod(armLength,sphereRadius);
        warning('armLength now reset to [%d]',armLength);
    end

    % generate 3D cartesian grid
    scalar = -armLength:2*sphereRadius:armLength;
    [X_mesh,Y_mesh,Z_mesh] = meshgrid(scalar);  % generate 3D grid
    grid_temp=[X_mesh(:),Y_mesh(:),Z_mesh(:)];  % collect points in array

    % prepare for update
    markIn='cartesian3';
    dataListIn.positionRow=grid_temp;
    metaListIn.armLength=armLength;
    metaListIn.sphereRadius=sphereRadius;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

