classdef generateGrid < handle
    %GENERATEGRID Process generateGrid
    %   used for generate the grid by recording different points
    properties (SetAccess = immutable)
    end 
    % end of immutable attributes

    properties (SetAccess = private)
        isActive(1,1) logical
        commonDataHandle(1,1) commonData
        robotMapsHandle(1,1)

        mark(1,:) cell {mustBeValidMarksNullPrepare}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
    end
    % end of the private attributes
    
    methods
        function obj = generateGrid()
            %GENERATEGRID constructor
            obj.isActive=false;
            obj.mark={'null'};
        end % end of the constructor
    end
    % end of the constructor

    methods (Access=private)
        function mustAllowCall(obj,callMarkIn)
        % mustAllowCall: throw error if function current call is not allowed
            arguments
                obj(1,1) generateGrid
                callMarkIn(1,:) char {mustBeValidMarks}
            end

            % isActive must be true
            mustIsActiveState(obj,true)

            % allow only one of {'cartesian2','cartesian3'} 
            mustMutuallyExclude(obj,callMarkIn,{'cartesian2','cartesian3'});
        end

        function updateData(obj,markIn,metaListIn,dataListIn)
        % update the data in this procees, called in functional methods
            arguments
                obj(1,1) generateGrid
                markIn(1,:) char {mustBeValidMarks}
                metaListIn(1,1) struct
                dataListIn(1,1) struct {mustBeValidDataList}
            end
            % check if this call is allowed
            obj.mustAllowCall(markIn);

            % update mark
            if isequal(obj.mark,{'prepared'})
                obj.mark={markIn};
            else
                obj.mark=[obj.mark,{markIn}];
            end

            %update metaList and dataList
            obj.metaList{length(obj.metaList)+1}=metaListIn;
            obj.dataList=combineStruct(obj.dataList,dataListIn);
        end
    end
    % end of the private methods

    methods (Access={?robotMaps})
        function mustAllowInitialize(obj)
        % mustAllowInitialze: check conditions for Initialize
            
            % isActive must equal false, otherwise means already initialized
            mustIsActiveState(obj,false)

            % marks must be 'null', otherwise means already finalized
            mustBeMark(obj,'null',true);
        end

        function mustAllowFinalize(obj)
        % mustAllowFinalize: check conditions for Finalize

            % isActive must be true, otherwise already finalized or not initialized
            mustIsActiveState(obj,true)

            % marks must be not 'prepared', which means nothing was done
            mustBeMark(obj,'prepared',false);
        end

        function initialize(obj,commonDataHandleIn,robotMapsHandleIn)
        % initalize this process
            arguments
                obj(1,1) generateGrid
                commonDataHandleIn(1,1) commonData
                robotMapsHandleIn(1,1) robotMaps
            end

            % check if initialization valid
            obj.mustAllowInitialize();

            obj.mark={'prepared'};
            obj.commonDataHandle=commonDataHandleIn;
            obj.robotMapsHandle=robotMapsHandleIn;
            obj.isActive=true;
        end

        function finalize(obj)
        % finialize this process

            % check if finalization valid
            obj.mustAllowFinalize();

            % set isActive state, lock write functions
            obj.isActive=false;

            % write data into commonData
            obj.commonDataHandle.updateDataGenerateGrid(obj.dataList.positionRow);
        end
    end
    % end of the robotMaps friend methods

    methods (Access=public)
        cartesian2(obj,armLength,sphereRadius) % Functional method: generate cartesian points in x-z plane
        cartesian3(obj,armLength,sphereRadius) % Functional method: generate cartesian points in space
    end
    % end of the public functional method (definition in same folder)
end


function mustBeValidMarks(marksIn)
% Check for Valid input as mark.
    mustBeMember(marksIn,{'cartesian2','cartesian3'})
end
 
function mustBeValidMarksNullPrepare(marksIn)
% Check for Valid input as mark. Allow null and prepare.
    mustBeMember(marksIn,{'null','prepared','cartesian2','cartesian3'})
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{'positionRow'})
end
