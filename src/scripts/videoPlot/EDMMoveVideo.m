function M = EDMMoveVideo(robotMapsClass,direction,rotation,startPos,endPos,step,flip)
    % kuka 1 1 (3208-4392)

    robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;
    solutionFlag = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    
    EDMValue = squeeze(sum(solutionFlag==1,4));
    jetBar = getJetColorbar();

    f = figure();
    frameNum = ceil((endPos - startPos) / step);
    M(frameNum) = struct('cdata',[],'colormap',[]);
    index = 1;

    for iPos = startPos:step:endPos
        if(iPos == startPos)
            positionList = iPos;
        else
            positionList = iPos-step+1:iPos;
        end

        solutionState = solutionFlag(direction,rotation,iPos,1,flip);
        configSol =  squeeze(configSolColumn(:,direction,rotation,iPos,1,flip));
        if (solutionState == 2)
            onState = "off";
        else
            onState = "on";
        end
        ax = show(robotColumn,configSol,"Frames","off","PreservePlot",false,"Visuals",onState);
        ax.NextPlot = 'add';
        scatterIndex(ax,squeeze(EDMValue(direction,rotation,positionList,flip)),positionRow(positionList,:), ...
                   "colorbar",jetBar,'markSize',20);
        xlim(ax,[-0.9,0.9])
        ylim(ax,[-0.9,0.9])
        zlim(ax,[-0.1,1.15])
        clim(ax,[0,24]);
        view(ax,-37.5,30)
        ax.CameraViewAngle = 4.466004766932223;
        M(index) = getframe(f);
        index = index + 1;
    end
end

