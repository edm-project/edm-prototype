function movie = NSUVideoRobot(robotMapsClass,direction,rotation,position,flipList)
%NSUVIDEOROBOT 
    % kuka 1 1 956 : 2
    robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;
    
    flipSize = numel(flipList);
    f = figure();
    M(flipSize*25 + 12*3) = struct('cdata',[],'colormap',[]);

    % 2s/1s 2s/1s 2s/1s
    index = 1;
    for iFlip = 1:flipSize
        flipNum = flipList(iFlip);
        for iNullspace = 1:25
            configSol = squeeze(configSolColumn(:,direction,rotation,position,mod(iNullspace-1,24)+1,flipNum));
            ax = show(robotColumn,configSol);
            xlim(ax,[-0.9,0.9])
            ylim(ax,[-0.9,0.9])
            zlim(ax,[-0.2,1.05])
            ax.CameraViewAngle = 4.256489935709123;
            M(index) = getframe(f);
            index = index + 1; 
        end
        for iNow = 1:12
            M(index) = getframe(f);
            index = index + 1;
        end
    end

    movie = M;
end

