function movie =  NSUVideoGraph(robotMapsClass,direction,rotation,position,flipList)

    robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;
    configSolCollected = squeeze(configSolColumn(:,direction,rotation,position,:,:));
    configSolCollectedNow = configSolCollected([2 4 6],:,:) - 0.2 * configSolCollected([1 3 5],:,:);
    configSolCollectedNow(:,25,:) = configSolCollectedNow(:,1,:);
    positionXYZ = robotMapsClass.commonDataSlot.positionRow(position,:);

    flipSize = numel(flipList);
    f = figure();
    ax = axes(f);
    ax.NextPlot = "add";
    for iFlip = 1:8
        plot3(ax,configSolCollectedNow(1,:,iFlip), ...
                 configSolCollectedNow(2,:,iFlip), ...
                 configSolCollectedNow(3,:,iFlip))
    end
    %legend(ax,{'NSU1','NSU2','NSU3','NSU4','NSU5','NSU6','NSU7','NSU8'})
    xlabel("$\theta_2-0.2\cdot\theta_1$",'Interpreter','latex');
    ylabel("$\theta_4-0.2\cdot\theta_3$",'Interpreter','latex');
    zlabel("$\theta_6-0.2\cdot\theta_5$",'Interpreter','latex');
    limsize = 2.5;
    xlim(ax,[-limsize,limsize]);
    ylim(ax,[-limsize,limsize]);
    zlim(ax,[-limsize,limsize]);
    grid(ax,"on");
    view(ax,59.884714015438192,24.705879467443253)
    title('Configuration space');
    t = text(-3,-2,3,"test");
    t2 = text(-3,-2,2.7,"test");

    % for iJoint = 1:7
    %     plot(ax,1:24,configSolCollected(iJoint,1:24,1));
    % end
    
    M(flipSize*25 + 12*3) = struct('cdata',[],'colormap',[]);

    % 2s/1s 2s/1s 2s/1s
    index = 1;
    scHandle = scatter3(ax,0,0,0,20,'MarkerEdgeColor','black','MarkerFaceColor','red');
    for iFlip = 1:flipSize
        flipNum = flipList(iFlip);
        for iNullspace = 1:25
            scHandle.XData = configSolCollectedNow(1,iNullspace,flipNum);
            scHandle.YData = configSolCollectedNow(2,iNullspace,flipNum);
            scHandle.ZData = configSolCollectedNow(3,iNullspace,flipNum);
            t.String = "position ["+num2str(positionXYZ(1))+","+num2str(positionXYZ(2))+","+num2str(positionXYZ(3))+ ...
             "] direction "+num2str(direction)+" rotation "+num2str(rotation)+...
            " nullspace "+num2str(mod(iNullspace-1,24)+1)+" flip"+num2str(flipNum);
            M(index) = getframe(f);
            t2.String = "joint config:" + mat2str(squeeze(configSolCollected(:,mod(iNullspace-1,24)+1,flipNum)),3);
            index = index + 1; 
        end
        for iNow = 1:12
            M(index) = getframe(f);
            index = index + 1;
        end
    end

    movie = M;
end

