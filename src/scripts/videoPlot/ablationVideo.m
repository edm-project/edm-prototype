function [MGraph,MRobot,MConsider] = ablationVideo(testHandle,map)
    arguments
        testHandle
        map  {mustBeMember(map,{'reach','dexNoFlip','dex'})}
    end
    %% extract data
    robotParameters = testHandle.robotMapsClassHandle.commonDataSlot.robotParameters;
    robotColumn = robotParameters.robotColumn;
    testDexterity = testHandle.robotMapsClassHandle.generateMapSlot.dataList.testDexterity;
    iLocalPositionS = testHandle.dataList.iLocalPositionS;
    iLocalPositionE = testHandle.dataList.iLocalPositionE;
    iLocalRotationS = testHandle.dataList.iLocalRotationS;
    iLocalRotationE = testHandle.dataList.iLocalRotationE;
    iGlobalPositionS = testHandle.dataList.iGlobalPositionS;
    iGlobalPositionE = testHandle.dataList.iGlobalPositionE;
    startTform = testHandle.robotMapsClassHandle.generateMapSlot.dataList.allTform(:,:,iLocalPositionS,iLocalRotationS,iGlobalPositionS);
    endTform = testHandle.robotMapsClassHandle.generateMapSlot.dataList.allTform(:,:,iLocalPositionE,iLocalRotationE,iGlobalPositionE);

    randomConfigSuccess = testHandle.dataList.randomConfigSuccess;
    randomJointWayPoints = testHandle.dataList.randomJointWayPoints;
    dexterityNoFlipConfigSuccess = testHandle.dataList.dexterityNoFlipConfigSuccess;
    dexterityNoFlipJointWayPoints = testHandle.dataList.dexterityNoFlipJointWayPoints;
    dexterityConfigSuccess = testHandle.dataList.dexterityConfigSuccess;
    dexterityJointWayPoints = testHandle.dataList.dexterityJointWayPoints;

    dexterityNoFlipStartList = testHandle.dataList.dexterityNoFlipStartList;
    dexterityStartList = testHandle.dataList.dexterityStartList;

    %% prepare calculation data
    % extract
    randomJointWayPointsIn = randomJointWayPoints(randomConfigSuccess==1);
    dexterityNoFlipJointWayPointsIn = dexterityNoFlipJointWayPoints(dexterityNoFlipConfigSuccess==1);
    dexterityJointWayPointsIn = dexterityJointWayPoints(dexterityConfigSuccess==1);
    
    % calculate
    [pathLengthRan,cartLengthRan] = pathAndCartDistance(robotParameters,randomJointWayPointsIn);
    [pathLengthDexNoFlip,cartLengthDexNoFlip] = pathAndCartDistance(robotParameters,dexterityNoFlipJointWayPointsIn);
    [pathLengthDex,cartLengthDex] = pathAndCartDistance(robotParameters,dexterityJointWayPointsIn);
    randomJointBoundCell = cellCalculateJointBoundDistance(robotColumn,randomJointWayPointsIn,'-inf');
    dexterityNoFlipJointBoundCell = cellCalculateJointBoundDistance(robotColumn,dexterityNoFlipJointWayPointsIn,'-inf');
    dexterityJointBoundCell = cellCalculateJointBoundDistance(robotColumn,dexterityJointWayPointsIn,'-inf');
    randomManipCell = cellCalculateManipulability(robotParameters,randomJointWayPointsIn,'SVD');
    dexterityNoFlipManipCell = cellCalculateManipulability(robotParameters,dexterityNoFlipJointWayPointsIn,'SVD');
    dexterityManipCell = cellCalculateManipulability(robotParameters,dexterityJointWayPointsIn,'SVD');

    %interpolate
    [randomJointBoundX,randomJointBoundMatrix,randomJointBoundAverage] = interpolateCell2MtxAndAvg(randomJointBoundCell);
    [dexterityNoFlipJointBoundX,dexterityNoFlipJointBoundMatrix,dexterityNoFlipJointBoundAverage] = interpolateCell2MtxAndAvg(dexterityNoFlipJointBoundCell);
    [dexterityJointBoundX,dexterityJointBoundMatrix,dexterityJointBoundAverage] = interpolateCell2MtxAndAvg(dexterityJointBoundCell);
    [randomManipX,randomManipMatrix,randomManipAverage] = interpolateCell2MtxAndAvg(randomManipCell);
    [dexterityNoFlipManipX,dexterityNoFlipManipMatrix,dexterityNoFlipManipAverage] = interpolateCell2MtxAndAvg(dexterityNoFlipManipCell);
    [dexterityManipX,dexterityManipMatrix,dexterityManipAverage] = interpolateCell2MtxAndAvg(dexterityManipCell);
    
    % score
    scoreAverageS = outputGoodConfigAverageScore(testDexterity,iLocalPositionS,iLocalRotationS,iGlobalPositionS);
    scoreAverageE = outputGoodConfigAverageScore(testDexterity,iLocalPositionE,iLocalRotationE,iGlobalPositionE);
    scoreFullS = outputGoodConfigScore(testDexterity,iLocalPositionS,iLocalRotationS,iGlobalPositionS);
    scoreFullE = outputGoodConfigScore(testDexterity,iLocalPositionE,iLocalRotationE,iGlobalPositionE);
    scoreSingleS = max(scoreAverageS,[],'all'); 
    scoreSingleE = max(scoreAverageE,[],'all');

    %% prepare plot
    fPlot = figure('Position',[15.4,357,1412,373.6]);
    t = tiledlayout(1,3);
    axPlot(1) = nexttile(t);
    axPlot(3) = nexttile(t);
    axPlot(2) = nexttile(t);
    
    title(axPlot(1),'Jointspace distance and workspace distance comparison')
    xlabel(axPlot(1),'jointspace Distance(rad)');
    ylabel(axPlot(1),'workspace Distance(m)');
    
    title(axPlot(2),'Manipulability Comparison')
    xlabel(axPlot(2),'Relative path');
    ylabel(axPlot(2),'Minimum singular value');
    xticks(axPlot(2),[0 1])
    xticklabels(axPlot(2),{'start','end'})    
    
    title(axPlot(3),'Joint Bound Distance Comparison')
    xlabel(axPlot(3),'Relative path');
    ylabel(axPlot(3),'Minimum distance to joint limit(rad)');
    xticks(axPlot(3),[0 1])
    xticklabels(axPlot(3),{'start','end'})   

    fConsider = figure('Position',[873.8,70.6,560.0,375.2]);
    t = tiledlayout(2,1);
    axConsider(1) = nexttile(t);
    title(axConsider(1),'Start score');
    axConsider(2) = nexttile(t);
    title(axConsider(2),'End score');
    xlabel(axConsider(1),'continuous nullspace');
    ylabel(axConsider(1),'flip region');
    xlabel(axConsider(2),'continuous nullspace');
    ylabel(axConsider(2),'flip region');
    colormap(axConsider(1),getJetColorbar());
    colormap(axConsider(2),getJetColorbar());
    cb = colorbar(axConsider(2));
    cb.Layout.Tile = 'east';
    cb.TickLabels = ["Bad","Good"];    
    
    fRobot = figure('Position',[10.6,85.8,469.6,368.8]);
    axRobot = show(robotColumn,randomConfiguration(robotColumn),'Frames','off','PreservePlot',false);
    axRobot.NextPlot = "add";
    plotTform(startTform,0.2);
    plotTform(endTform,0.2);

    %% constants consideration
    colorRandom = [57 106 177]./255;
    colorDexNoFlip = [204 37 41]./255;
    colorDex = [62 150 81]./255;
    styleRandom = 'o';
    styleDexNoFlip = '^';
    styleDex = 's';
    legendList = {'Manip','Manip+','EDM'};
    framePerPath = 12;
    markerSize = 24;
    lengthRandom = size(randomJointBoundMatrix,1);
    lengthDexNoFlip = size(dexterityNoFlipJointBoundMatrix,1);
    lengthDex = size(dexterityJointBoundMatrix,1);
    switch map
        case 'reach'
            lengthAll = lengthRandom;
        case 'dexNoFlip'
            lengthAll = lengthDexNoFlip;
        case 'dex'
            lengthAll = lengthDex;
    end

    MGraph(lengthAll + ceil(24/framePerPath)) = struct('cdata',[],'colormap',[]);
    MRobot(framePerPath*lengthAll + 24) = struct('cdata',[],'colormap',[]);
    MConsider(lengthAll + ceil(24/framePerPath)) = struct('cdata',[],'colormap',[]);
    indexSlow = 1;
    indexQuick = 1;

    %% plot value
    %%%%%%%%%%%%%%%%%%% common
    axPlot(1).NextPlot = "add";
    axPlot(2).NextPlot = "add";
    axPlot(3).NextPlot = "add";
    scatterStyleRandom = {styleRandom,'MarkerFaceColor',colorRandom,'MarkerEdgeColor',colorRandom};
    scatterStyleDexNoFlip = {styleDexNoFlip,'MarkerFaceColor',colorDexNoFlip,'MarkerEdgeColor',colorDexNoFlip};
    scatterStyleDex = {styleDex,'MarkerFaceColor',colorDex,'MarkerEdgeColor',colorDex};
    lineStyleRandomMain = {'-','Color',colorRandom,'LineWidth',2};
    lineStyleRandomSingle = {':','Color',colorRandom};
    lineStyleDexNoFlipMain = {'-','Color',colorDexNoFlip,'LineWidth',2};
    lineStyleDexNoFlipSingle = {':','Color',colorDexNoFlip};
    lineStyleDexMain = {'-','Color',colorDex,'LineWidth',2};
    lineStyleDexSingle = {':','Color',colorDex};

    %%%%%%%%%%%%%%%%%%% random
    % 3 graphs above legen
    axConsider(1).NextPlot = "replacechildren";
    axConsider(2).NextPlot = "replacechildren";
    scatter(axConsider(1),1,1,markerSize,scoreSingleS,"filled")
    scatter(axConsider(2),1,1,markerSize,scoreSingleE,"filled")
    axConsider(1).NextPlot = "add";
    axConsider(2).NextPlot = "add";
    scatter(axConsider(1),1,1,markerSize+30,'black','LineWidth',1)
    scatter(axConsider(2),1,1,markerSize+30,'black','LineWidth',1)
    
    maxValue = max(scoreSingleS,scoreSingleE);
    clim(axConsider(1),[0,maxValue]);
    clim(axConsider(2),[0,maxValue]);
    xticks(axConsider(1),[]);
    yticks(axConsider(1),[]);
    xticks(axConsider(2),[]);
    yticks(axConsider(2),[]);
    cb.Ticks = [0,maxValue];

    maxManipWayPoint = numel(randomManipX);
    hd2_first1 = plot(axPlot(2),randomManipX,repelem(-1,maxManipWayPoint),lineStyleRandomMain{:});
    maxJointWayPoint = numel(randomJointBoundX);
    hd3_first1 = plot(axPlot(3),randomJointBoundX,repelem(-1,maxJointWayPoint),lineStyleRandomMain{:});
    pathXList = [1,floor(linspace(33,size(randomJointWayPointsIn{1},2),framePerPath-1))];
    for iRandom = 1:lengthRandom
        % 3 graphs above
        hd_tmp1 = scatter(axPlot(1),pathLengthRan(iRandom),cartLengthRan(iRandom),'filled',scatterStyleRandom{:});
        plot(axPlot(2),randomManipX,randomManipMatrix(iRandom,:),lineStyleRandomSingle{:});
        plot(axPlot(3),randomJointBoundX,randomJointBoundMatrix(iRandom,:),lineStyleRandomSingle{:});

        if iRandom == 1
            hd_first1 = hd_tmp1;
        end
        legend(axPlot(1),hd_first1,legendList(1:1));
        legend(axPlot(2),hd2_first1,legendList(1:1));
        legend(axPlot(3),hd3_first1,legendList(1:1));
        xlim(axPlot(1),[2 8]);
        ylim(axPlot(1),[0 5]);
        xlim(axPlot(2),[0 1]);
        ylim(axPlot(2),[0 0.2]);
        xlim(axPlot(3),[0 1]);
        ylim(axPlot(3),[0 1.6]);

        % score graph
        % Non
        if isequal(map,'reach')
            MGraph(indexSlow) = getframe(fPlot);
            MConsider(indexSlow) = getframe(fConsider);
            indexSlow = indexSlow + 1;
        end

        % capture the frame in need
        % robot graph
        if isequal(map,'reach')
        for iRobot = 1:framePerPath
            show(robotColumn,randomJointWayPointsIn{iRandom}(:,pathXList(iRobot)),'Frames','off','PreservePlot',false);
            MRobot(indexQuick) = getframe(fRobot);
            indexQuick = indexQuick + 1;
        end
        end

    end
    plot(axPlot(2),randomManipX,randomManipAverage,lineStyleRandomMain{:});
    plot(axPlot(3),randomJointBoundX,randomJointBoundAverage,lineStyleRandomMain{:});
    legend(axPlot(2),hd2_first1,legendList(1:1));
    legend(axPlot(3),hd3_first1,legendList(1:1));
    if isequal(map,'reach')
        for iTmp = 1:ceil(24/framePerPath)
            MGraph(indexSlow) = getframe(fPlot);
            MConsider(indexSlow) = getframe(fConsider);
            indexSlow = indexSlow + 1;
        end
        for iTmp = 1:24
            MRobot(indexQuick) = getframe(fRobot);
            indexQuick = indexQuick + 1;
        end
        return;
    end

    %%%%%%%%%%%%%%%%%%% dexNoFlip
    axConsider(1).NextPlot = "replacechildren";
    axConsider(2).NextPlot = "replacechildren";
    scatter(axConsider(1),1:24,repelem(1,24,1),markerSize,scoreAverageS,"filled")
    scatter(axConsider(2),1:24,repelem(1,24,1),markerSize,scoreAverageE,"filled")
    axConsider(1).NextPlot = "add";
    axConsider(2).NextPlot = "add";
    startPoint_hd = scatter(axConsider(1),1,1,markerSize+30,'black','LineWidth',1);
    endPoint_hd = scatter(axConsider(2),1,1,markerSize+30,'black','LineWidth',1);

    maxValue = max([scoreAverageS;scoreAverageS],[],'all');
    clim(axConsider(1),[0,maxValue]);
    clim(axConsider(2),[0,maxValue]);
    xticks(axConsider(1),1:24);
    xticks(axConsider(2),1:24);
    yticks(axConsider(1),1);
    yticks(axConsider(2),1);
    cb.Ticks = [0,maxValue];

    maxManipWayPoint = numel(dexterityNoFlipManipX);
    hd2_first2 = plot(axPlot(2),dexterityNoFlipManipX,repelem(-1,maxManipWayPoint),lineStyleDexNoFlipMain{:});
    maxJointWayPoint = numel(randomJointBoundX);
    hd3_first2 = plot(axPlot(3),dexterityNoFlipManipX,repelem(-1,maxJointWayPoint),lineStyleDexNoFlipMain{:});
    pathXList = [1,floor(linspace(33,size(dexterityNoFlipJointWayPointsIn{1},2),framePerPath-1))];
    for iDexNoFlip = 1:lengthDexNoFlip
        % 3 graphs above
        hd_tmp2 = scatter(axPlot(1),pathLengthDexNoFlip(iDexNoFlip),cartLengthDexNoFlip(iDexNoFlip),'filled',scatterStyleDexNoFlip{:});
        plot(axPlot(2),dexterityNoFlipManipX,dexterityNoFlipManipMatrix(iDexNoFlip,:),lineStyleDexNoFlipSingle{:});
        plot(axPlot(3),dexterityNoFlipJointBoundX,dexterityNoFlipJointBoundMatrix(iDexNoFlip,:),lineStyleDexNoFlipSingle{:});

        if iDexNoFlip == 1
            hd_first2 = hd_tmp2;
        end
        legend(axPlot(1),[hd_first1,hd_first2],legendList(1:2));
        legend(axPlot(2),[hd2_first1,hd2_first2],legendList(1:2));
        legend(axPlot(3),[hd3_first1,hd3_first2],legendList(1:2));
        xlim(axPlot(1),[2 8]);
        ylim(axPlot(1),[0 5]);
        xlim(axPlot(2),[0 1]);
        ylim(axPlot(2),[0 0.2]);
        xlim(axPlot(3),[0 1]);
        ylim(axPlot(3),[0 1.6]);

        % score graph
        startPoint_hd.XData = dexterityNoFlipStartList(iDexNoFlip,1);
        endPoint_hd.XData = dexterityNoFlipStartList(iDexNoFlip,2);

        if isequal(map,'dexNoFlip')
        MGraph(indexSlow) = getframe(fPlot);
        MConsider(indexSlow) = getframe(fConsider);
        indexSlow = indexSlow + 1;
        end

        % capture the frame in need
        % robot graph
        if isequal(map,'dexNoFlip')
        for iRobot = 1:framePerPath
            show(robotColumn,dexterityNoFlipJointWayPointsIn{iDexNoFlip}(:,pathXList(iRobot)),'Frames','off','PreservePlot',false);
            MRobot(indexQuick) = getframe(fRobot);
            indexQuick = indexQuick + 1;
        end
        end
    end
    plot(axPlot(2),dexterityNoFlipManipX,dexterityNoFlipManipAverage,lineStyleDexNoFlipMain{:});
    plot(axPlot(3),dexterityNoFlipJointBoundX,dexterityNoFlipJointBoundAverage,lineStyleDexNoFlipMain{:});
    legend(axPlot(2),[hd2_first1,hd2_first2],legendList(1:2));
    legend(axPlot(3),[hd3_first1,hd3_first2],legendList(1:2));
    if isequal(map,'dexNoFlip')
        for iTmp = 1:ceil(24/framePerPath)
            MGraph(indexSlow) = getframe(fPlot);
            MConsider(indexSlow) = getframe(fConsider);
            indexSlow = indexSlow + 1;
        end
        for iTmp = 1:24
            MRobot(indexQuick) = getframe(fRobot);
            indexQuick = indexQuick + 1;
        end
        return;
    end

    %%%%%%%%%%%%%%%%%%% dex
    axConsider(1).NextPlot = "replacechildren";
    axConsider(2).NextPlot = "replacechildren";
    scatter(axConsider(1),repmat(1:24,1,8),repelem(1:8,1,24),markerSize,scoreFullS(:),"filled")
    scatter(axConsider(2),repmat(1:24,1,8),repelem(1:8,1,24),markerSize,scoreFullE(:),"filled")
    axConsider(1).NextPlot = "add";
    axConsider(2).NextPlot = "add";
    startPoint_hd = scatter(axConsider(1),1,1,markerSize+30,'black','LineWidth',1);
    endPoint_hd = scatter(axConsider(2),1,1,markerSize+30,'black','LineWidth',1);

    maxValue = max([scoreFullS;scoreFullE],[],'all');
    clim(axConsider(1),[0,maxValue]);
    clim(axConsider(2),[0,maxValue]);
    xticks(axConsider(1),1:24);
    xticks(axConsider(2),1:24);
    yticks(axConsider(1),1:8);
    yticks(axConsider(2),1:8);
    cb.Ticks = [0,maxValue];

    maxManipWayPoint = numel(dexterityManipX);
    hd2_first3 = plot(axPlot(2),dexterityManipX,repelem(-1,maxManipWayPoint),lineStyleDexMain{:});
    maxJointWayPoint = numel(randomJointBoundX);
    hd3_first3 = plot(axPlot(3),dexterityManipX,repelem(-1,maxJointWayPoint),lineStyleDexMain{:});
    pathXList = [1,floor(linspace(33,size(dexterityJointWayPointsIn{1},2),framePerPath-1))];
    for iDex = 1:lengthDex
        % 3 graphs above
        hd_tmp3 = scatter(axPlot(1),pathLengthDex(iDex),cartLengthDex(iDex),'filled',scatterStyleDex{:});
        plot(axPlot(2),dexterityManipX,dexterityManipMatrix(iDex,:),lineStyleDexSingle{:});
        plot(axPlot(3),dexterityJointBoundX,dexterityJointBoundMatrix(iDex,:),lineStyleDexSingle{:});

        if iDex == 1
            hd_first3 = hd_tmp3;
        end
        legend(axPlot(1),[hd_first1,hd_first2,hd_first3],legendList);
        legend(axPlot(2),[hd2_first1,hd2_first2,hd2_first3],legendList);
        legend(axPlot(3),[hd3_first1,hd3_first2,hd3_first3],legendList);
        xlim(axPlot(1),[2 8]);
        ylim(axPlot(1),[0 5]);
        xlim(axPlot(2),[0 1]);
        ylim(axPlot(2),[0 0.2]);
        xlim(axPlot(3),[0 1]);
        ylim(axPlot(3),[0 1.6]);

        % score graph
        startPoint_hd.XData = dexterityStartList(iDex,1);
        endPoint_hd.XData = dexterityStartList(iDex,2);
        startPoint_hd.YData = dexterityStartList(iDex,3);
        endPoint_hd.YData = dexterityStartList(iDex,3);

        if isequal(map,'dex')
        MGraph(indexSlow) = getframe(fPlot);
        MConsider(indexSlow) = getframe(fConsider);
        indexSlow = indexSlow + 1;
        end

        % capture the frame in need
        % robot graph
        if isequal(map,'dex')
        for iRobot = 1:framePerPath
            show(robotColumn,dexterityJointWayPointsIn{iDex}(:,pathXList(iRobot)),'Frames','off','PreservePlot',false);
            MRobot(indexQuick) = getframe(fRobot);
            indexQuick = indexQuick + 1;
        end
        end
    end
    plot(axPlot(2),dexterityManipX,dexterityManipAverage,lineStyleDexMain{:});
    plot(axPlot(3),dexterityJointBoundX,dexterityJointBoundAverage,lineStyleDexMain{:});
    legend(axPlot(2),[hd2_first1,hd2_first2,hd2_first3],legendList);
    legend(axPlot(3),[hd3_first1,hd3_first2,hd3_first3],legendList);
    if isequal(map,'dex')
        for iTmp = 1:ceil(24/framePerPath)
            MGraph(indexSlow) = getframe(fPlot);
            MConsider(indexSlow) = getframe(fConsider);
            indexSlow = indexSlow + 1;
        end
        for iTmp = 1:24
            MRobot(indexQuick) = getframe(fRobot);
            indexQuick = indexQuick + 1;
        end
        return;
    end

end

function cellOut = cellCalculateManipulability(robotParameters,cellIn,reasonType)
    robotColumn= robotParameters.robotColumn;
    endEffectorName = robotParameters.endEffectorName;
    nCell = length(cellIn);
    cellOut = cell(nCell,1);
    for iCell=1:nCell
        dataIn = cellIn{iCell};
        nData = size(dataIn,2);
        dataOut = zeros(nData,1);
        for iData =1:nData
            jacobian = geometricJacobian(robotColumn,dataIn(:,iData),endEffectorName);
            if isequal(reasonType,'mu')
                dataOut(iData) = sqrt(det(jacobian*jacobian'));
            elseif isequal(reasonType,'SVD')
                [~,S,~] = svd(jacobian);
                minSize = min(size(S,1),size(S,2));
                dataOut(iData) = S(minSize,minSize);
            else
                error('No such type supported!');
            end
        end
        cellOut{iCell}=dataOut;
    end
end

function cellOut = cellCalculateJointBoundDistance(robotColumn,cellIn,normType)
    nCell = length(cellIn);
    cellOut = cell(nCell,1);
    for iCell=1:nCell
        dataIn = cellIn{iCell};
        nData = size(dataIn,2);
        dataOut = zeros(nData,1);
        for iData =1:nData
            if isequal(normType,'-inf')
                dataOut(iData) = jointBoundDistance(robotColumn,dataIn(:,iData));
            elseif isequal(normType,'2')
                dataOut(iData) = jointBoundDistanceNorm2(robotColumn,dataIn(:,iData));
            else
                error('No such type available!');
            end
        end
        cellOut{iCell}=dataOut;
    end
end

function [xValue,yValueMatrix,yValueAverage] = interpolateCell2MtxAndAvg(yValueCell)
    maxLength = 0;
    nCell = length(yValueCell);
    for iCell = 1:nCell
        maxLength=max(maxLength,length(yValueCell{iCell}));
    end
    
    yValueMatrix = zeros(nCell,maxLength);
    for iCell = 1:nCell
        yValueMatrix(iCell,:) = plotInterpolation(yValueCell{iCell},maxLength);
    end

    yValueAverage = mean(yValueMatrix,1);
    xValue = linspace(0,1,maxLength);
end