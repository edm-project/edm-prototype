function M = EDMValueVideo(robotMapsClass,direction,rotation,position,flip)
    % kuka 1 1 181(3366) (3211-4389)
    arguments
        robotMapsClass
        direction
        rotation
        position
        flip
    end

    robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;
    solutionFlag = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag;
    robotName = robotMapsClass.commonDataSlot.robotParameters.robotName;
    nullspaceNum = 24;

    f = figure();
    M(nullspaceNum) = struct('cdata',[],'colormap',[]);
    ax = plotRobotBase(f,robotName);
    xlim(ax,[-0.9,0.9])
    ylim(ax,[-0.9,0.9])
    zlim(ax,[-0.1,1.15])
    view(ax,-37.5,30)
    ax.CameraViewAngle = 4.466004766932223;

    for i = 1:1
        configSol = squeeze(configSolColumn(:,direction,rotation,position,i,flip));
        color = ''; %#ok<NASGU>
        switch solutionFlag(direction,rotation,position,i,flip)
            case 1
                color = 'green';
            otherwise
                color = [0.5,0.5,0.5];
        end
        plotRobotWithColor(ax,robotColumn,robotName,configSol,color);
        
        M(i) = getframe(f);
    end
end

