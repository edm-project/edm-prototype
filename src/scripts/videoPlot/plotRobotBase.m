function ax = plotRobotBase(f,robotName)

    robotColumn = loadrobot(robotName,"DataFormat","column");

    % get the base frame name and mesh name
    switch robotName
        case 'kukaIiwa14'
            removeBody(robotColumn,'iiwa_link_1')
        case 'frankaEmikaPanda'
            removeBody(robotColumn,'panda_link1')
        otherwise
    end

    % copy the object to the new axes
    figure(f);
    ax = show(robotColumn,homeConfiguration(robotColumn),'Frames','off');
end

