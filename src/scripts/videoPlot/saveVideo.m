function saveVideo(movieIn,fileName,frameRateIn)
    v = VideoWriter(fileName+".mp4",'MPEG-4');
    v.FrameRate = frameRateIn;
    open(v)
    for i=1:numel(movieIn)
        writeVideo(v,movieIn(i));
    end
    close(v)
end

