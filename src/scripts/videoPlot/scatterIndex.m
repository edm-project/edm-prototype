function scHandle = scatterIndex(axesHandle,plottedIndex,positionRow,options)
    arguments
        axesHandle(1,1)
        plottedIndex(:,1) double
        positionRow(:,3) double
        options.colorbar(:,3) double = ''
        options.markSize(1,1) double = 15
    end

    if size(plottedIndex,1)~=size(positionRow,1)
        error('The size of two inputs are not fit for plotting! [%d][%d]', ...
               size(plottedIndex,1), ...``
               size(positionRow,1))
    end

    % plot graph on the figure
    positionRowX = positionRow(:,1);
    positionRowY = positionRow(:,2);
    positionRowZ = positionRow(:,3);
    scHandle = scatter3(axesHandle,positionRowX,positionRowY,positionRowZ,options.markSize,plottedIndex,"filled");

    % change the colorbar
    if ~isempty(options.colorbar)
        colormap(axesHandle,options.colorbar)
    end

end

