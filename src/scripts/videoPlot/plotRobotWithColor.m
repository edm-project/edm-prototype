function plotRobotWithColor(ax,robotColumn,robotName,config,color)
    f_tmp = figure('Visible','off');
    ax_tmp = show(robotColumn,config,'Frames','off');
    
    switch robotName
        case 'kukaIiwa14'
            frameName = robotColumn.BodyNames(2:8)+"_mesh";
        case 'frankaEmikaPanda'
            frameName = robotColumn.BodyNames(1:7)+"_mesh";
        otherwise
    end

    for iRBT = 1:numel(frameName)
        bodyHandle = findobj(ax_tmp,'DisplayName',frameName(iRBT));
        bodyHandle.FaceColor = color;
        copyobj(bodyHandle,ax);
    end

    delete(f_tmp);
end

