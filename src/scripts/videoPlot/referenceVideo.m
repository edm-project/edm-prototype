function movie = referenceVideo(robotMapsClass,direction,rotation,position,flip)
    % franka 1 1 780 22 : 
    % kuka 1 1 956 : 2
    arguments
        robotMapsClass
        direction
        rotation
        position
        flip
    end

    robotParameters = robotMapsClass.commonDataSlot.robotParameters;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;
    nullList = [18:24,1:6,6:-1:1,24:-1:18];

    f = figure();
    M(numel(nullList)) = struct('cdata',[],'colormap',[]);

    configSol = squeeze(configSolColumn(:,direction,rotation,position,nullList(1),flip));
    dexTest_plotReferenceVectors(robotParameters,configSol)
    M(1) = getframe(f);
    delete(f);

    for i = 2:numel(nullList)
        f = figure('Visible','off');
        configSol = squeeze(configSolColumn(:,direction,rotation,position,nullList(i),flip));
        dexTest_plotReferenceVectors(robotParameters,configSol)
        M(i) = getframe(f);
        delete(f);
    end

    movie = M;
end

