function movie = nullspaceVideo(robotMapsClass,direction,rotation,position,flip,options)
    % franka 1 1 878 : 2
    % kuka 1 1 956 : 1
    arguments
        robotMapsClass
        direction
        rotation
        position
        flip
        options.mode {mustBeMember(options.mode,{'direct','nullCalc'})} = 'direct'
    end

    robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;
    
    f = figure();
    M(24) = struct('cdata',[],'colormap',[]);

    configSol = squeeze(configSolColumn(:,direction,rotation,position,1,flip));
    ax = show(robotColumn,configSol);
    xlim(ax,[-0.9,0.9])
    ylim(ax,[-0.9,0.9])
    zlim(ax,[-0.2,1.05])
    ax.CameraViewAngle = 4.256489935709123;
    M(1) = getframe(f);

    for i = 2:24
        configSol = squeeze(configSolColumn(:,direction,rotation,position,i,flip));
        ax = show(robotColumn,configSol);
        xlim(ax,[-0.9,0.9])
        ylim(ax,[-0.9,0.9])
        zlim(ax,[-0.2,1.05])
        ax.CameraViewAngle = 4.256489935709123;
        M(i) = getframe(f);
    end

    movie = M;
end

