function movie = flipVideo(robotMapsClass,direction,rotation,position,nullspace)
    % franka 1 1 780 22 : 
    % kuka 1 1 956 1 :
    arguments
        robotMapsClass
        direction
        rotation
        position
        nullspace
    end

    robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;
    sizeFrame = 8;

    f = figure();
    M(sizeFrame) = struct('cdata',[],'colormap',[]);

    configSol = squeeze(configSolColumn(:,direction,rotation,position,nullspace,1));
    ax = show(robotColumn,configSol);
    xlim(ax,[-0.9,0.9])
    ylim(ax,[-0.9,0.9])
    zlim(ax,[-0.2,1.05])
    ax.CameraViewAngle = 4.256489935709123;
    M(1) = getframe(f);

    for i = 2:sizeFrame
        configSol = squeeze(configSolColumn(:,direction,rotation,position,nullspace,i));
        ax = show(robotColumn,configSol);
        xlim(ax,[-0.9,0.9])
        ylim(ax,[-0.9,0.9])
        zlim(ax,[-0.2,1.05])
        ax.CameraViewAngle = 4.256489935709123;
        M(i) = getframe(f);
    end

    movie = M;
end

