function M = fullEDMVideo(robotMapsClass)

    %robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;
    solutionFlag = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag;
    %configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    robotName = robotMapsClass.commonDataSlot.robotParameters.robotName;
    endEffectorTform = robotMapsClass.commonDataSlot.endEffectorTform;

    EDMValue = squeeze(sum(solutionFlag==1,4));
    % prepare figure
    f = figure('Position',[26.6,123.4,1168,551.2]);
    t = tiledlayout(2,4);
    titleMain = title(t,'test');
    axTrans = axes(f,"Position",[0.03,0.45,0.05,0.1]);
    
    % initialize the figure
    for iFlip = 1:8
        ax(iFlip) = nexttile(t);
        scHandle(iFlip) = scatterIndex(ax(iFlip),zeros(size(positionRow,1),1),positionRow, ...
                            colorbar=getJetColorbar(),markSize=8);
        title(ax(iFlip),"Flipped Region "+num2str(iFlip));
        setVisual(ax(iFlip));
    end
    cb = colorbar(ax(1));
    cb.Layout.Tile = 'east';
    cb.Ticks = 0:4:24;

    % prepare animation
    toDirection = 20;
    M(toDirection) = struct('cdata',[],'colormap',[]);
    index = 1;

    % update the value
    for iDirection = 1:toDirection
        titleMain.String = robotName+" EDM: direction " + num2str(iDirection) + " rotation 1";
        for iFlip = 1:8
            scHandle(iFlip).CData = squeeze(EDMValue(iDirection,1,:,iFlip))';
        end
        delete(axTrans);
        axTrans = axes(f,"Position",[0.03,0.45,0.05,0.1]);
        xticks(axTrans,[]);
        yticks(axTrans,[]);
        zticks(axTrans,[]);
        %xlim(axTrans,[-0.025,0.025]);
        %ylim(axTrans,[-0.025,0.025]);
        %zlim(axTrans,[-0.025,0.025]);
        view(axTrans,-37.5,30)
        plotTform(endEffectorTform(:,:,iDirection,1),0.025);
        axis(axTrans,'equal');
        M(index) = getframe(f);
        index = index + 1;
    end
    
end

function setVisual(ax)
    clim(ax,[0,24]);
    alpha(ax,0.6);
    view(ax,-37.5,30)
end
