function reachTest_reachabilityIndex(robotMapsIn)
% This function show the reachability graph of robotMapsIn in reachMap
    arguments
        robotMapsIn(1,1) robotMaps
    end
    
    % export data
    testReachability=robotMapsIn.generateMapSlot.dataList.testReachability;
    positionRow=robotMapsIn.generateMapSlot.dataList.positionRow;
    robotName = robotMapsIn.commonDataSlot.robotParameters.robotName;

    % export points
    positionRowX=positionRow(:,1);
    positionRowY=positionRow(:,2);
    positionRowZ=positionRow(:,3);

    scatter3(positionRowX,positionRowY,positionRowZ,60,testReachability.dataList.reachabilityIndex,'filled')
    title(['Reachability map for ',robotName]);
    view(0,0)
    colorbar;
    x=colormap("jet");
    x=flipud(x);
    matrixHeight=size(x,1);
    x=x(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(x);
    caxis([0, 100]);
    xlabel('X');
    zlabel('Z');
end

