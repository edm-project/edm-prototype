function reachabilityIndex3D(robotMapsIn)
% This function show the reachability graph of robotMapsIn in reachMap
    arguments
        robotMapsIn(1,1) robotMaps
    end
    
    
    
    % export data
    testReachability=robotMapsIn.generateMapSlot.dataList.testReachability;
    positionRow=robotMapsIn.generateMapSlot.dataList.positionRow;
    robotName = robotMapsIn.commonDataSlot.robotParameters.robotName;
    robotColumn = robotMapsIn.commonDataSlot.robotParameters.robotColumn;
    show(robotColumn);
    hold on;

    % export points
    positionRowX=positionRow(:,1);
    positionRowY=positionRow(:,2);
    positionRowZ=positionRow(:,3);
    condition = positionRowX<=0|positionRowY<=0|positionRowZ<=0;

    scatter3(positionRowX(condition),positionRowY(condition),positionRowZ(condition),60,testReachability.dataList.reachabilityIndex(condition),'filled','MarkerEdgeColor','black')
    title(['Reachability map for ',robotName]);
    colorbar;
    x=colormap("jet");
    x=flipud(x);
    matrixHeight=size(x,1);
    x=x(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(x);
    caxis([0, 100]);
    xlabel('X');
    ylabel('Y');
    zlabel('Z');
    view(135,45);
end

