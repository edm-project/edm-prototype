function reachTest_viewReachability(robotMapsIn)
%REACHTEST_VIEWREACHABILITY 此处显示有关此函数的摘要
    arguments
        robotMapsIn(1,1) robotMaps
    end

    % warning for version
    version = robotMapsIn.generateMapSlot.dataList.testReachability.version;
    if version ~= 1
        warning(['This panel script only support version 1 of the testReachability. ' ...
                 'Version mismatch [v:%d] can cause potential error!'],version)
    end

    % export data
    testReachability=robotMapsIn.generateMapSlot.dataList.testReachability;
    robotColumn=robotMapsIn.commonDataSlot.robotParameters.robotColumn;
    stepRobot=robotMapsIn.generateGridSlot.metaList{1}.sphereRadius *2;
    robotName=robotMapsIn.commonDataSlot.robotParameters.robotName;
    positionRow=robotMapsIn.generateMapSlot.dataList.positionRow;
    reachabilityIndex=testReachability.dataList.reachabilityIndex;
    solutionFlag=testReachability.dataList.solutionFlag;
    configSolColumn=testReachability.dataList.configSolColumn;
    nLocalPosition=size(solutionFlag,1);
    nLocalRotation=size(solutionFlag,2);
    endEffectorTform=robotMapsIn.generateMapSlot.dataList.endEffectorTform;

    % some constant
    sizeMark=17;
    scale=0.025;

    % export points
    positionRowX=positionRow(:,1);
    positionRowY=positionRow(:,2);
    positionRowZ=positionRow(:,3);

    %% define the UI layout
    % plot the UI
    fig = uifigure('Position',[28,57,366,759]);
    g = uigridlayout(fig,[4 1]);
    g.RowHeight = {20,'3x','2x',200};
    g.ColumnWidth = {'1x'};

    % Title array
    title = uilabel(g,'Text',['Reachability ',robotName]);
    title.HorizontalAlignment = 'center';
    title.FontSize = 16;
    title.Layout.Row = 1;
    title.Layout.Column = 1;

    % reachability plot
    reachabilityMap = uiaxes(g);
    reachabilityMap.Title.String = 'Reachability Map';
    reachabilityMap.Layout.Row = 2;
    reachabilityMap.Layout.Column = 1;
    reachabilityMap.NextPlot='add';

    % orientation and information plot:
    subGrid = uigridlayout(g,[1 2]);
    subGrid.Layout.Row = 3;
    subGrid.Layout.Column = 1;
    subGrid.RowHeight = {'1x'};
    subGrid.ColumnWidth = {'2x','3x'};

    directionMap = uiaxes(subGrid);
    directionMap.Layout.Row = 1;
    directionMap.Layout.Column = 2;
    directionMap.NextPlot='add';

    infoPlace = uitextarea(subGrid);
    infoPlace.Layout.Row = 1;
    infoPlace.Layout.Column = 1;
    infoPlace.Editable = 'off';

    % panel structure
    subGrid2 = uigridlayout(g,[1 2]);
    subGrid2.Layout.Row = 4;
    subGrid2.Layout.Column = 1;
    subGrid2.RowHeight = {'1x'};
    subGrid2.ColumnWidth = {'2x','3x'};
    panelPosition = uipanel(subGrid2,'Title','Position');
    panelOrientation = uipanel(subGrid2,'Title','Orientation');

    % panelPosition
    subGridPosition = uigridlayout(panelPosition,[4 2]);
    subGridPosition.RowHeight = {22,22,22,22};
    subGridPosition.ColumnWidth = {'fit','1x'};

    % left-right / up-down
    % x label
    xlabel = uilabel(subGridPosition);
    xlabel.HorizontalAlignment = 'right';
    xlabel.Text = 'x:';
    xPos = uispinner(subGridPosition);
    xPos.Step = stepRobot;
    xPos.Value = 0;

    % y label
    ylabel = uilabel(subGridPosition);
    ylabel.HorizontalAlignment = 'right';
    ylabel.Text = 'y:';
    yPos = uispinner(subGridPosition);
    yPos.Step = stepRobot;
    yPos.Value = 0;

    % z label
    zlabel = uilabel(subGridPosition);
    zlabel.HorizontalAlignment = 'right';
    zlabel.Text = 'z:';
    zPos = uispinner(subGridPosition);
    zPos.Step = stepRobot;
    zPos.Value = 0;

    % button
    buttonPos = uibutton(subGridPosition,'Text','Search');
    buttonPos.Layout.Row = 4;
    buttonPos.Layout.Column = [1 2];

    % panelOrientation
    subGridOrientation = uigridlayout(panelOrientation,[4 2]);
    subGridOrientation.RowHeight = {22,22,22,14,14};
    subGridOrientation.ColumnWidth = {'fit','1x'};

    % direction label
    dirlabel = uilabel(subGridOrientation);
    dirlabel.HorizontalAlignment = 'right';
    dirlabel.Text = 'direction:';
    directionSpinner = uispinner(subGridOrientation,'Limits', [1 nLocalPosition],...
                                'LowerLimitInclusive','on',...
                                'UpperLimitInclusive','on');
    directionSpinner.Value = 1;

    % rotation label
    rotlabel = uilabel(subGridOrientation);
    rotlabel.HorizontalAlignment = 'right';
    rotlabel.Text = 'rotation:';
    rotationSpinner = uispinner(subGridOrientation,'Limits', [1 nLocalRotation],...
                                'LowerLimitInclusive','on',...
                                'UpperLimitInclusive','on');
    rotationSpinner.Value = 1;

    % button
    buttonRot = uibutton(subGridOrientation,'Text','Update');
    buttonRot.Layout.Row = 3;
    buttonRot.Layout.Column = [1 2];

    % check boxes
    cbxImmediately = uicheckbox(subGridOrientation,'Text','update immediately');
    cbxImmediately.Layout.Row = 4;
    cbxImmediately.Layout.Column = [1 2];
%     cbxShowReach = uicheckbox(subGridOrientation,'Text','show Reach-Index');
%     cbxShowReach.Layout.Row = 5;
%     cbxShowReach.Layout.Column = [1 2];

    %% plot fixed
    % reachabilityMap
    scatter3(reachabilityMap,positionRowX,positionRowY,positionRowZ,sizeMark,reachabilityIndex,'filled');
    colorbarMark=colormap(reachabilityMap,"jet");
    colorbarMark=flipud(colorbarMark);
    matrixHeight=size(colorbarMark,1);
    colorbarMark=colorbarMark(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(reachabilityMap,colorbarMark);
    caxis(reachabilityMap,[0,100]);
    reachabilityMap.View = [0 0];
    reachabilityMap.XLim = [-1.25 1.25];
    reachabilityMap.ZLim = [-1 1.5];
    
    % handle for the marking point
    hScatter=scatter3(reachabilityMap,xPos.Value,yPos.Value,zPos.Value,sizeMark,'white','filled',MarkerEdgeColor='black');

    % directionMap
    for iquiver=1:nLocalPosition
        tformIn=endEffectorTform(:,:,iquiver,1);
        rotMat=tformIn(1:3,1:3)*scale*0.6;
        transVec=tformIn(1:3,4);
        quiverHandle(iquiver)=quiver3(directionMap, ... 
                                      transVec(1),transVec(2),transVec(3), ...
                                      rotMat(1,3),rotMat(2,3),rotMat(3,3),'off','black'); %#ok<AGROW> %z
    end
    quiverHandleX=quiver3(directionMap,0,0,0,0,0,0);
    quiverHandleY=quiver3(directionMap,0,0,0,0,0,0);
    quiverHandleZ=quiver3(directionMap,0,0,0,0,0,0);
    directionMap.XLim=[-0.05 0.05];
    directionMap.YLim=[-0.05 0.05];
    directionMap.ZLim=[-0.05 0.05];
    directionMap.View=[0 0];

    % searched Index
    GlobalIndex = searchPositionIndex(positionRow,[xPos.Value yPos.Value zPos.Value]);

    updateDirectionPlot;
    updateRobotFigure;

    %% define trigger rule
    buttonPos.ButtonPushedFcn= @buttonPosPushed;
    buttonRot.ButtonPushedFcn= @buttonRotPushed;
    cbxImmediately.ValueChangedFcn= @(cbxImmediately,event)cbxImmediatelyPushed(cbxImmediately,buttonPos,buttonRot);
    directionSpinner.ValueChangedFcn=@spinnerChanged;
    rotationSpinner.ValueChangedFcn=@spinnerChanged;
    xPos.ValueChangedFcn=@spinnerPositionChanged;
    yPos.ValueChangedFcn=@spinnerPositionChanged;
    zPos.ValueChangedFcn=@spinnerPositionChanged;

    function buttonPosPushed(~,~)
        index = searchPositionIndex(positionRow,[xPos.Value yPos.Value zPos.Value]);
        if index==0
            message = {'Position invalid!',['[',num2str(xPos.Value),',',num2str(yPos.Value),',',num2str(zPos.Value),'] not found in Map']};
            uialert(fig, message,'Warning',...
                'Icon','warning');
       else
            GlobalIndex=index;
            updatePosition;
        end
    end 

    function buttonRotPushed(~,~)
        updateDirectionPlot;
        updateRobotFigure;
    end 

    function cbxImmediatelyPushed(cbx,buttonPos,buttonRot)
        if cbx.Value==true
            buttonRot.Enable='off';
            buttonPos.Enable='off';
        else
            buttonRot.Enable='on';
            buttonPos.Enable='off';
        end
    end

    function spinnerChanged(~,~)
        if cbxImmediately.Value==true
            updateDirectionPlot;
            updateRobotFigure;
        end
    end

    function spinnerPositionChanged(~,~)
        if cbxImmediately.Value==true
            buttonPosPushed;
        end
    end

    % other functions
    function updatePosition
        % update the marker in updateFigure
        delete(hScatter);
        hScatter=scatter3(reachabilityMap,xPos.Value,yPos.Value,zPos.Value,sizeMark,'white','filled',MarkerEdgeColor='black');   

        % update the direction plot
        updateDirectionPlot;

        % update the robot figure
        updateRobotFigure;
    end

    function updateDirectionPlot
        % color the SolutionFlag
        for iquiverInner=1:nLocalPosition
            if solutionFlag(iquiverInner,rotationSpinner.Value,GlobalIndex) == 1
                quiverHandle(iquiverInner).Color='cyan';
            elseif solutionFlag(iquiverInner,rotationSpinner.Value,GlobalIndex) == 2
                quiverHandle(iquiverInner).Color='black';
            else
                quiverHandle(iquiverInner).Color=[0.6350 0.0780 0.1840];
            end
        end
        % plot the coordinate

        delete(quiverHandleX) 
        delete(quiverHandleY) 
        delete(quiverHandleZ)
        tformInLocal=endEffectorTform(:,:,directionSpinner.Value,rotationSpinner.Value);
        rotMatLocal=tformInLocal(1:3,1:3)*scale*0.4;
        transVecLocal=tformInLocal(1:3,4);
        quiverHandleX=quiver3(directionMap, ...
                              transVecLocal(1),transVecLocal(2),transVecLocal(3), ...
                              rotMatLocal(1,1),rotMatLocal(2,1),rotMatLocal(3,1),'off','r'); %x
        quiverHandleY=quiver3(directionMap, ...
                              transVecLocal(1),transVecLocal(2),transVecLocal(3), ...
                              rotMatLocal(1,2),rotMatLocal(2,2),rotMatLocal(3,2),'off','g'); %y
        quiverHandleZ=quiver3(directionMap, ...
                              transVecLocal(1),transVecLocal(2),transVecLocal(3), ...
                              rotMatLocal(1,3),rotMatLocal(2,3),rotMatLocal(3,3),'off','b'); %z

        directionMap.XLim=[-0.05 0.05];
        directionMap.YLim=[-0.05 0.05];
        directionMap.ZLim=[-0.05 0.05];
        directionMap.View=[0 0];

    end

    function updateRobotFigure
        if (solutionFlag(directionSpinner.Value,rotationSpinner.Value,GlobalIndex) == 1)
            Visible = 'on';
        else
            Visible = 'off';
        end 
        show(robotColumn,configSolColumn(:,directionSpinner.Value,rotationSpinner.Value,GlobalIndex), ...
            'PreservePlot',false,'FastUpdate',true,'Visuals',Visible,'Frames',Visible);

        % update the information
        infoPlace.Value = {['GloPos: ','[',num2str(xPos.Value),',',num2str(yPos.Value),',',num2str(zPos.Value),']'], ...
                           ['Index:',num2str(GlobalIndex)], ...
                           ['Reach:',num2str(reachabilityIndex(GlobalIndex))],...
                           ['LocPos:',num2str(directionSpinner.Value)],...
                           ['LocRot:',num2str(rotationSpinner.Value)],...
                           ['SolFlag:',num2str(solutionFlag(directionSpinner.Value,rotationSpinner.Value,GlobalIndex))],...
                           ['ConfigSol:',mat2str(configSolColumn(:,directionSpinner.Value,rotationSpinner.Value,GlobalIndex),3)]};
    end

end



