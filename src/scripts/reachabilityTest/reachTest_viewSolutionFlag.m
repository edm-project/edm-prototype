function reachTest_viewSolutionFlag(robotMapsIn,directionIn)
%REACHTEST_VIEWSOLUTIONFLAG 此处显示有关此函数的摘要
    arguments
        robotMapsIn(1,1) robotMaps
        directionIn(1,1) double {mustBeInteger,mustBePositive}
    end
    
    % export data
    testReachability=robotMapsIn.generateMapSlot.dataList.testReachability;
    positionRow=robotMapsIn.generateMapSlot.dataList.positionRow;
    reachabilityIndex=testReachability.dataList.reachabilityIndex;
    solutionFlag=testReachability.dataList.solutionFlag;
    nLocalPosition=size(solutionFlag,1);
    nLocalRotation=size(solutionFlag,2);
    endEffectorTform=robotMapsIn.generateMapSlot.dataList.endEffectorTform;

    % some constant
    sizeMark=17;
    scale=0.025;

    % export points
    positionRowX=positionRow(:,1);
    positionRowY=positionRow(:,2);
    positionRowZ=positionRow(:,3);

    % plot the UI
    fig = uifigure('Position',[15,298,1500,500]);
    g = uigridlayout(fig,[3 6]);
    g.RowHeight = {20,'1x',150};
    g.ColumnWidth = {'1x','1x','1x','1x','1x','1x'};

    %Title array
    title = uilabel(g,'Text',['Reachability Mark with direction ',num2str(directionIn),'/',num2str(nLocalPosition)]);
    title.HorizontalAlignment = 'center';
    title.FontSize = 16;
    title.Layout.Row = 1;
    title.Layout.Column = [1,6];

    % Left-up reachability plot
    reachabilityMap = uiaxes(g);
    reachabilityMap.Title.String = 'Reachability Map';
    reachabilityMap.Layout.Row = 2;
    reachabilityMap.Layout.Column = 1;

    % five Map with different rotations
    for irotationMap=1:5
        % five Map with different rotations
        rotationReachMap(irotationMap) = uiaxes(g); %#ok<*AGROW>  % I cannot avoid this
        rotationReachMap(irotationMap).Title.String = ['Reachability Map Rot:',num2str(irotationMap)];
        rotationReachMap(irotationMap).Layout.Row = 2;
        rotationReachMap(irotationMap).Layout.Column = irotationMap+1;
        rotationReachMap(irotationMap).NextPlot='add';
    end

    % five Map to plot the Tform
    for irotationMap=1:5
        % five Map with different rotations
        subgrid(irotationMap) = uigridlayout(g,[1 3]);
        subgrid(irotationMap).Layout.Row = 3;
        subgrid(irotationMap).Layout.Column = irotationMap+1;
        subgrid(irotationMap).RowHeight = {'1x'};
        subgrid(irotationMap).ColumnWidth = {'1x',150,'1x'};
        rotationTform(irotationMap) = uiaxes(subgrid(irotationMap));
        rotationTform(irotationMap).Layout.Row = 1;
        rotationTform(irotationMap).Layout.Column = 2;
        rotationTform(irotationMap).NextPlot='add';
    end

    % Plot the data
    scatter3(reachabilityMap,positionRowX,positionRowY,positionRowZ,sizeMark,reachabilityIndex,'filled');
    %colorbar(reachabilityMap);
    colorbarMark=colormap(reachabilityMap,"jet");
    colorbarMark=flipud(colorbarMark);
    matrixHeight=size(colorbarMark,1);
    colorbarMark=colorbarMark(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(reachabilityMap,colorbarMark);
    caxis(reachabilityMap,[0, 100]);
    reachabilityMap.View = [0 0];

    % Plot the rotation for different directions
    for irotationMap=1:5
        % five Map with different rotations
        % green=1,red=0,yellow=2,magenta=3;
        GreenSolutionFlag=squeeze(solutionFlag(directionIn,irotationMap,:))==1;
        scatter3(rotationReachMap(irotationMap), ...
                 positionRowX(GreenSolutionFlag),positionRowY(GreenSolutionFlag),positionRowZ(GreenSolutionFlag), ...
                 sizeMark,"green",'filled');
        RedSolutionFlag=squeeze(solutionFlag(directionIn,irotationMap,:))==2;
        scatter3(rotationReachMap(irotationMap), ...
                 positionRowX(RedSolutionFlag),positionRowY(RedSolutionFlag),positionRowZ(RedSolutionFlag), ...
                 sizeMark,"red",'filled');
        YellowSolutionFlag=squeeze(solutionFlag(directionIn,irotationMap,:))==3;
        scatter3(rotationReachMap(irotationMap), ...
                 positionRowX(YellowSolutionFlag),positionRowY(YellowSolutionFlag),positionRowZ(YellowSolutionFlag), ...
                 sizeMark,"yellow",'filled');
        MagnetaSolutionFlag=squeeze(solutionFlag(directionIn,irotationMap,:))==4;
        scatter3(rotationReachMap(irotationMap), ...
                 positionRowX(MagnetaSolutionFlag),positionRowY(MagnetaSolutionFlag),positionRowZ(MagnetaSolutionFlag), ...
                 sizeMark,'magenta','filled');
        rotationReachMap(irotationMap).View = [0 0];
    end

    % plot the tform underneath
    for irotationMap=1:5
        % five Map with different rotations
        tformIn = endEffectorTform(:,:,directionIn,irotationMap);
        % extract rotm and tansvec
        rotMat=tformIn(1:3,1:3)*scale;
        transVec=tformIn(1:3,4);
        % plot the vectors
        quiver3(rotationTform(irotationMap),transVec(1),transVec(2),transVec(3),rotMat(1,1),rotMat(2,1),rotMat(3,1),'off','r') %x
        quiver3(rotationTform(irotationMap),transVec(1),transVec(2),transVec(3),rotMat(1,2),rotMat(2,2),rotMat(3,2),'off','g') %y
        quiver3(rotationTform(irotationMap),transVec(1),transVec(2),transVec(3),rotMat(1,3),rotMat(2,3),rotMat(3,3),'off','b') %z
        rotationTform(irotationMap).XLim = [-0.05 0.05];
        rotationTform(irotationMap).YLim = [-0.05 0.05];
        rotationTform(irotationMap).ZLim = [-0.05 0.05];
        rotationTform(irotationMap).View = [0 0];
    end

end

