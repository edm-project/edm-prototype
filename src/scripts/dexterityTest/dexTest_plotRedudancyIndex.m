function f =  dexTest_plotRedudancyIndex(robotMapsClass)
%DEXTEST_PLOTDEXTERITYINDEX 
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    plottedIndex = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.redudancyIndex;
    plottedIndex(isnan(plottedIndex))=0;
    robotName = robotMapsClass.commonDataSlot.robotParameters.robotName;

    colorbarMark = getJetColorbar();

    f = figure;
    axesHandle = plotIndex(f,plottedIndex,positionRow,colorbar=colorbarMark,markSize=40);
    view(axesHandle,0,0);
    title(['nullspace redundacy index for ',robotName]);
    colorbar();
    caxis([0 100]);
end

