function f = dexTest_plotSectionDistance(robotMapsClass,iLocalPosition,iLocalRotation,flip1,flip2,options)
%DEXTEST_PLOTSECTIONDISTANCE
    arguments
        robotMapsClass
        iLocalPosition
        iLocalRotation
        flip1
        flip2
        options.solvable(1,1) logical = false
    end
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    flipDistanceIndexMap = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.flipDistanceIndexMap;
    if options.solvable==true
        flipDistance = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.flipDistanceSolvable;
    else
        flipDistance = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.flipDistance;
    end
    
    if flip1==flip2
        error('should not be same flip!')
    elseif flip1>flip2
        warning('flip1 should be smaller that flip2')
        tmp = flip2;
        flip1 = flip2;
        flip2 = tmp;
    end
    
    flipIndex = flipDistanceIndexMap([num2str(flip1),',',num2str(flip2)]);
    plottedIndex = squeeze(flipDistance(iLocalPosition,iLocalRotation,:,flipIndex));
    plottedIndexMask = plottedIndex ~=0;
    plottedIndexAltMask = plottedIndex ==0;
    plottedIndexMasked = plottedIndex(plottedIndexMask);
    positionRowMasked = positionRow(plottedIndexMask,:);
    
    f = figure;
    axesHandle = plotIndex(f,plottedIndexMasked,positionRowMasked);
    axesHandle.NextPlot="add";
    view(axesHandle,0,0);
    positionRowX = positionRow(plottedIndexAltMask,1);
    positionRowY = positionRow(plottedIndexAltMask,2);
    positionRowZ = positionRow(plottedIndexAltMask,3);
    hold on;
    scatter3(positionRowX,positionRowY,positionRowZ,15,[0.8 0.8 0.8],"filled");
    colorbar(axesHandle);
    clim(axesHandle,[0,pi]);
end

