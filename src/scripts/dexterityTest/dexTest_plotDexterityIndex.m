function f =  dexTest_plotDexterityIndex(robotMapsClass)
%DEXTEST_PLOTDEXTERITYINDEX 
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    plottedIndex = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.dexterityIndex;
    robotName = robotMapsClass.commonDataSlot.robotParameters.robotName;

    colorbarMark = getJetColorbar();

    f = figure;
    set(f,'renderer','painters');
    axesHandle = plotIndex(f,plottedIndex,positionRow,colorbar=colorbarMark,markSize=40);
    view(axesHandle,0,0);
    title(['Nullspace dexterity index for ',robotName]);
    colorbar();
    caxis([0 85]);
    xlabel('x');
    zlabel('z');
end

