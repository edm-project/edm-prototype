function dexTest_plotReferenceVectors(robotParameters,configSol)
%DEXTEST_PLOTREFERENCEVECTORS
    arguments
        robotParameters(1,1) struct
        configSol(:,1) double
    end

    % get information needed
    robotColumn=robotParameters.robotColumn;
    baseName=robotParameters.baseName;
    shoulderName=robotParameters.shoulderName;
    elbowName=robotParameters.elbowName;
    endEffectorName=robotParameters.endEffectorName;
    
    % position vectors
    p_shoulder = getTransform(robotColumn,configSol,shoulderName,baseName);
    p_shoulder = p_shoulder(1:3,4);
    p_elbow = getTransform(robotColumn,configSol,elbowName,baseName);
    p_elbow = p_elbow(1:3,4);
    p_endEffetor = getTransform(robotColumn,configSol,endEffectorName,baseName);
    p_endEffetor = p_endEffetor(1:3,4);

    % reference vectors
    normReference = dexterityNormalVector(robotParameters,configSol);
    [reference,target,normal] = dexterityReferenceVectors(robotParameters,configSol);

    % plot vectors
    show(robotColumn,configSol);
    hold on
    quiver3(p_shoulder(1),p_shoulder(2),p_shoulder(3),normal(1),normal(2),normal(3),'off','r',LineWidth=2) %normal
    quiver3(p_elbow(1),p_elbow(2),p_elbow(3),reference(1)/3,reference(2)/3,reference(3)/3,'off','b',LineWidth=2) %reference
    quiver3(p_elbow(1),p_elbow(2),p_elbow(3),target(1)/3,target(2)/3,target(3)/3,'off','g',LineWidth=2) %reference

    referencePlane=[p_shoulder,p_shoulder+normReference,p_endEffetor];
    fill3(referencePlane(1,:),referencePlane(2,:),referencePlane(3,:),'blue',FaceAlpha=0.3,EdgeAlpha=0);

    movingPlane=[p_shoulder,p_elbow,p_endEffetor];
    fill3(movingPlane(1,:),movingPlane(2,:),movingPlane(3,:),'green',FaceAlpha=0.3,EdgeAlpha=0);
    view(1.420618636221858e+02,49.92855689420896);
    ax = gca;
    ax.CameraPosition = [10.192598519930169,12.955240288999683,20.07336093205176];
    ax.CameraTarget = [-0.090097055961483,-0.23518000526047,0.19173664844988];
    ax.CameraViewAngle = 2.262820036583153;
    dim = [.1 .6 .3 .3];
    str = "Swivel Angle (rad) = " + num2str(angleDifference(reference,target,normal));
    annotation('textbox',dim,'String',str,'FitBoxToText','on');
end

