function [positionRowOut,iGlobalList] = dexTest_detectPossiblePoints(robotMapsClass,iLocalPosition,iLocalRotation,iFlipSolution)
    % constants
    sizeMark = 15;
    timeStep = 0.3;
    increaseStep = 50;
    maxStep = 200;
    
    % extract data
    solutionFlag = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;
    robotParameters = robotMapsClass.commonDataSlot.robotParameters;
    testDexterity = robotMapsClass.generateMapSlot.dataList.testDexterity;
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    allTform  = robotMapsClass.generateMapSlot.dataList.allTform;
    nLocalPosition = size(allTform,3);
    nGlobalPosition = size(allTform,5);
    positionRowX = positionRow(:,1);
    positionRowY = positionRow(:,2);
    positionRowZ = positionRow(:,3);
    nSection = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.nSection;
    robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;
    robotName = robotParameters.robotName;
    nAngle = length(homeConfiguration(robotColumn));
    nullspaceFixedHandle = MEXnullspaceFixedGenerate(robotParameters);
    nullspaceFixedReverseHandle = MEXnullspaceFixedReverseGenerate(robotParameters);
    sectionHandle = MEXdexteritySectionGenerate(robotParameters);

    % collect data (graph1)
    solutionFlagNow = squeeze(solutionFlag(iLocalPosition,iLocalRotation,:,:,iFlipSolution));
    solutionFlagSlice = zeros(nGlobalPosition,1);
    solutionFlagSlice(any(solutionFlagNow==2,2)) = 2;
    solutionFlagSlice(any(solutionFlagNow==3,2)) = 3;
    solutionFlagSlice(any(solutionFlagNow==4,2)) = 4;
    solutionFlagSlice(any(solutionFlagNow==1,2)) = 1;
    configSolColumnSlice = squeeze(configSolColumn(:,iLocalPosition,iLocalRotation,:,:,iFlipSolution));
    
    
    % calculate index (graph2)
    withSolutionMask = solutionFlagSlice~=2;
    jointDistanceMtx = zeros(nLocalPosition,1);
    configSolColumnStart = zeros(nAngle,nLocalPosition);
    withSolutionindex = find(solutionFlagSlice~=2);
    for iWithSolution = 1:length(withSolutionindex)
        sectionIndex = selectIndex(testDexterity,iLocalPosition,iLocalRotation,withSolutionindex(iWithSolution),iFlipSolution);
        configSolColumnStart(:,withSolutionindex(iWithSolution)) = configSolColumnSlice(:,withSolutionindex(iWithSolution),sectionIndex);
        jointDistanceMtx(withSolutionindex(iWithSolution)) = jointBoundDistance(robotColumn,configSolColumnStart(:,withSolutionindex(iWithSolution))); 
    end

    % calculate index (graph3)
    jointDistanceFullMtx = zeros(nLocalPosition,nSection);
    for iWithSolution = 1:length(withSolutionindex)
        [configSolCol,haveSolutionCol] = nullspaceRoundMEX(robotParameters,nullspaceFixedHandle,nullspaceFixedReverseHandle,sectionHandle, ...
            configSolColumnStart(:,withSolutionindex(iWithSolution)),nSection, ...
            timeStep,...
            increaseStep, ...
            maxStep);

        for iSection = 1:nSection
            if haveSolutionCol(iSection) == 0
                jointDistanceFullMtx(withSolutionindex(iWithSolution),iSection) = 0;
            else
                % check the collision and joint Limit
                jointDistanceFullMtx(withSolutionindex(iWithSolution),iSection) = jointBoundDistance(robotColumn,configSolCol(:,iSection));
            end
        end
    end
    jointDistanceMaxMtx = max(jointDistanceFullMtx,[],2);

    % output
    positionRowOut = positionRow(withSolutionMask,:);
    iGlobalList = withSolutionindex;

    % plotting
    f = figure('Position',[80,260,1400,380]);
    f.Name = ['Dexterity and joint hint ',robotName,': (',num2str(iLocalPosition),',',num2str(iLocalRotation),',:,:,',num2str(iFlipSolution),')'];
    f.NumberTitle = "off";
    tiledlayout(f,1,3)
    
    nexttile
    hold on
    view(0,0)
    positionRowFiltered=positionRow(solutionFlagSlice==1,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'green','filled');
    positionRowFiltered=positionRow(solutionFlagSlice==2,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'red','filled');
    positionRowFiltered=positionRow(solutionFlagSlice==3,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'yellow','filled');
    positionRowFiltered=positionRow(solutionFlagSlice==4,:);
    scatter3(positionRowFiltered(:,1),positionRowFiltered(:,2),positionRowFiltered(:,3),sizeMark,'magenta','filled');
    hold off
    title('solution Flag distribution')

    nexttile
    scatter3(positionRowX(withSolutionMask),positionRowY(withSolutionMask),positionRowZ(withSolutionMask),sizeMark,jointDistanceMtx(withSolutionMask),'filled');
    view(0,0)
    title('current joint bound distance')

    nexttile
    scatter3(positionRowX(withSolutionMask),positionRowY(withSolutionMask),positionRowZ(withSolutionMask),sizeMark,jointDistanceMaxMtx(withSolutionMask),'filled');
    view(0,0)
    title('max joint bound distance')
end

function sectionIndex = selectIndex(testDexterity,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iFlipSolution)
    % get the data needed
    robotParameters=testDexterity.generateMapHandle.commonDataHandle.robotParameters;
    solutionFlag = testDexterity.dataList.solutionFlag;
    configSolColumn = testDexterity.dataList.configSolColumn;
    adjacentMapFace = testDexterity.dataList.adjacentMap;
    solutionFlagSection = squeeze(solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,:,iFlipSolution));

    % search for "the correct data point"
        if sum(solutionFlagSection~=0,"all") == 1
            sectionIndex = solutionFlagSection~=0;
        elseif sum(solutionFlagSection==1,"all") >= 1
            sectionIndex = solutionFlagSection==1;
        elseif sum(solutionFlagSection==3,"all") >= 1
            sectionIndex = solutionFlagSection==3;
        elseif sum(solutionFlagSection==4,"all") >= 1
            sectionIndex = solutionFlagSection==4;
        else
            error('Global position [%d] has no data point. This is not correct.',iGlobalPosition);
        end

        % if the data above has two or more data point, check if they
        % belongs to the same nullspace motion
        if sum(sectionIndex,"all") > 1
            sectionIndex = find(sectionIndex);
            chooseIndex = 1;
            for iSection = 1:length(sectionIndex)-1
                if ~isNullspaceReachable(robotParameters, ...
                                        configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex(iSection),iFlipSolution), ...
                                        configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex(iSection+1),iFlipSolution))
                    warning('Global position [%d] has two data points [%d][%d] that cannot reach each other with program. Please check and decide.', ...
                        iGlobalPosition,sectionIndex(iSection),sectionIndex(iSection+1));
                    
                    [oneIndex,sectionNumber] = nearestOneIndex(solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution),iGlobalPosition,adjacentMapFace);
                    normDifference = zeros(length(length(sectionIndex)),1);
                    for iNormDifference = 1:length(sectionIndex)
                        normDifference(iNormDifference) = norm( ...
                         configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex(iNormDifference),iFlipSolution)- ...
                         configSolColumn(:,iLocalPositionIn,iLocalRotationIn,oneIndex,sectionNumber,iFlipSolution));
                    end
                    
                    [~,chooseIndex] = min(normDifference);
                    disp(['Automatic choose section ',num2str(sectionIndex(chooseIndex))]);
                end
            end
            sectionIndex = sectionIndex(chooseIndex);
        else
            sectionIndex = find(sectionIndex);
        end
end

function [oneIndex,sectionNumber] = nearestOneIndex(solutionFlagParted,indexNow,adjacentMapFace)
    queue = indexNow;
    queueIndex = 1;

    while queueIndex<=length(queue)
        % if the current solutionFlag is have one and only one = 1 point, then we use it as the index
        currentSolutionFlagSlice = squeeze(solutionFlagParted(1,1,queue(queueIndex),:,1));
        if any(currentSolutionFlagSlice==1) && length(find(currentSolutionFlagSlice==1))==1
            oneIndex = queue(queueIndex);
            sectionNumber = find(currentSolutionFlagSlice==1);
            break;
        end
        
        % if not, then we add it's adjacent point into it to see.
        adjacentArray = adjacentMapFace(queue(queueIndex));
        for iAdjacent=1:length(adjacentArray)
            if ~any(queue==adjacentArray(iAdjacent))
                queue = [queue,adjacentArray(iAdjacent)]; %#ok<AGROW> % because this is a queue, we cannot avoid reallocaltion
            end
        end

        queueIndex = queueIndex+1;
    end
end