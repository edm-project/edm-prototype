function dexTest_plotCalcDexterityIndex(robotMapsClass)
% constants
    markSize = 15;
    
    % extract data
    solutionFlag = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag;
    solutionFlagReachability = robotMapsClass.generateMapSlot.dataList.testReachability.dataList.solutionFlag;
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    allTform  = robotMapsClass.generateMapSlot.dataList.allTform;
    nLocalPosition = size(allTform,3);
    positionRowX = positionRow(:,1);
    positionRowY = positionRow(:,2);
    positionRowZ = positionRow(:,3);
    nSection = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.nSection;
%     flipCounter = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.flipCounter;

    % summerize the map for dexterity (only for reachable=1)
    rotationSumUp = any(solutionFlag==1,2);
    nSectionSumUp = sum(rotationSumUp,4);
    directionSumUp = sum(nSectionSumUp,1);
    flippingSumUp = max(directionSumUp,[],5);
    dexterityValue = squeeze(flippingSumUp);

    % summerize the map for reachability
    rotationSumUpReach = any(solutionFlagReachability==1,2);
    directionSumUpReach = sum(rotationSumUpReach,1);
    reachabilityValue = squeeze(directionSumUpReach);

    % index calculation
    reachabilityIndex = reachabilityValue/(nLocalPosition) *100;
    dexterityIndex = dexterityValue/(nLocalPosition*nSection) *100;
    redudancyIndex = dexterityValue ./(reachabilityValue*nSection) *100;

    % plot the graph
    f = figure('Position',[80,260,1400,380]);
    tiledlayout(f,1,3)

    nexttile
    scatter3(positionRowX,positionRowY,positionRowZ,markSize,reachabilityIndex,"filled");
    view(0,0);
    colorbarMark=colormap("jet");
    colorbarMark=flipud(colorbarMark);
    matrixHeight=size(colorbarMark,1);
    colorbarMark=colorbarMark(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(colorbarMark);
    caxis([0,100]);
    title('Reachability Index')

    nexttile
    scatter3(positionRowX,positionRowY,positionRowZ,markSize,dexterityIndex,"filled");
    view(0,0);
    colormap(colorbarMark);
    caxis([0,100]);
    title('Dexterity Index')

    nexttile
    scatter3(positionRowX,positionRowY,positionRowZ,markSize,redudancyIndex,"filled");
    view(0,0);
    colormap(colorbarMark);
    caxis([0,100]);
    title('Redudancy Index')

    cb = colorbar;
    cb.Ticks=0:10:100;
    cb.Layout.Tile = 'east';
end

