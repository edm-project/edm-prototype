function f = dexTest_plotTotalJointBound(robotMapsClass,options)
%DEXTEST_PLOTTOTALJOINTBOUND 
    arguments
        robotMapsClass
        options.solvable(1,1) logical = false
    end
    allTform  = robotMapsClass.generateMapSlot.dataList.allTform;
    nLocalPosition=size(allTform,3);
    nLocalRotation=size(allTform,4);

    positionRow = robotMapsClass.commonDataSlot.positionRow;
    if options.solvable==true
        distanceMtx = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.jointBoundSolvableDistanceMtx;
    else
        distanceMtx = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.jointBoundDistanceMtx;
    end
    
    maxflipIndex = max(distanceMtx,[],4);
    averageRotationIndex = sum(maxflipIndex,2) / nLocalRotation;
    averageLocaltionIndex = sum(averageRotationIndex,1) / nLocalPosition;

    plottedIndex = squeeze(averageLocaltionIndex);

    f = figure;
    axesHandle = plotIndex(f,plottedIndex,positionRow);
    view(axesHandle,0,0);
end

