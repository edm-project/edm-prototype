function dexTest_plotSlicedFlipSolution(robotMapsClass,iLocalPosition,iLocalRotation)
    % constants
    markSize = 15;
    
    % extract data
    testDexterity = robotMapsClass.generateMapSlot.dataList.testDexterity;
    solutionFlagSliced = squeeze(testDexterity.dataList.solutionFlag(iLocalPosition,iLocalRotation,:,:,:));
    testFlippedDexterity = robotMapsClass.generateMapSlot.dataList.testFlippedDexterity;
    solutionFlagSlicedFlipped = squeeze(testFlippedDexterity.dataList.solutionFlag(iLocalPosition,iLocalRotation,:,:,:)); 
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    positionRowX = positionRow(:,1);
    positionRowY = positionRow(:,2);
    positionRowZ = positionRow(:,3);
    robotName = robotMapsClass.commonDataSlot.robotParameters.robotName;
    flipCounter = testDexterity.dataList.flipCounter;

    % calculation
    haveSolutionMask = solutionFlagSliced == 1 | solutionFlagSliced == 3 | solutionFlagSliced == 4;
    existDataInSection = any(haveSolutionMask,2);
    existDataLength = squeeze(sum(existDataInSection,3));
    haveSolutionMaskFlipped = solutionFlagSlicedFlipped == 1 | solutionFlagSlicedFlipped == 3 | solutionFlagSlicedFlipped == 4;
    existDataInSectionFlipped = any(haveSolutionMaskFlipped,2);
    existDataLengthFlipped = squeeze(sum(existDataInSectionFlipped,3));

    % plotting
    f = figure('Position',[205,304,804,357]);
    f.Name = ['Flipped Dexterity ',robotName,': (',num2str(iLocalPosition),',',num2str(iLocalRotation),',:,:,:)'];
    f.NumberTitle = "off";
    tiledlayout(f,1,2)

    nexttile
    scatter3(positionRowX,positionRowY,positionRowZ,markSize,existDataLength,"filled");
    view(0,0);
    colorbarMark=colormap("jet");
    colorbarMark=flipud(colorbarMark);
    matrixHeight=size(colorbarMark,1);
    colorbarMark=colorbarMark(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(colorbarMark);
    caxis([0,flipCounter]);
    colorbar;
    title('dexterity map result');

    nexttile
    scatter3(positionRowX,positionRowY,positionRowZ,markSize,existDataLengthFlipped,"filled");
    view(0,0);
    colormap(colorbarMark);
    caxis([0,flipCounter]);
    colorbar;
    title('flipped dexterity map result');

end

