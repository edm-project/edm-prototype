function distanceAvgTable = dexTest_calcSectionDistanceTable(robotMapsClass,options)
%DEXTEST_CALCSECTIONDISTANCTABLE 
    arguments
        robotMapsClass
        options.solvable(1,1) logical = false
    end

    % read in the data
    flipDistanceIndexMap = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.flipDistanceIndexMap;
    if options.solvable==true
        flipDistance = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.flipDistanceSolvable;
    else
        flipDistance = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.flipDistance;
    end
    flipDistance(flipDistance==inf) = 0;
    flipCounter = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.flipCounter-1;

    % calculate the data
    distanceMatrix = zeros(flipCounter,flipCounter);
    for iFlipStart = 1:flipCounter
        for iFlipEnd = iFlipStart+1:flipCounter
            if iFlipStart==iFlipEnd
                continue;
            end
            flipIndex = flipDistanceIndexMap([num2str(iFlipStart),',',num2str(iFlipEnd)]);
            distanceMtx = flipDistance(:,:,:,flipIndex);
            
            distanceRotationAvg = sum(distanceMtx,2) ./ sum(distanceMtx~=0,2);
            distanceRotationAvg(isnan(distanceRotationAvg)) = 0;
            
            distanceDirectionAvg = sum(distanceRotationAvg,1) ./ sum(distanceRotationAvg~=0,1);
            distanceDirectionAvg(isnan(distanceDirectionAvg)) = 0;
            
            distanceDirectionAndRotationAvg = squeeze(distanceDirectionAvg);
            withSolutionMask = distanceDirectionAndRotationAvg~=0;
            
            flipAvg = sum(distanceDirectionAndRotationAvg(withSolutionMask),'all') / sum(withSolutionMask);
            distanceMatrix(iFlipStart,iFlipEnd) = flipAvg;
        end
    end

    distanceAvgTable = distanceMatrix;
end

