function f =  dexTest_plotSlicedDexterityIndex(robotMapsClass,iLocalPosition)
%DEXTEST_PLOTSLICEDDEXTERITYINDEX 
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    dexteritySlicedIndex = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.dexteritySlicedIndex;
    plottedIndex = squeeze(dexteritySlicedIndex(iLocalPosition,:));

    colorbarMark = getJetColorbar();

    f = figure;
    axesHandle = plotIndex(f,plottedIndex,positionRow,colorbar=colorbarMark);
    view(axesHandle,0,0);
end

