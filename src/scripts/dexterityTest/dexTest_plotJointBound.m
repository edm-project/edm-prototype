function f = dexTest_plotJointBound(robotMapsClass,iLocalPosition,iLocalRotation,iFlipSolution,options)
%DEXTEST_PLOTJOINTBOUND 
    arguments
        robotMapsClass
        iLocalPosition
        iLocalRotation
        iFlipSolution
        options.solvable(1,1) logical = false
    end
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    if options.solvable==true
        distanceMtx = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.jointBoundSolvableDistanceMtx;
    else
        distanceMtx = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.jointBoundDistanceMtx;
    end
    
    
    plottedIndex = squeeze(distanceMtx(iLocalPosition,iLocalRotation,:,iFlipSolution));

    f = figure;
    axesHandle = plotIndex(f,plottedIndex,positionRow);
    view(axesHandle,0,0);
end

