function dexTest_plotSignDistribution(robotMapsClass,iDirection,iRotation,angleX,angleY)
%DEXTEST_PLOTSLICEDDEXTERITYINDEX 
    solutionFlag = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;


    f = figure;
%     set(f,'renderer','painters');
    hold on;
    % change rederer to painer


    solutionFlag =  squeeze(solutionFlag(iDirection,iRotation,:,:,1));
    configSolColumn1 = squeeze(configSolColumn(:,iDirection,iRotation,:,:,1));
    configSolColumn2 = squeeze(configSolColumn(:,iDirection,iRotation,:,:,2));
    configSolColumn3 = squeeze(configSolColumn(:,iDirection,iRotation,:,:,3));
    configSolColumn4 = squeeze(configSolColumn(:,iDirection,iRotation,:,:,4));
    color1 = [0 0.4470 0.7410];
    color2 = [0.8500 0.3250 0.0980];
    color3 = [0.9290 0.6940 0.1250];
    color4 = [0.4940 0.1840 0.5560];
    marksize = 5;

    for iSection = 1:24
        X = squeeze(configSolColumn1(angleX,:,iSection));
        Y = squeeze(configSolColumn1(angleY,:,iSection));
        mask = squeeze(solutionFlag(:,iSection)~=2);
        scatter(X(mask),Y(mask),marksize,".",'MarkerEdgeColor',color1);
    end


    for iSection = 1:24
        X = squeeze(configSolColumn2(angleX,:,iSection));
        Y = squeeze(configSolColumn2(angleY,:,iSection));
        mask = squeeze(solutionFlag(:,iSection)~=2);
        scatter(X(mask),Y(mask),marksize,".",'MarkerEdgeColor',color2);
    end

    for iSection = 1:24
        X = squeeze(configSolColumn3(angleX,:,iSection));
        Y = squeeze(configSolColumn3(angleY,:,iSection));
        mask = squeeze(solutionFlag(:,iSection)~=2);
        scatter(X(mask),Y(mask),marksize,".",'MarkerEdgeColor',color3);
    end

    for iSection = 1:24
        X = squeeze(configSolColumn4(angleX,:,iSection));
        Y = squeeze(configSolColumn4(angleY,:,iSection));
        mask = squeeze(solutionFlag(:,iSection)~=2);
        scatter(X(mask),Y(mask),marksize,".",'MarkerEdgeColor',color4);
    end

    xlabel([num2str(angleX),'th joint']);
    ylabel([num2str(angleY),'th joint']);
    xlim([-pi,pi]);
    ylim([-1/2*pi,3/2*pi]);
%     ylim([-pi,pi]);
end


