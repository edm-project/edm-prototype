function [positionList,recordList,configList] = dexTest_findSpecialSecnario2(robotMapsClass,iLocalPosition,iLocalRotation,iFlip1,iFlip2)
%DEXTEST_FINDERRORONEPOINTS 
    % extract data
%     robotParameters = robotMapsClass.commonDataSlot.robotParameters;
    testDexterity = robotMapsClass.generateMapSlot.dataList.testDexterity;
    solutionFlag = testDexterity.dataList.solutionFlag;
    configSolColumn = testDexterity.dataList.configSolColumn;
    allTform  = robotMapsClass.generateMapSlot.dataList.allTform;
    nGlobalPosition = size(allTform,5);
    nSection = size(solutionFlag,4);

    solutionFlagSlice = squeeze(solutionFlag(iLocalPosition,iLocalRotation,:,:,:));
    configSolColumnSlice = squeeze(configSolColumn(:,iLocalPosition,iLocalRotation,:,:,:));
    potentialList = zeros(nGlobalPosition,1,'logical');
    recordList = zeros(nGlobalPosition,1);
    configList = struct();
    for iGlobalPosition = 1:nGlobalPosition
        closeCounter = 0;
        distance = zeros(nSection,1);
        for iSection=1:nSection
            if solutionFlagSlice(iGlobalPosition,iSection,iFlip1)~=2 && solutionFlagSlice(iGlobalPosition,iSection,iFlip2)~=2
                dist = jointJointDistance(configSolColumnSlice(:,iGlobalPosition,iSection,iFlip1),configSolColumnSlice(:,iGlobalPosition,iSection,iFlip2));
                distance(iSection) = dist;
                if dist<=deg2rad(45)
                    closeCounter=closeCounter+1;
                end
            end
        end
        if closeCounter>=3 && closeCounter<=20
            potentialList(iGlobalPosition) = true;
            recordList(iGlobalPosition) = closeCounter;
            configList(iGlobalPosition).before = configSolColumnSlice(:,iGlobalPosition,:,iFlip1);
            configList(iGlobalPosition).after = configSolColumnSlice(:,iGlobalPosition,:,iFlip2);
            configList(iGlobalPosition).distance = distance;
        end
    end
    positionList = find(potentialList);
    recordList = recordList(positionList);
end
