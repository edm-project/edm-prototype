function f = dexTest_plotThesis(robotMapsClass,iLocalPosition,iLocalRotation,maxFlip)
%DEXTEST_PLOTSLICERESULT
    % constants
    markSize = 15;
    
    % extract data
    solutionFlagSliced = squeeze(robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag(iLocalPosition,iLocalRotation,:,:,:));
    
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    positionRowX = positionRow(:,1);
    positionRowY = positionRow(:,2);
    positionRowZ = positionRow(:,3);
    nSection = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.nSection;

    % plotting
    f = figure();
    set(f,'renderer','painters');
   
    colorbarMark = getJetColorbar();
    if maxFlip == 4
        f.Position = [210,250,1075,250];
        tiledlayout(1,4)
    elseif maxFlip == 8
        f.Position = [210,250,1075,500];
        tiledlayout(2,4)
    end

    greenSum = squeeze(sum(solutionFlagSliced==1,2));

    for iFlip=1:maxFlip
        nexttile
        scatter3(positionRowX,positionRowY,positionRowZ,markSize,greenSum(:,iFlip),"filled");
        view(0,0);
        colormap(colorbarMark);
        caxis([0,nSection]);
        title(['Flip Region Index ',num2str(iFlip)])
    end
    cb = colorbar;
    cb.Layout.Tile = 'east';
    cb.Ticks=0:4:24;
end

