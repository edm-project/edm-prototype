function [positionList,distList,configList] = dexTest_findSpecialSecnario1(robotMapsClass,iLocalPosition,iLocalRotation,startSection,iFlip)
%DEXTEST_FINDERRORONEPOINTS 
    % extract data
    robotParameters = robotMapsClass.commonDataSlot.robotParameters;
    testDexterity = robotMapsClass.generateMapSlot.dataList.testDexterity;
    solutionFlag = testDexterity.dataList.solutionFlag;
    configSolColumn = testDexterity.dataList.configSolColumn;
    allTform  = robotMapsClass.generateMapSlot.dataList.allTform;
    nGlobalPosition = size(allTform,5);

    solutionFlagSlice = squeeze(solutionFlag(iLocalPosition,iLocalRotation,:,:,iFlip));
    configSolColumnSlice = squeeze(configSolColumn(:,iLocalPosition,iLocalRotation,:,:,iFlip));
    potentialList = zeros(nGlobalPosition,1,'logical');
    distRecordList = zeros(nGlobalPosition,1);
    for iGlobalPosition = 1:nGlobalPosition
        configSol = squeeze(configSolColumnSlice(:,iGlobalPosition,startSection));
        if solutionFlagSlice(iGlobalPosition,startSection)~=2
            nullspaceFixedHandle = MEXnullspaceFixedGenerate(robotParameters);
            [configSolCol,~,sectionCol] = nullspaceFixedHandle(configSol,0.3,24,200);
            sectionNumber = find(sectionCol == startSection);
            sectionNumber = sectionNumber(sectionNumber>=10);
            if isempty(sectionNumber)
                continue;
            end
            nextConfig = configSolCol(:,sectionNumber(1));
            jointDistance = jointJointDistance(configSol,nextConfig);

            if jointDistance>=deg2rad(90)
                potentialList(iGlobalPosition) = true;
                distRecordList(iGlobalPosition) = jointDistance;
                configList(iGlobalPosition).before = configSol;
                configList(iGlobalPosition).after = nextConfig;
            end
        end
    end
    positionList = find(potentialList);
    distList = distRecordList(potentialList);
    [distList,I] = sort(distList,'descend');
    positionList = positionList(I);
end

