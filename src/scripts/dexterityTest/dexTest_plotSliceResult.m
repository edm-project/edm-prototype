function f = dexTest_plotSliceResult(robotMapsClass,iLocalPosition,iLocalRotation,iFlipSolution)
%DEXTEST_PLOTSLICERESULT
    % constants
    markSize = 15;
    
    % extract data
    solutionFlagSliced = squeeze(robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag(iLocalPosition,iLocalRotation,:,:,iFlipSolution));
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    positionRowX = positionRow(:,1);
    positionRowY = positionRow(:,2);
    positionRowZ = positionRow(:,3);
    robotName = robotMapsClass.commonDataSlot.robotParameters.robotName;
    nSection = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.nSection;

    % plotting
    f = figure("Position",[210,50,575,700]);
    f.Name = ['Dexterity ',robotName,': (',num2str(iLocalPosition),',',num2str(iLocalRotation),',:,:,',num2str(iFlipSolution),')'];
    f.NumberTitle = "off";

    tiledlayout(3,2)
%     subplot(2,2,1)
    nexttile
    greenSum = sum(solutionFlagSliced==1,2);
    scatter3(positionRowX,positionRowY,positionRowZ,markSize,greenSum,"filled");
    view(0,0);
    colorbarMark=colormap("jet");
    colorbarMark=flipud(colorbarMark);
    matrixHeight=size(colorbarMark,1);
    colorbarMark=colorbarMark(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(colorbarMark);
    caxis([0,nSection]);
    title haveSolution(1)

%     subplot(2,2,2)
    nexttile
    redSum = sum(solutionFlagSliced==2,2);
    scatter3(positionRowX,positionRowY,positionRowZ,markSize,redSum,"filled");
    view(0,0);
    colormap(colorbarMark);
    caxis([0,nSection]);
    title noSolution(2)

%     subplot(2,2,3)
    nexttile
    yellowSum = sum(solutionFlagSliced==3,2);
    scatter3(positionRowX,positionRowY,positionRowZ,markSize,yellowSum,"filled");
    view(0,0);
    colormap(colorbarMark);
    caxis([0,nSection]);
    title jointLimit(3)

%     subplot(2,2,4)
    nexttile
    magentaSum = sum(solutionFlagSliced==4,2);
    scatter3(positionRowX,positionRowY,positionRowZ,markSize,magentaSum,"filled");
    view(0,0);
    colormap(colorbarMark);
    caxis([0,nSection]);
    title selfCollision(4)
    cb = colorbar;
    cb.Ticks=0:4:24;
    cb.Layout.Tile = 'east';

    nexttile
    graySum = sum(solutionFlagSliced==0,2);
    scatter3(positionRowX,positionRowY,positionRowZ,markSize,graySum,"filled");
    view(0,0);
    colormap(colorbarMark);
    caxis([0,nSection]);
    title notCalculated(0)
 
end

