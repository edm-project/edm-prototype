function f = dexTest_plotTotalFlipSolution(robotMapsClass,options)
%DEXTEST_PLOTTOTALFLIPSOLUTION
    arguments
        robotMapsClass
        options.solvable(1,1) logical = true
    end
    % extract data
    testDexterity = robotMapsClass.generateMapSlot.dataList.testDexterity;
    solutionFlag = testDexterity.dataList.solutionFlag;
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    robotName = robotMapsClass.commonDataSlot.robotParameters.robotName;

    % parameter
    allTform  = robotMapsClass.generateMapSlot.dataList.allTform;
    nLocalPosition = size(allTform,3);
    colorbarMark = getJetColorbar();

    % calculation
    if options.solvable==true
        haveSolutionMask = solutionFlag == 1 ;
    else
        haveSolutionMask = solutionFlag == 1 | solutionFlag == 3 | solutionFlag == 4;
    end

    existDataInSectionAndRotation = any(haveSolutionMask,[2 4]);
    existDataAverageDirection = sum(existDataInSectionAndRotation,1) / nLocalPosition;
    existDataLength = squeeze(sum(existDataAverageDirection,5));

    % plotting
    f = figure;
    set(f,'renderer','painters');
    axesHandle = plotIndex(f,existDataLength,positionRow,colorbar=colorbarMark,markSize=40);
    view(axesHandle,0,0);
    title(['Flipped dexterity index for ',robotName]);
    colorbar('XTickLabel',{'0','1','2','3','4'},'XTick',0:4);
    caxis([0 4]);
%     caxis([0 8]);
    xlabel('x');
    zlabel('z');
end

