function [positionList,distList] = dexTest_findLargeDistancePoints(robotMapsClass,iLocalPosition,iLocalRotation,iFlip)
%DEXTEST_FINDERRORONEPOINTS 
    % extract data
    testDexterity = robotMapsClass.generateMapSlot.dataList.testDexterity;
    solutionFlag = testDexterity.dataList.solutionFlag;
    configSolColumn = testDexterity.dataList.configSolColumn;
    allTform  = robotMapsClass.generateMapSlot.dataList.allTform;
    nGlobalPosition = size(allTform,5);

    solutionFlagSlice = squeeze(solutionFlag(iLocalPosition,iLocalRotation,:,:,iFlip));
    configSolColumnSlice = squeeze(configSolColumn(:,iLocalPosition,iLocalRotation,:,:,iFlip));
    potentialList = zeros(nGlobalPosition,1,'logical');
    distRecordList = zeros(nGlobalPosition,1);
    for iGlobalPosition = 1:nGlobalPosition
        if solutionFlagSlice(iGlobalPosition,12)~=2 && solutionFlagSlice(iGlobalPosition,13)~=2 
            jointDistance = jointJointDistance(configSolColumnSlice(:,iGlobalPosition,12),configSolColumnSlice(:,iGlobalPosition,13));
            if jointDistance>=deg2rad(90)
                potentialList(iGlobalPosition) = true;
                distRecordList(iGlobalPosition) = jointDistance;
            end
        end
    end
    positionList = find(potentialList);
    distList = distRecordList(potentialList);
    [distList,I] = sort(distList,'descend');
    positionList = positionList(I);
end

