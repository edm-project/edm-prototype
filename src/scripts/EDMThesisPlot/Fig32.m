function fm = Fig32(robotMapsClass,direction,rotation,position,flip,sectionStep)
    % prepare the data
    robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;

    fm = figure(11);
%     set(fm,'Position',[100,158,1100,570])
    setRenderer(fm);
    nSection = size(configSolColumn,5);

    for i=1:sectionStep:nSection
        configSol = squeeze(configSolColumn(:,direction,rotation,position,i,flip));
        ax = show(robotColumn,configSol);
        setDisplay(ax,'');
        hold on;
    end
    
end

function setRenderer(f)
      set(f,'renderer','painters');
end

function setDisplay(ax,titletext)
    xlim(ax,[-0.75,0.75])
    ylim(ax,[-0.75,0.75])
    zlim(ax,[-0.25,1.25])
    view(ax,[135,5])
    title(ax,titletext);
end