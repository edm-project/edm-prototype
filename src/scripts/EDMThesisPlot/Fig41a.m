function f = Fig41a(robotMapsClass)
%FIG41a

    % extract the data
    positionRow = robotMapsClass.commonDataSlot.positionRow;

    f = figure;
    set(f,'renderer','painters');
    hold on;
    scatter3(positionRow(:,1),positionRow(:,2),positionRow(:,3),'black','MarkerFaceColor',[0 .75 .75]);

    % cube
    R = 1.2;
    cubeAlpha = 0.1;
    cubeLineWidth = 0.3;
    [X,Y] = meshgrid(-R:0.3:R);
    Z = repelem(R,size(X,1),size(X,2));
    mesh(X,Y,Z,'EdgeColor',[0.7 0.7 0.7],'FaceAlpha',cubeAlpha,'EdgeAlpha',0.8,'LineWidth',cubeLineWidth);
    mesh(X,Y,-Z,'EdgeColor',[0.7 0.7 0.7],'FaceAlpha',cubeAlpha,'EdgeAlpha',0.8,'LineWidth',cubeLineWidth);
    mesh(X,Z,Y,'EdgeColor',[0.7 0.7 0.7],'FaceAlpha',cubeAlpha,'EdgeAlpha',0.8,'LineWidth',cubeLineWidth);
    mesh(X,-Z,Y,'EdgeColor',[0.7 0.7 0.7],'FaceAlpha',cubeAlpha,'EdgeAlpha',0.8,'LineWidth',cubeLineWidth);
    mesh(Z,Y,X,'EdgeColor',[0.7 0.7 0.7],'FaceAlpha',cubeAlpha,'EdgeAlpha',0.8,'LineWidth',cubeLineWidth);
    mesh(-Z,Y,X,'EdgeColor',[0.7 0.7 0.7],'FaceAlpha',cubeAlpha,'EdgeAlpha',0.8,'LineWidth',cubeLineWidth);

    % sphere
    r = 0.955;
    shift = [0 0 0.335];
    [X,Y,Z] = sphere(20);
    X=X*r + shift(1);
    Y=Y*r + shift(2);
    Z=Z*r + shift(3);
    mesh(X,Y,Z,'EdgeColor',[0.7 0.7 0.7],'FaceAlpha',0.4,'EdgeAlpha',0.8,'LineWidth',cubeLineWidth);
    
    % plain
    height = -0.41;
    [X,Y] = meshgrid(-1.25:0.25:1.25);
    Z = repelem(height,size(X,1),size(X,2));
    mesh(X,Y,Z,'EdgeColor',[0.7 0.7 0.7],'FaceAlpha',0.6,'EdgeAlpha',0.8,'LineWidth',cubeLineWidth);

    % axis equal;
%     xticks('')
%     yticks('')
%     zticks('')
    xlabel('x')
    ylabel('y')
    zlabel('z')
    view(-30,5);
    xlim([-1.25,1.25]);
    ylim([-1.25,1.25]);
    zlim([-1,1.5]);
end

