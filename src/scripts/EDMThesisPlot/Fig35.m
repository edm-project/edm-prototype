function fm = Fig35(robotMapsClass,angleXY1,flipArray)
    
    solutionFlag = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;

    fm = figure;
    set(fm,'renderer','painters');
    set(fm,'Position',[100,158,500,500])

    % constants
    color(1,:) = [0 0.4470 0.7410];
    color(2,:) = [0.8500 0.3250 0.0980];
    color(3,:) = [0.9290 0.6940 0.1250];
    color(4,:) = [0.4940 0.1840 0.5560];
    marksize = 5;

    % figure 1
    hold on;
    for iDirection = 1:2:size(solutionFlag,1)
        for iRotation = 1:1
%     for iDirection = 1:1
%         for iRotation = 1:1
            for iFlip = 1:4
                for iSection = 1:4:size(solutionFlag,5)
                    configSol = squeeze(configSolColumn(:,iDirection,iRotation,:,:,flipArray(iFlip)));
                    X = squeeze(configSol(angleXY1(1),:,iSection));
                    Y = squeeze(configSol(angleXY1(2),:,iSection));
                    mask = squeeze(solutionFlag(iDirection,iRotation,:,iSection,flipArray(iFlip))~=2);
                    scatter(X(mask),Y(mask),marksize,".",'MarkerEdgeColor',color(iFlip,:));
                end
            end
        end
    end
    title('')
    xlim([-pi,pi]);
%     ylim([-pi,pi]);
    ylim([-pi/2,3/2*pi]);
    xlabel([num2str(angleXY1(1)),'th joint']);
    ylabel([num2str(angleXY1(2)),'th joint']);
%     title('kuka iiwa 14 all EDM solution');
    title('franka Emika Panda all EDM solution');
end

