function fm = Fig34(robotMapsClass,direction,rotation,position,section,flipArray)
% prepare the data
    robotColumn = loadrobot('kukaIiwa14','DataFormat','column');
    removeBody(robotColumn,'iiwa_link_3');
%     solutionFlag = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;

    % prepare the target
    fm = figure(11);
    set(fm,'Position',[100,158,280,570])
    setRenderer(fm);
    axm1 = subplot(2,1,1,'parent',fm);
    axm2 = subplot(2,1,2,'parent',fm);

    fo1 = figure(1);
    setRenderer(fo1);
    configSol1 = squeeze(configSolColumn(1:2,direction,rotation,position,section,flipArray(1)));
    ax1 = show(robotColumn,configSol1);
    setDisplay(ax1,'negative critical joint');
    moveToGraph(ax1,axm1,fo1,fm);

    fo2 = figure(2);
    setRenderer(fo2);
    configSol2 = squeeze(configSolColumn(1:2,direction,rotation,position,section,flipArray(2)));
    ax2 = show(robotColumn,configSol2);
    setDisplay(ax2,'positive critical joint');
    moveToGraph(ax2,axm2,fo2,fm);
end

function setRenderer(f)
      set(f,'renderer','painters');
end

function setDisplay(ax,titletext)
    xlim(ax,[-0.2525,0.2525])
    ylim(ax,[-0.2525,0.2525])
    zlim(ax,[-0,0.55])
    view(ax,[135,10])
    title(ax,titletext);
end

function axcp = moveToGraph(axo,axm,fo,fm)
    axcp = copyobj(axo, fm);
    set(axcp,'Position',get(axm,'position'));
    delete(axm);
    delete(fo);
end