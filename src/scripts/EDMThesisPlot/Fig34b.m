function fm = Fig34b()
    robotColumn1 = loadrobot('kukaIiwa14','DataFormat','column');
    robotColumn2 = loadrobot('frankaEmikaPanda','DataFormat','column');
    removeBody(robotColumn2,'panda_hand')
    configSol1 = [0;0;0;0;0;0;0];
    configSol2 = [0;0;0;0;0;pi;0];

     % prepare the target
    fm = figure(11);
    set(fm,'Position',[100,158,560,570])
    setRenderer(fm);
    axm1 = subplot(1,2,1,'parent',fm);
    axm2 = subplot(1,2,2,'parent',fm);

    fo1 = figure(1);
    setRenderer(fo1);
    ax1 = show(robotColumn1,configSol1);
    axcp1 = moveToGraph(ax1,axm1,fo1,fm,[-0.027872235434396,-0.041569864373458,0.617883106826186,1.130808980841389]);
    setDisplay(axcp1,'');

    fo2 = figure(2);
    setRenderer(fo2);
    ax2 = show(robotColumn2,configSol2);
    axcp2 = moveToGraph(ax2,axm2,fo2,fm,[0.395045760513901,-0.020554640920538,0.617824973564643,1.092098958868734]);
%     lightangle(axcp2,90,0);
    setDisplay(axcp2,'');
%     view(axcp2,[0,0])
end

function setRenderer(f)
      set(f,'renderer','painters');
end

function setDisplay(ax,titletext)
    xlim(ax,[-0.375,0.375])
    ylim(ax,[-0.375,0.375])
    zlim(ax,[-0,1.5])
    view(ax,[90,0])
    ax.XGrid = 'off';
    ax.YGrid = 'off';
    ax.ZGrid = 'off';
    ax.XTick = [];
    ax.YTick = [];
    ax.ZTick = [];
    ax.XLabel = [];
    ax.YLabel = [];
    ax.ZLabel = [];
    ax.XAxis.Visible = 'off';
    ax.YAxis.Visible = 'off';
    ax.ZAxis.Visible = 'off';
    title(ax,titletext);
end

function axcp = moveToGraph(axo,axm,fo,fm,position)
    axcp = copyobj(axo, fm);
    set(axcp,'Position',position);
    delete(axm);
    delete(fo);
end