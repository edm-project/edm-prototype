function f = Fig41bc(robotMapsClass)
%FIG41a

    % extract the data
    endEffectorTform = robotMapsClass.generateMapSlot.dataList.endEffectorTform;
    nLocalPos=size(endEffectorTform,3);
    nLocalRot=size(endEffectorTform,4);
    
    f = figure;
    set(f,'renderer','painters');
    hold on;
    for iLocalPos=18:18
        for iLocalRot=1:nLocalRot
            plotTform(endEffectorTform(:,:,iLocalPos,iLocalRot),0.012);
        end
    end

    % sphere
    r = 0.025;
    shift = [0 0 0];
    [X,Y,Z] = sphere(20);
    X=X*r + shift(1);
    Y=Y*r + shift(2);
    Z=Z*r + shift(3);
    mesh(X,Y,Z,'EdgeColor',[0.7 0.7 0.7],'FaceAlpha',0.5,'EdgeAlpha',0.8,'LineWidth',0.3);

    % axis equal;
    grid off;
    xticks('')
    yticks('')
    zticks('')
    xlabel('x')
    ylabel('y')
    zlabel('z')
    view(-149.8618802564884,33.260746844363752);
    xlim([-0.03,0.005]);
    ylim([-0.005,0.03]);
    zlim([-0.005,0.03]);
end

