function fm = Fig31(robotMapsClass,direction,rotation,position,section)

    % prepare the data
    robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;
%     solutionFlag = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag;
    configSolColumn = robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn;

    % prepare the target
    fm = figure(11);
    set(fm,'Position',[100,158,1100,570])
    setRenderer(fm);
%     axm1 = subplot(2,2,1,'parent',fm);
%     axm2 = subplot(2,2,2,'parent',fm);
%     axm3 = subplot(2,2,3,'parent',fm);
%     axm4 = subplot(2,2,4,'parent',fm);
    axm1 = subplot(2,4,1,'parent',fm);
    axm2 = subplot(2,4,2,'parent',fm);
    axm3 = subplot(2,4,3,'parent',fm);
    axm4 = subplot(2,4,4,'parent',fm);
    axm5 = subplot(2,4,5,'parent',fm);
    axm6 = subplot(2,4,6,'parent',fm);
    axm7 = subplot(2,4,7,'parent',fm);
    axm8 = subplot(2,4,8,'parent',fm);


    % prepare the original one
    fo1 = figure(1);
    setRenderer(fo1);
    configSol1= squeeze(configSolColumn(:,direction,rotation,position,section,1));
    ax1 = show(robotColumn,configSol1);
    setDisplay(ax1,'Flipped solution 1');
    moveToGraph(ax1,axm1,fo1,fm);

    fo2 = figure(2);
    setRenderer(fo2);
    configSol2= squeeze(configSolColumn(:,direction,rotation,position,section,2));
    ax2 = show(robotColumn,configSol2);
    setDisplay(ax2,'Flipped solution 2');
    moveToGraph(ax2,axm2,fo2,fm);
    
    fo3 = figure(3);
    setRenderer(fo3);
    configSol3= squeeze(configSolColumn(:,direction,rotation,position,section,3));
    ax3 = show(robotColumn,configSol3);
    setDisplay(ax3,'Flipped solution 3');
    moveToGraph(ax3,axm3,fo3,fm);
    
    fo4 = figure(4);
    setRenderer(fo4);
    configSol4= squeeze(configSolColumn(:,direction,rotation,position,section,4));
    ax4 = show(robotColumn,configSol4);
    setDisplay(ax4,'Flipped solution 4');
    moveToGraph(ax4,axm4,fo4,fm);

    % part5

    fo5 = figure(5);
    setRenderer(fo5);
    configSol5= squeeze(configSolColumn(:,direction,rotation,position,section,5));
    ax5 = show(robotColumn,configSol5);
    setDisplay(ax5,'Flipped solution 5');
    moveToGraph(ax5,axm5,fo5,fm);

    fo6 = figure(6);
    setRenderer(fo6);
    configSol6= squeeze(configSolColumn(:,direction,rotation,position,section,6));
    ax6 = show(robotColumn,configSol6);
    setDisplay(ax6,'Flipped solution 6');
    moveToGraph(ax6,axm6,fo6,fm);

    fo7 = figure(7);
    setRenderer(fo7);
    configSol7= squeeze(configSolColumn(:,direction,rotation,position,section,7));
    ax7 = show(robotColumn,configSol7);
    setDisplay(ax7,'Flipped solution 7');
    moveToGraph(ax7,axm7,fo7,fm);

    fo8 = figure(8);
    setRenderer(fo8);
    configSol8= squeeze(configSolColumn(:,direction,rotation,position,section,8));
    ax8 = show(robotColumn,configSol8);
    setDisplay(ax8,'Flipped solution 8');
    moveToGraph(ax8,axm8,fo8,fm);
    
end

function setRenderer(f)
     set(f,'renderer','painters');
end

function setDisplay(ax,titletext)
    xlim(ax,[-0.75,0.75])
    ylim(ax,[-0.75,0.75])
    zlim(ax,[-0.25,1.25])
    view(ax,[135,10])
    title(ax,titletext);
end

function axcp = moveToGraph(axo,axm,fo,fm)
    axcp = copyobj(axo, fm);
    set(axcp,'Position',get(axm,'position'));
    delete(axm);
    delete(fo);
end

