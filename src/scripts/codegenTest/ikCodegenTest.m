function [qConfig,solInfo] = ikCodegenTest(endEffectorName,tform,weights,initialGuess)
	%#codegen

    robotColumnIn=loadrobot('frankaEmikaPanda','DataFormat','column');
    removeBody(robotColumnIn,'panda_hand');

	ik = inverseKinematics('RigidBodyTree',robotColumnIn);
    ik.SolverParameters.MaxIterations=5000;
	
	[qConfig,solInfo] = ik(endEffectorName,tform,weights,initialGuess); 
end 
