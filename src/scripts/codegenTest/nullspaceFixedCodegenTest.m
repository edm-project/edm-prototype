function [configSolCol,angleCol,sectionCol] = nullspaceFixedCodegenTest(configSol,timeStep,nSection,maxStep)
    %#codegen
    % comment out the function arguments block:
    % circularDifference, circularMod
    % angleDifference, angleInSection
    % cartesianDifference, compensateCartesianDifference
    % dexterity...*3
    % jointNormalize
    % nullspaceFixed, nullspaceStep

    % comment out the error, warning and throw:
    % angleDifference, angleInSection, dexterityReferenceVectors

    % enable the reshape function:
    % nullspaceStep
    robotColumnIn=loadrobot('frankaEmikaPanda','DataFormat','column');
    removeBody(robotColumnIn,'panda_hand');

    robotParameters.robotColumn=robotColumnIn;
    robotParameters.baseName='panda_link0';
    robotParameters.shoulderName='panda_link2';
    robotParameters.elbowName='panda_link4';
    robotParameters.endEffectorName='panda_link8';
    robotParameters.robotName='frankaEmikaPanda';

    [configSolCol,angleCol,sectionCol] = nullspaceFixed(robotParameters,configSol,timeStep,nSection,maxStep);
end

