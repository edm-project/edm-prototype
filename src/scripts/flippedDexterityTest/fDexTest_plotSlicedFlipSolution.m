function fDexTest_plotSlicedFlipSolution(robotMapsClass,iLocalPosition,iLocalRotation)
    % constants
    markSize = 15;
    
    % extract data
    testFlippedDexterity = robotMapsClass.generateMapSlot.dataList.testFlippedDexterity;
    solutionFlagSliced = squeeze(testFlippedDexterity.dataList.solutionFlag(iLocalPosition,iLocalRotation,:,:,:));
    positionRow = robotMapsClass.commonDataSlot.positionRow;
    positionRowX = positionRow(:,1);
    positionRowY = positionRow(:,2);
    positionRowZ = positionRow(:,3);
    robotName = robotMapsClass.commonDataSlot.robotParameters.robotName;
    nSection = testFlippedDexterity.dataList.nSection;
    flipCounter = testFlippedDexterity.dataList.flipCounter;

    % calculation
    haveSolutionMask = solutionFlagSliced == 1 | solutionFlagSliced == 3 | solutionFlagSliced == 4;
    existDataInSection = any(haveSolutionMask,2);
    existDataLength = squeeze(sum(existDataInSection,3));

    % plotting
    f = figure();
    f.Name = ['Flipped Dexterity ',robotName,': (',num2str(iLocalPosition),',',num2str(iLocalRotation),',:,:,:)'];
    f.NumberTitle = "off";

    scatter3(positionRowX,positionRowY,positionRowZ,markSize,existDataLength,"filled");
    view(0,0);
    colorbarMark=colormap("jet");
    colorbarMark=flipud(colorbarMark);
    matrixHeight=size(colorbarMark,1);
    colorbarMark=colorbarMark(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(colorbarMark);
    caxis([0,flipCounter]);
    colorbar;
    title(['flipCounter=',num2str(flipCounter)]);

end

