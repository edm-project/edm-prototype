function mergeEDM(robotMapsClass,robotMapsClass2)
%MERGEEDM 
% notice: when running this func, please temporarily take down the private
% limit of commonData,generateMap, testDexterity
robotMapsClass.generateMapSlot.dataList.testFlippedDexterity = 0;
robotMapsClass.commonDataSlot.positionRow = ...
    cat(1,robotMapsClass.commonDataSlot.positionRow,robotMapsClass2.commonDataSlot.positionRow);
robotMapsClass.generateMapSlot.dataList.allTform = ...
    cat(5,robotMapsClass.generateMapSlot.dataList.allTform,robotMapsClass2.generateMapSlot.dataList.allTform);
robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag = ...
    cat(3,robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.solutionFlag,robotMapsClass2.generateMapSlot.dataList.testDexterity.dataList.solutionFlag);
robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn = ...
    cat(4,robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn,robotMapsClass2.generateMapSlot.dataList.testDexterity.dataList.configSolColumn);
end

