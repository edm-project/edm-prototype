for iRepeat=7:8

    disp(['iRepeat=',num2str(iRepeat)]);

    rng('shuffle')
    disp('additive Calculation...');
    robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.additiveCalculation(8);

    disp('marginal Calculation...');
    for iMarginal=1:3
        robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.marginalCalculation();
    end

    disp('full Calculation extra...');
    robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.marginalCalculation(wideSearch=true);
    robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.marginalCalculation();

    if mod(iRepeat,2)==0
        savePoint(robotMapsClass,'FLIP',['8+',num2str(iRepeat)],'./log/kinova_Flip/')
    end
end