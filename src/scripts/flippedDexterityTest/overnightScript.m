for iRepeat=1:8

    disp(['iRepeat=',num2str(iRepeat)]);

    rng('shuffle')
    disp('full Calculation...');
    robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.fullCalculation();

    disp('marginal Calculation...');
    for iMarginal=1:min([iRepeat,3],[],'all')
        rng('shuffle')
        robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.marginalCalculation();
    end

    disp('full Calculation extra...');
    robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.marginalCalculation(wideSearch=true);
    robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.marginalCalculation();


    if mod(iRepeat,2)==0
        savePoint(robotMapsClass,'FLIP',[num2str(iRepeat)],'./log/FLIP_UR5/')
    end

end