% This script show the points to be calculated in robotMapsClass
f = figure();
hold on;
scatter3(robotMapsClass.commonDataSlot.positionRow(:,1),robotMapsClass.commonDataSlot.positionRow(:,2),robotMapsClass.commonDataSlot.positionRow(:,3),'black','MarkerFaceColor',[0 .75 .75])