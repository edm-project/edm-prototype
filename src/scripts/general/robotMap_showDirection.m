% This script show the directions to be used in robotMapsClass
hold on
nLocalPos=size(robotMapsClass.generateMapSlot.dataList.endEffectorTform,3);
nLocalRot=size(robotMapsClass.generateMapSlot.dataList.endEffectorTform,4);
for iLocalPos=1:nLocalPos
    for iLocalRot=1:nLocalRot
        plotTform(robotMapsClass.generateMapSlot.dataList.endEffectorTform(:,:,iLocalPos,iLocalRot),0.01);
    end
end
[X,Y,Z] = sphere(20);
r = 0.025;
X=X*r;
Y=Y*r;
Z=Z*r;
mesh(X,Y,Z,'EdgeColor',[0.7 0.7 0.7],'FaceAlpha',0.6,'EdgeAlpha',0.8,'LineWidth',0.1);
% axis equal;
xticks('')
yticks('')
zticks('')
xlabel('x')
ylabel('y')
zlabel('z')