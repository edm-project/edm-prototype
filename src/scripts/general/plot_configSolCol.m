function plot_configSolCol(robotParameters,configSolCol)
%PLOT_CONFIGSOLCOL
    robotColumn=robotParameters.robotColumn;
    show(robotColumn,configSolCol(:,1));
    hold on
    for i=2:size(configSolCol,2)
        show(robotColumn,configSolCol(:,i));
    end
end

