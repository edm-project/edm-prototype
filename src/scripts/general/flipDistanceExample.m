function [distance] = flipDistanceExample(robotMapsClass)
    configSolColP1 = squeeze(robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn(:,7,1,839,:,:));
    configSolColP2 = squeeze(robotMapsClass.generateMapSlot.dataList.testDexterity.dataList.configSolColumn(:,7,1,840,:,:));
    
    distance = zeros(4,4);
    for iStart = 1:4
        for iEnd = iStart:4
            configSolCol1=squeeze(configSolColP1(:,:,iStart));
            configSolCol2=squeeze(configSolColP2(:,:,iEnd));
            distance(iStart,iEnd)=sectionSectionDistance(configSolCol1,configSolCol2);
        end
    end
end

