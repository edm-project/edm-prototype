function plot_solutionFlagSlice(solutionFlagSlice,positionRow)
%PLOT_SOLUTIONFLAGSLICE 此处显示有关此函数的摘要
%   此处显示详细说明
    
    % export points
    positionRowX=positionRow(:,1);
    positionRowY=positionRow(:,2);
    positionRowZ=positionRow(:,3);

    % constants
    sizeMark = 24;

    % figure
    figure;
    hold on;
    view(0,0);

    % plot the graph
    GreenSolutionFlag= solutionFlagSlice==1;
    scatter3(positionRowX(GreenSolutionFlag),positionRowY(GreenSolutionFlag),positionRowZ(GreenSolutionFlag), ...
        sizeMark,"green",'filled');
    
    RedSolutionFlag= solutionFlagSlice==2;
    scatter3(positionRowX(RedSolutionFlag),positionRowY(RedSolutionFlag),positionRowZ(RedSolutionFlag), ...
        sizeMark,"red",'filled');
    
    YellowSolutionFlag= solutionFlagSlice==3;
    scatter3(positionRowX(YellowSolutionFlag),positionRowY(YellowSolutionFlag),positionRowZ(YellowSolutionFlag), ...
        sizeMark,"yellow",'filled');
    
    MagnetaSolutionFlag= solutionFlagSlice==4;
    scatter3(positionRowX(MagnetaSolutionFlag),positionRowY(MagnetaSolutionFlag),positionRowZ(MagnetaSolutionFlag), ...
        sizeMark,'magenta','filled');
end

