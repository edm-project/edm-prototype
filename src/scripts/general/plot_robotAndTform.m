function plot_robotAndTform(robotIn,pose,tform)
% This function plot a robot+pose and a tform in a graph quickly
    arguments
        robotIn(1,1) rigidBodyTree
        pose(:,1) double
        tform(4,4) double
    end

    show(robotIn,pose);
    hold on;
    plotTform(tform,0.15);
end