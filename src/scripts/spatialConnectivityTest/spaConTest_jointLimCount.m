function spaConTest_jointLimCount(robotMapsIn)
% This function Solve show the spatialConnectivityCount of robotMapsIn in spatialConnectivityMap
    arguments
        robotMapsIn(1,1) robotMaps
    end
    
    % export data
    testSpatialConnectivity=robotMapsIn.generateMapSlot.dataList.testSpatialConnectivity;
    positionRow=robotMapsIn.generateMapSlot.dataList.positionRow;
    maxAdjacent=testSpatialConnectivity.dataList.maxAdjacent;

    % export points
    positionRowX=positionRow(:,1);
    positionRowY=positionRow(:,2);
    positionRowZ=positionRow(:,3);

    scatter3(positionRowX,positionRowY,positionRowZ,60,testSpatialConnectivity.dataList.jointLimitCount,'filled')
    colorbar;
    x=colormap("jet");
    x=flipud(x);
    matrixHeight=size(x,1);
    x=x(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(x);
    caxis([0, maxAdjacent]);
end

