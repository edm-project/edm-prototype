function spaConTest_viewFullDiagram(robotMapsIn)
%SPACONTEST_FULLDIAGRAM 
    arguments
        robotMapsIn(1,1) robotMaps
    end

    % export data
    testSpatialConnectivity=robotMapsIn.generateMapSlot.dataList.testSpatialConnectivity;
    robotName=robotMapsIn.commonDataSlot.robotParameters.robotName;
    fullCalculationMarkIndex=getIndexCell(testSpatialConnectivity.mark,'assignAdjacent');
    directionNumber=testSpatialConnectivity.metaList{fullCalculationMarkIndex}.directionNumber;
    rotationNumber=testSpatialConnectivity.metaList{fullCalculationMarkIndex}.rotationNumber;
    adjacentSolvableMap=testSpatialConnectivity.dataList.adjacentSolvableMap;
    spatialConnectivityCount=testSpatialConnectivity.dataList.spatialConnectivityCount;
    jointLimitCount=testSpatialConnectivity.dataList.jointLimitCount;
    selfCollisionCount=testSpatialConnectivity.dataList.selfCollisionCount;
    maxAdjacent=testSpatialConnectivity.dataList.maxAdjacent;
    
    
    % some constant
    sizeMark=17;
    scale=0.025;
    %franka
%     xLim=[-1 1];
%     zLim=[-0.4 1.2];
    %UR5
    xLim=[-1.25 1.25];
    zLim=[-1 1.5];

    % export points
    positionRow=robotMapsIn.generateMapSlot.dataList.positionRow;
    positionRowX=positionRow(:,1);
    positionRowY=positionRow(:,2);
    positionRowZ=positionRow(:,3);
    nGlobalPosition = size(positionRow,1);

    % calculate the relative portion
    adjacentSolvableCount=zeros(1,nGlobalPosition);
    for iGlobalPosition=1:nGlobalPosition
        adjacentSolvableCount(iGlobalPosition)=length(adjacentSolvableMap(iGlobalPosition));
    end

    % plot the UI
    fig = uifigure('Position',[234,108,1000,600]);
    g = uigridlayout(fig,[3 4]);
    g.RowHeight = {20,'1x','1x'};
    g.ColumnWidth = {'1x','1x','1x',60};

    %Title array
    title = uilabel(g,'Text',['Spatical Connectivity ',robotName,' with direction ' ...
                             ,num2str(directionNumber),', rotation ',num2str(rotationNumber)]);
    title.HorizontalAlignment = 'center';
    title.FontSize = 16;
    title.Layout.Row = 1;
    title.Layout.Column = [1,4];

    % adjecent and Solvable counter
    adjacentSolvableMap = uiaxes(g);
    adjacentSolvableMap.Title.String = 'adjecent and Solvable counter';
    adjacentSolvableMap.Layout.Row = 2;
    adjacentSolvableMap.Layout.Column = 2;

    % spatialConnec Counter
    spatialConCounterMap = uiaxes(g);
    spatialConCounterMap.Title.String = 'spatial Connectivity success';
    spatialConCounterMap.Layout.Row = 3;
    spatialConCounterMap.Layout.Column = 1;

    % joint Limit Counter
    jointLimitMap = uiaxes(g);
    jointLimitMap.Title.String = 'spatial Connectivity failed (joint Limit)';
    jointLimitMap.Layout.Row = 3;
    jointLimitMap.Layout.Column = 2;

    % selfCollision Counter
    selfCollisionMap = uiaxes(g);
    selfCollisionMap.Title.String = 'spatial Connectivity failed (self collision)';
    selfCollisionMap.Layout.Row = 3;
    selfCollisionMap.Layout.Column = 3;


    % adjacentSolvableCount
    scatter3(adjacentSolvableMap,positionRowX,positionRowY,positionRowZ,sizeMark, ...
             adjacentSolvableCount,'filled')
%     colorbar(adjacentSolvableMap);
    adjacentSolvableMap.View = [0 0];
    x=colormap(adjacentSolvableMap,"jet");
    x=flipud(x);
    matrixHeight=size(x,1);
    x=x(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(adjacentSolvableMap,x);
    caxis(adjacentSolvableMap,[0, maxAdjacent]);
    adjacentSolvableMap.XLim=xLim;
    adjacentSolvableMap.ZLim=zLim;

    % spatialConCounterMap
    scatter3(spatialConCounterMap,positionRowX,positionRowY,positionRowZ,sizeMark, ...
             spatialConnectivityCount,'filled')
    spatialConCounterMap.View = [0 0];
    x=colormap(spatialConCounterMap,"jet");
    x=flipud(x);
    matrixHeight=size(x,1);
    x=x(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(spatialConCounterMap,x);
    caxis(spatialConCounterMap,[0, maxAdjacent]);
    spatialConCounterMap.XLim=xLim;
    spatialConCounterMap.ZLim=zLim;

    % jointLimitMap
    jLMask = (jointLimitCount~=0);
    scatter3(jointLimitMap,positionRowX(jLMask),positionRowY(jLMask),positionRowZ(jLMask),sizeMark, ...
             jointLimitCount(jLMask),'filled')
    jointLimitMap.View = [0 0];
    x=colormap(jointLimitMap,"jet");
    x=flipud(x);
    matrixHeight=size(x,1);
    x=x(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(jointLimitMap,x);
    caxis(jointLimitMap,[0, maxAdjacent]);
    jointLimitMap.XLim=spatialConCounterMap.XLim;
    jointLimitMap.YLim=spatialConCounterMap.YLim;
    jointLimitMap.ZLim=spatialConCounterMap.ZLim;

    % jointLimitMap
    sCMask = (selfCollisionCount~=0);
    scatter3(selfCollisionMap,positionRowX(sCMask),positionRowY(sCMask),positionRowZ(sCMask),sizeMark, ...
             selfCollisionCount(sCMask),'filled')
    selfCollisionMap.View = [0 0];
    x=colormap(selfCollisionMap,"jet");
    x=flipud(x);
    matrixHeight=size(x,1);
    x=x(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(selfCollisionMap,x);
    caxis(selfCollisionMap,[0, maxAdjacent]);
    selfCollisionMap.XLim=spatialConCounterMap.XLim;
    selfCollisionMap.YLim=spatialConCounterMap.YLim;
    selfCollisionMap.ZLim=spatialConCounterMap.ZLim;

    % draw colorbar
    %colorBarDummy = uiaxes(g);
    %colorBarDummy.Layout.Row = [2 3];
    %colorBarDummy.Layout.Column = 4;
    colorbar(adjacentSolvableMap,"manual",Position=[0.92 0.05 0.04 0.85]);

end

