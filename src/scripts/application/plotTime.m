function f = plotTime(testHandle)
%PLOTTIME 此处显示有关此函数的摘要
%   此处显示详细说明
    randomTimer = testHandle.metaList{3}.randomTimer;
    reachabilityTimer = testHandle.metaList{3}.reachabilityTimer;
    dexterityTimer = testHandle.metaList{3}.dexterityTimer;

    f = figure;
    set(f,'renderer','painters');
    a = axes(f);
    a.NextPlot = 'add';
    plotScatter1D(a,randomTimer,0,'o');
    plotScatter1D(a,reachabilityTimer,1,'^');
    plotScatter1D(a,[dexterityTimer(:,1);dexterityTimer(:,2)],2,'d');
    xlabel('time(seconds)')
    yticks('');
    yticklabels('');
    title('Time used in RRT path planning');
    legend(a,'Brute Force','Reachability','EDM');
    ylim([-1 3]);
    xlim([0 4]);
end

