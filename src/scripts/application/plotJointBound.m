function f = plotJointBound(testHandle,options)
    arguments
        testHandle
        options.normType {mustBeMember(options.normType,{'-inf','2'})} = '-inf';
    end

    % import Data
    robotParameters = testHandle.robotMapsClassHandle.commonDataSlot.robotParameters;
    randomConfigSuccess = testHandle.dataList.randomConfigSuccess;
    reachabilityConfigSuccess = testHandle.dataList.reachabilityConfigSuccess;
    dexterityConfigSuccess = testHandle.dataList.dexterityConfigSuccess;
    randomJointWayPoints = testHandle.dataList.randomJointWayPoints;
    reachabilityJointWayPoints = testHandle.dataList.reachabilityJointWayPoints;
    dexterityJointWayPoints = testHandle.dataList.dexterityJointWayPoints;

    % calculate data
    randomJointWayPointsIn = randomJointWayPoints(randomConfigSuccess==1);
    randomCell = cellCalculateJointBoundDistance(robotParameters.robotColumn,randomJointWayPointsIn,options.normType);
    reachabilityJointWayPointsIn = reachabilityJointWayPoints(reachabilityConfigSuccess==1);
    reachabilityCell = cellCalculateJointBoundDistance(robotParameters.robotColumn,reachabilityJointWayPointsIn,options.normType);
    dexterityJointWayPointsIn = dexterityJointWayPoints(dexterityConfigSuccess==1);
    dexterityCell = cellCalculateJointBoundDistance(robotParameters.robotColumn,dexterityJointWayPointsIn,options.normType);

    % plot data
    f = figure;
    set(f,'renderer','painters');
    a = axes(f);
    a.NextPlot = 'add';
    p1 = plotLineAndAverage(a,randomCell,'-',[57 106 177]./255);
    p2 = plotLineAndAverage(a,reachabilityCell,'-',[204 37 41]./255);
    p3 = plotLineAndAverage(a,dexterityCell,'-',[62 150 81]./255);
    xlabel('relative distance')
    xticks([0 1])
    xticklabels({'start','end'})
    ylabel('minimum distance to joint Bound(rad)');
    title('Joint Bound Distance Comparison');
    if isempty(p1)
        legend(a,[p2 p3],{'Reachability','Dexterity'});
    else
        legend(a,[p1 p2 p3],{'Brute Force','Reachability','EDM'});
    end
    xlim([0 1]);
end

function cellOut = cellCalculateJointBoundDistance(robotColumn,cellIn,normType)
    nCell = length(cellIn);
    cellOut = cell(nCell,1);
    for iCell=1:nCell
        dataIn = cellIn{iCell};
        nData = size(dataIn,2);
        dataOut = zeros(nData,1);
        for iData =1:nData
            if isequal(normType,'-inf')
                dataOut(iData) = jointBoundDistance(robotColumn,dataIn(:,iData));
            elseif isequal(normType,'2')
                dataOut(iData) = jointBoundDistanceNorm2(robotColumn,dataIn(:,iData));
            else
                error('No such type available!');
            end
        end
        cellOut{iCell}=dataOut;
    end
end
