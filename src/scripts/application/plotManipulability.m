function f = plotManipulability(testHandle,options)
    arguments
        testHandle
        options.reasonType {mustBeMember(options.reasonType,{'SVD','mu'})} = 'SVD';
    end

    % import Data
    robotParameters = testHandle.robotMapsClassHandle.commonDataSlot.robotParameters;
    randomConfigSuccess = testHandle.dataList.randomConfigSuccess;
    reachabilityConfigSuccess = testHandle.dataList.reachabilityConfigSuccess;
    dexterityConfigSuccess = testHandle.dataList.dexterityConfigSuccess;
    randomJointWayPoints = testHandle.dataList.randomJointWayPoints;
    reachabilityJointWayPoints = testHandle.dataList.reachabilityJointWayPoints;
    dexterityJointWayPoints = testHandle.dataList.dexterityJointWayPoints;

    % calculate data
    randomCell = cellCalculateManipulability(robotParameters, ...
                                                randomJointWayPoints(randomConfigSuccess==1), ...
                                                options.reasonType);
    reachabilityCell = cellCalculateManipulability(robotParameters, ...
                                                reachabilityJointWayPoints(reachabilityConfigSuccess==1), ...
                                                options.reasonType);
    dexteirtyCell = cellCalculateManipulability(robotParameters, ...
                                                dexterityJointWayPoints(dexterityConfigSuccess==1), ...
                                                options.reasonType);
    
    f = figure;
    set(f,'renderer','painters');
    a = axes(f);
    a.NextPlot = 'add';

    p1 = plotLineAndAverage(a,randomCell,'-',[57 106 177]./255);
    p2 = plotLineAndAverage(a,reachabilityCell,'-',[204 37 41]./255);
    p3 = plotLineAndAverage(a,dexteirtyCell,'-',[62 150 81]./255);

    xlabel(a,'relative distance')
    xticks(a,[0 1])
    xticklabels(a,{'start','end'})
    if isempty(p1)
        legend(a,[p2 p3],{'Reachability','Dexterity'});
    else
        legend(a,[p1 p2 p3],{'Brute Force','Reachability','EDM'});
    end
    xlim(a,[0 1]);

    % plot the figure
    if isequal(options.reasonType,'mu')
        ylabel(a,'manipulability measure $\mu = \sqrt{det|JJ^T|}$','Interpreter','latex');
        title(a,'Manipulability Comparison');

    elseif isequal(options.reasonType,'SVD')
        ylabel(a,'minimum $\sigma_{min}$ in SVD of J','Interpreter','latex');
        title(a,'Manipulability Comparison');
    else
        error('No such type supported!');
    end
end

function cellOut = cellCalculateManipulability(robotParameters,cellIn,reasonType)
    robotColumn= robotParameters.robotColumn;
    endEffectorName = robotParameters.endEffectorName;
    nCell = length(cellIn);
    cellOut = cell(nCell,1);
    for iCell=1:nCell
        dataIn = cellIn{iCell};
        nData = size(dataIn,2);
        dataOut = zeros(nData,1);
        for iData =1:nData
            jacobian = geometricJacobian(robotColumn,dataIn(:,iData),endEffectorName);
            if isequal(reasonType,'mu')
                dataOut(iData) = sqrt(det(jacobian*jacobian'));
            elseif isequal(reasonType,'SVD')
                [~,S,~] = svd(jacobian);
                minSize = min(size(S,1),size(S,2));
                dataOut(iData) = S(minSize,minSize);
            else
                error('No such type supported!');
            end
        end
        cellOut{iCell}=dataOut;
    end
end