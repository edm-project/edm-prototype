function f = tpRRT_reachabilityCorrelation(twoPointRRTHandle)
%TWOPOINTRRT_VIEWDEXTERITYCORRELATION
    positionRow = twoPointRRTHandle.robotMapsClassHandle.commonDataSlot.positionRow;
    plottedIndex = twoPointRRTHandle.dataList.correlationReachability;

    colorbarMark = getJetColorbar();

    f = figure;
    axesHandle = plotIndex(f,plottedIndex,positionRow,colorbar=colorbarMark);
    view(axesHandle,0,0);
end

