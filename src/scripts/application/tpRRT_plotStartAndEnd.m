function f = tpRRT_plotStartAndEnd(twoPointRRTHandle)
%TPRRT_PLOTSTARTANDEND 
    f = figure;
    hold on;
    tformStart = twoPointRRTHandle.dataList.tformMatrix(:,:,1);
    tformEnd = twoPointRRTHandle.dataList.tformMatrix(:,:,end);

    plotTform(tformStart,0.05);
    plotTform(tformEnd,0.05);
    xlim([-1 1])
    zlim([-0.5 1.5])
    ylim([-1 1])
end

