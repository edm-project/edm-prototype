function f = tpRRT_dexterityCorrelation(twoPointRRTHandle,options)
    arguments
        twoPointRRTHandle
        options.nFlipSolution = 4
        options.markSize = 15
    end
    
    % import data
    positionRow = twoPointRRTHandle.robotMapsClassHandle.commonDataSlot.positionRow;
    positionRowX = positionRow(:,1);
    positionRowY = positionRow(:,2);
    positionRowZ = positionRow(:,3);
    correlationDexterity = twoPointRRTHandle.dataList.correlationDexterity;
    maxCorrelation = max(correlationDexterity,[],'all');

    % initialize data
    f = figure();
    set(f,'renderer','painters');
    colorbarMark = getJetColorbar();
    if options.nFlipSolution == 4
        f.Position = [210,250,1075,250];
        tiledlayout(1,4)
    elseif options.nFlipSolution == 8
        f.Position = [210,250,1075,500];
        tiledlayout(2,4)
    end
    
    % plot
    for iFlipSolution = 1:options.nFlipSolution
        nexttile
        scatter3(positionRowX,positionRowY,positionRowZ,options.markSize,correlationDexterity(:,iFlipSolution),"filled");
        view(0,0); 
        colormap(colorbarMark);
        caxis([0,maxCorrelation]);
        title(['Flip Region Index',num2str(iFlipSolution)]);
    end


    cb = colorbar;
    cb.Layout.Tile = 'east';

end

