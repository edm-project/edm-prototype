function f = plotStartAndEndT(testHandle)
    startTform = testHandle.dataList.tformMatrix(:,:,1);
    endTform = testHandle.dataList.tformMatrix(:,:,end);

    f = figure;
    plotTform(startTform,0.025);
    plotTform(endTform,0.025);
%     xlim([-1 1])
%     zlim([-0.5 1.5])
%     ylim([-1 1])
end

