function f = plotPathLength(testHandle)
%PLOTPATHLENGTH 
    % import Data
    robotParameters = testHandle.robotMapsClassHandle.commonDataSlot.robotParameters;
    randomConfigSuccess = testHandle.dataList.randomConfigSuccess;
    reachabilityConfigSuccess = testHandle.dataList.reachabilityConfigSuccess;
    dexterityConfigSuccess = testHandle.dataList.dexterityConfigSuccess;
    randomJointWayPoints = testHandle.dataList.randomJointWayPoints;
    reachabilityJointWayPoints = testHandle.dataList.reachabilityJointWayPoints;
    dexterityJointWayPoints = testHandle.dataList.dexterityJointWayPoints;

    % calculate data
    [pathLengthRan,cartLengthRan] = pathAndCartDistance(robotParameters,randomJointWayPoints);
    [pathLengthRea,cartLengthRea] = pathAndCartDistance(robotParameters,reachabilityJointWayPoints);
    [pathLengthDex,cartLengthDex] = pathAndCartDistance(robotParameters,dexterityJointWayPoints);

    f = figure;
    set(f,'renderer','painters');
    a = axes(f);
    a.NextPlot = 'add';
    plotScatter2D(a,pathLengthRan(randomConfigSuccess==1),cartLengthRan(randomConfigSuccess==1),'o');
    plotScatter2D(a,pathLengthRea(reachabilityConfigSuccess==1),cartLengthRea(reachabilityConfigSuccess==1),'^');
    plotScatter2D(a,pathLengthDex(dexterityConfigSuccess==1),cartLengthDex(dexterityConfigSuccess==1),'s');
    xlabel('jointspace Distance(rad)')
    ylabel('workspace Distance(m)');
    title('Jointspace distance and workspace distance comparison');
    legend(a,'Brute Force','Reachability','EDM');
    xlim([0 8])
    ylim([0 2])
end

