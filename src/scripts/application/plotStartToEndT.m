function f = plotStartToEndT(testHandle)
    tformMatrix = testHandle.dataList.tformMatrix;
    
    f = figure;
    set(f,'renderer','painters');
    for i=1:size(tformMatrix,3)
        plotTform(tformMatrix(:,:,i),0.025);
    end
    xlim([-0.15 0.15])
    zlim([-0.05 0.25])
    ylim([-0.15 0.15])
    view(0,0);
    xlabel('x');
    ylabel('y');
    zlabel('z');
end

