function f = plotSolutionFlagMap(testHandle)
%PLOTSOLUTIONFLAGMAP 
    robotMapsClass = testHandle.robotMapsClassHandle;
    solutionFlagMap = testHandle.dataList.solutionFlagMap;
    allTform = robotMapsClass.generateMapSlot.dataList.allTform;
    nLocalPosition = size(allTform,3);
    nLocalRotation = size(allTform,4);
    nGlobalPosition = size(allTform,5);

    f = figure;
    set(f,'renderer','painters');
    for iGlobalPosition = 1:nGlobalPosition
        for iLocalPostion = 1:nLocalPosition
            for iLocalRotation = 1:nLocalRotation
                if solutionFlagMap(iLocalPostion,iLocalRotation,iGlobalPosition) == true
                    plotTform(allTform(:,:,iLocalPostion,iLocalRotation,iGlobalPosition),0.025);
                end
            end
        end
    end
    xlim([-0.15 0.15])
    zlim([-0.05 0.25])
    ylim([-0.15 0.15])
    view(0,0);
    xlabel('x');
    ylabel('y');
    zlabel('z');
end

