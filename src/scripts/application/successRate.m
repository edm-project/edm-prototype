function successRate(testHandle)
    % import Data
    randomConfigSuccess = testHandle.dataList.randomConfigSuccess;
    reachabilityConfigSuccess = testHandle.dataList.reachabilityConfigSuccess;
    dexterityConfigSuccess = testHandle.dataList.dexterityConfigSuccess;
    
    randomAll = length(randomConfigSuccess);
    randomSuccess = sum(randomConfigSuccess==1,'all');
    reachAll = length(randomConfigSuccess);
    reachSuccess = sum(reachabilityConfigSuccess==1,'all');
    dexAll = length(randomConfigSuccess);
    dexSuccess = sum(dexterityConfigSuccess==1,'all');

    disp(['random:',num2str(randomSuccess),'/',num2str(randomAll)]);
    disp(['reach:',num2str(reachSuccess),'/',num2str(reachAll)]);
    disp(['dex:',num2str(dexSuccess),'/',num2str(dexAll)]);

end

