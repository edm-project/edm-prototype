function [configSol,info] = ikPredict(robotMapsClass,tform,weights)
%IKPREDICT 
    arguments
        robotMapsClass
        tform
        weights = [1.0 0.1]
    end
    position = tform(1:3,4);
    rotation = tform(1:3,1:3);

    positionRow = robotMapsClass.commonDataSlot.positionRow;
    endEffectorTform = robotMapsClass.commonDataSlot.endEffectorTform;
    testDexterity = robotMapsClass.generateMapSlot.dataList.testDexterity;

    nGlobalPosition = size(positionRow,1);
    nLocalPosition = size(endEffectorTform,3);
    nLocalRotation = size(endEffectorTform,4);

    translationDistance = zeros(nGlobalPosition,1);
    for iGlobalPosition = 1:size(positionRow,1)
        translationDistance(iGlobalPosition) = norm(position-positionRow(iGlobalPosition,:)');
    end
    
    rotationDistance = zeros(nLocalPosition,nLocalRotation);
    for iLocalPostion = 1:nLocalPosition
        for iLocalRotation = 1:nLocalRotation
            rotDistantTmp = rotm2axang(rotation*endEffectorTform(1:3,1:3,iLocalPostion,iLocalRotation)');
            rotationDistance(iLocalPostion,iLocalRotation) = abs(rotDistantTmp(4));
        end
    end

    totalDistance = repmat(translationDistance,1,nLocalPosition,nLocalRotation)*weights(1) + ...
                    permute(repmat(rotationDistance,1,1,nGlobalPosition),[3 1 2])*weights(2);

    minValue = inf;
    minGlobalPosition = 0;
    minLocalPosition = 0;
    minLocalRotation = 0;
    for iGlobalPosition = 1:size(positionRow,1)
        for iLocalPostion = 1:nLocalPosition
            for iLocalRotation = 1:nLocalRotation
                if totalDistance(iGlobalPosition,iLocalPostion,iLocalRotation) <minValue
                    minValue = totalDistance(iGlobalPosition,iLocalPostion,iLocalRotation);
                    minGlobalPosition = iGlobalPosition;
                    minLocalPosition = iLocalPostion;
                    minLocalRotation = iLocalRotation;
                end
            end
        end
    end

    

end

