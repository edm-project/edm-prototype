% according to 2 research_calc experiments, we find that both are correct
% - the same point have a very different Reach-value at the same point(e.g.[0,0,0.8])
% - the same iksolver behaves different
% Therefore I suspect that something went wrong by the generation of Tform
% This script is a comparison of 2 Tforms in the same point

point_searched=[0,0,0.8];

index_searched_1=searchPositionIndex([X,Y,Z],point_searched);

all_Tform=M{index_searched_1};
for iLocalPos=1:length(all_Tform)
    plotTform(all_Tform{iLocalPos}(1:4,1:4),0.01);
    %scatter(all_Tform{iLocalPos}(1,4),all_Tform{iLocalPos}(2,4),all_Tform{iLocalPos}(3,4),'filled','MarkerEdgeColor','g')
    hold on;
end

scatter3(point_searched(1),point_searched(2),point_searched(3),'filled');

% figure;
index_searched_2=searchPositionIndex(robotMapsClass.generateMapSlot.dataList.positionRow,point_searched);
all_Tform2=robotMapsClass.generateMapSlot.dataList.allTform(:,:,:,:,index_searched_2);
for iLocalPos=1:size(all_Tform2,3)
    for iLocalRot=1:size(all_Tform2,4)
        plotTform(all_Tform2(:,:,iLocalPos,iLocalRot),0.009);
        hold on;
    end
    %scatter(all_Tform2(1,4,iLocalPos,1),all_Tform2(2,4,iLocalPos,1),all_Tform2(3,4,iLocalPos,1),'filled','MarkerEdgeColor','r')
end

clear point_searched index_searched_1 all_Tform all_Tform2