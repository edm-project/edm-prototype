% dexterity calculation helper functions

try 
%% automatic diary prepare 
diary(nullAndRotationName)
diary on;

%% calculation part
for iLocalPosition=1:length(flipSolutionLength)
    for iFlipSolution=1:flipSolutionLength(iLocalPosition)
    disp(['[Dexterity Main]: start nullspace motion for (',num2str(iLocalPosition),',', ...
                                                        num2str(iLocalRotation),',:,:,', ...
                                                        num2str(iFlipSolutionList(iFlipSolution)),').']);
    robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotion(iLocalPosition,iLocalRotation,iFlipSolutionList(iFlipSolution));
    
    disp(['[Dexterity Main]: start eeRotation for (',num2str(iLocalPosition),',', ...
                                                        num2str(iLocalRotation),',:,:,', ...
                                                        num2str(iFlipSolutionList(iFlipSolution)),').']);
    robotMapsClass.generateMapSlot.dataList.testDexterity.eeRotation(iLocalPosition,iLocalRotation,iFlipSolutionList(iFlipSolution));
    end
end

catch ME
    % save the log for the result
    disp(['[Dexterity Main catch ERROR]:',ME.message]);
    diary off;
    movefile(nullAndRotationName,[logPath,nullAndRotationName]);
    rethrow(ME);
end

diary off;
movefile(nullAndRotationName,[logPath,nullAndRotationName]);