% do gikMotion
failCounter = 0;
while failCounter <maxFailRepeat
    [calculationFinished,~] = robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotionEmpirical(iLocalPosition,iLocalRotation,iFlip,logPath=logPath);
    if calculationFinished==false
        failCounter=failCounter+1;
    else
        failCounter=0;
    end
end

% do nullspace motion
robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotion(iLocalPosition,iLocalRotation,iFlip);