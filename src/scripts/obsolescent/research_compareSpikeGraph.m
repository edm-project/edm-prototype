% research_compareTform and former experiments shows to us that the both
% data are correct. Now we have to suspect that the original way of
% generating data is incorrect

point_searched=[0,0,0.8];
scale=0.01;

index_searched_1=searchPositionIndex([X,Y,Z],point_searched);

all_Tform=M{index_searched_1};
for iLocalPos=1:length(all_Tform)
    Mtemp=all_Tform{iLocalPos}(1:4,1:4);
    %quiver3(Mtemp(1,4),Mtemp(2,4),Mtemp(3,4),Mtemp(1,3)*scale,Mtemp(2,3)*scale,Mtemp(3,3)*scale,'off','b');
    hold on;
end

%scatter3(point_searched(1),point_searched(2),point_searched(3),'filled');

% figure;
index_searched_2=searchPositionIndex(robotMapsClass.generateMapSlot.dataList.positionRow,point_searched);
have_solu_temp = squeeze(any(robotMapsClass.generateMapSlot.dataList.reachRoundTest.haveSolution,2));
all_Tform2=robotMapsClass.generateMapSlot.dataList.allTform(:,:,:,:,index_searched_2);
for iLocalPos=1:size(all_Tform2,3)
    Mtemp=all_Tform2(:,:,iLocalPos,1);
    if have_solu_temp(iLocalPos,index_searched_2)==true
        quiver3(Mtemp(1,4),Mtemp(2,4),Mtemp(3,4),Mtemp(1,3)*scale,Mtemp(2,3)*scale,Mtemp(3,3)*scale,'off','g');
    else
        quiver3(Mtemp(1,4),Mtemp(2,4),Mtemp(3,4),Mtemp(1,3)*scale,Mtemp(2,3)*scale,Mtemp(3,3)*scale,'off','black');
    end
    
    hold on;
end

clear point_searched index_searched_1 all_Tform all_Tform2