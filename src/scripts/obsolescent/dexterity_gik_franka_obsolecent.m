% dexterity calculation helper functions
%% path helpers
addpath(genpath('./log'));

%% constants
targetedFlipSolution = 2;
maxFlipRepeat = 2;
maxCompareRepeat = 3;
iLocalRotation = 1;

% prepare for fileName
robotTag=robotMapsClass.commonDataSlot.robotParameters.tag;
startTime=datestr(datetime('now'),'yyyymmddHHMM');
dirName = ['DEX_',startTime,'_',robotTag];
gikName = 'record_gikMotion';
nullAndRotationName = 'record_nullspaceAndRotation';
logPath = ['./log/',dirName,'/'];


try 
%% automatic gikMotion prepare 
status = mkdir('./log',dirName);
addpath(genpath('./log'));
if status == 0
    ME = MException('robotMapsClass:fileFailed', ...
            'mkdir failed.', ...
            gridMark);
    throw(ME)
end

diary(gikName)
diary on;

%% 1 (manual because forceCrossing = 1)
% constants
startGlobalPosition = 811;
iLocalPosition = 1;
suggestList = [193;305];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);

% doing gikMotion for different flipSolutions (manual)
for iFlipSolution = 1:length(iFlipSolutionList)
    disp(['[Dexterity Main]: start gikMotion for (',num2str(iLocalPosition),',', ...
                                                  num2str(iLocalRotation),',', ...
                                                  num2str(startGlobalPosition),',', ...
                                                  num2str(sectionNumberList(iFlipSolution)),',', ...
                                                  num2str(iFlipSolutionList(iFlipSolution)),').']);

    robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotion(iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList(iFlipSolution),iFlipSolutionList(iFlipSolution), ...
        forceCrossing=true, ...
        maxAngleTolerance=pi/2, ...
        logPath=logPath,skipGraphDisplay=true); % important extra parameters
    
    for iCompareRepeat=1:maxCompareRepeat
        if iCompareRepeat<=size(suggestList,2)
            suggestPoint=suggestList(iFlipSolutionList(iFlipSolution),iCompareRepeat);
        else
            suggestPoint=0;
        end
        [shouldContinue,iGlobalPositionRepeat,iSectionNumberRepeat] = robotMapsClass.generateMapSlot.dataList.testDexterity.compareGikMotion(iLocalPosition,iLocalRotation,iFlipSolutionList(iFlipSolution),logPath=logPath,skipGraphDisplay=true,suggestPoint=suggestPoint);
        if shouldContinue == false
            disp(['[Dexterity Main]: gikMotion for (',num2str(iLocalPosition),',', ...
                                                  num2str(iLocalRotation),',', ...
                                                  ':,:,', ...
                                                  num2str(iFlipSolutionList(iFlipSolution)),') finished.'])
            break;
        end

        disp(['[Dexterity Main]: repeat gikMotion for (',num2str(iLocalPosition),',', ...
                                                  num2str(iLocalRotation),',', ...
                                                  num2str(iGlobalPositionRepeat),',', ...
                                                  num2str(iSectionNumberRepeat),',', ...
                                                  num2str(iFlipSolutionList(iFlipSolution)),').']);

        robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotion(iLocalPosition,iLocalRotation,iGlobalPositionRepeat,iSectionNumberRepeat,iFlipSolutionList(iFlipSolution), ...
            forceCrossing=true, ...
            maxAngleTolerance=pi/2, ...
            logPath=logPath,skipGraphDisplay=true); % important extra parameters
    end
end

flipSolutionLength(iLocalPosition) = length(iFlipSolutionList);

%% 2
% constants
startGlobalPosition = 581;
iLocalPosition = 2;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);

%% 3
% constants
startGlobalPosition = 582;
iLocalPosition = 3;
suggestList = [431 0;153 772];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);

%% 4
% constants
startGlobalPosition = 516;
iLocalPosition = 4;
suggestList = [861;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);

%% 5
% constants
startGlobalPosition = 581;
iLocalPosition = 5;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);

%% 6 (manual because forceCrossing = 1)
% constants
startGlobalPosition = 581;
iLocalPosition = 6;
suggestList = [462;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);

% doing gikMotion for different flipSolutions (manual)
for iFlipSolution = 1:length(iFlipSolutionList)
    disp(['[Dexterity Main]: start gikMotion for (',num2str(iLocalPosition),',', ...
                                                  num2str(iLocalRotation),',', ...
                                                  num2str(startGlobalPosition),',', ...
                                                  num2str(sectionNumberList(iFlipSolution)),',', ...
                                                  num2str(iFlipSolutionList(iFlipSolution)),').']);

    robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotion(iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList(iFlipSolution),iFlipSolutionList(iFlipSolution), ...
        forceCrossing=true, ...
        maxAngleTolerance=pi/2, ...
        logPath=logPath,skipGraphDisplay=true); % important extra parameters
    
    for iCompareRepeat=1:maxCompareRepeat
        if iCompareRepeat<=size(suggestList,2)
            suggestPoint=suggestList(iFlipSolutionList(iFlipSolution),iCompareRepeat);
        else
            suggestPoint=0;
        end
        [shouldContinue,iGlobalPositionRepeat,iSectionNumberRepeat] = robotMapsClass.generateMapSlot.dataList.testDexterity.compareGikMotion(iLocalPosition,iLocalRotation,iFlipSolutionList(iFlipSolution),logPath=logPath,skipGraphDisplay=true,suggestPoint=suggestPoint);
        if shouldContinue == false
            disp(['[Dexterity Main]: gikMotion for (',num2str(iLocalPosition),',', ...
                                                  num2str(iLocalRotation),',', ...
                                                  ':,:,', ...
                                                  num2str(iFlipSolutionList(iFlipSolution)),') finished.'])
            break;
        end

        disp(['[Dexterity Main]: repeat gikMotion for (',num2str(iLocalPosition),',', ...
                                                  num2str(iLocalRotation),',', ...
                                                  num2str(iGlobalPositionRepeat),',', ...
                                                  num2str(iSectionNumberRepeat),',', ...
                                                  num2str(iFlipSolutionList(iFlipSolution)),').']);

        robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotion(iLocalPosition,iLocalRotation,iGlobalPositionRepeat,iSectionNumberRepeat,iFlipSolutionList(iFlipSolution), ...
            forceCrossing=true, ...
            maxAngleTolerance=pi/2, ...
            logPath=logPath,skipGraphDisplay=true); % important extra parameters
    end
end

flipSolutionLength(iLocalPosition) = length(iFlipSolutionList);

%% 7
% constants
startGlobalPosition = 775;
iLocalPosition = 7;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);

%% 8
% constants
startGlobalPosition = 713;
iLocalPosition = 8;
suggestList = [0;596];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 9
% constants
startGlobalPosition = 482;
iLocalPosition = 9;
suggestList = [574 632;0 0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 10
% constants
startGlobalPosition = 613;
iLocalPosition = 10;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 11
% constants
startGlobalPosition = 481;
iLocalPosition = 11;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);

%% 12
% constants
startGlobalPosition = 481;
iLocalPosition = 12;
suggestList = [370 697;692 0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 13
% constants
startGlobalPosition = 679;
iLocalPosition = 13;
suggestList = [202 460;0 0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 14
% constants
startGlobalPosition = 678;
iLocalPosition = 14;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 15
% constants
startGlobalPosition = 579;
iLocalPosition = 15;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 16
% constants
startGlobalPosition = 415;
iLocalPosition = 16;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 17
% constants
startGlobalPosition = 481;
iLocalPosition = 17;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 18
% constants
startGlobalPosition = 481;
iLocalPosition = 18;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 19
% constants
startGlobalPosition = 512;
iLocalPosition = 19;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);
%% 20
% constants
startGlobalPosition = 512;
iLocalPosition = 20;
suggestList = [0;0];

% import the data and try to calculate the flipped version of it
[robotMapsClass,sectionNumberList,iFlipSolutionList] = dexterSinglePoint(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition, ...
                                                                       targetedFlipSolution,maxFlipRepeat);
% doing gikMotion for different flipSolutions
[robotMapsClass,flipSolutionLength(iLocalPosition)] = dexterGikMotion(robotMapsClass,iLocalPosition,iLocalRotation,startGlobalPosition,sectionNumberList,iFlipSolutionList, ...
                                                                    maxCompareRepeat,logPath,suggestList);

catch ME
    % save the log for the result
    disp(['[Dexterity Main catch ERROR]:',ME.message]);
    diary off;
    movefile(gikName,[logPath,gikName]);
    rethrow(ME);
end

diary off;
movefile(gikName,[logPath,gikName]);