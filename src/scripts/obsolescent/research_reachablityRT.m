function research_reachablityRT(robotMapsIn)
% This function Solve show the reachability graph of robotMapsIn in reachRoundTest
    arguments
        robotMapsIn(1,1) robotMaps
    end
    % export data
    nLocalPos=size(robotMapsIn.generateMapSlot.dataList.reachRoundTest.haveSolution,1);
    positionRow=robotMapsIn.generateMapSlot.dataList.positionRow;

    % calculate reachability
    haveSolutionSumRot = squeeze(any(robotMapsIn.generateMapSlot.dataList.reachRoundTest.haveSolution,2));
    reachablePoints    = sum(haveSolutionSumRot,1);
    reachability = reachablePoints/nLocalPos;

    % export points
    positionRowX=positionRow(:,1);
    positionRowY=positionRow(:,2);
    positionRowZ=positionRow(:,3);

    scatter3(positionRowX,positionRowY,positionRowZ,[],reachability,'filled')
    colorbar;
    x=colormap("jet");
    x=flipud(x);
    colormap(x);
    caxis([0, 1]);
end