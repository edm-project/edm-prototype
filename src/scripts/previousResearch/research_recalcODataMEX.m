% This is a code that directly (but variables here must be assigned) to
% solve the ik for original data again. 

% codeGen used. Be careful when transferring data.

point_searched=[0,0,0.8];
repeatMaxIn=10;

    % prepare data from original
    index_searched=searchPositionIndex([X,Y,Z],point_searched);
    all_Tform=M{index_searched};
    endEffectorName='panda_link8';

    configSolOriginal=zeros(length(all_Tform),repeatMaxIn,length(homeConfiguration(robotColumnIn)));
    exitFlagOriginal=zeros(length(all_Tform),repeatMaxIn);
    haveSolOriginal=zeros(length(all_Tform),1,'logical');

    for iLocalPos=1:length(all_Tform)
        for iRepeatCounter=1:repeatMaxIn
                initialguess=randomConfiguration(robotColumnIn);
                [configSol,solInfo] = ikCodegenTest_mex(endEffectorName, ...
                                         all_Tform{iLocalPos}(1:4,1:4), ...
                                         [0.1 0.1 0.1 0.5 0.5 0.5], ...
                                         initialguess);

                configSolOriginal(iLocalPos,iRepeatCounter,:)=configSol;
                solInfoOriginal(iLocalPos,iRepeatCounter)=solInfo;
                
                disp(['iLocalPos=',num2str(iLocalPos),' rc=',num2str(iRepeatCounter)]);

                if isequal(solInfo.Status,'best available')
                    exitFlagOriginal(iLocalPos,iRepeatCounter)=2;
                    break;
                elseif isequal(solInfo.Status,'success')
                    if checkCollision(robotColumnIn,configSol)==true
                        exitFlagOriginal(iLocalPos,iRepeatCounter)=3;
                        continue;
                    elseif checkJointLimits(robotColumnIn,configSol)==true
                        exitFlagOriginal(iLocalPos,iRepeatCounter)=4;
                        continue;
                    else
                        exitFlagOriginal(iLocalPos,iRepeatCounter)=1;
                        haveSolOriginal(iLocalPos)=true;
                        break;
                    end
                else
                    ME = MException('research_recalcPoint:elseError', ...
                                    'if-else should not run into this branch(normal mode)');
                    throw(ME);
                end
        end
    end


clear point_searched index_searched all_Tform