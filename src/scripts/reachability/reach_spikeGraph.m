
% search a specific point and plot it's spike graph
point_searched=[0,0,0.8];
scale=0.01;

index_searched = searchPositionIndex(robotMapsClass.generateMapSlot.dataList.positionRow,point_searched);
% index_searched = 300;
disp(mat2str(robotMapsClass.generateMapSlot.dataList.positionRow(index_searched,:)));
disp(num2str(index_searched));

solutionFlag=robotMapsClass.generateMapSlot.dataList.mapReachability.dataList.solutionFlag;
allTform=robotMapsClass.generateMapSlot.dataList.allTform(:,:,:,:,index_searched);

for iLocalPos=1:size(allTform,3)
    Mtemp=allTform(:,:,iLocalPos,1);
    if solutionFlag(iLocalPos,index_searched) == 1
        quiver3(Mtemp(1,4),Mtemp(2,4),Mtemp(3,4),Mtemp(1,3)*scale,Mtemp(2,3)*scale,Mtemp(3,3)*scale,'off','g');
    elseif solutionFlag(iLocalPos,index_searched) == 2
        quiver3(Mtemp(1,4),Mtemp(2,4),Mtemp(3,4),Mtemp(1,3)*scale,Mtemp(2,3)*scale,Mtemp(3,3)*scale,'off','black');
    elseif solutionFlag(iLocalPos,index_searched) == 3
        quiver3(Mtemp(1,4),Mtemp(2,4),Mtemp(3,4),Mtemp(1,3)*scale,Mtemp(2,3)*scale,Mtemp(3,3)*scale,'off','yellow');
    else
        error('Should not go into this branch');
    end
    
    hold on;
end

axis equal

clear point_searched allTform scale iLocalPos index_searched Mtemp solutionFlag