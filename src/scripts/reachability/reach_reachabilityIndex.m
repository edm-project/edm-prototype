function research_reachability(robotMapsIn)
% This function Solve show the reachability graph of robotMapsIn in reachMap
    arguments
        robotMapsIn(1,1) robotMaps
    end
    
    % export data
    mapReachability=robotMapsIn.generateMapSlot.dataList.mapReachability;
    positionRow=robotMapsIn.generateMapSlot.dataList.positionRow;

    % export points
    positionRowX=positionRow(:,1);
    positionRowY=positionRow(:,2);
    positionRowZ=positionRow(:,3);

    scatter3(positionRowX,positionRowY,positionRowZ,40,mapReachability.dataList.reachabilityIndex,'filled')
    colorbar;
    x=colormap("jet");
    x=flipud(x);
    matrixHeight=size(x,1);
    x=x(floor(matrixHeight*0.1):floor(matrixHeight*0.9),:);
    colormap(x);
    caxis([0, 100]);
end

