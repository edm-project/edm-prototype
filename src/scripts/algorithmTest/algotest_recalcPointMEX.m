function [configSolSlot,exitFlagSlot,haveSolSlot,solInfoSlot]=algotest_recalcPointMEX(robotMapsIn,positionIn,repeatMaxIn)
%RESEARCH_RECALCPOINT use IK solver to resolve point in robotMapsIn
% use MEX to speed up. But robot are restricted in the ikCodegenTest!
    arguments
        robotMapsIn(1,1) robotMaps
        positionIn(3,1) double
        repeatMaxIn(1,1) double
    end
    robotColumn=robotMapsIn.generateMapSlot.dataList.robotParameters.robotColumn;
    %ik=inverseKinematics('RigidBodyTree',robotColumn);

    positionIndex=searchPositionIndex(robotMapsIn.generateMapSlot.dataList.positionRow,positionIn);
    
    allTform=robotMapsIn.generateMapSlot.dataList.allTform(:,:,:,:,positionIndex);
    endEffectorName='panda_link8';

    configSolSlot=zeros(size(allTform,3),size(allTform,4),repeatMaxIn,length(homeConfiguration(robotColumn)));
    exitFlagSlot=zeros(size(allTform,3),size(allTform,4),repeatMaxIn);
    haveSolSlot=zeros(size(allTform,3),size(allTform,4),'logical');
    % 0 = unused; 1 = accepted; 2 = noSolution; 3 = collsion; 4 = jointLimits
    weightsIn=[0.1 0.1 0.1 0.5 0.5 0.5];

    for iLocalPos=1:size(allTform,3)
        for iLocalRot=1:size(allTform,4)
            for iRepeatCounter=1:repeatMaxIn
                initialguess=randomConfiguration(robotColumn);
                [configSol,solInfo] = ikCodegenTest_mex(endEffectorName, ...
                                         allTform(:,:,iLocalPos,iLocalRot), ...
                                         weightsIn, ...
                                         initialguess);

                configSolSlot(iLocalPos,iLocalRot,iRepeatCounter,:)=configSol;
                solInfoSlot(iLocalPos,iLocalRot,iRepeatCounter)=solInfo;
                
                disp(['iLocalPos=',num2str(iLocalPos),' iLocalRot=',num2str(iLocalRot),' rc=',num2str(iRepeatCounter)]);

                if isequal(solInfo.Status,'best available')
                    exitFlagSlot(iLocalPos,iLocalRot,iRepeatCounter)=2;
                    break;
                elseif isequal(solInfo.Status,'success')
                    if checkCollision(robotColumn,configSol)==true
                        exitFlagSlot(iLocalPos,iLocalRot,iRepeatCounter)=3;
                        continue;
                    elseif checkJointLimits(robotColumn,configSol)==true
                        exitFlagSlot(iLocalPos,iLocalRot,iRepeatCounter)=4;
                        continue;
                    else
                        exitFlagSlot(iLocalPos,iLocalRot,iRepeatCounter)=1;
                        haveSolSlot(iLocalPos,iLocalRot)=true;
                        break;
                    end
                else
                    ME = MException('research_recalcPoint:elseError', ...
                                    'if-else should not run into this branch(normal mode)');
                    throw(ME);
                end
            end
        end
    end
end

