function configSolOut = algotest_NullDirStep(robotParameters,configSolIn,configSolInitial,timeStep,factor,options)
% ALGOTEST_NULLFORWARD make one step of nullmotion in cartesian space.
    arguments
        robotParameters(1,1) struct
        configSolIn(:,1) double
        configSolInitial(:,1) double
        timeStep(1,1) double
        factor(1,1) double
        options.direction(1,1) double = 1
        options.projector(1,:) char {mustBeMember(options.projector,{'CLIK','Damp'})} = 'Damp'
        options.damping(1,1) double = 0.01
    end

    % get information needed
    robotColumn=robotParameters.robotColumn;
    endEffectorName=robotParameters.endEffectorName;
    elbowName=robotParameters.elbowName;
    shoulderName=robotParameters.shoulderName;
    baseName=robotParameters.baseName;

    % getParameters needed
    nAngle = length(homeConfiguration(robotColumn));
    nElbow = 4;

    % reference Vector calculation
    p_shoulder = getTransform(robotColumn,configSolIn,shoulderName,baseName);
    p_shoulder = p_shoulder(1:3,4);
    p_elbow = getTransform(robotColumn,configSolIn,elbowName,baseName);
    p_elbow = p_elbow(1:3,4);
    p_endEffetor = getTransform(robotColumn,configSolIn,endEffectorName,baseName);
    p_endEffetor = p_endEffetor(1:3,4);
    generalAxis = p_endEffetor - p_shoulder;
    upperArm = p_elbow - p_shoulder;
    normalVector = cross(generalAxis,upperArm);
    normalVector = normalVector/norm(normalVector)/4;
    if options.direction==-1
        normalVector=-normalVector;
    end

    % calculate gradient Vector
    jacobianElbow = geometricJacobian(robotColumn,configSolIn,elbowName);
    gradientVector = pinv(jacobianElbow(4:6,1:nElbow-1))*normalVector;
    projectionVector = [gradientVector;zeros(nAngle-nElbow+1,1)];
    projectionVector = projectionVector / norm(projectionVector);

    % calculate the projection core
    jacobianEE = geometricJacobian(robotColumn,configSolIn,endEffectorName);
    if isequal(options.projector,'CLIK')
        P = (eye(7) - pinv(jacobianEE)*jacobianEE);
    elseif isequal(options.projector,'Damp')
        jacobianInverse = (jacobianEE'*pinv(jacobianEE*jacobianEE' + options.damping*eye(6)));
        P = eye(7) - pinv(jacobianInverse*jacobianInverse');
    end

    % nullspace motion with nullspace of jacobian
    configSolOut = configSolIn+timeStep*factor*P*projectionVector;

    % codegen helper
    configSolOut = reshape(configSolOut,[7,1]);

    % get Cartesian error form the previous step
    cartesianError = cartesianDifference(robotParameters,configSolInitial,configSolOut);

    % Compensate the cartesian error
    jacobianEEAfter = geometricJacobian(robotColumn,configSolOut,endEffectorName);
    jacobianInverseAfter = pinv(jacobianEEAfter);
    configSolOut = configSolOut + jacobianInverseAfter*cartesianError;
end

