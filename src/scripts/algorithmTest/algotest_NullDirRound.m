function [configSolCol,haveSolutionCol] = algotest_NullDirRound(robotParameters,configSol,nSection,options)
    arguments
        robotParameters(1,1) struct
        configSol(:,1) double
        nSection(1,1) double
        options.timeStep(1,1) double
        options.increaseStep(1,1) double
        options.maxStep(1,1) double
    end

    stepCount = 0;

    while true
        % the iteration counter
        stepCount = stepCount + options.increaseStep;
        if stepCount > options.maxStep
            break
        end

        % initialize the result
        configSolCol = zeros(length(homeConfiguration(robotParameters.robotColumn)),nSection);
        haveSolutionCol = zeros(nSection,1);

        % collect the information
        [configSolColFix,~,sectionColFix] = algotest_NullDirFix(robotParameters, ...
                                                            configSol, ...
                                                            options.timeStep, ...
                                                            nSection, ...
                                                            stepCount, ...
                                                            1);

        [configSolColFixR,~,sectionColFixR] = algotest_NullDirFix(robotParameters, ...
                                                            configSol, ...
                                                            options.timeStep, ...
                                                            nSection, ...
                                                            stepCount, ...
                                                            -1);

        [~,sectionNumber] = dexteritySection(robotParameters,configSol,nSection);
        
        % sort the information(之后也许可以更加聪明一点，而不是这样选)
        for iSection = 1:nSection
            if sectionNumber <= 12
                if iSection <= 12 && iSection >=sectionNumber  % choose the forward
                    if sum(sectionColFix==iSection,'all')~=0
                        firstIndex = find(sectionColFix==iSection);
                        configSolCol(:,iSection)=configSolColFix(:,firstIndex(1));
                        haveSolutionCol(iSection)=1;
                    end
                else
                    if sum(sectionColFixR==iSection,'all')~=0  % choose the reverse
                        firstIndex = find(sectionColFixR==iSection);
                        configSolCol(:,iSection)=configSolColFixR(:,firstIndex(1));
                        haveSolutionCol(iSection)=1;
                    end
                end
            else
                if iSection >= 13 && iSection <= sectionNumber  % choose the forward
                    if sum(sectionColFixR==iSection,'all')~=0
                        firstIndex = find(sectionColFixR==iSection);
                        configSolCol(:,iSection)=configSolColFixR(:,firstIndex(1));
                        haveSolutionCol(iSection)=1;
                    end
                else
                    if sum(sectionColFix==iSection,'all')~=0  % choose the reverse
                        firstIndex = find(sectionColFix==iSection);
                        configSolCol(:,iSection)=configSolColFix(:,firstIndex(1));
                        haveSolutionCol(iSection)=1;
                    end
                end
            end
        end

        % determine the loop end
        if all(haveSolutionCol)
            break;
        end
    end
end

