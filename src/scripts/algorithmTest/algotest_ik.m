function [configSol,solInfo]=algotest_ik(robotIn,linkIn,tform)
% This function Solve IK problem quickly for a robot+link+tform
    arguments
        robotIn(1,1) rigidBodyTree
        linkIn(1,:) char
        tform(4,4) double
    end
    ik=inverseKinematics('RigidBodyTree',robotIn);
    initialguess=randomConfiguration(robotIn);
    [configSol,solInfo] = ik(linkIn,tform,[0.1 0.1 0.1 0.5 0.5 0.5],initialguess);

    show(robotIn,configSol);
    hold on;
    plotTform(tform,0.15);
end