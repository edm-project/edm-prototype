function [configSolCol,angleCol,sectionCol] = algotest_NullDirFix(robotParameters,configSol,timeStep,nSection,maxStep,direction)
%NULLSPACEFIXED moving nullspace with a fixed step and time
% [robotParameters]: defined as previous
% [configSol]: the joint configuration
% [timeStep]: the step length of each nullmotion
% [nSection]: dexterity Section, defined in testDexterity
% [maxStep]: maximum Step of calculation
%     arguments
%         robotParameters(1,1) struct
%         configSol(:,1) double
%         timeStep(1,1) double {mustBePositive}
%         nSection(1,1) double {mustBeInteger,mustBePositive}
%         maxStep(1,1) double {mustBeInteger,mustBePositive}
%     end

    persistent factor;
    if isempty(factor)
        factor = 1;
    end
    robotColumn=robotParameters.robotColumn;
    endEffectorName=robotParameters.endEffectorName;

    % pre-allocation of output
    configSolCol=zeros(length(configSol),maxStep);
    angleCol=zeros(maxStep,1);
    sectionCol=zeros(maxStep,1);
    
    % initializations for the first round
    configSolNow=configSol;
    [angle,sectionNumber] = dexteritySection(robotParameters,configSol,nSection);
    configSolCol(:,1)=configSol;
    angleCol(1) = angle; 
    sectionCol(1) = sectionNumber; 

    % round 2 to maximum round
    for iConfigSol=2:maxStep
        % go for one step
        repeatflag = true;
        while repeatflag == true
            configSolNext = algotest_NullDirStep(robotParameters,configSolNow,configSol,timeStep,factor,direction=direction);
%             [angle,~] = dexteritySection(robotParameters,configSolNext,nSection);
% 
%             % if the step is not the forward direction
%             if circularDifference(angle,angleCol(iConfigSol-1),2*pi) < 0
%                 % go reverse for one step
%                 configSolNextTemp = nullspaceStep(robotParameters,configSolNow,configSol,-timeStep);
%                 [angleTemp,~] = dexteritySection(robotParameters,configSolNextTemp,nSection);
% 
%                 % If two steps are all backward, only record the "most near to forward" one
%                 if circularDifference(angleTemp,angle,2*pi) > 0
%                     configSolNext=configSolNextTemp;
%                 end
%             end

            % check if the step is too far from the initial state
            configSolNext = compensateCartesianDifference(robotParameters,configSolNext,configSol);

            % normalize the angle into a fixed section (instead of having angles like 4*pi or more)
            configSolNext = jointNormalize(robotParameters,configSolNext);

            % decide if the factor need to be changed
            jacobianEE = geometricJacobian(robotColumn,configSolNext,endEffectorName);

            s=svd(jacobianEE);
            smin=s(end);
            [~,sectionBefore] = dexteritySection(robotParameters,configSolNow,nSection);
            [~,sectionAfter] = dexteritySection(robotParameters,configSolNext,nSection);
            difference = abs(circularDifference(sectionAfter,sectionBefore,nSection));
            if difference==0 && smin <= 0.1
                factor = factor + 1;
            elseif difference>=2
                factor = factor - 0.1;
                continue;
            else
                factor = 1;
            end
            break;
        end
        % calculate the real angle and sectionNumber
        [angle,sectionNumber] = dexteritySection(robotParameters,configSolNext,nSection);

        % collect the data
        configSolCol(:,iConfigSol) = configSolNext; 
        angleCol(iConfigSol) = angle; 
        sectionCol(iConfigSol) = sectionNumber; 

        % update the configSolNow for the next iteration
        configSolNow = configSolNext;
    end
end


