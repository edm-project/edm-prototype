function configSolOut = algotest_DampStep(robotParameters,configSolIn,configSolInitial,timeStep)
% ALGOTEST_NULLFORWARD make one step of nullmotion in cartesian space.
%     arguments
%         robotParameters(1,1) struct
%         configSolIn(:,1) double
%         configSolInitial(:,1) double
%         timeStep(1,1) double
%     end

    % get information needed
    robotColumn=robotParameters.robotColumn;
    endEffectorName=robotParameters.endEffectorName;

    % nullspace motion with nullspace of jacobian
    jacobianBefore = geometricJacobian(robotColumn,configSolIn,endEffectorName);
    robustInverse =  jacobianBefore'*pinv(jacobianBefore*jacobianBefore' + 0.01*eye(6));
    configSolOut = configSolIn+timeStep*(eye(7) - pinv(robustInverse * robustInverse'))*[1;0;0;0;0;0;0];

    % codegen helper
    configSolOut = reshape(configSolOut,[7,1]);

    % get Cartesian error form the previous step
    cartesianError = cartesianDifference(robotParameters,configSolInitial,configSolOut);

    % Compensate the cartesian error
    jacobianAfter = geometricJacobian(robotColumn,configSolOut,endEffectorName);
    configSolOut = configSolOut + pinv(jacobianAfter)*cartesianError;
end

