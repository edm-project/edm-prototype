function [configSolCol,angleCol,sectionCol] = algotest_nullstep(robotParameters,configSol,timeStep,nSection,maxStep)
%ALGOTEST_NULLSTEP calculate Nullspace stepping with 
    arguments
        robotParameters(1,1) struct
        configSol(:,1) double
        timeStep(1,1) double {mustBePositive}
        nSection(1,1) double {mustBeInteger,mustBePositive}
        maxStep(1,1) double {mustBeInteger,mustBePositive}
    end

    % pre-allocation of output
    configSolCol=zeros(length(configSol),maxStep);
    angleCol=zeros(maxStep,1);
    sectionCol=zeros(maxStep,1);
    
    % initializations
    configSolNow=configSol;
    configSolCol(:,1)=configSol;
    [angle,sectionNumber] = dexteritySection(robotParameters,configSol,nSection);
    angleCol(1) = angle; % collect2
    sectionCol(1) = sectionNumber; % collect3

    for iConfigSol=2:maxStep

        configSolNext = algotest_nullForward(robotParameters,configSolNow,configSol,timeStep);
%         algotest_mustBeSameTform(robotParameters,configSol,configSolNext); % check if tform too far away
        [angle,sectionNumber] = dexteritySection(robotParameters,configSolNext,nSection);
        if circularDifference(angle,angleCol(iConfigSol-1),2*pi) < 0
            configSolNextTemp = algotest_nullForward(robotParameters,configSolNow,configSol,-timeStep);        
            [angleTemp,sectionNumberTemp] = dexteritySection(robotParameters,configSolNextTemp,nSection);
            if circularDifference(angleTemp,angle,2*pi) > 0
                angle=angleTemp;
                sectionNumber=sectionNumberTemp;
                configSolNext=configSolNextTemp;
            end
        end
        
        algotest_mustBeSameTform(robotParameters,configSolNext,configSol); % check if tform too far away

        configSolCol(:,iConfigSol) = configSolNext; % collect1
        angleCol(iConfigSol) = angle; % collect2
        sectionCol(iConfigSol) = sectionNumber; % collect3
        configSolNow = configSolNext;
    end

end

