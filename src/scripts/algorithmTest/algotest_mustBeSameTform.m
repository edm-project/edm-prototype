function algotest_mustBeSameTform(robotParameters,configSol1,configSol2)
    arguments
        robotParameters(1,1) struct
        configSol1(:,1) double
        configSol2(:,1) double
    end

    robotColumn=robotParameters.robotColumn;
    baseName=robotParameters.baseName;
    endEffectorName=robotParameters.endEffectorName;

    tform1 = getTransform(robotColumn,configSol1,endEffectorName,baseName);
    tform2 = getTransform(robotColumn,configSol2,endEffectorName,baseName);

    if norm(tform1-tform2) > 1e-3
        ME = MException('robotMapsClass:algotest_mustBeSameTform', ...
            'norm of the tform is too far away [%d]', ...
            norm(tform1-tform2));
        throw(ME)
    end
end

