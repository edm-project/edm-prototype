function cartesianError = algotest_cartesianError(robotParameters,configSolIn,configSolOut)
%ALGOTEST_CARTESIANERROR get the cartesian Error with different expressions
    arguments
        robotParameters(1,1) struct
        configSolIn(:,1) double
        configSolOut(:,1) double
    end

    % get information needed
    robotColumn=robotParameters.robotColumn;
    baseName=robotParameters.baseName;
    endEffectorName=robotParameters.endEffectorName;

    % get Cartesian error
    tformIn = getTransform(robotColumn,configSolIn,endEffectorName,baseName);
    tformBuffer = getTransform(robotColumn,configSolOut,endEffectorName,baseName);

    % we want to move the arm back to the initial state, therefore old-new
    positionDifference = tformIn(1:3,4) - tformBuffer(1:3,4);
    rotmDifference = tformIn(1:3,1:3)*tformBuffer(1:3,1:3)';
    eulXYZ = rotm2eul(rotmDifference,'XYZ');
    cartesianError = [eulXYZ';positionDifference];
end

