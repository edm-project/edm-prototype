function configSolOut = algotest_nullForward(robotParameters,configSolIn,configSolInitial,timeStep)
%ALGOTEST_NULLFORWARD Directly one step forward with nullmotion
    arguments
        robotParameters(1,1) struct
        configSolIn(:,1) double
        configSolInitial(:,1) double
        timeStep(1,1) double
    end

    % get information needed
    robotColumn=robotParameters.robotColumn;
    endEffectorName=robotParameters.endEffectorName;

    % nullspace motion
    jacobianBefore=geometricJacobian(robotColumn,configSolIn,endEffectorName);
    nullJacobian = null(jacobianBefore);
    nullJacobian = nullJacobian/norm(nullJacobian);
    configSolOut = configSolIn+nullJacobian*timeStep;

    % get Cartesian error
    cartesianError = algotest_cartesianError(robotParameters,configSolInitial,configSolOut);

    % Compensate the cartesian error
    jacobianAfter=geometricJacobian(robotColumn,configSolOut,endEffectorName);
    configSolOut = configSolOut + pinv(jacobianAfter)*cartesianError;
end

