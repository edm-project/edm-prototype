function testSave(testHandle)
    robotTag=testHandle.robotMapsClassHandle.commonDataSlot.robotParameters.tag;
    robotMapMarks='';
    for iMark = 1:length(testHandle.mark)
        if isequal(testHandle.mark{iMark},'twoPointRRTTest')
            robotMapMarks='twoPointRRT';
        end
    end
    saveTime=datestr(datetime('now'),'yyyymmddHHMM');

    save(['data/savedRobotMaps/',robotTag,'_',robotMapMarks,'_',saveTime],'testHandle');
end

