function fileSave(robotMapsClass)
    arguments
        robotMapsClass(1,1) robotMaps
    end

    robotTag=robotMapsClass.commonDataSlot.robotParameters.tag;
    robotMapMarks='';
    for iMarks=2:length(robotMapsClass.generateMapSlot.mark)
        robotMapMarks=[robotMapMarks,'_',robotMapsClass.generateMapSlot.mark{iMarks}]; %#ok<AGROW> 
    end
    saveTime=datestr(datetime('now'),'yyyymmddHHMM');

    save(['data/savedRobotMaps/',robotTag,robotMapMarks,'_',saveTime],'robotMapsClass');
end

