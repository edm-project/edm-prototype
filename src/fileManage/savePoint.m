function savePoint(robotMapsClass,mission,comment,logPath)
    arguments
        robotMapsClass(1,1) robotMaps
        mission(1,:) char
        comment(1,:) char
        logPath(1,:) char
    end

    robotTag=robotMapsClass.commonDataSlot.robotParameters.tag;
    saveTime=datestr(datetime('now'),'yyyymmddHHMM');

    save([logPath,robotTag,'_',mission,'_',saveTime,'(',comment,')'],'robotMapsClass');
end

