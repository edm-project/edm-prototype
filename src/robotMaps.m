classdef robotMaps < handle
    % structure for the whole robot generation
    properties (SetAccess=immutable)
        processNameList(1,:) cell {mustBeValidProcessName}
    end
    properties (SetAccess = private)
        commonDataSlot(1,1) commonData
        assignRobotSlot(1,1) assignRobot
        generateGridSlot(1,1) generateGrid
        filterGridSlot(1,1) filterGrid
        generateDirectionSlot(1,1) generateDirection
        generateMapSlot(1,1) generateMap
    end
    
    methods (Access=public)
        function obj = robotMaps()
            %GENERAL_DATATRANS empty constructor with empty class
            obj.processNameList={'assignRobot','generateGrid','filterGrid','generateDirection','generateMap'};

            obj.commonDataSlot=commonData();
            obj.assignRobotSlot=assignRobot();
            obj.generateGridSlot=generateGrid();
            obj.filterGridSlot=filterGrid();
            obj.generateDirectionSlot=generateDirection();
            obj.generateMapSlot=generateMap();
        end

        function initialize(obj,processNameIn)
            arguments
                obj(1,1) robotMaps
                processNameIn(1,:) char {mustBeValidProcessName} 
            end
            processIndex = getIndexCell(obj.processNameList,processNameIn);
            
            % check the mustAllowInitialize for data Intergrity
            obj.commonDataSlot.mustAllowInitialize(processNameIn);
            obj.(append(obj.processNameList{processIndex},"Slot")).mustAllowInitialize();

            % initialize the commonData and process
            obj.commonDataSlot.initializeProcess(processNameIn);
            obj.(append(obj.processNameList{processIndex},"Slot")).initialize(obj.commonDataSlot,obj);
        end

        function finalize(obj,processNameIn)
            arguments
                obj(1,1) robotMaps
                processNameIn(1,:) char {mustBeValidProcessName} 
            end
            processIndex = getIndexCell(obj.processNameList,processNameIn);

            % check the mustAllowInitialize for data Intergrity
            obj.commonDataSlot.mustAllowFinalize(processNameIn);
            obj.(append(obj.processNameList{processIndex},"Slot")).mustAllowFinalize();

            % finalize the commonData and process
            obj.commonDataSlot.finalizeProcess(processNameIn);
            obj.(append(obj.processNameList{processIndex},"Slot")).finalize();
        end
    end
end

function mustBeValidProcessName(processNameIn)
    mustBeMember(processNameIn,{'assignRobot','generateGrid','filterGrid','generateDirection','generateMap'})
end
