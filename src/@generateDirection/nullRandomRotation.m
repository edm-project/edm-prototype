function nullRandomRotation(obj,nRotation)
%NULLRANDOM use null() to generate frame xyz and use a randomc sstart point
% z-rotations are evenly distributed
%   
% nRotation: numbers of rotation (along z axis) on a single rotation (pose) 
    arguments
        obj(1,1) generateDirection
        nRotation(1,1) double {mustBePositive}
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('nullRandomRotation');

    % check if endEffectorAngle exists or is nonempty
    if ~isfield(obj.dataList,'endEffectorAngle')
        ME = MException('generateDirection:nullRandomRotation:notActive', ...
                        'endEffectorAngle is not a field of dataList');
        throw(ME)
    end
    mustBeNonempty(obj.dataList.endEffectorAngle)

    % convert spherical to cartesian xyz
    [x_temp,y_temp,z_temp]=sph2cart(obj.dataList.endEffectorAngle.azimuth, ...
                                    obj.dataList.endEffectorAngle.elevation, ...
                                    obj.dataList.endEffectorAngle.radius);

    % pre-allocation
    tform_temp=zeros(4,4,length(x_temp),nRotation);

    % generate z col of Rotation matrix
    translationColumn=[x_temp;y_temp;z_temp];
    matrixRzColumn=-[x_temp;y_temp;z_temp];
    matrixRzColumn=matrixRzColumn/norm(matrixRzColumn(:,1));

    % generation and randomization of Rotation matrix
    for iMatrixR=1:length(x_temp)
        % get one vector from nullspace of matrix z. Because getting nullspace is solving Az=0,
        % which is exactly 1.orthogonal to z / 2. norm equals 1 
        matrixRxColumn = null(matrixRzColumn(:,iMatrixR)');
        matrixRxColumn = matrixRxColumn(:,1);

        % get the y vector
        matrixRyColumn = cross(matrixRzColumn(:,iMatrixR),matrixRxColumn);

        % put everything back in a matrix
        matrixR=[matrixRxColumn,matrixRyColumn,matrixRzColumn(:,iMatrixR)];

         % make a random z-rotation
        matrixR=axang2rotm([matrixRzColumn(:,iMatrixR)',rand(1,1)*2*pi])*matrixR;
        % now matrixR is a rotation matrix with fixed z and random x-y distribution

        rotationScalar=linspace(0,2*pi,nRotation+1);
        for iRotation=1:(length(rotationScalar)-1)
            % evenly rotate initial matrixR
            matrixCandidate=axang2rotm([matrixRzColumn(:,iMatrixR)',rotationScalar(iRotation)])*matrixR;
            
            % convert rotm to tform and add translation
            matrixCandidate=rotm2tform(matrixCandidate);
            matrixCandidate(1:3,4)=translationColumn(:,iMatrixR);

            % store the result
            tform_temp(:,:,iMatrixR,iRotation)=matrixCandidate;       
        end
    end

    % prepare for update
    markIn='nullRandomRotation';
    dataListIn.endEffectorTform=tform_temp;
    metaListIn.nRotation=nRotation;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

