function alwaysNorthRotation(obj,nRotation)
%ALWAYSNORTHROTATION always point the 1 rotation's x axis to north. If at
% north pole, then point the 1st x axis to 
% z-rotations are evenly distributed
%   
% nRotation: numbers of rotation (along z axis) on a single rotation (pose) 
    arguments
        obj(1,1) generateDirection
        nRotation(1,1) double {mustBePositive}
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('alwaysNorthRotation');

    % check if endEffectorAngle exists or is nonempty
    if ~isfield(obj.dataList,'endEffectorAngle')
        ME = MException('generateDirection:nullRandomRotation:notActive', ...
                        'endEffectorAngle is not a field of dataList');
        throw(ME)
    end
    mustBeNonempty(obj.dataList.endEffectorAngle)

    % convert spherical to cartesian xyz
    [x_temp,y_temp,z_temp]=sph2cart(obj.dataList.endEffectorAngle.azimuth, ...
                                    obj.dataList.endEffectorAngle.elevation, ...
                                    obj.dataList.endEffectorAngle.radius);

    % pre-allocation
    tform_temp=zeros(4,4,length(x_temp),nRotation);

    % generate z col of Rotation matrix
    translationColumn=[x_temp;y_temp;z_temp];
    matrixRzColumn=-[x_temp;y_temp;z_temp];
    matrixRzColumn=matrixRzColumn/norm(matrixRzColumn(:,1));

    % generation and randomization of Rotation matrix
    for iMatrixR=1:length(x_temp)
        % Do very simple numeric Tranformation. 
        matrixRzColumnTemp = matrixRzColumn(:,iMatrixR);
        matrixRzColumnX = matrixRzColumnTemp(1);
        matrixRzColumnY = matrixRzColumnTemp(2);
        matrixRzColumnZ = matrixRzColumnTemp(3);

        matrixRxColumnZ = norm([matrixRzColumnX matrixRzColumnY]);
        if abs(matrixRxColumnZ) < eps
            matrixRxColumnX = 1;
            matrixRxColumnY = 0;
        else
            if matrixRzColumnZ < 0
                matrixRxColumnX = abs(matrixRzColumnZ) / matrixRxColumnZ * matrixRzColumnX;
                matrixRxColumnY = abs(matrixRzColumnZ) / matrixRxColumnZ * matrixRzColumnY;
            else
                matrixRxColumnX = - abs(matrixRzColumnZ) / matrixRxColumnZ * matrixRzColumnX;
                matrixRxColumnY = - abs(matrixRzColumnZ) / matrixRxColumnZ * matrixRzColumnY;
            end
        end
        
        % get the x vector
        matrixRxColumn = [matrixRxColumnX;matrixRxColumnY;matrixRxColumnZ];

        % get the y vector
        matrixRyColumn = cross(matrixRzColumn(:,iMatrixR),matrixRxColumn);

        % put everything back in a matrix
        matrixR=[matrixRxColumn,matrixRyColumn,matrixRzColumn(:,iMatrixR)];

        rotationScalar=linspace(0,2*pi,nRotation+1);
        for iRotation=1:(length(rotationScalar)-1)
            % evenly rotate initial matrixR
            matrixCandidate=axang2rotm([matrixRzColumn(:,iMatrixR)',rotationScalar(iRotation)])*matrixR;
            
            % convert rotm to tform and add translation
            matrixCandidate=rotm2tform(matrixCandidate);
            matrixCandidate(1:3,4)=translationColumn(:,iMatrixR);

            % store the result
            tform_temp(:,:,iMatrixR,iRotation)=matrixCandidate;       
        end
    end

    % prepare for update
    markIn='alwaysNorthRotation';
    dataListIn.endEffectorTform=tform_temp;
    metaListIn.nRotation=nRotation;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

