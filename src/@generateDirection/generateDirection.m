classdef generateDirection < handle
    %GENERATEDIRECTION Process generateDirection
    %   used for generate the directions(rotations) on each position
    properties (SetAccess = immutable)
    end 
    % end of immutable attributes

    properties (SetAccess = private)
        isActive(1,1) logical
        commonDataHandle(1,1) commonData
        robotMapsHandle(1,1)

        mark(1,:) cell {mustBeValidMarksNullPrepare}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
    end
    % end of the private attributes
    
    methods
        function obj = generateDirection()
            obj.isActive=false;
            obj.mark={'null'};
        end % end of the constructor
    end
    % end of the constructor

    methods (Access=private)
        function mustAllowCall(obj,callMarkIn)
        % mustAllowCall: throw error if function current call is not allowed
            arguments
                obj(1,1) generateDirection
                callMarkIn(1,:) char {mustBeValidMarks}
            end

            % check if status Active
            mustIsActiveState(obj,true);

            % allow only one of {'saffAndKuijlaars'} 
            mustMutuallyExclude(obj,callMarkIn,{'saffAndKuijlaars'});

            % allow only one of {'nullRandomRotation','alwaysNorthRotation'} 
            mustMutuallyExclude(obj,callMarkIn,{'nullRandomRotation','alwaysNorthRotation'});

            % check if {'nullRandomRotation','alwaysNorthRotation'} is after {'saffAndKuijlaars'} 
            mustInSequence(obj,callMarkIn,{'nullRandomRotation','alwaysNorthRotation'},{'saffAndKuijlaars'});
        end

        function updateData(obj,markIn,metaListIn,dataListIn)
        % update the data in this procees, called in functional methods
            arguments
                obj(1,1) generateDirection
                markIn(1,:) char {mustBeValidMarks}
                metaListIn(1,1) struct
                dataListIn(1,1) struct {mustBeValidDataList}
            end
            % check if this call is allowed
            obj.mustAllowCall(markIn);

            % update mark
            if isequal(obj.mark,{'prepared'})
                obj.mark={markIn};
            else
                obj.mark=[obj.mark,{markIn}];
            end

            %update metaList and dataList
            obj.metaList{length(obj.metaList)+1}=metaListIn;
            obj.dataList=combineStruct(obj.dataList,dataListIn);
        end
    end
    % end of the private methods

    methods (Access={?robotMaps})
        function mustAllowInitialize(obj)
        % mustAllowInitialze: check conditions for Initialize
            
            % isActive must equal false, otherwise means already initialized
            mustIsActiveState(obj,false)

            % marks must be 'null', otherwise means already finalized
            mustBeMark(obj,'null',true);
        end

        function mustAllowFinalize(obj)
        % mustAllowFinalize: check conditions for Finalize

            % isActive must be true, otherwise already finalized or not initialized
            mustIsActiveState(obj,true)

            % marks must be not 'prepared', which means nothing was done
            mustBeMark(obj,'prepared',false);
        end

        function initialize(obj,commonDataHandleIn,robotMapsHandleIn)
        % initalize this process
            arguments
                obj(1,1) generateDirection
                commonDataHandleIn(1,1) commonData
                robotMapsHandleIn(1,1) robotMaps
            end

            % check if initialization valid
            obj.mustAllowInitialize();

            obj.mark={'prepared'};
            obj.commonDataHandle=commonDataHandleIn;
            obj.robotMapsHandle=robotMapsHandleIn;
            obj.isActive=true;
        end

        function finalize(obj)
        % finialize this process

            % check if finalization valid
            obj.mustAllowFinalize();

            % set isActive state, lock write functions
            obj.isActive=false;

            % write data into commonData
            obj.commonDataHandle.updateDataGenerateDirection(obj.dataList.endEffectorTform);
        end
    end
    % end of the robotMaps friend methods

    methods (Access=public)
        saffAndKuijlaars(obj,radius,nDirection) % Functional method: generate endEffectorAngle according to saffAndKuijlaars

        nullRandomRotation(obj,nRotation) % Functional method: generate endEffectorTform by random distribution
        alwaysNorthRotation(obj,nRotation) % Functional method: generate endEffectorTform by setting 1sy  vector to north pole 
    end
    % end of the public functional method (definition in same folder)
end


function mustBeValidMarks(marksIn)
% Check for Valid input as mark.
    mustBeMember(marksIn,{'saffAndKuijlaars','nullRandomRotation','alwaysNorthRotation'})
end
 
function mustBeValidMarksNullPrepare(marksIn)
% Check for Valid input as mark. Allow null and prepare.
    mustBeMember(marksIn,{'null','prepared','saffAndKuijlaars','nullRandomRotation','alwaysNorthRotation'})
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{'endEffectorAngle','endEffectorTform'})
end

