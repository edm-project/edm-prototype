function saffAndKuijlaars(obj,radius,nDirection)
%SAFFANDKUIJLAARS generate the direction according to the saffAndKuijlaars algorithm
%   
% nDirection: number of directions on one position to be generated
    arguments
        obj(1,1) generateDirection
        radius(1,1) double {mustBePositive}
        nDirection(1,1) double {mustBePositive}
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('saffAndKuijlaars');

    % generate angles according to the algo (see thesis for details)
    radius_temp=radius*ones(1,nDirection);
    scalar=1:nDirection;
    
    h_temp= -1 + 2*(scalar-1)/(nDirection-1);

    elevation_temp= pi/2-acos(h_temp);       %sph2cart's definition of elevation angle is different. Conversion here.
    
    azimuth_temp = zeros (1,nDirection);
    for iElevation = 2:(nDirection-1)
        azimuth_temp(iElevation)= ...
        mod(azimuth_temp(iElevation-1) + 3.6 /(sqrt(nDirection)*sqrt(1-h_temp(iElevation))),2*pi);
    end

    % prepare for update
    markIn='saffAndKuijlaars';
    dataListIn.endEffectorAngle.radius=radius_temp;
    dataListIn.endEffectorAngle.azimuth=azimuth_temp;
    dataListIn.endEffectorAngle.elevation=elevation_temp;
    metaListIn.radius=radius;
    metaListIn.nDirection=nDirection;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

