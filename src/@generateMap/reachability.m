function reachability(obj,modeIn,options)
%REACHABILITY Calculate the Reachability map of the robot
%  in:
%  [modeIn]: assign acceleration mode for the calculation
%  [maxIkIterationIn]: maximum Itertion for numeric IK solver
%  [maxConstraintRepeatIn]: maximum repeat when solution don't satisfy the constraints
%  [weightsIn]: weights Needed for the solution
%  [ignoreWarning]: forcely ignore the warnings of the subfunctions
%
%  out: 
%  [solutionFlag]: mark up the solution status for each Tform:
%     0=undefined 1=haveSolution 2=noSolution 3=maxConstrainExceeds
%  [configSolColumn]: the joint configuration saved for each Tform
%  [reachabilityIndex]: the reachability Index for each Tform
    arguments
        obj(1,1) generateMap
        modeIn(1,:) char
        options.maxIkIterationIn(1,1) double {mustBePositive} = 1500
        options.maxConstraintRepeatIn(1,1) double {mustBePositive} = 10
        options.weightsIn(1,6) double = [0.1 0.1 0.1 0.5 0.5 0.5]
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    % check if allow call
    obj.mustAllowCall('reachability');

    % prepare variables for calculation and storage
    %parameters
    maxIkIterationIn=options.maxIkIterationIn;
    maxConstraintRepeatIn=options.maxConstraintRepeatIn;
    weightsIn=options.weightsIn;
    mapReachabilityTemp=mapReachability(obj);

    % Call the first calculation
    mapReachabilityTemp.fullCalculation(maxIkIterationIn,maxConstraintRepeatIn,weightsIn, ...
                                        mode=modeIn,ignoreWarning=options.ignoreWarning, ...
                                        forceGenerate=options.forceGenerate)

    % prepare the updating data
    markIn='reachability';

    metaListIn.mode=modeIn;
    metaListIn.maxIkInteration=maxIkIterationIn;
    metaListIn.maxConstraintRepeat=maxConstraintRepeatIn;
    metaListIn.weights=weightsIn;

    dataListIn.mapReachability=mapReachabilityTemp;

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

