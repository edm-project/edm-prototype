classdef generateMap < handle
    %GENERATEMAP Generate the Maps of the robot (the core algorithm)

    properties (SetAccess=immutable)
        version(1,1) double
    end 
    % end of immutable attributes

    properties (SetAccess=private)
        isActive(1,1) logical
        commonDataHandle(1,1) commonData
        robotMapsHandle(1,1)

        mark(1,:) cell {mustBeValidMarksNullPrepare}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
    end 
    % end of private attributes
    
    methods
        function obj = generateMap()
        %GENERATEMAP constructor
            obj.isActive=false;
            obj.mark={'null'};
            obj.version=3;
        end 
    end
    % end of the constructor

    methods (Access=private)
        function mustAllowCall(obj,callMarkIn)
        % mustAllowCall: throw error if function current call is not allowed
            arguments
                obj(1,1) generateMap
                callMarkIn(1,:) char {mustBeValidMarks}
            end

            % check if status Active
            mustIsActiveState(obj,true);

            % no repeat for {'assignAllTform','reachability','reachabilityTest','dexterity','dexterityTest'} 
            mustNoRepeatGroup(obj,callMarkIn,{'assignAllTform','reachability','reachabilityTest','dexterity','dexterityTest'});

            % check if {'reachability','reachabilityTest','dexterity','dexterityTest'} is after {'assignAllTform'}
            mustInSequence(obj,callMarkIn,{'reachability','reachabilityTest','dexterity','dexterityTest','flippedDexterityTest'}, ...
                                          {'assignAllTform'});

            % (only for now) check if {'spatialConnectivityTest'} is after {'reachabilityTest'}
            mustInSequence(obj,callMarkIn,{'spatialConnectivityTest'},{'reachabilityTest'}); 
        end

        function updateData(obj,markIn,metaListIn,dataListIn)
        % update the data in this procees, called in functional methods
            arguments
                obj(1,1) generateMap
                markIn(1,:) char {mustBeValidMarks}
                metaListIn(1,1) struct
                dataListIn(1,1) struct {mustBeValidDataList}
            end
            % check if this call is allowed
            obj.mustAllowCall(markIn);

            % update mark
            if isequal(obj.mark,{'prepared'})
                obj.mark={markIn};
            else
                obj.mark=[obj.mark,{markIn}];
            end

            %update metaList and dataList
            obj.metaList{length(obj.metaList)+1}=metaListIn;
            obj.dataList=mergeUpdateStruct(obj.dataList,dataListIn);
        end
    end
    % end of the private methods

    methods (Access = {?robotMaps})
        function mustAllowInitialize(obj)
        % mustAllowInitialze: check conditions for Initialize
        
            % isActive must equal false, otherwise means already initialized
            mustIsActiveState(obj,false)

            % marks must be 'null', otherwise means already finalized
            mustBeMark(obj,'null',true);
        end

        function mustAllowFinalize(obj)
        % mustAllowFinalize: check conditions for Finalize
            
            % isActive must be true, otherwise already finalized or not initialized
            mustIsActiveState(obj,true)

            % marks must be not 'prepared', which means nothing was done
            mustBeMark(obj,'prepared',false);
        end

        function initialize(obj,commonDataHandleIn,robotMapsHandleIn)
        % initalize this process
            arguments
                obj(1,1) generateMap
                commonDataHandleIn(1,1) commonData
                robotMapsHandleIn(1,1) robotMaps
            end

            % check if initialization valid
            obj.mustAllowInitialize();

            % set isActive and Mark for functional calls
            obj.isActive=true;
            obj.mark={'prepared'};

            % store the handle of commonData
            obj.commonDataHandle=commonDataHandleIn;
            obj.robotMapsHandle=robotMapsHandleIn;

            % read the data needed
            obj.dataList.robotParameters=commonDataHandleIn.robotParameters;
            obj.dataList.positionRow=commonDataHandleIn.positionRow;
            obj.dataList.endEffectorTform=commonDataHandleIn.endEffectorTform;
        end

%         function finalize(obj)
%         % finialize this process
% 
%             % check if finalization valid
%             obj.mustAllowFinalize();
% 
%             % set isActive state, lock write functions
%             obj.isActive=false;
% 
%             % write nothing into commonData
%         end
    end
    % end of the robotMaps friend methods

    methods (Access = public)
        assignAllTform(obj,options)    
        % Functional method: generate the Tform to be analyzed
        
        reachability(obj,mode,options)
        % Functional method: calculate the reachability map for the first time.
        reachabilityTest(obj,mode,options) 
        % Functional method: detailed reachability test

        dexterityTest(obj,options)
        % Functional method: initialize a dexterity test

        flippedDexterityTest(obj,options)
        % Functional method: initialize a flipped dexterity test

        index = spatialConnectivityTest(obj) 
        % Functional method: initialize a spatialConnectivity test
    end 
    % end of the public functional method (definition in same folder)
end

function mustBeValidMarks(marksIn)
    % Check for Valid input as mark.
    mustBeMember(marksIn,{'assignAllTform', ...
                          'reachability','dexterity', ...
                          'reachabilityTest','dexterityTest','flippedDexterityTest', ...
                          'spatialConnectivityTest'})
end
 
function mustBeValidMarksNullPrepare(marksIn)
    % Check for Valid input as mark. Allow null and prepare.
    mustBeMember(marksIn,{'null','prepared', ...
                          'assignAllTform'...
                          'reachability','dexterity', ...
                          'reachabilityTest','dexterityTest','flippedDexterityTest', ...
                          'spatialConnectivityTest'})
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{ 'robotParameters','positionRow','endEffectorTform','allTform', ...
                                     'mapReachability','mapDexterity', ...
                                     'testReachability', 'testDexterity','testFlippedDexterity',...
                                     'testSpatialConnectivity'})
end
