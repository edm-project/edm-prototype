function dexterityTest(obj,options)
%DEXTERITYTEST initialize the testDexterity object
    arguments
        obj(1,1) generateMap
        options.ignoreWarning(1,1) logical = false
    end

    % check if allow call
    obj.mustAllowCall('dexterityTest');

    % get the old handle and add new handle
    testDexterityTemp=testDexterity(obj);
    testDexterityTemp.initialize(ignoreWarning=options.ignoreWarning);

    % prepare the updating data
    markIn='dexterityTest';
    dataListIn.testDexterity=testDexterityTemp;
    metaListIn=struct();

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

