function assignAllTform(obj)
%ASSIGNALLTFORM Combine 'positionRow','endEffectorTform' to generate
%'allTform', which is the full list of Tform used in IK calculation
    arguments
        obj(1,1) generateMap
    end

    % check if allow call
    obj.mustAllowCall('assignAllTform');

    %Generate the temp data
    allTformTemp=repmat(obj.dataList.endEffectorTform, ...
                        1, 1, 1, 1, size(obj.dataList.positionRow,1));
    positionRowTemp=obj.dataList.positionRow;

    % generate all the information for allTformTemp
    % allTform is a 5D matrix:(TformHeight,TformWidth,LocalPosition,LocalRotation,GlobalPosition)
    % which stores all the Tform matrix in the code.
    for iGlobalPos=1:size(positionRowTemp,1)
        allTformTemp(1:3,4,:,:,iGlobalPos)= ...
        allTformTemp(1:3,4,:,:,iGlobalPos)+positionRowTemp(iGlobalPos,1:3)';
    end

    % update the data and status
    markIn='assignAllTform';
    metaListIn=struct();
    dataListIn.allTform=allTformTemp;

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

