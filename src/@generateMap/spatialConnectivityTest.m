function index = spatialConnectivityTest(obj)
%SPATIALCONNECTIVITYTEST initialize a spatialConnectivityTest
% [index]= this test can be made multiple times, and therefore
% obj.dataList.testSpatialConnectivity is a array. The index is the index
% for the current map.
    arguments
        obj(1,1) generateMap
    end

    % check if allow call
    obj.mustAllowCall('spatialConnectivityTest');

    % get the old handle and add new handle
    if isfield(obj.dataList, 'testSpatialConnectivity')
        testSpatialConnectivityTemp=obj.dataList.testSpatialConnectivity;
        index=length(testSpatialConnectivityTemp)+1;
        testSpatialConnectivityHandle=testSpatialConnectivity(obj);
        testSpatialConnectivityTemp=[testSpatialConnectivityTemp,testSpatialConnectivityHandle];
    else
        testSpatialConnectivityTemp=testSpatialConnectivity(obj);
        index=1;
    end 

    % prepare the updating data
    markIn='spatialConnectivityTest';
    dataListIn.testSpatialConnectivity=testSpatialConnectivityTemp;
    metaListIn=struct();

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);

end

