function flippedDexterityTest(obj,options)
    arguments
        obj(1,1) generateMap
        options.ignoreWarning(1,1) logical = false
    end

    % check if allow call
    obj.mustAllowCall('flippedDexterityTest');

    % get the old handle and add new handle
    testFlippedDexterityTemp=testFlippedDexterity(obj);
    testFlippedDexterityTemp.initialize(ignoreWarning=options.ignoreWarning);

    % prepare the updating data
    markIn='flippedDexterityTest';
    dataListIn.testFlippedDexterity=testFlippedDexterityTemp;
    metaListIn=struct();

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

