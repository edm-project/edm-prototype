classdef testReachability < handle
    %TESTREACHABILITY  A Class that calculate Reachability but stores more
    %information
    
    properties (SetAccess = immutable)
        version(1,1) double
    end 
    % end of immutable attributes
    
    properties (SetAccess=private)
        generateMapHandle(1,1) generateMap

        mark(1,:) cell = {}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
    end 
    % end of private attributes
    
    methods
        function obj = testReachability(generateMapHandleIn)
            %TESTREACHABILITY constructor
            obj.generateMapHandle=generateMapHandleIn;
            obj.version=1;
        end
    end
    % end of the constructor

    methods (Access=private)
        function initialize(obj,options)
        % initialize the object. 
            arguments
                obj(1,1) testReachability
                options.ignoreWarning(1,1) logical = false
            end

            % warning when this class is not empty 
            if options.ignoreWarning==false && ~isequal(obj.dataList,struct())
                warning('This object testReachability is not empty. Initialization will wipe out the old data.')
                disp('Continue Anyway?');
                pause on;
                pause;
            end

            % initialize(reset) the data section of this class
            obj.mark={};
            obj.metaList={};
            obj.dataList=struct();           
        end

        function updateData(obj,markIn,metaListIn,dataListIn)
            arguments
                obj(1,1) testReachability
                markIn(1,:) char
                metaListIn(1,1) struct
                dataListIn(1,1) struct {mustBeValidDataList}
            end

            % update mark
            obj.mark=[obj.mark,{markIn}];

            %update metaList and dataList
            obj.metaList{length(obj.metaList)+1}=metaListIn;
            obj.dataList=mergeUpdateStruct(obj.dataList,dataListIn);
        end
    end
    % end of the private methods

    methods (Access = {?testReachability,?generateMap}, Static)
        function isConsistent = checkConsistency()
        % check the data consistency with the outside (generateMap and other maps)
            isConsistent=true;
        end
    end
    % end of the friend methods

    methods (Access = private)
        [solutionFlag,experimentRecord,configSolColumn] = numericMPHardSkip ...
                    (obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,options);
    end
    % end of the private functional method (definition in same folder)

    methods (Access = public)
        fullCalculation(obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,mode,options)
        % Functional method. Do a full calculation of reachability
    end 
    % end of the public functional method (definition in same folder)
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{'solutionFlag','experimentRecord','configSolColumn','reachabilityIndex'})
end