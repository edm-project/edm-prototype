function [solutionFlag,experimentRecord,configSolColumn] = numericMPHardSkip(obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,options)
%NUMERICMEXPARALLEL calculation reachability with numeric mode with parallel and code generation
    arguments
        obj(1,1) testReachability
        maxIkIterationIn(1,1) double {mustBePositive}
        maxConstraintRepeatIn(1,1) double {mustBePositive}
        weightsIn(1,6) double
        options.range(1,:) char {mustBeMember(options.range,{'full','targeted','null'})} = 'null'
        options.solutionFlagMask(:,:) logical = []
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    % check if the ranging method is not assigned
    if isequal(options.range,'null')
        ME = MException('mapReachability:numeric:modeNull', ...
                        'mode not assigned.');
        throw(ME)
    end

    % prepare variables for calculation
    % input tform and it's mask
    tform = obj.generateMapHandle.dataList.allTform;
    solutionFlagMask = options.solutionFlagMask;
    if isequal(options.range,'full')
        solutionFlagMask = true(size(tform,3),size(tform,4),size(tform,5));
    end

    % check if the size is same
    if (size(tform,3)~=size(solutionFlagMask,1)) || ... 
       (size(tform,4)~=size(solutionFlagMask,2)) || ...
       (size(tform,5)~=size(solutionFlagMask,3))
        ME = MException('testReachability:numericMPHardSkip:sizeNotMatch', ...
                        'Two matrix does not match in size [%s][%s]', ...
                         mat2str(size(tform)),mat2str(size(solutionFlagMask)));
        throw(ME)
    end
    
    % other inputs
    robotParameters = obj.generateMapHandle.dataList.robotParameters;
    robotColumn = obj.generateMapHandle.dataList.robotParameters.robotColumn;
    endEffectorName = obj.generateMapHandle.dataList.robotParameters.endEffectorName;

    % generate the MEX code
    ikHandle = MEXikGenerate(obj.generateMapHandle.dataList.robotParameters, ...
                  maxIkIterationIn, ...
                  ignoreWarning=options.ignoreWarning, ...
                  forceGenerate=options.forceGenerate);

    solutionFlagCheckHandle = MEXsolutionFlagCheckGenerate(robotParameters, ...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);


    %parameters
    nLocalPosition = size(tform,3);
    nLocalRotation = size(tform,4);
    nGlobalPostition = size(tform,5);
    robotConfigSize = length(homeConfiguration(robotColumn)); 
    
    %progressbar
    fig = progressbar(['testReachability: hardSkip mode ',options.range,' range'],nLocalPosition*nGlobalPostition);
    parallelQueue = parallel.pool.DataQueue; % use queue to async-update the progressbar
    afterEach(parallelQueue,@progressbar); 

    %output set
    solutionFlag = zeros(nLocalPosition,nLocalRotation,nGlobalPostition);
    experimentRecord = zeros(nLocalPosition,nLocalRotation,nGlobalPostition,maxConstraintRepeatIn);
    configSolColumn = zeros(robotConfigSize,nLocalPosition,nLocalRotation,nGlobalPostition);

    % data calculation
    parfor iGlobalPosition=1:nGlobalPostition
        for iLocalPosition=1:nLocalPosition    % for each position(global and local)
            for iLocalRotation=1:nLocalRotation  % for each rotation on same position
                if solutionFlagMask(iLocalPosition,iLocalRotation,iGlobalPosition)==false
                    % when the solution is not targeted, skip the calculation.
                    continue;
                end
                for iRepeatCounter=1:maxConstraintRepeatIn % iteration for constrains
                    initialguess = randomConfiguration(robotColumn);
                    [configSol,solInfo] = ikMEX(ikHandle,endEffectorName, ...
                                          tform(:,:,iLocalPosition,iLocalRotation,iGlobalPosition), ...
                                          weightsIn, ...
                                          initialguess);
                    
                    % if IK solver can't find Solution.
                    if isequal(solInfo.Status,'best available')
                        solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition) = 2;
                        experimentRecord(iLocalPosition,iLocalRotation,iGlobalPosition,iRepeatCounter) = 2;
                        break;
                    
                    % if IK solver find Solution.
                    elseif isequal(solInfo.Status,'success')
                        [solutionFlagOut,configSolOut] = solutionFlagCheckMEX(solutionFlagCheckHandle, ...
                                                                    configSol);
                        
                        if solutionFlagOut == 1
                            solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition) = solutionFlagOut;
                            experimentRecord(iLocalPosition,iLocalRotation,iGlobalPosition,iRepeatCounter) = solutionFlagOut;
                            configSolColumn(:,iLocalPosition,iLocalRotation,iGlobalPosition) = configSolOut;
                            break;
                        else
                            solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition) = solutionFlagOut;
                            experimentRecord(iLocalPosition,iLocalRotation,iGlobalPosition,iRepeatCounter) = solutionFlagOut;
                        end
                    else
                        ME = MException('testReachability:numericMPHardSkip:elseError', ...
                                        'if-else should not run into this branch');
                        throw(ME);
                    end
                end
            end
            % update the progressbar(with Queue)
            send(parallelQueue,'');
        end
    end

    % close the progressbar
    close(fig);
end

