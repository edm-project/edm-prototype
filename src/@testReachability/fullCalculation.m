function fullCalculation(obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,mode,options)
%FULLCALCULATION Functional method. Do a full calculation of reachability
%  in:
%  [maxIkIterationIn]: maximum Itertion for numeric IK solver (defined by IK solver)
%  [maxConstraintRepeatIn]: maximum repeat when solution don't satisfy constraints (self collision or joint constaints)
%  [weightsIn]: weights Needed for the solution (defined by IK solver)
%  [mode]: skipping strategy when the ik Solver return "no Solution" for a single point
%
%  out(update): 
%  [solutionFlag]: mark up the solution status for each position:
%     0=undefined 1=haveSolution 2=noSolution 3=maxConstrainExceeds
%     4=selfCollision
%  [experimentRecord]: mark up the solution status for each Tform:
%     0=undefined 1=haveSolution 2=noSolution 3=jointConstraint
%     4=selfCollision
%  [configSolColumn]: the joint configuration saved for each Tform
%  [reachabilityIndex]: the reachability Index for each Tform
    arguments
        obj(1,1) testReachability
        maxIkIterationIn(1,1) double {mustBePositive}
        maxConstraintRepeatIn(1,1) double {mustBePositive}
        weightsIn(1,6) double
        mode(1,:) char {mustBeValidMode(mode)}
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    % initialize the class
    obj.initialize(ignoreWarning=options.ignoreWarning);

    % start a time counter
    startTime= datetime('now');

    % prepare variables for calculation and storage
    nLocalPosition = size(obj.generateMapHandle.dataList.allTform,3);

    % ik calculation with different modes
    if isequal(mode,'hardSkip')
        [solutionFlag,experimentRecord,configSolColumn] = obj.numericMPHardSkip( ...
                                                         maxIkIterationIn,maxConstraintRepeatIn,weightsIn, ...
                                                         range='full', ...
                                                         ignoreWarning=options.ignoreWarning, ...
                                                         forceGenerate=options.forceGenerate);
    elseif isequal(mode,'softSkip')
        error("still not implemented!")
    elseif isequal(mode,'nonSkip') 
        error("still not implemented!")
    else
        ME = MException('testReachability:fullCalculation:elseError', ...
                        'mode if-else should not run into this branch');
        throw(ME);
    end

    % reachability Index calculation
    reachabilityIndex=squeeze(sum(any(solutionFlag==1,2),1)*100/nLocalPosition);

    % prepare the data
    markIn='fullCalculation';
    dataListIn.solutionFlag=solutionFlag;
    dataListIn.experimentRecord=experimentRecord;
    dataListIn.configSolColumn=configSolColumn;
    dataListIn.reachabilityIndex=reachabilityIndex;

    metaListIn.maxIkInteration=maxIkIterationIn;
    metaListIn.maxConstraintRepeat=maxConstraintRepeatIn;
    metaListIn.weights=weightsIn;
    metaListIn.mode=mode;
    metaListIn.startTime=startTime;
    metaListIn.endTime=datetime('now');
    metaListIn.numericMEXStr = fileread('MEXik.m');

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

