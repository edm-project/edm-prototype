function sphereOut(obj,center,radius)
%SPHEREIN Functional method: only include point outside sphere of [center,radius]
%
% center: [x,y,z] position of the sphere
% radius: radius of the sphere
    arguments
        obj(1,1) filterGrid
        center(1,3) double
        radius(1,1) double {mustBePositive}
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('sphereOut');

    % filter positionRow, accept points inside sphere [center,radius] 
    positionRow_temp=obj.dataList.positionRow;
    positionRow_filter = sum( (positionRow_temp-center).^2 , 2) >= radius^2;
    positionRow_temp=positionRow_temp(positionRow_filter,:);

    % prepare for update
    markIn='sphereOut';
    dataListIn.positionRow=positionRow_temp;
    metaListIn.center=center;
    metaListIn.radius=radius;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

