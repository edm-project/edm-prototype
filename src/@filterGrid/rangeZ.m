function rangeZ(obj,zMin,zMax)        
%RANGEZ only include point with z between zMin and zMax
%
%zMin: minimum accepted z
%zMax: maximum accepted z
    arguments
        obj(1,1) filterGrid
        zMin(1,1) double
        zMax(1,1) double
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('rangeZ');

    % check for zMin and zMax relationship
    if zMin>=zMax
        ME = MException('filterGrid:rangeZ:rangeError', ...
                        'zMin should smaller that zMax');
        throw(ME)
    end

    % filter positionRow according to rangeZ
    positionRow_temp=obj.dataList.positionRow;
    positionRow_filter = positionRow_temp(:,3) >= zMin & positionRow_temp(:,3) <= zMax;
    positionRow_temp=positionRow_temp(positionRow_filter,:);

    % prepare for update
    markIn='rangeZ';
    dataListIn.positionRow=positionRow_temp;
    metaListIn.zMin=zMin;
    metaListIn.zMax=zMax;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

