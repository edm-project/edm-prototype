classdef filterGrid < handle
    %FILTERGRID class for modification of positionRow in commonData
    
    properties (SetAccess=immutable)
    end 
    % end of immutable attributes

    properties (SetAccess=private)
        isActive(1,1) logical
        commonDataHandle(1,1) commonData
        robotMapsHandle(1,1)

        mark(1,:) cell {mustBeValidMarksNullPrepare}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
    end 
    % end of private attributes
    
    methods
        function obj = filterGrid()
            obj.isActive = false;
            obj.mark = {'null'};
        end
    end
    % end of the constructor

    methods (Access = private)
        function mustAllowCall(obj,callMarkIn)
        % mustAllowCall: throw error if function call is not allowed
            arguments
                obj(1,1) filterGrid
                callMarkIn(1,:) char {mustBeValidMarks}
            end

            % check if status Active
            mustIsActiveState(obj,true);

            % forbid Mark repeat
            mustNoRepeat(obj,callMarkIn);
        end

        function updateData(obj,markIn,metaListIn,dataListIn)
        % update the data in this procees, called in functional methods
            arguments
                obj(1,1) filterGrid
                markIn(1,:) char {mustBeValidMarks}
                metaListIn(1,1) struct
                dataListIn(1,1) struct
            end

            % check if this call is allowed
            obj.mustAllowCall(markIn);

            % update mark
            if isequal(obj.mark,{'prepared'})
                obj.mark={markIn};
            else
                obj.mark=[obj.mark,{markIn}];
            end

            %update metaList and dataList
            obj.metaList{length(obj.metaList)+1}=metaListIn;
            obj.dataList=mergeUpdateStruct(obj.dataList,dataListIn);
        end
    end
    % end of the private methods

    methods (Access = {?robotMaps})
        function mustAllowInitialize(obj)
        % mustAllowInitialze: check conditions for Initialize
        
            % isActive must equal false, otherwise means already initialized
            mustIsActiveState(obj,false)

            % marks must be 'null', otherwise means already finalized
            mustBeMark(obj,'null',true);
        end

        function mustAllowFinalize(obj)
        % mustAllowFinalize: check conditions for Finalize
            
            % isActive must be true, otherwise already finalized or not initialized
            mustIsActiveState(obj,true)
        end

        function initialize(obj,commonDataHandleIn,robotMapsHandleIn)
        % initalize this process
            arguments
                obj(1,1) filterGrid
                commonDataHandleIn(1,1) commonData
                robotMapsHandleIn(1,1) robotMaps
            end

            % check if initialization valid
            obj.mustAllowInitialize();

            % set isActive and Mark for functional calls
            obj.isActive=true;
            obj.mark={'prepared'};

            % store the handle of commonData
            obj.commonDataHandle=commonDataHandleIn;
            obj.robotMapsHandle=robotMapsHandleIn;

            % read the data
            obj.dataList.positionRow=commonDataHandleIn.positionRow;
        end

        function finalize(obj)
        % finialize this process

            % check if finalization valid
            obj.mustAllowFinalize();

            % set isActive state, lock write functions
            obj.isActive=false;

            % set status to empty if nothing was done
            if isequal(obj.mark,{'prepared'})
                obj.mark={'empty'};
            end

            % write data into commonData
            obj.commonDataHandle.updateDataFilterGrid(obj.dataList.positionRow);
        end
    end
    % end of the robotMaps friend methods
    
    methods (Access = public)
        rangeZ(obj,zMin,zMax)        % Functional method: only include point with z between zMin and zMax

        sphereIn(obj,center,radius)  % Functional method: only include point in sphere of [center,radius]
        sphereOut(obj,center,radius) % Functional method: only include point out sphere of [center,radius]
    end
    % end of the public functional method (definition in same folder)
end

function mustBeValidMarks(marksIn)
    % Check for Valid input as mark.
    mustBeMember(marksIn,{'rangeZ','sphereIn','sphereOut'})
end
 
function mustBeValidMarksNullPrepare(marksIn)
    % Check for Valid input as mark. Allow null and prepare.
    mustBeMember(marksIn,{'null','prepared','empty','rangeZ','sphereIn','sphereOut'})
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{'positionRow'})
end