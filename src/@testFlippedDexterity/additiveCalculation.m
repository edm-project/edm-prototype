function additiveCalculation(obj,expectFlipSolution,options)
    arguments
        obj(1,1) testFlippedDexterity
        expectFlipSolution(1,1) double
        options.maxIkIterationIn(1,1) double {mustBePositive} = 1500 
        options.weightsIn(1,6) double = [0.1 0.1 0.1 0.5 0.5 0.5]
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    obj.mustAllowCall('additiveCalculation')

    % start a time counter
    startTime= datetime('now');

    % prepare variables for calculation and storage
    solutionFlag = obj.dataList.solutionFlag;
    haveSolutionMask = solutionFlag == 1 | solutionFlag == 3 | solutionFlag == 4;
    existDataInSection = any(haveSolutionMask,4);
    existDataLength = squeeze(sum(existDataInSection,5));

    % calculate the mask
    updateMask = existDataLength < expectFlipSolution;                

    % calculate the result
    [solutionFlag,configSolColumn,flipCounter] = obj.numericMP(updateMask,options.maxIkIterationIn,options.weightsIn, ...
                                                   forceGenerate=options.forceGenerate, ...
                                                   ignoreWarning=options.ignoreWarning);

    % prepare the data
    markIn='additiveCalculation';
    dataListIn.solutionFlag=solutionFlag;
    dataListIn.configSolColumn=configSolColumn;
    dataListIn.flipCounter=flipCounter;

    metaListIn.maxIkInteration=options.maxIkIterationIn;
    metaListIn.weights=options.weightsIn;
    metaListIn.flipCounter=flipCounter;
    metaListIn.startTime=startTime;
    metaListIn.updateCandidate=sum(updateMask,"all");
    metaListIn.endTime=datetime('now');
    metaListIn.numericMEXStr = fileread('MEXik.m');

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

