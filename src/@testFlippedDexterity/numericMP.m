function [solutionFlag,configSolColumn,flipCounter] = numericMP(obj,updateMask,maxIkIterationIn,weightsIn,options)
    arguments
        obj(1,1) testFlippedDexterity
        updateMask(:,:,:) logical
        maxIkIterationIn(1,1) double {mustBePositive}
        weightsIn(1,6) double
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
        options.timeStep(1,1) double {mustBePositive} = 0.3
        options.increaseStep(1,1) double {mustBePositive,mustBeInteger} = 50
        options.maxStep(1,1) double {mustBePositive,mustBeInteger} = 200
    end

    % import the existing data
    solutionFlag = obj.dataList.solutionFlag;
    configSolColumn = obj.dataList.configSolColumn;
    tform = obj.generateMapHandle.dataList.allTform;

    % check the size of update mask and the current solutionFlag
    if size(solutionFlag,1) ~=size(updateMask,1) || ... 
       size(solutionFlag,2) ~=size(updateMask,2) || ...
       size(solutionFlag,3) ~=size(updateMask,3)
        ME = MException('testFlippedDexterity:numericMP:sizeNotMatch', ...
                        'size of mask and solutionFlag is not matched S:%s M:%s.', ...
                        mat2str(size(solutionFlag),5),mat2str(size(updateMask),5));
        throw(ME)
    end

    % import other parameters
    robotParameters = obj.generateMapHandle.dataList.robotParameters;
    robotColumn = robotParameters.robotColumn;
    endEffectorName = robotParameters.endEffectorName;
    
    % calculate the parameter
    nLocalPosition = size(tform,3);
    nLocalRotation = size(tform,4);
    nGlobalPosition = size(tform,5);
    nSection = obj.dataList.nSection;
    isRedudant = obj.dataList.isRedudant;
    nAngle = length(randomConfiguration(robotColumn));

    %progressbar
    fig = progressbar('testFlippedDexterity: numericMEXParallel mode',nLocalPosition*nGlobalPosition);
    parallelQueue = parallel.pool.DataQueue; % use queue to async-update the progressbar
    afterEach(parallelQueue,@progressbar);

    % MEX code preparation
    ikHandle = MEXikGenerate(robotParameters, ...
                  maxIkIterationIn, ...
                  ignoreWarning=options.ignoreWarning, ...
                  forceGenerate=options.forceGenerate);

    sectionHandle = MEXdexteritySectionGenerate(robotParameters, ...
                  ignoreWarning=options.ignoreWarning, ...
                  forceGenerate=options.forceGenerate);
    
    solutionFlagCheckHandle = MEXsolutionFlagCheckGenerate(robotParameters, ...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);

    if isRedudant==true
        nullspaceFixedHandle = MEXnullspaceFixedGenerate(robotParameters,...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);

        nullspaceFixedReverseHandle = MEXnullspaceFixedReverseGenerate(robotParameters,...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);

        isReachableHandle = MEXisNullspaceReachableGenerate(robotParameters, ...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);
    else
        nullspaceFixedHandle = @()(1);
        isReachableHandle = @()(1);
        nullspaceFixedReverseHandle = @()(1); 
    end

    % parallel preparation
    haveSolutionMask = solutionFlag == 1 | solutionFlag == 3 | solutionFlag == 4;
    existDataInSection = any(haveSolutionMask,4);
    existDataLength = squeeze(sum(existDataInSection,5));
    solutionFlagOut = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,nSection);
    configSolColumnOut = zeros(nAngle,nLocalPosition,nLocalRotation,nGlobalPosition,nSection);
    timeStep = options.timeStep;
    increaseStep = options.increaseStep;
    maxStep = options.maxStep;

    % iteration calculation
    parfor iGlobalPosition=1:nGlobalPosition
        for iLocalPosition=1:nLocalPosition    % for each position(global and local)
            for iLocalRotation=1:nLocalRotation  % for each rotation on same position
                if updateMask(iLocalPosition,iLocalRotation,iGlobalPosition)==false
                    % when the solution is not targeted, skip the calculation.
                    continue;
                end

                initialguess = randomConfiguration(robotColumn);
                [configSol,solInfo] = ikMEX(ikHandle,endEffectorName, ...
                                      tform(:,:,iLocalPosition,iLocalRotation,iGlobalPosition), ...
                                      weightsIn, ...
                                      initialguess);
                
                % if IK solver find Solution for redudant robot
                if isRedudant == true && isequal(solInfo.Status,'success')
                    % decide if it can be reached by previous solutions
                    isReachable = false;
                    for iExistData = 1:existDataLength(iLocalPosition,iLocalRotation,iGlobalPosition)
                        isReachable = isNullspaceReachableMEX(isReachableHandle,sectionHandle, ...
                                        nSection,...
                                        squeeze(configSolColumn(:,iLocalPosition,iLocalRotation,iGlobalPosition,:,iExistData)),...
                                        configSol);
                        if isReachable == true
                            break;
                        end
                    end

                    % do nullspace motion and fill in the data
                    if isReachable == false
                        [configSolCol,haveSolutionCol] = nullspaceRoundMEX(robotParameters,nullspaceFixedHandle,nullspaceFixedReverseHandle,sectionHandle, ...
                                                            configSol,nSection, ...
                                                            timeStep,...
                                                            increaseStep, ...
                                                            maxStep);

                        for iSection = 1:nSection
                            if haveSolutionCol(iSection) == 0
                                solutionFlagOut(iLocalPosition,iLocalRotation,iGlobalPosition,iSection) = 2;
                            else
                                % check the collision and joint Limit
                                configSolSection = configSolCol(:,iSection);
                                [solutionFlagTemp,configSolTemp] = solutionFlagCheckMEX(solutionFlagCheckHandle,configSolSection);
                                solutionFlagOut(iLocalPosition,iLocalRotation,iGlobalPosition,iSection) = solutionFlagTemp;
                                configSolColumnOut(:,iLocalPosition,iLocalRotation,iGlobalPosition,iSection) = configSolTemp;
                            end
                        end
                    end
                
                % if ik solver find a solution for non-redudant robot
                elseif isRedudant == false && isequal(solInfo.Status,'success')
                    [~,sectionNumber] = dexteritySectionMEX(sectionHandle,configSol,nSection);
                    isEqual = false;
                    for iExistData = 1:existDataLength(iLocalPosition,iLocalRotation,iGlobalPosition)
                        if any(solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition,:,iExistData)~=2)
                            isEqual = checkEqual(squeeze(configSolColumn(:,iLocalPosition,iLocalRotation,iGlobalPosition,:,iExistData)), ...
                                                configSol, ...
                                                sectionNumber);
                            if isEqual == true
                                break;
                            end
                        end
                    end

                    if isEqual == false
                        for iSection = 1:nSection
                            if iSection==sectionNumber
                                [solutionFlagTemp,configSolTemp] = solutionFlagCheckMEX(solutionFlagCheckHandle,configSol);
                                solutionFlagOut(iLocalPosition,iLocalRotation,iGlobalPosition,iSection) = solutionFlagTemp;
                                configSolColumnOut(:,iLocalPosition,iLocalRotation,iGlobalPosition,iSection) = configSolTemp;
                            else
                                solutionFlagOut(iLocalPosition,iLocalRotation,iGlobalPosition,iSection) = 2;
                            end
                        end
                    end

                % if no solution was found
                else
                    for iSection = 1:nSection
                        solutionFlagOut(iLocalPosition,iLocalRotation,iGlobalPosition,iSection) = 2;
                    end
                end
            end
            % update the progressbar(with Queue)
            send(parallelQueue,'');
        end
    end

    % sent the data back to solutionFlag and configSolColumn (when each section have solution)
    for iGlobalPosition=1:nGlobalPosition
        for iLocalPosition=1:nLocalPosition    % for each position(global and local)
            for iLocalRotation=1:nLocalRotation  % for each rotation on same position
                if updateMask(iLocalPosition,iLocalRotation,iGlobalPosition)==false
                    % when the solution is not targeted, skip the calculation.
                    continue;
                end
                if any(solutionFlagOut(iLocalPosition,iLocalRotation,iGlobalPosition,:))
                    solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition,:,existDataLength(iLocalPosition,iLocalRotation,iGlobalPosition)+1) = ...
                        solutionFlagOut(iLocalPosition,iLocalRotation,iGlobalPosition,:);
                    configSolColumn(:,iLocalPosition,iLocalRotation,iGlobalPosition,:,existDataLength(iLocalPosition,iLocalRotation,iGlobalPosition)+1) = ...
                        configSolColumnOut(:,iLocalPosition,iLocalRotation,iGlobalPosition,:);
                end
            end
        end
    end

    haveSolutionMaskAfter = solutionFlag == 1 | solutionFlag == 3 | solutionFlag == 4;
    existDataInSectionAfter = any(haveSolutionMaskAfter,4);
    existDataLengthAfter = squeeze(sum(existDataInSectionAfter,5));
    flipCounter = max(existDataLengthAfter,[],"all");

    % close the progressbar
    close(fig);
end

function isEqual = checkEqual(configSolSlice,configSol,sectionNumber)
    if norm(configSolSlice(:,sectionNumber)-configSol)<0.02
        isEqual=true;
    else
        isEqual=false;
    end
end

