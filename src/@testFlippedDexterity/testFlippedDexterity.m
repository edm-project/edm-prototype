classdef testFlippedDexterity  < handle
    %TESTREACHABILITY  A Class that calculate Reachability but stores more
    %information
    
    properties (SetAccess = immutable)
        version(1,1) double
    end 
    % end of immutable attributes
    
    properties (SetAccess=private)
        generateMapHandle(1,1) generateMap

        mark(1,:) cell {mustBeValidMarksNullPrepare} = {}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
    end 
    % end of private attributes

    methods
        function obj = testFlippedDexterity(generateMapHandleIn)
        %GENERATEGRID constructor
            arguments
                generateMapHandleIn(1,1) generateMap
            end
            obj.generateMapHandle=generateMapHandleIn;
            obj.version=1;
            obj.mark={'null'};
        end
    end
    % end of the constructor

    methods (Access=private)
        function mustAllowCall(obj,callMarkIn)
        % mustAllowCall: throw error if function current call is not allowed
            arguments
                obj(1,1) testFlippedDexterity
                callMarkIn(1,:) char {mustBeValidMarks}
            end

            % allow no repeat for {'assignSection','assignAdjacent'}
            mustNoRepeatGroup(obj,callMarkIn,{'assignSection','assignAdjacent'});
        end

        function updateData(obj,markIn,metaListIn,dataListIn)
        % update the data in this process, called in functional methods
            arguments
                obj(1,1) testFlippedDexterity
                markIn(1,:) char {mustBeValidMarks}
                metaListIn(1,1) struct
                dataListIn(1,1) struct {mustBeValidDataList}
            end
            % check if this call is allowed
            obj.mustAllowCall(markIn);

            % update mark
            if isequal(obj.mark,{'prepared'})
                obj.mark={markIn};
            else
                obj.mark=[obj.mark,{markIn}];
            end

            %update metaList and dataList
            if isequal(markIn,'fullCalculation') || isequal(markIn,'marginalCalculation') || ... 
               isequal(markIn,'additiveCalculation') 
                metaListReturn = obj.marginalUpdateData(dataListIn);    % function for marginal Update of some Data
                metaListIn = combineStruct(metaListIn,metaListReturn);
            else
                obj.dataList = combineStruct(obj.dataList,dataListIn);
            end
            obj.metaList{length(obj.metaList)+1}=metaListIn;
        end

        function metaListReturn = marginalUpdateData(obj,dataListIn)
        % update the data with rules:
        % if old data ==0 and new data ~=0, then update the data
        % if old data ~=1 and new data ==1, then update the data
            if isfield(dataListIn,'solutionFlag')||isfield(dataListIn,'configSolColumn')||isfield(dataListIn,'flipCounter')
                if isfield(dataListIn,'solutionFlag')&&isfield(dataListIn,'configSolColumn')&& isfield(dataListIn,'flipCounter')
                    nAngles=size(dataListIn.configSolColumn,1);

                    % if old data ==0 and new data ~=0, then update the data
                    oldZeroMask = (obj.dataList.solutionFlag == 0);
                    newNotZeroMask = (dataListIn.solutionFlag == 1)|(dataListIn.solutionFlag == 2)| ...
                                     (dataListIn.solutionFlag == 3)|(dataListIn.solutionFlag == 4);
                    solutionMaskOne = oldZeroMask & newNotZeroMask;
                    configSolMaskOne = permute(repmat(solutionMaskOne,1,1,1,1,1,nAngles),[6 1 2 3 4 5]);

                    % if old data ~=1 and new data ==1, then update the data
                    oldNotOneMask = (obj.dataList.solutionFlag == 2)|(obj.dataList.solutionFlag == 3)|(obj.dataList.solutionFlag == 4);
                    newOneMask = (dataListIn.solutionFlag == 1);
                    solutionMaskTwo = oldNotOneMask & newOneMask;
                    configSolMaskTwo = permute(repmat(solutionMaskTwo,1,1,1,1,1,nAngles),[6 1 2 3 4 5]);

                    % if old data == 2 and new data == 3 / 4 (==1 are already included by the above situation)
                    oldNoSolutionMask = (obj.dataList.solutionFlag == 2);
                    newConstraintMask = (dataListIn.solutionFlag == 3)|(dataListIn.solutionFlag == 4);
                    solutionMaskThree = oldNoSolutionMask & newConstraintMask;
                    configSolMaskThree = permute(repmat(solutionMaskThree,1,1,1,1,1,nAngles),[6 1 2 3 4 5]);

                    % update data with mask
                    obj.dataList.solutionFlag(solutionMaskOne)=dataListIn.solutionFlag(solutionMaskOne);
                    obj.dataList.configSolColumn(configSolMaskOne)=dataListIn.configSolColumn(configSolMaskOne);
                    obj.dataList.solutionFlag(solutionMaskTwo)=dataListIn.solutionFlag(solutionMaskTwo);
                    obj.dataList.configSolColumn(configSolMaskTwo)=dataListIn.configSolColumn(configSolMaskTwo);
                    obj.dataList.solutionFlag(solutionMaskThree)=dataListIn.solutionFlag(solutionMaskThree);
                    obj.dataList.configSolColumn(configSolMaskThree)=dataListIn.configSolColumn(configSolMaskThree);
                    obj.dataList.flipCounter=dataListIn.flipCounter;

                    % collect some metadata for later
                    metaListReturn.totalCalculation = sum(dataListIn.solutionFlag~=0,"all");
                    metaListReturn.newHasSolution = sum(dataListIn.solutionFlag==1,"all");
                    metaListReturn.newNoSolution = sum(dataListIn.solutionFlag==2,"all");
                    metaListReturn.newJointLimit = sum(dataListIn.solutionFlag==3,"all");
                    metaListReturn.newSelfCollision = sum(dataListIn.solutionFlag==4,"all");
                    metaListReturn.zero2OtherCounter = sum(solutionMaskOne,"all");
                    metaListReturn.other2OneCounter = sum(solutionMaskTwo,"all");
                    metaListReturn.two2ConstraintCounter = sum(solutionMaskThree,"all");
                else
                    ME = MException('testDexterity:marginalUpdateData:dataListNotComplete', ...
                        'dataList should contain solutionFlag [%d] configSolColumn [%d]', ...
                        isfield(dataListIn,'solutionFlag'),isfield(dataListIn,'configSolColumn'));
                    throw(ME)
                end
            else
                error('The dataList does not contain the solutionFlag and configSolColumn');
            end
        end
    end

    methods (Access = public)
        function initialize(obj,options)
        % initalize this process
            arguments
                obj(1,1) testFlippedDexterity
                options.ignoreWarning(1,1) logical = false
            end

            % warning when this class is not empty 
            if options.ignoreWarning==false && ~isequal(obj.dataList,struct())
                warning('This object testFlippedDexterity is not empty. Initialization will wipe out the old data.')
                disp('Continue Anyway?');
                pause on;
                pause;
            end

            % initialize(reset) the data section of this class
            obj.mark={'prepared'};
            obj.metaList={};
            obj.dataList=struct(); 
        end
    end
    % end of the robotMaps public methods

    methods (Access = private)
        [solutionFlag,configSolColumn,flipCounter] = numericMP(obj,updateMask,maxIkIterationIn,weightsIn,options)
    end
    % end of the private functional method (definition in same folder)

    methods (Access = public)
        assignSection(obj,options)
        % Functional method. Do marginal update of reachability

        assignAdjacent(obj)
        % Functional method. Do marginal update of reachability

        fullCalculation(obj,options)
        % Functional method. calculate the flipped solution

        marginalCalculation(obj,options)
        % Functional method. calculate the flipped solution with marginal update

        additiveCalculation(obj,expectFlipSolution,options)
        % Functional method. calculate the flipped solution with manual assigning 

    end
    % end of the public functional method (definition in same folder)
    
end

function mustBeValidMarks(marksIn)
% Check for Valid input as mark.
    mustBeMember(marksIn,{'assignAdjacent','assignSection', ...
                          'fullCalculation','marginalCalculation','additiveCalculation'})
end

function mustBeValidMarksNullPrepare(marksIn)
% Check for Valid input as mark. Allow null and prepare.
    mustBeMember(marksIn,{'null','prepared','assignAdjacent','assignSection', ...
                          'fullCalculation','marginalCalculation','additiveCalculation'})
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{'solutionFlag','configSolColumn','nSection','isRedudant','adjacentMap','flipCounter'})
end

