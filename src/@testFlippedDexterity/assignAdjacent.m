function assignAdjacent(obj)
%ASSIGNADJACENT assign the adjacentMap
    arguments
        obj(1,1) testFlippedDexterity
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('assignAdjacent');

    % validate the input
    nGlobalPosition=size(obj.generateMapHandle.dataList.allTform,5);

    % collect the data of the Grid and generate adjacentMap
    gridMark=obj.generateMapHandle.robotMapsHandle.generateGridSlot.mark{1};
    gridMetaList=obj.generateMapHandle.robotMapsHandle.generateGridSlot.metaList{1};
    positionRow=obj.generateMapHandle.commonDataHandle.positionRow;
    
    % initialize containerMaps
    adjacentMap=containers.Map(1:nGlobalPosition,cell(1,nGlobalPosition));

    if isequal(gridMark,'cartesian2') || isequal(gridMark,'cartesian3')
        % get the adjacent infomation
        shiftUnit=gridMetaList.sphereRadius*2;
        [shiftMatrix,~]=shiftMatrixCartesian(gridMark,'face',shiftUnit);

        % merge the adjacent pairs
        for iShift=1:size(shiftMatrix,1)
            % full adjacent List
            [keys,values] = shiftPointsCartesian(positionRow,shiftMatrix(iShift,:));
            adjacentMap = mergeContainerMap(adjacentMap,keys,values);
        end
    else
        ME = MException('testDexterity:assignAdjacent:noImplementation', ...
            'The assignAdjacent is not implemented for [%s] grid', ...
            gridMark);
        throw(ME)
    end

    % prepare for update
    markIn='assignAdjacent';
    dataListIn.adjacentMap=adjacentMap;
    metaListIn=struct();

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);

end

