function fullCalculation(obj,options)
    arguments
        obj(1,1) testFlippedDexterity
        options.maxIkIterationIn(1,1) double {mustBePositive} = 1500 
        options.weightsIn(1,6) double = [0.1 0.1 0.1 0.5 0.5 0.5]
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    obj.mustAllowCall('fullCalculation')

    % start a time counter
    startTime= datetime('now');

    % prepare variables for calculation and storage
    nLocalPosition = size(obj.generateMapHandle.dataList.allTform,3);
    nLocalRotation = size(obj.generateMapHandle.dataList.allTform,4);
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);

    % calculate the mask
    updateMask = ones(nLocalPosition,nLocalRotation,nGlobalPosition,'logical');

    % calculate the result
    [solutionFlag,configSolColumn,flipCounter] = obj.numericMP(updateMask,options.maxIkIterationIn,options.weightsIn, ...
                                                   forceGenerate=options.forceGenerate, ...
                                                   ignoreWarning=options.ignoreWarning);

    % prepare the data
    markIn='fullCalculation';
    dataListIn.solutionFlag=solutionFlag;
    dataListIn.configSolColumn=configSolColumn;
    dataListIn.flipCounter=flipCounter;

    metaListIn.maxIkInteration=options.maxIkIterationIn;
    metaListIn.weights=options.weightsIn;
    metaListIn.flipCounter=flipCounter;
    metaListIn.startTime=startTime;
    metaListIn.endTime=datetime('now');
    metaListIn.numericMEXStr = fileread('MEXik.m');

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

