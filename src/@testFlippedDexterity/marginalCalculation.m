function marginalCalculation(obj,options)
    arguments
        obj(1,1) testFlippedDexterity
        options.maxIkIterationIn(1,1) double {mustBePositive} = 1500 
        options.weightsIn(1,6) double = [0.1 0.1 0.1 0.5 0.5 0.5]
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
        options.wideSearch(1,1) logical = false
    end

    obj.mustAllowCall('marginalCalculation')

    % start a time counter
    startTime= datetime('now');

    % prepare variables for calculation and storage
    nLocalPosition = size(obj.generateMapHandle.dataList.allTform,3);
    nLocalRotation = size(obj.generateMapHandle.dataList.allTform,4);
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);
    solutionFlag = obj.dataList.solutionFlag;
    adjacentMap = obj.dataList.adjacentMap;
    haveSolutionMask = solutionFlag == 1 | solutionFlag == 3 | solutionFlag == 4;
    existDataInSection = any(haveSolutionMask,4);
    existDataLength = squeeze(sum(existDataInSection,5));

    % calculate the mask
    updateMask = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,'logical');
    for iGlobalPosition=1:nGlobalPosition
        for iLocalPosition=1:nLocalPosition
            for iLocalRotaion=1:nLocalRotation
                adjacentList=adjacentMap(iGlobalPosition);
                currentDataLength=existDataLength(iLocalPosition,iLocalRotaion,iGlobalPosition);
                adjacentDataLength=existDataLength(iLocalPosition,iLocalRotaion,adjacentList);
                if options.wideSearch==false && sum(currentDataLength<adjacentDataLength,'all')/length(adjacentList) > 1/2
                    updateMask(iLocalPosition,iLocalRotaion,iGlobalPosition)=true;
                elseif sum(currentDataLength<adjacentDataLength,'all')/length(adjacentList) >= 1/2
                    updateMask(iLocalPosition,iLocalRotaion,iGlobalPosition)=true;
                end
            end
        end
    end

    % calculate the result
    [solutionFlag,configSolColumn,flipCounter] = obj.numericMP(updateMask,options.maxIkIterationIn,options.weightsIn, ...
                                                   forceGenerate=options.forceGenerate, ...
                                                   ignoreWarning=options.ignoreWarning);

    % prepare the data
    markIn='marginalCalculation';
    dataListIn.solutionFlag=solutionFlag;
    dataListIn.configSolColumn=configSolColumn;
    dataListIn.flipCounter=flipCounter;

    metaListIn.maxIkInteration=options.maxIkIterationIn;
    metaListIn.weights=options.weightsIn;
    metaListIn.flipCounter=flipCounter;
    metaListIn.startTime=startTime;
    metaListIn.updateCandidate=sum(updateMask,"all");
    metaListIn.endTime=datetime('now');
    metaListIn.numericMEXStr = fileread('MEXik.m');

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

