function [qConfig,solInfo] = MEXik(endEffectorName,tform,weights,initialGuess)
	%#codegen

    robotColumnIn=loadrobot('frankaEmikaPanda','DataFormat','column');
    removeBody(robotColumnIn,'panda_hand');

%     robotColumnIn=loadrobot('universal  UR5','DataFormat','column');

%     robotColumnIn=loadrobot('kukaIiwa  14','DataFormat','column');

%     robotColumnIn=loadrobot('kinova Gen3','DataFormat','column');

	ik = inverseKinematics('RigidBodyTree',robotColumnIn);
    ik.SolverParameters.MaxIterations=1500;
	
	[qConfig,solInfo] = ik(endEffectorName,tform,weights,initialGuess); 
end 