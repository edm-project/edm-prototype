function gikHandle = MEXgikGenerate(robotParameters,maxIkIterationIn,options)
%MEXGIKGENERATE generate the MEXGik MEX code (if it is different from backup)
    arguments
        robotParameters(1,1) struct
        maxIkIterationIn(1,1) double
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    tag = robotParameters.tag;
    backup_name = ['MEXgik_backup_',tag];
    mex_name = ['MEXgik_mex_',tag];
    file_name = './src/codegen/MEXgik.m';
    
    % read in the MEX text and backup
    MEXfile = fileread('MEXgik.m');
    if exist(backup_name, 'file') == 2
        MEXfileBackup = fileread(backup_name);
    else
        MEXfileBackup = "";
    end

    %% search if MEX file is same with the robotParameters
    % search the backup file if the backup totally matches the
    % robotParameters and parameters
    isMatched = true;
    isMatchedTemp = isKeywordMatched(MEXfileBackup,'generalizedInverseKinematics');
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isKeywordMatched(MEXfileBackup,eval2str(robotParameters.robotName));
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isEvaluationMatched(MEXfileBackup,'MaxIterations',num2str(maxIkIterationIn));
    isMatched = isMatched & isMatchedTemp;

    % compare changes and generate MEX file if necesssary
    if options.forceGenerate==true || isMatched == false
        disp(['tag [',tag,']: Attempting new generation with ',file_name,'...']);

        % search if the keyword existes
        searchKeyword(MEXfile,'generalizedInverseKinematics',file_name,options.ignoreWarning)
        searchKeyword(MEXfile,eval2str(robotParameters.robotName),file_name,options.ignoreWarning)

        % search if the file has a evaluation block
        searchEvaluation(MEXfile,'MaxIterations',num2str(maxIkIterationIn),file_name,options.ignoreWarning);

        % generate code
        disp('Generating new MEX code...');
        initialguess=randomConfiguration(robotParameters.robotColumn);      %#ok<NASGU>
        endEffectorName=robotParameters.endEffectorName;                    %#ok<NASGU>
        targetTform=zeros(4,4);                                             %#ok<NASGU>
        boundsIn=constraintJointBounds(robotParameters.robotColumn).Bounds; %#ok<NASGU>
        % These variables are prototype for code generation. They
        % are not used in Matlab, the code analyzer will give a warning.
        % Therefore we suppress the warning.
        codegen MEXgik -args {initialguess,endEffectorName,targetTform,boundsIn}
    
        % update backup
        currentFolder = pwd;
        fid = fopen([currentFolder,'/data/codegenBackup/',backup_name],'w');
        fprintf(fid,"%s",MEXfile);
        fclose(fid);

        % rename the generated file
        movefile(['MEXgik_mex.',mexext],[mex_name,'.',mexext]);
    end

    % search the return handle
    gikHandle = str2func(mex_name); 
end

