function jointDist = MEXjointBoundDistance(configSol)
    %#codegen
    robotColumnIn=loadrobot('frankaEmikaPanda','DataFormat','column');
    removeBody(robotColumnIn,'panda_hand');
    
%     robotColumnIn=loadrobot('universal  UR5','DataFormat','column');

%     robotColumnIn=loadrobot('kukaIiwa  14','DataFormat','column');
    
    jointDist = jointBoundDistance(robotColumnIn,configSol);
end

