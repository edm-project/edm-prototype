function [configSolCol,angleCol,sectionCol] = MEXnullspaceFixed(configSol,timeStep,nSection,maxStep)
    %#codegen
    % comment out the function arguments block:
    % circularDifference, circularMod
    % angleDifference, angleInSection
    % cartesianDifference, compensateCartesianDifference
    % dexterity...*3
    % jointNormalize
    % nullspaceFixed, nullspaceStep

    % comment out the error, warning and throw:
    % angleDifference, angleInSection, jointNormalize,
    % compensateCartesianDifference,dexterityReferenceVectors

    % enable the reshape function:
    % nullspaceStep
%     robotColumnIn=loadrobot('frankaEmikaPanda','DataFormat','column');
%     removeBody(robotColumnIn,'panda_hand');
% 
%     robotParameters.robotColumn=robotColumnIn;
%     robotParameters.base Name='panda_link0';
%     robotParameters.shoulderName='panda_link2';
%     robotParameters.elbowName='panda_link4';
%     robotParameters.endEffectorName='panda_link8';
%     robotParameters.robotName='frankaEmikaPanda';

%     robotColumnIn=loadrobot('universalUR5','DataFormat','column');
%     robotParameters.robotColumn=robotColumnIn;
%     robotParameters.base  Name='world';
%     robotParameters.shoulderName='shoulder_link';
%     robotParameters.elbowName='forearm_link';
%     robotParameters.endEffectorName='tool0';
%     robotParameters.robotName='universalUR5';

%     robotColumnIn=loadrobot('kukaIiwa14','DataFormat','column');
%     robotParameters.robotColumn=robotColumnIn;
%     robotParameters.base Name='world';
%     robotParameters.shoulderName='iiwa_link_2';
%     robotParameters.elbowName='iiwa_link_4';
%     robotParameters.endEffectorName='iiwa_link_ee_kuka';
%     robotParameters.robotName='kukaIiwa14';

    robotColumnIn=loadrobot('kinovaGen3','DataFormat','column');
    robotParameters.robotColumn=robotColumnIn;
    robotParameters.baseName='base_link';
    robotParameters.shoulderName='HalfArm1_Link';
    robotParameters.elbowName='ForeArm_Link';
    robotParameters.endEffectorName='EndEffector_Link';
    robotParameters.robotName='kinovaGen3';


    [configSolCol,angleCol,sectionCol] = nullspaceFixed(robotParameters,configSol,timeStep,nSection,maxStep);
end

