function jointBoundHandle = MEXjointBoundDistanceGenerate(robotParameters,options)
%MEXJOINTBOUNDDISTANCEGENERATE 
    arguments
        robotParameters(1,1) struct
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    tag = robotParameters.tag;
    backup_name = ['MEXjointBoundDistance_backup_',tag];
    mex_name = ['MEXjointBoundDistance_mex_',tag];
    file_name = './src/codegen/MEXjointBoundDistance.m';
    
    % read in the MEX text and backup
    MEXfile = fileread('MEXjointBoundDistance.m');
    if exist(backup_name, 'file') == 2
        MEXfileBackup = fileread(backup_name);
    else
        MEXfileBackup = "";
    end

    %% search if MEX file is same with the robotParameters
    % search the backup file if the backup totally matches the
    % robotParameters and parameters

    isMatched = true;
    isMatchedTemp = isKeywordMatched(MEXfileBackup,eval2str(robotParameters.robotName));
    isMatched = isMatched & isMatchedTemp;

    % compare changes and generate MEX file if necesssary
    if options.forceGenerate == true || isMatched == false 
        disp(['tag [',tag,']: Attempting new generation with ',file_name,'...']);

        % search if the keyword existes
        searchKeyword(MEXfile,eval2str(robotParameters.robotName),file_name,options.ignoreWarning)

        % generate code
        disp('Generating new MEX code...');
        configSol=randomConfiguration(robotParameters.robotColumn);  %#ok<NASGU>
        % These four variables are prototype for code generation. They
        % are not used in Matlab, the code analyzer will give a warning.
        % Therefore we suppress the warning.
        codegen MEXjointBoundDistance -args {configSol}
    
        % update backup
        currentFolder = pwd;
        fid = fopen([currentFolder,'/data/codegenBackup/',backup_name],'w');
        fprintf(fid,"%s",MEXfile);
        fclose(fid);

        % rename the generated file
        movefile(['MEXjointBoundDistance_mex.',mexext],[mex_name,'.',mexext]);
    end

    % search the return handle
    jointBoundHandle = str2func(mex_name); 

end

