function solutionFlagCheckHandle = MEXsolutionFlagCheckGenerate(robotParameters,options)
%MEXSOLUTIONFLAGCHECKGENERATE
%  - check if the backup is the same
%  - generate the code
%  - backup the time
%  - rename the file with the tag name
    arguments
        robotParameters(1,1) struct
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    tag = robotParameters.tag;
    if isMATLABReleaseOlderThan("R2022b")
        backup_name = ['MEXsolutionFlagCheck_backup_',tag];
        mex_name = ['MEXsolutionFlagCheck_mex_',tag];
        file_name = './src/codegen/MEXsolutionFlagCheck.m';
        MEXfile = fileread('MEXsolutionFlagCheck.m');
    else
        backup_name = ['MEXsolutionFlagCheck22b_backup_',tag];
        mex_name = ['MEXsolutionFlagCheck22b_mex_',tag];
        file_name = './src/codegen/MEXsolutionFlagCheck22b.m';
        MEXfile = fileread('MEXsolutionFlagCheck22b.m');
    end

    % read in the MEX text and backup
    if exist(backup_name, 'file') == 2
        MEXfileBackup = fileread(backup_name);
    else
        MEXfileBackup = "";
    end

    %% search if MEX file is same with the robotParameters
    % search the backup file if the backup totally matches the
    % robotParameters and parameters
    isMatched = true;
    isMatchedTemp = isKeywordMatched(MEXfileBackup,eval2str(robotParameters.robotName));
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isKeywordMatched(MEXfileBackup,'solutionFlagCheck');
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isEvaluationMatched(MEXfileBackup,'robotColumn','[^\s\n;]*');
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isEvaluationMatched(MEXfileBackup,'robotName',eval2str(robotParameters.robotName));
    isMatched = isMatched & isMatchedTemp;

        % generate the new code (check the matching before the generation)
    if options.forceGenerate == true || isMatched == false 
        disp(['tag [',tag,']: Attempting new generation with ',file_name,'...']);

        % search if the keyword existes
        searchKeyword(MEXfile,eval2str(robotParameters.robotName),file_name,options.ignoreWarning)
        searchKeyword(MEXfile,'solutionFlagCheck',file_name,options.ignoreWarning)

        % search if the file has a evaluation block
        searchEvaluation(MEXfile,'robotColumn','[^\s\n;]*',file_name,options.ignoreWarning);
        searchEvaluation(MEXfile,'robotName',eval2str(robotParameters.robotName),file_name,options.ignoreWarning);

        disp('Generating new MEX code...');
        % generate code
        configSol=randomConfiguration(robotParameters.robotColumn);  %#ok<NASGU>
        % These four variables are prototype for code generation. They
        % are not used in Matlab, the code analyzer will give a warning.
        % Therefore we suppress the warning.
        if isMATLABReleaseOlderThan("R2022b")
            codegen MEXsolutionFlagCheck -args {configSol}
        else
            codegen MEXsolutionFlagCheck22b -args {configSol}
        end
            

        % update backup
        currentFolder = pwd;
        fid = fopen([currentFolder,'/data/codegenBackup/',backup_name],'w');
        fprintf(fid,"%s",MEXfile);
        fclose(fid);

        % rename the generated file
        if isMATLABReleaseOlderThan("R2022b")
            movefile(['MEXsolutionFlagCheck_mex.',mexext],[mex_name,'.',mexext]);
        else
            movefile(['MEXsolutionFlagCheck22b_mex.',mexext],[mex_name,'.',mexext]);
        end

    end

    % search the return handle
    solutionFlagCheckHandle = str2func(mex_name); 

end

