function searchEvaluation(MEXfile,identifierName,evaluationString,fileString,ignoreWarning)
    % search if MEX has a line with "identifierName = [evaluationString];"
    % and warn+pause when not found
    searchReg=['[^\n]*',identifierName,'[\s]*=[\s]*',evaluationString,'[\s]*;[^\n]*'];
    matches = regexp(MEXfile,searchReg,'match');

    % warn user for MEX without "identifierName = [evaluationString];"
    if ignoreWarning==false && isempty(matches)
        warning(['There are no lines with "%s = %s;" in "%s" file, ' ...
                 'please check if you need to edit it'], ...
                 identifierName, evaluationString,fileString)
        disp('Continue Anyway?');
        pause on;
        pause;
    end
end

