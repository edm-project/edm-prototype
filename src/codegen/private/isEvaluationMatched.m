function isMatched = isEvaluationMatched(MEXfile,identifierName,evaluationString)
    % search if MEX has a line with "identifierName = [evaluationString];"
    % and return result
    searchReg=['[^\n]*',identifierName,'[\s]*=[\s]*',evaluationString,'[\s]*;[^\n]*'];
    matches = regexp(MEXfile,searchReg,'match');

    % warn user for MEX without "identifierName = [evaluationString];"
    if isempty(matches)
        disp(['There are no lines with "',identifierName,' = ',evaluationString,';" in backup file for the tag']);
        isMatched = false;
    else
        isMatched = true;
    end
end
