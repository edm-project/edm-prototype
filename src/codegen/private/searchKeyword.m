function searchKeyword(MEXfile,keyword,fileString,ignoreWarning)
    % search if MEX has a line with "keyword"
    % and warn+pause when not found
    searchReg=['[^\n]*',keyword,'[^\n]*'];
    matches = regexp(MEXfile,searchReg,'match');

    % warn user for MEX without "keyword"
    if ignoreWarning==false && isempty(matches)
        warning(['There are no lines with %s in %s file, ', ...
                 'please check if you need to edit it'], ...
                 keyword, fileString)
        disp('Continue Anyway?');
        pause on;
        pause;
    end
end

