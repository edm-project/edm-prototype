function isMatched = isKeywordMatched(MEXfile,keyword)
    % search if MEX has a line with "keyword;"
    % and return result
    searchReg=['[^\n]*',keyword,'[^\n]*'];
    matches = regexp(MEXfile,searchReg,'match');

    % warn user for MEX without "identifierName = [evaluationString];"
    if isempty(matches)
        disp(['There are no lines with "',keyword,'" in backup file for the tag']);
        isMatched = false;
    else
        isMatched = true;
    end
end

