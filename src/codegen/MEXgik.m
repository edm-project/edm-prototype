function [configSol,solInfo] = MEXgik(initialguess,endEffectorName,targetTform,boundsIn)
    %#codegen
    robotColumnIn=loadrobot('frankaEmikaPanda','DataFormat','column');
    removeBody(robotColumnIn,'panda_hand');
    
%     robotColumnIn=loadrobot('universal  UR5','DataFormat','column');

%     robotColumnIn=loadrobot('kukaIiwa  14','DataFormat','column');
    
    poseConst = constraintPoseTarget(endEffectorName,TargetTransform=targetTform);
    jointConst = constraintJointBounds(robotColumnIn);
    jointConst.Bounds=boundsIn;
    
    gik = generalizedInverseKinematics('RigidBodyTree',robotColumnIn,'ConstraintInputs',{'pose','jointbounds'});
    gik.SolverParameters.MaxIterations=2000;

    [configSol,solInfo] = gik(initialguess,poseConst,jointConst);
end

