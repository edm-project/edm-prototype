function [solutionFlagOut,configSolOut] = MEXsolutionFlagCheck22b(configSol)
    %#codegen

    robotColumnIn=loadrobot('frankaEmikaPanda','DataFormat','column');
    removeBody(robotColumnIn,'panda_hand');
    robotParameters.robotColumn=robotColumnIn;
    robotParameters.robotName='frankaEmikaPanda';

%     robotColumnIn=loadrobot('universalUR5','DataFormat','column');
%     robotParameters.robot  Column=robotColumnIn;
%     robotParameters.robotName='universalUR5';

%     robotColumnIn=loadrobot('kukaIiwa14','DataFormat','column');
%     robotParameters.robot  Column=robotColumnIn;
%     robotParameters.robotName='kukaIiwa14';

    [solutionFlagOut,configSolOut] = solutionFlagCheck22b(robotParameters,configSol);
end

