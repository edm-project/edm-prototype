function [solutionFlagOut,configSolOut] = MEXsolutionFlagCheck(configSol)
    %#codegen

    robotColumnIn=loadrobot('frankaEmikaPanda','DataFormat','column');
    removeBody(robotColumnIn,'panda_hand');
    robotParameters.robotColumn=robotColumnIn;
    robotParameters.robotName='frankaEmikaPanda';

%     robotColumnIn=loadrobot('universalUR5','DataFormat','column');
%     robotParameters.robot  Column=robotColumnIn;
%     robotParameters.robotName='universalUR5';

%     robotColumnIn=loadrobot('kukaIiwa14','DataFormat','column');
%     robotParameters.robot  Column=robotColumnIn;
%     robotParameters.robotName='kukaIiwa14';

%     robotColumnIn=loadrobot('kinovaGen3','DataFormat','column');
%     robotParameters.robot Column=robotColumnIn;
%     robotParameters.robotName='kinovaGen3';
    
    [solutionFlagOut,configSolOut] = solutionFlagCheck(robotParameters,configSol);
end

