function nullspaceFixedHandle = MEXnullspaceFixedGenerate(robotParameters,options)
%MEXNULLSPACEFIXEDGENERATE generate the MEXnullspaceFixed MEX function
%  - check if the backup is the same
%  - generate the code
%  - backup the time
%  - rename the file with the tag name
    arguments
        robotParameters(1,1) struct
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    tag = robotParameters.tag;
    backup_name = ['MEXnullspaceFixed_backup_',tag];
    mex_name = ['MEXnullspaceFixed_mex_',tag];
    file_name = './src/codegen/MEXnullspaceFixed.m';

    % read in the MEX text and backup
    MEXfile = fileread('MEXnullspaceFixed.m');
    if exist(backup_name, 'file') == 2
        MEXfileBackup = fileread(backup_name);
    else
        MEXfileBackup = "";
    end

    %% search if MEX file is same with the robotParameters
    % search the backup file if the backup totally matches the
    % robotParameters and parameters
    isMatched = true;
    isMatchedTemp = isKeywordMatched(MEXfileBackup,eval2str(robotParameters.robotName));
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isEvaluationMatched(MEXfileBackup,'robotColumn','[^\s\n;]*');
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isEvaluationMatched(MEXfileBackup,'baseName',eval2str(robotParameters.baseName));
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isEvaluationMatched(MEXfileBackup,'shoulderName',eval2str(robotParameters.shoulderName));
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isEvaluationMatched(MEXfileBackup,'elbowName',eval2str(robotParameters.elbowName));
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isEvaluationMatched(MEXfileBackup,'endEffectorName',eval2str(robotParameters.endEffectorName));
    isMatched = isMatched & isMatchedTemp;
    isMatchedTemp = isEvaluationMatched(MEXfileBackup,'robotName',eval2str(robotParameters.robotName));
    isMatched = isMatched & isMatchedTemp;
    
    % generate the new code (check the matching before the generation)
    if options.forceGenerate == true || isMatched == false 
        disp(['tag [',tag,']: Attempting new generation with ',file_name,'...']);

        % search if the keyword existes
        searchKeyword(MEXfile,eval2str(robotParameters.robotName),file_name,options.ignoreWarning)

        % search if the file has a evaluation block
        searchEvaluation(MEXfile,'robotColumn','[^\s\n;]*',file_name,options.ignoreWarning);
        searchEvaluation(MEXfile,'baseName',eval2str(robotParameters.baseName),file_name,options.ignoreWarning);
        searchEvaluation(MEXfile,'shoulderName',eval2str(robotParameters.shoulderName),file_name,options.ignoreWarning);
        searchEvaluation(MEXfile,'elbowName',eval2str(robotParameters.elbowName),file_name,options.ignoreWarning);
        searchEvaluation(MEXfile,'endEffectorName',eval2str(robotParameters.endEffectorName),file_name,options.ignoreWarning);
        searchEvaluation(MEXfile,'robotName',eval2str(robotParameters.robotName),file_name,options.ignoreWarning);

        disp('Generating new MEX code...');
        % generate code
        configSol=randomConfiguration(robotParameters.robotColumn);  %#ok<NASGU>
        timeStep = 0.3;                                              %#ok<NASGU>
        nSection = 24;                                               %#ok<NASGU>
        maxStep = 200;                                               %#ok<NASGU>
        % These four variables are prototype for code generation. They
        % are not used in Matlab, the code analyzer will give a warning.
        % Therefore we suppress the warning.
        codegen MEXnullspaceFixed -args {configSol,timeStep,nSection,maxStep}

        % update backup
        currentFolder = pwd;
        fid = fopen([currentFolder,'/data/codegenBackup/',backup_name],'w');
        fprintf(fid,"%s",MEXfile);
        fclose(fid);

        % rename the generated file
        movefile(['MEXnullspaceFixed_mex.',mexext],[mex_name,'.',mexext]);

    end

    % search the return handle
    nullspaceFixedHandle = str2func(mex_name); 
end


