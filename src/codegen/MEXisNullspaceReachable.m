function isReachable = MEXisNullspaceReachable(configSolInput,configSolNew)
    %#codegen
    robotColumnIn=loadrobot('frankaEmikaPanda','DataFormat','column');
    removeBody(robotColumnIn,'panda_hand');

    robotParameters.robotColumn=robotColumnIn;
    robotParameters.robotName='frankaEmikaPanda';
    robotParameters.baseName='panda_link0';
    robotParameters.shoulderName='panda_link2';
    robotParameters.elbowName='panda_link4';
    robotParameters.endEffectorName='panda_link8';

%     robotColumnIn=loadrobot('universalUR5','DataFormat','column');
%     robotParameters.robotColumn=robotColumnIn;
%     robotParameters.base  Name='world';
%     robotParameters.shoulderName='shoulder_link';
%     robotParameters.elbowName='forearm_link';
%     robotParameters.endEffectorName='tool0';
%     robotParameters.robotName='universalUR5';

%     robotColumnIn=loadrobot('kukaIiwa14','DataFormat','column');
%     robotParameters.robotColumn=robotColumnIn;
%     robotParameters.base  Name='world';
%     robotParameters.shoulderName='iiwa_link_2';
%     robotParameters.elbowName='iiwa_link_4';
%     robotParameters.endEffectorName='iiwa_link_ee_kuka';
%     robotParameters.robotName='kukaIiwa14';


    isReachable = isNullspaceReachable(robotParameters,configSolInput,configSolNew);
end

