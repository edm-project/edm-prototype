function spatialConnectivityCount(obj)
%SPATIALCONNECTIVITYCOUNT sum up the spatial connectivity count
% which means sum up the point which have connection with neigbouring points
    arguments
        obj(1,1) testSpatialConnectivity
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('spatialConnectivityCount');

    % input the infomation needed
    adjacentSolvableMap = obj.dataList.adjacentSolvableMap;
    adjacentIndexMap = obj.dataList.adjacentIndexMap;
    tajectoryValidFlag = obj.dataList.tajectoryValidFlag;
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);

    % preallocate the output
    spatialConnectivityCountTemp=zeros(nGlobalPosition,1);
    jointLimitCountTemp=zeros(nGlobalPosition,1);
    selfCollisionCountTemp=zeros(nGlobalPosition,1);

    % calculation for the spatialConnectivityCount
    for iGlobalPosition = 1:nGlobalPosition
        adjacentSolvableList = adjacentSolvableMap(iGlobalPosition);
        for iAdjacentListIndex = 1:length(adjacentSolvableList)
            adjacentPointIndex = adjacentSolvableList(iAdjacentListIndex); % for each Pair in adjacentSolvableMap
            
            % if the tajectory is valid, then add the counter.
            adjacentIndex = searchAdjecentIndex(adjacentIndexMap,iGlobalPosition,adjacentPointIndex);
            if tajectoryValidFlag(adjacentIndex)==1
                spatialConnectivityCountTemp(iGlobalPosition)=spatialConnectivityCountTemp(iGlobalPosition)+1;
            end
            if tajectoryValidFlag(adjacentIndex)==2
                jointLimitCountTemp(iGlobalPosition)=jointLimitCountTemp(iGlobalPosition)+1;
            end
            if tajectoryValidFlag(adjacentIndex)==3
                selfCollisionCountTemp(iGlobalPosition)=selfCollisionCountTemp(iGlobalPosition)+1;
            end
        end
    end

    % prepare for update
    markIn='spatialConnectivityCount';
    dataListIn.spatialConnectivityCount=spatialConnectivityCountTemp;
    dataListIn.jointLimitCount=jointLimitCountTemp;
    dataListIn.selfCollisionCount=selfCollisionCountTemp;
    metaListIn=struct;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

