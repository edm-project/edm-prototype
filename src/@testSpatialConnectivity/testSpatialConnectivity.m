classdef testSpatialConnectivity < handle
    %TESTSPATIALCONNECTIVITY class container of Spatial Connectivity test
    
    properties (SetAccess = immutable)
        version(1,1) double
    end 
    % end of immutable attributes

    properties (SetAccess = private)
        generateMapHandle(1,1) generateMap

        mark(1,:) cell {mustBeValidMarksNullPrepare}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
    end
    % end of the private attributes
    
    methods
        function obj = testSpatialConnectivity(generateMapHandleIn)
        %GENERATEGRID constructor
            arguments
                generateMapHandleIn(1,1) generateMap
            end
            obj.generateMapHandle=generateMapHandleIn;
            obj.version=2;
            obj.mark={'null'};
        end
    end
    % end of the constructor
        
    methods (Access=private)
        function mustAllowCall(obj,callMarkIn)
        % mustAllowCall: throw error if function current call is not allowed
            arguments
                obj(1,1) testSpatialConnectivity
                callMarkIn(1,:) char {mustBeValidMarks}
            end

            % allow no repeat for {'assignMode','assignAdjacent','spatialConnectivityCount'}
            mustNoRepeatGroup(obj,callMarkIn,{'assignMode','assignAdjacent','spatialConnectivityCount'});

            % allow only call {'fullCalculation'} after {'assignMode'} and {'assignAdjacent'}
            mustInSequence(obj,callMarkIn,{'fullCalculation'},{'assignMode'});
            mustInSequence(obj,callMarkIn,{'fullCalculation'},{'assignAdjacent'});

            % allow only call {'spatialConnectivityCount'} after {'fullCalculation'}
            mustInSequence(obj,callMarkIn,{'spatialConnectivityCount'},{'fullCalculation'});
        end

        function updateData(obj,markIn,metaListIn,dataListIn)
        % update the data in this procees, called in functional methods
            arguments
                obj(1,1) testSpatialConnectivity
                markIn(1,:) char {mustBeValidMarks}
                metaListIn(1,1) struct
                dataListIn(1,1) struct {mustBeValidDataList}
            end
            % check if this call is allowed
            obj.mustAllowCall(markIn);

            % update mark
            if isequal(obj.mark,{'prepared'})
                obj.mark={markIn};
            else
                obj.mark=[obj.mark,{markIn}];
            end

            %update metaList and dataList
            obj.metaList{length(obj.metaList)+1}=metaListIn;
            obj.dataList=combineStruct(obj.dataList,dataListIn);
        end
    end
    % end of the private methods

    methods (Access = public)
        function initialize(obj,options)
        % initalize this process
            arguments
                obj(1,1) testSpatialConnectivity
                options.ignoreWarning(1,1) logical = false
            end

            % warning when this class is not empty 
            if options.ignoreWarning==false && ~isequal(obj.dataList,struct())
                warning('This object testReachability is not empty. Initialization will wipe out the old data.')
                disp('Continue Anyway?');
                pause on;
                pause;
            end

            % initialize(reset) the data section of this class
            obj.mark={};
            obj.metaList={};
            obj.dataList=struct(); 
        end
    end
    % end of the robotMaps public methods

    methods (Access = private)
        [adjacentIndexMap,adjacentRoute,tajectoryValidFlag] = cubicNumeric(obj,timePoints,tSamples)
    end
    % end of the private methods

    methods (Access = public)
        assignMode(obj,mode)
        % Functional method. Assign the mode of the methods

        assignAdjacent(obj,adjacentRange,directionNumber,rotationNumber)
        % Functional method. Do marginal update of reachability

        fullCalculation(obj,algorithm,options)
        % Functional method. Do the calculation every adjacent point.

        spatialConnectivityCount(obj);
        % Functional method. Count the neigbouring point with valid tajectory

%         spatialConnectivityIndex(obj,counting);
        % Functional method. Do the calculation 
    end
    % end of the public functional method (definition in same folder)
end

function mustBeValidMarks(marksIn)
% Check for Valid input as mark.
    mustBeMember(marksIn,{'assignMode','assignAdjacent','fullCalculation', ...
                          'spatialConnectivityCount'})
end
 
function mustBeValidMarksNullPrepare(marksIn)
% Check for Valid input as mark. Allow null and prepare.
    mustBeMember(marksIn,{'null','prepared','assignMode','assignAdjacent','fullCalculation', ...
                          'spatialConnectivityCount'})
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{'mode', ...
                'adjacentMap','adjacentSolvableMap','adjacentIndexMap', ...
                'adjacentRoute','tajectoryValidFlag' ...
                'maxAdjacent', ...
                'spatialConnectivityCount', 'jointLimitCount','selfCollisionCount'...
                'spatialConnectivityIndex'});
end

