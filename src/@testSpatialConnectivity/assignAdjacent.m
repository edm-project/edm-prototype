function assignAdjacent(obj,adjacentRange,directionNumber,rotationNumber)
%ASSIGNADJACENT assign the adjacentMap and adjacentSolvableMap
    arguments
        obj(1,1) testSpatialConnectivity
        adjacentRange(1,:) char {mustBeValidRange}
        directionNumber(1,1) double {mustBeInteger,mustBePositive}
        rotationNumber(1,1) double {mustBeInteger,mustBePositive}
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('assignAdjacent');

    % validate the input
    nLocalPosition=size(obj.generateMapHandle.dataList.allTform,3);
    nLocalRotation=size(obj.generateMapHandle.dataList.allTform,4);
    nGlobalRotation=size(obj.generateMapHandle.dataList.allTform,5);

    if nLocalPosition<=directionNumber || nLocalRotation<=rotationNumber
        ME = MException('testSpatialConnectivity:assignAdjacent', ...
            'directionNumber[%d] or rotationNumber[%d] out of range[%d,%d]', ...
            directionNumber,rotationNumber,nLocalPosition,nLocalRotation);
        throw(ME)
    end

    % collect the data of the Grid and generate adjacentMap
    gridMark=obj.generateMapHandle.robotMapsHandle.generateGridSlot.mark{1};
    gridMetaList=obj.generateMapHandle.robotMapsHandle.generateGridSlot.metaList{1};
    positionRow=obj.generateMapHandle.commonDataHandle.positionRow;
    solutionFlag=squeeze(obj.generateMapHandle.dataList.testReachability.dataList.solutionFlag(directionNumber,rotationNumber,:));
    
    % initialize containerMaps
    adjacentMap=containers.Map(1:nGlobalRotation,cell(1,nGlobalRotation));
    adjacentSolvableMap=containers.Map(1:nGlobalRotation,cell(1,nGlobalRotation));

    if isequal(gridMark,'cartesian2') || isequal(gridMark,'cartesian3')
        % get the adjacent infomation
        shiftUnit=gridMetaList.sphereRadius*2;
        [shiftMatrix,maxAdjacent]=shiftMatrixCartesian(gridMark,adjacentRange,shiftUnit);
        
        % merge the adjacent pairs
        for iShift=1:size(shiftMatrix,1)
            % full adjacent List
            [keys,values] = shiftPointsCartesian(positionRow,shiftMatrix(iShift,:));
            adjacentMap = mergeContainerMap(adjacentMap,keys,values);
            
            % filter out the keys (and values) with no solution
            solutionMask=(solutionFlag==1);
            solutionIndex=find(solutionMask);
            [~,~,solutionKeyIndex]=intersect(solutionIndex,keys);
            solutionKeyMask=zeros(1,length(keys),'logical');
            solutionKeyMask(solutionKeyIndex)=true;
            keysFiltered=keys(solutionKeyMask); 
            valuesFiltered=values(solutionKeyMask);

            %filter out the values (and keys) with no reachability solution
            noSolutionMask=(solutionFlag~=1);
            noSolutionIndex=find(noSolutionMask);
            [~,~,noSolutionValuesIndex]=intersect(noSolutionIndex,valuesFiltered);
            noSolutionValuesMask=ones(1,length(valuesFiltered),'logical');
            noSolutionValuesMask(noSolutionValuesIndex)=false;
            keysFiltered=keysFiltered(noSolutionValuesMask);
            valuesFiltered=valuesFiltered(noSolutionValuesMask);

            % adjacent List with reachability solutions
            adjacentSolvableMap = mergeContainerMap(adjacentSolvableMap,keysFiltered,valuesFiltered);
        end
    else
        ME = MException('testSpatialConnectivity:assignAdjacent:noImplementation', ...
            'The assignAdjacent is not implemented for [%s] grid', ...
            gridMark);
        throw(ME)
    end

    % prepare for update
    markIn='assignAdjacent';
    dataListIn.adjacentMap=adjacentMap;
    dataListIn.adjacentSolvableMap=adjacentSolvableMap;
    dataListIn.maxAdjacent=maxAdjacent;
    metaListIn.adjacentRange=adjacentRange;
    metaListIn.directionNumber=directionNumber;
    metaListIn.rotationNumber=rotationNumber;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);

end

function mustBeValidRange(modeIn)
    % Check for Valid input as mode for calculation
    mustBeMember(modeIn,{'face','faceEdge','faceEdgeCorner'});
end