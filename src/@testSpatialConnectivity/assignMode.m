function assignMode(obj,mode)
%ASSIGNMODE assign the mode of the testSpatialConnectivity
    arguments
        obj(1,1) testSpatialConnectivity
        mode(1,:) char {mustBeValidMode} 
    end
    
    % check if allow callable for efficiency
    obj.mustAllowCall('assignMode');

    % prepare for update
    markIn='assignMode';
    dataListIn.mode=mode;
    metaListIn=struct();

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

function mustBeValidMode(modeIn)
    % Check for Valid input as mode for calculation
    mustBeMember(modeIn,{'numeric'});
end

