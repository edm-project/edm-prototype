function [adjacentIndexMap,adjacentRoute,tajectoryValidFlag] = cubicNumeric(obj,timePoints,tSamples)
%GENERALCUBIC general cubic mode of the reachability test
% in:
% [timePoints] and [tSamples] are defined by cubicpolytaj, which controls the interpolation
% out:
% [adjacentIndexMap] a containers.Map type that accept 'x,y' and return the
%   index i, which means adjacentRoute(:,:,i) is the connection between point
%   x and y.(x should be smaller than y)
% [adjacentRoute] a double Matrix that record the interpolation points. The
%   3 index are for: (joint angles in one pose,interpolation in one tajectory,different tajectories)
% [tajectoryValidFlag] a double array that record the validity of the
%   interpolation (route planning)
%   1=valid   2=jointLimit  3=selfCollision
    arguments
        obj(1,1) testSpatialConnectivity
        timePoints(1,2) double = [0 1]
        tSamples(1,:) double = 0:0.05:1
    end

    % collect the data needed
    adjacentSolvableMap = obj.dataList.adjacentSolvableMap;
    configSolColumn = obj.generateMapHandle.dataList.testReachability.dataList.configSolColumn;
    markAssignAdjacentIndex = getIndexCell(obj.mark,'assignAdjacent');
    directionNumber = obj.metaList{markAssignAdjacentIndex}.directionNumber;
    rotationNumber = obj.metaList{markAssignAdjacentIndex}.rotationNumber;
    robotColumn = obj.generateMapHandle.commonDataHandle.robotParameters.robotColumn;

    % calculate size infomation
    nAngle = size(configSolColumn,1);
    nSamples = length(tSamples);
    nPairs = lengthContainerMap(adjacentSolvableMap)/2;

    %progressbar
    fig = progressbar('Spatial Connectivity: numeric mode cubic algorithm',nPairs);

    % initialization (pre-allocation) of the new data
    adjacentIndex = 0;
    adjacentIndexMap = containers.Map('KeyType','char','ValueType','any');
    adjacentRoute = zeros(nAngle,nSamples,nPairs);
    tajectoryValidFlag = ones(nPairs,1);

    % doing interpolation with cubicpoly
    for iPointStart=1:adjacentSolvableMap.Count
        adjacentSolvablePointStart = adjacentSolvableMap(iPointStart);
        for iPointEndIndex=1:length(adjacentSolvablePointStart)
            iPointEnd = adjacentSolvablePointStart(iPointEndIndex);
            if iPointStart<iPointEnd  % for each pair that iPointStart<iPointEnd in the adjacentSolvableMap
                % directly calculate the cubicpolynom interpolation
                wayPoints = [configSolColumn(:,directionNumber,rotationNumber,iPointStart), ...
                            configSolColumn(:,directionNumber,rotationNumber,iPointEnd)];
                [tajectory,~,~,~]= cubicpolytraj(wayPoints,timePoints,tSamples);

                % assign data to the points
                adjacentIndex = adjacentIndex + 1;
                adjacentRoute(:,:,adjacentIndex) = tajectory;
                adjacentIndexMap([num2str(iPointStart),',',num2str(iPointEnd)]) = adjacentIndex;

                % check the joint limits and dexterity for points on tajectory
                for iPosition=1:size(adjacentRoute,2)
                    if checkJointLimits(robotColumn,tajectory(:,iPosition))==true
                        tajectoryValidFlag(adjacentIndex) = 2;
                        break;
                    end
                    if checkCollision(robotColumn,tajectory(:,iPosition))==true
                        tajectoryValidFlag(adjacentIndex) = 3;
                        break;
                    end
                end
                % update the progressbar
                progressbar(); 
            end
        end
    end
    
    % close the progressbar
    close(fig);
end

