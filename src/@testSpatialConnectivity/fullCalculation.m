function fullCalculation(obj,algorithm,options)
%FULLCALCULATION Do the full calculation. 
    arguments
        obj(1,1) testSpatialConnectivity
        algorithm(1,:) char {mustBeMember(algorithm,{'cubicpoly'})}
        options.timePoints(1,2) double = [0 1]
        options.tSamples(1,:) double = 0:0.05:1
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('fullCalculation');

    % error if tSamples and timePoints not matched
    if options.timePoints(1)~=options.tSamples(1) || ...
       options.timePoints(2)~=options.tSamples(length(options.tSamples))
        ME = MException('testSpatialConnectivity:fullCalculation:paramNoMatch', ...
            'Start [%d][%d] and End [%d][%d] time not matched!', ...
            options.timePoints(1),options.tSamples(1), ...
            options.timePoints(2),options.tSamples(length(tSamples)));
        throw(ME)
    end

    % start a time counter
    startTime= datetime('now');

    % collect mode
    mode = obj.dataList.mode;

    % call different functions according to modes and algorithms
    if isequal(algorithm,'cubicpoly') && isequal(mode,'numeric')
        [adjacentIndexMap,adjacentRoute,tajectoryValidFlag] = obj.cubicNumeric(options.timePoints,options.tSamples);
    else
        ME = MException('testSpatialConnectivity:fullCalculation:noAlgorithm', ...
            'Algorithm with mode [%s][%s] unsupported!', ...
            algorithm,mode);
        throw(ME)
    end
    
    % prepare for update
    markIn='fullCalculation';
    dataListIn.adjacentIndexMap=adjacentIndexMap;
    dataListIn.adjacentRoute=adjacentRoute;
    dataListIn.tajectoryValidFlag=tajectoryValidFlag;
    metaListIn.algorithm=algorithm;
    metaListIn.timePoints=options.timePoints;
    metaListIn.tSamples=options.tSamples;
    metaListIn.startTime=startTime;
    metaListIn.endTime=datetime('now');

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

