classdef twoPointRRT  < handle

    properties (SetAccess = immutable)
        version(1,1) double
    end 
    % end of immutable attributes
    
    properties (SetAccess=private)
        mark(1,:) cell {mustBeValidMarksNullPrepare} = {}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
        
        robotMapsClassHandle(1,1) robotMaps
    end 
    % end of private attributes

    methods
        function obj = twoPointRRT(robotMapsClassIn)
            obj.version=1;
            obj.mark={'null'};
            obj.robotMapsClassHandle=robotMapsClassIn;
        end
        
        function initialize(obj,options)
        % initalize this process
            arguments
                obj(1,1) twoPointRRT
                options.ignoreWarning(1,1) logical = false
            end

            % warning when this class is not empty 
            if options.ignoreWarning==false && ~isequal(obj.dataList,struct())
                warning('This object testDexterity is not empty. Initialization will wipe out the old data.')
                disp('Continue Anyway?');
                pause on;
                pause;
            end

            % initialize(reset) the data section of this class
            obj.mark={'prepared'};
            obj.metaList={};
            obj.dataList=struct(); 
        end
    end
    % end of the constructor

    methods (Access=private)
        function mustAllowCall(obj,callMarkIn)
            % mustAllowCall: throw error if function current call is not allowed
            arguments
                obj(1,1) 
                callMarkIn(1,:) char {mustBeValidMarks}
            end

            mustNoRepeatGroup(obj,callMarkIn,{'twoPointRRTStart','correlationCalculation','twoPointRRTTest'});
            mustInSequence(obj,callMarkIn,{'correlationCalculation'},{'twoPointRRTStart'});
            mustInSequence(obj,callMarkIn,{'twoPointRRTTest'},{'correlationCalculation'});
        end

        function updateData(obj,markIn,metaListIn,dataListIn)
            % update the data in this process, called in functional methods
            arguments
                obj(1,1) 
                markIn(1,:) char {mustBeValidMarks}
                metaListIn(1,1) struct
                dataListIn(1,1) struct {mustBeValidDataList}
            end
            % check if this call is allowed
            obj.mustAllowCall(markIn);

            % update mark
            if isequal(obj.mark,{'prepared'})
                obj.mark={markIn};
            else
                obj.mark=[obj.mark,{markIn}];
            end

            obj.dataList = combineStruct(obj.dataList,dataListIn);
            obj.metaList{length(obj.metaList)+1}=metaListIn;
        end
    end

    methods (Access = public)
        twoPointRRTStart(obj,startTform,endTform);
        % assign start and end Tform, then smoothly interpolate

        correlationCalculation(obj);
        % calculate the correlation of the Maps
        
        twoPointRRTTest(obj,options);
        % predict and Test the result
    end
    % end of the public functional method (definition in same folder)
    
end

function mustBeValidMarks(marksIn)
% Check for Valid input as mark.
    mustBeMember(marksIn,{'twoPointRRTStart','correlationCalculation','twoPointRRTTest'})
end

function mustBeValidMarksNullPrepare(marksIn)
% Check for Valid input as mark. Allow null and prepare.
    mustBeMember(marksIn,{'null','prepared','twoPointRRTStart','correlationCalculation','twoPointRRTTest'})
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{'tformMatrix','markSequence','solutionFlagMap', ...
                                     'correlationReachability','correlationDexterity','rrtPlaner','worldObject',...
                                     'randomStartList','reachabilityStartList','dexterityStartList', ...
                                     'randomConfigSuccess','reachabilityConfigSuccess','dexterityConfigSuccess', ...
                                     'randomJointWayPoints','reachabilityJointWayPoints','dexterityJointWayPoints'});
end

