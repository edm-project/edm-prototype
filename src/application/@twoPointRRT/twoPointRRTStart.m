function twoPointRRTStart(obj,startTform,endTform,tSamplesNum)
    arguments
        obj(1,1) twoPointRRT
        startTform(4,4) double
        endTform(4,4) double
        tSamplesNum(1,1) double
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('twoPointRRTStart');

    % interpolate smoothly
    [tformMatrix,~,~] = transformtraj(startTform,endTform,[0 1],linspace(0,1,tSamplesNum));

    % prepare the data
    markIn='twoPointRRTStart';
    dataListIn.tformMatrix = tformMatrix;
    metaListIn.startTform = startTform;
    metaListIn.endTform = endTform;

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

