function correlationCalculation(obj)
    arguments
        obj(1,1)
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('correlationCalculation');

    % read in the data
    tformMatrix = obj.dataList.tformMatrix;
    robotMapsClass = obj.robotMapsClassHandle;
    [markSequence,solutionFlagMap] = batchPredictor(robotMapsClass,tformMatrix);
    correlationReachability = correlationReachability2D(robotMapsClass,solutionFlagMap);
    correlationDexterity = correlationDexterity2D(robotMapsClass,solutionFlagMap);

    % prepare the data
    markIn='correlationCalculation';
    dataListIn.markSequence = markSequence;
    dataListIn.solutionFlagMap = solutionFlagMap;
    dataListIn.correlationReachability = correlationReachability;
    dataListIn.correlationDexterity = correlationDexterity;
    metaListIn = struct();

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

