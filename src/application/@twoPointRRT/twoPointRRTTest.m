function twoPointRRTTest(obj,options)
    arguments
        obj(1,1) twoPointRRT
        options.collisionObjects = {}
        options.randomSample(1,1) double = 20
        options.reachabilitySample(1,1) double = 20
        options.dexteritySample(1,1) double = 20
        options.dexteritySection(1,1) double = 2
        options.numInterp(1,1) double = 30
        options.method {mustBeMember(options.method,{'normal','MEX'})} = 'MEX'
        options.maxItertation(1,1) double = 1500
        options.maxConstraintRepeat(1,1) double = 15
        options.weightsIn(1,6) double = [0.1 0.1 0.1 0.5 0.5 0.5]
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('twoPointRRTTest');

    % get the data
    correlationReachability = obj.dataList.correlationReachability;
    correlationDexterity = obj.dataList.correlationDexterity;
    markSequencePair =  obj.dataList.markSequence(:,[1,end]);
    fig = progressbar('Two Point RRT Test',options.randomSample+options.reachabilitySample+options.dexteritySample);
    
    testDexterity = obj.robotMapsClassHandle.generateMapSlot.dataList.testDexterity;
    configSolColumn = testDexterity.dataList.configSolColumn;
    allTform = obj.robotMapsClassHandle.generateMapSlot.dataList.allTform;
    positionRow = obj.robotMapsClassHandle.commonDataSlot.positionRow;

    robotParameters = obj.robotMapsClassHandle.commonDataSlot.robotParameters;

    % get the parameters
    nGlobalPosition = size(positionRow,1);
    nFlip = size(correlationDexterity,2);

    % get the inverseKinematicsHandle
    if isequal(options.method,'MEX')
        ikHandle = MEXikGenerate(robotParameters,options.maxItertation, ...
                   forceGenerate=options.forceGenerate, ...
                   ignoreWarning=options.ignoreWarning);
    elseif isequal(options.method,'normal')
        ikHandle = inverseKinematics('RigidBodyTree',robotParameters.robotColumn);
        ikHandle.SolverParameters.MaxIterations=options.maxItertation;
    end

    % get the rrt Handle
    rrt = manipulatorRRT(robotParameters.robotColumn,options.collisionObjects);

    % shuffle the random
    rng('shuffle');
    randomTimer = zeros(options.randomSample,1);
    reachabilityTimer = zeros(options.reachabilitySample,1);
    dexterityTimer = zeros(options.dexteritySample,options.dexteritySection);

    %% calculate the sample point
    % calculate the random Sample
    randomStartList = randi(nGlobalPosition,[options.randomSample 1]);

    % get suggested reachability Sample
    [valueTemp,reachabilityStartList] = sort(correlationReachability,'descend');
    reachabilityStartList = randomSampleSorted(valueTemp,reachabilityStartList,options.reachabilitySample);

    % get suggested dexterity Sample (of Position)
    dexterityTmpList = zeros(nGlobalPosition*nFlip,2);
    for iFlip = 1:nFlip
        dexterityTmpList(1+(iFlip-1)*nGlobalPosition:iFlip*nGlobalPosition,1) = 1:nGlobalPosition;
        dexterityTmpList(1+(iFlip-1)*nGlobalPosition:iFlip*nGlobalPosition,2) = correlationDexterity(:,iFlip);
        dexterityTmpList(1+(iFlip-1)*nGlobalPosition:iFlip*nGlobalPosition,3) = iFlip;
    end
    [valueTemp,IndexList] = sort(dexterityTmpList(:,2),'descend');
    sortedIndexList = randomSampleSorted(valueTemp,IndexList,options.dexteritySample);
    dexterityStartList = dexterityTmpList(sortedIndexList,:);

    %% calculate the RRT result
    % calculate the random path
    randomConfigSuccess = zeros(options.randomSample,1);
    randomJointWayPoints = cell(options.randomSample,1);
    for iRandom = 1:options.randomSample
        progressbar();
        % calculate the shifting
        shiftVector = positionRow(randomStartList(iRandom),:);
        [successFlag,markSequenceShift] = shiftMarkSequence(positionRow,markSequencePair,shiftVector);
        if successFlag==false
            randomConfigSuccess(iRandom) = 2;
            continue;
        end
        
        % calculate the inverseKinematics
        tformStart = allTform(:,:,markSequenceShift(1,1),markSequenceShift(2,1),markSequenceShift(3,1));
        tformEnd = allTform(:,:,markSequenceShift(1,2),markSequenceShift(2,2),markSequenceShift(3,2));

        [configSolStart,successMarkStart] = solveIK(ikHandle,robotParameters,tformStart, ...
                                          options.weightsIn,options.maxConstraintRepeat);
        [configSolEnd,successMarkEnd] = solveIK(ikHandle,robotParameters,tformEnd, ...
                                          options.weightsIn,options.maxConstraintRepeat);
        if successMarkStart~=1 || successMarkEnd~=1
            if successMarkStart==2 || successMarkEnd==2
                randomConfigSuccess(iRandom) = 2;
            elseif successMarkStart==4 || successMarkEnd==4
                randomConfigSuccess(iRandom) = 4;
            else
                randomConfigSuccess(iRandom) = 3;
            end
            continue;
        end

        % calculate the RRT
        tic
        path = plan(rrt,configSolStart',configSolEnd');
        randomTimer(iRandom) = toc;
        interpPath = interpolate(rrt,path,options.numInterp);
        successMark = checkPathValidity(robotParameters,interpPath);
        randomConfigSuccess(iRandom) = successMark;
        randomJointWayPoints{iRandom} = interpPath';
    end

    % calculate the reachability path
    reachabilityConfigSuccess = zeros(options.reachabilitySample,1);
    reachabilityJointWayPoints = cell(options.reachabilitySample,1);
    for iReachability = 1:options.reachabilitySample
        progressbar();
        % calculate the shifting
        shiftVector = positionRow(reachabilityStartList(iReachability),:);
        [successFlag,markSequenceShift] = shiftMarkSequence(positionRow,markSequencePair,shiftVector);
        if successFlag==false
            reachabilityConfigSuccess(iReachability) = 2;
            continue;
        end
        
        % calculate the inverseKinematics
        tformStart = allTform(:,:,markSequenceShift(1,1),markSequenceShift(2,1),markSequenceShift(3,1));
        tformEnd = allTform(:,:,markSequenceShift(1,2),markSequenceShift(2,2),markSequenceShift(3,2));

        [configSolStart,successMarkStart] = solveIK(ikHandle,robotParameters,tformStart, ...
                                          options.weightsIn,options.maxConstraintRepeat);
        [configSolEnd,successMarkEnd] = solveIK(ikHandle,robotParameters,tformEnd, ...
                                          options.weightsIn,options.maxConstraintRepeat);
        if successMarkStart~=1 || successMarkEnd~=1
            if successMarkStart==2 || successMarkEnd==2
                reachabilityConfigSuccess(iReachability) = 2;
            elseif successMarkStart==4 || successMarkEnd==4
                reachabilityConfigSuccess(iReachability) = 4;
            else
                reachabilityConfigSuccess(iReachability) = 3;
            end
            continue;
        end

        % calculate the RRT
        tic
        path = plan(rrt,configSolStart',configSolEnd');
        reachabilityTimer(iReachability) = toc;
        interpPath = interpolate(rrt,path,options.numInterp);
        successMark = checkPathValidity(robotParameters,interpPath);
        reachabilityConfigSuccess(iReachability) = successMark;
        reachabilityJointWayPoints{iReachability} = interpPath';
    end

    % calculate the dexterity path
    dexterityConfigSuccess = zeros(options.dexteritySample,1);
    dexterityJointWayPoints = cell(options.dexteritySample,1);
    for iDexterity = 1:options.dexteritySample
        progressbar();
        shiftVector = positionRow(dexterityStartList(iDexterity,1),:);
        [successFlag,markSequenceShift] = shiftMarkSequence(positionRow,markSequencePair,shiftVector);
        if successFlag==false
            dexterityConfigSuccess(iReachability) = 2;
            continue;
        end

        startSection = largestJointBoundSection(testDexterity,markSequenceShift(1,1), ...
                                                   markSequenceShift(2,1), ...
                                                   markSequenceShift(3,1), ...
                                                   dexterityStartList(iDexterity,3), ...
                                                   options.dexteritySection);

        endSection = largestJointBoundSection(testDexterity,markSequenceShift(1,2), ...
                                           markSequenceShift(2,2), ...
                                           markSequenceShift(3,2), ...
                                           dexterityStartList(iDexterity,3), ...
                                           options.dexteritySection);
        
        if isempty(startSection)||isempty(endSection)
            dexterityConfigSuccess(iReachability) = 2;
            continue;
        end

        sectionList = zeros(length(startSection),2);
        for iStart = 1:length(startSection)
            sectionList(iStart,1) = startSection(iStart);
            distance = zeros(length(endSection),1);
            for iEnd =1:length(endSection)
                distance(iEnd) = norm(configSolColumn(:,markSequenceShift(1,2),markSequenceShift(2,2),markSequenceShift(3,2),endSection(iEnd),dexterityStartList(iDexterity,3))- ...
                                      configSolColumn(:,markSequenceShift(1,1),markSequenceShift(2,1),markSequenceShift(3,1),startSection(iStart),dexterityStartList(iDexterity,3)));
            end
            value = endSection(min(distance,[],'all')==distance);
            sectionList(iStart,2) = value(1);
        end

        distTmp = inf;
        for iStart = 1:length(startSection)
            configSolStart = configSolColumn(:,markSequenceShift(1,1),markSequenceShift(2,1),markSequenceShift(3,1),sectionList(iStart,1),dexterityStartList(iDexterity,3));
            configSolEnd = configSolColumn(:,markSequenceShift(1,2),markSequenceShift(2,2),markSequenceShift(3,2),sectionList(iStart,2),dexterityStartList(iDexterity,3));
            [solutionFlagOutStart,~] = solutionFlagCheck(robotParameters,configSolStart);
            [solutionFlagOutEnd,~] = solutionFlagCheck(robotParameters,configSolStart);

            if solutionFlagOutStart~=1||solutionFlagOutEnd~=1
                continue;
            end
            
            tic
            path = plan(rrt,configSolStart',configSolEnd');
            dexterityTimer(iDexterity,iStart) = toc;

            interpPath = interpolate(rrt,path,options.numInterp);
            successMark = checkPathValidity(robotParameters,interpPath);
            if successMark ~=1
                continue
            end
            [pathLength,cartLength] = pathAndCartDistance(robotParameters,{interpPath'});
            dist = pathLength + cartLength;
            if dist<distTmp
                distTmp = dist;
                pathTmp = interpPath;
            end
        end

        dexterityConfigSuccess(iDexterity) = successMark;
        if distTmp~=inf
            dexterityJointWayPoints{iDexterity} = pathTmp';
        end

    end

    close(fig);

    % prepare the data
    markIn='twoPointRRTTest';
    dataListIn.randomStartList = randomStartList;
    dataListIn.reachabilityStartList = reachabilityStartList;
    dataListIn.dexterityStartList = dexterityStartList;
    dataListIn.randomConfigSuccess= randomConfigSuccess;
    dataListIn.randomJointWayPoints=randomJointWayPoints;
    dataListIn.reachabilityConfigSuccess=reachabilityConfigSuccess;
    dataListIn.reachabilityJointWayPoints=reachabilityJointWayPoints;
    dataListIn.dexterityConfigSuccess = dexterityConfigSuccess;
    dataListIn.dexterityJointWayPoints = dexterityJointWayPoints;
    metaListIn.randomSample = options.randomSample;
    metaListIn.reachabilitySample = options.reachabilitySample;
    metaListIn.dexteritySample = options.dexteritySample;
    metaListIn.dexteritySection = options.dexteritySection;
    metaListIn.randomTimer = randomTimer;
    metaListIn.reachabilityTimer = reachabilityTimer;
    metaListIn.dexterityTimer = dexterityTimer;

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);

end

function sampleOut = randomSampleSorted(value,index,nSample)
    cutIndex = 1;
    while true
        maxCurrent = max(value(cutIndex:end),[],'all');
        maxCount = sum(value>=maxCurrent,'all');
        cutIndex = maxCount+1;
        if maxCount>=nSample
            break;
        end
    end
    sampleOut = index(randperm(cutIndex-1,nSample));
end

function [configSol,successMark] = solveIK(ikHandle,robotParameters,targetTform,weightsIn,maxConstraintRepeat)
    for iRepeatCounter=1:maxConstraintRepeat
        initialguess = randomConfiguration(robotParameters.robotColumn);
        [configSol,solInfo] = ikHandle(robotParameters.endEffectorName,targetTform,weightsIn,initialguess);
        
        % if IK solver can't find Solution.
        if isequal(solInfo.Status,'best available')
            successMark = 2;
            break;

        elseif isequal(solInfo.Status,'success')
            % check the collision and joint Limit
            [solutionFlagOut,configSolOut] = solutionFlagCheck(robotParameters,configSol);
        
            if solutionFlagOut == 1
                configSol = configSolOut;
                successMark = 1;
                break;
            else
                successMark = solutionFlagOut;
                continue;
            end
        end
    end
end

function successMark = checkPathValidity(robotParameters,path)
    successMark = 1;
    for iPositionMark = 1:size(path,1)
        [solutionFlagOut,~] = solutionFlagCheck(robotParameters,path(iPositionMark,:)');
        
        if solutionFlagOut ~= 1
            successMark = solutionFlagOut;
            break;
        end
    end
end