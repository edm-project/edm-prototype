function twoPointRRTTest(obj,options)
    arguments
        obj(1,1) twoPointRRTFixed
        options.collisionObjects = {}
        options.randomSample(1,1) double = 20
        options.dexterityNoFlipSample(1,1) double = 20
        options.dexteritySample(1,1) double = 20
        options.dexteritySuggestPair(1,1) double = 10
        options.numInterp(1,1) double = 30
        options.method {mustBeMember(options.method,{'normal','MEX'})} = 'MEX'
        options.maxItertation(1,1) double = 1500
        options.maxConstraintRepeat(1,1) double = 15
        options.weightsIn(1,6) double = [0.1 0.1 0.1 0.5 0.5 0.5]
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('twoPointRRTTest');

    % get the data
    iLocalPositionS = obj.dataList.iLocalPositionS;
    iLocalRotationS = obj.dataList.iLocalRotationS;
    iGlobalPositionS = obj.dataList.iGlobalPositionS;
    iLocalPositionE = obj.dataList.iLocalPositionE;
    iLocalRotationE = obj.dataList.iLocalRotationE;
    iGlobalPositionE = obj.dataList.iGlobalPositionE;

    fig = progressbar('Two Point RRT Test',options.randomSample+options.dexterityNoFlipSample+options.dexteritySample);
    
    testDexterity = obj.robotMapsClassHandle.generateMapSlot.dataList.testDexterity;
    configSolColumn = testDexterity.dataList.configSolColumn;
    allTform = obj.robotMapsClassHandle.generateMapSlot.dataList.allTform;
    startTform = squeeze(allTform(:,:,iLocalPositionS,iLocalRotationS,iGlobalPositionS));
    endTform = squeeze(allTform(:,:,iLocalPositionE,iLocalRotationE,iGlobalPositionE));
%     positionRow = obj.robotMapsClassHandle.commonDataSlot.positionRow;

    robotParameters = obj.robotMapsClassHandle.commonDataSlot.robotParameters;

    % get the parameters
%     nGlobalPosition = size(positionRow,1);
%     nFlip = testDexterity.dataList.flipCounter-1;

    % get the inverseKinematicsHandle
    if isequal(options.method,'MEX')
        ikHandle = MEXikGenerate(robotParameters,options.maxItertation, ...
                   forceGenerate=options.forceGenerate, ...
                   ignoreWarning=options.ignoreWarning);
    elseif isequal(options.method,'normal')
        ikHandle = inverseKinematics('RigidBodyTree',robotParameters.robotColumn);
        ikHandle.SolverParameters.MaxIterations=options.maxItertation;
    end

    % get the rrt Handle
    rrt = manipulatorRRT(robotParameters.robotColumn,options.collisionObjects);
    rrt.SkippedSelfCollisions = "parent";

    % shuffle the random
    rng('shuffle');
    randomTimer = zeros(options.randomSample,1);
    dexterityNoFlipTimer = zeros(options.dexterityNoFlipSample,1);
    dexterityTimer = zeros(options.dexteritySample,1);

    %% calculate the sample point

    

    %% calculate the RRT result
    % calculate the random path
    randomConfigSuccess = zeros(options.randomSample,1);
    randomJointWayPoints = cell(options.randomSample,1);
    for iRandom = 1:options.randomSample
        progressbar();

        % calculate the start and end
        [configSolStart,successMarkStart] = solveIK(ikHandle,robotParameters,startTform, ...
                                          options.weightsIn,options.maxConstraintRepeat);
        [configSolEnd,successMarkEnd] = solveIK(ikHandle,robotParameters,endTform, ...
                                          options.weightsIn,options.maxConstraintRepeat);
        if successMarkStart~=1 || successMarkEnd~=1
            if successMarkStart==2 || successMarkEnd==2
                randomConfigSuccess(iRandom) = 2;
            elseif successMarkStart==4 || successMarkEnd==4
                randomConfigSuccess(iRandom) = 4;
            else
                randomConfigSuccess(iRandom) = 3;
            end
            continue;
        end

        % calculate the RRT
        tic
        path = plan(rrt,configSolStart',configSolEnd');
        randomTimer(iRandom) = toc;
        interpPath = interpolate(rrt,path,options.numInterp);
        successMark = checkPathValidity(robotParameters,interpPath);
        randomConfigSuccess(iRandom) = successMark;
        randomJointWayPoints{iRandom} = interpPath';
    end



    % calculate the dexterity path but without the flip consideration
    dexterityNoFlipConfigSuccess = zeros(options.dexterityNoFlipSample,1);
    dexterityNoFlipJointWayPoints = cell(options.dexterityNoFlipSample,1);

    [sectionS,scoreS] = outputGoodConfigAverage(testDexterity,iLocalPositionS,iLocalRotationS,iGlobalPositionS,10);
    [sectionE,scoreE] = outputGoodConfigAverage(testDexterity,iLocalPositionE,iLocalRotationE,iGlobalPositionE,10);
    StartAndEndPair = decideGoodCombinationAverage(sectionS,scoreS,sectionE,scoreE,options.dexterityNoFlipSample);
    dexterityNoFlipStartList = StartAndEndPair;

    for iDexterityNoFLip = 1:options.dexterityNoFlipSample
        progressbar();
        configSolStart = [0;0;0;0;0;0;0];
        configSolEnd = [0;0;0;0;0;0;0];
        successMarkStart = 0;
        successMarkEnd = 0;

        nFlip = testDexterity.dataList.flipCounter - 1;
        trial = 0;
        while trial<=10 && (~any(configSolStart) && successMarkStart~=1)
        configSolStart = squeeze(configSolColumn(:,iLocalPositionS,iLocalRotationS,iGlobalPositionS,StartAndEndPair(iDexterityNoFLip,1),randi(nFlip)));
        successMarkStart = solutionFlagCheck22b(robotParameters,configSolStart);
        trial = trial +1;
        end

        trial = 0;
        while trial<=10 && (~any(configSolEnd) || successMarkEnd~=1)
        configSolEnd = squeeze(configSolColumn(:,iLocalPositionE,iLocalRotationE,iGlobalPositionE,StartAndEndPair(iDexterityNoFLip,1),randi(nFlip)));
        successMarkEnd = solutionFlagCheck22b(robotParameters,configSolEnd);
        trial = trial +1;
        end
        
        if successMarkStart~=1 || successMarkEnd~=1
            if successMarkStart==2 || successMarkEnd==2
                dexterityNoFlipConfigSuccess(iRandom) = 2;
            elseif successMarkStart==4 || successMarkEnd==4
                dexterityNoFlipConfigSuccess(iRandom) = 4;
            else
                dexterityNoFlipConfigSuccess(iRandom) = 3;
            end
            continue;
        end

        tic
        path = plan(rrt,configSolStart',configSolEnd');
        dexterityNoFlipTimer(iDexterityNoFLip) = toc;
        interpPath = interpolate(rrt,path,options.numInterp);
        successMark = checkPathValidity(robotParameters,interpPath);
%         if successMark ~=1
%             continue
%         end
%         [pathLength,cartLength] = pathAndCartDistance(robotParameters,{interpPath'});
%         dist = pathLength + cartLength;
%         if dist<distTmp
%             distTmp = dist;
%             pathTmp = interpPath;
%         end
        dexterityNoFlipConfigSuccess(iDexterityNoFLip) = successMark;
        dexterityNoFlipJointWayPoints{iDexterityNoFLip} = interpPath';
    end


    % calculate the dexterity path
    dexterityConfigSuccess = zeros(options.dexteritySample,1);
    dexterityJointWayPoints = cell(options.dexteritySample,1);
    
    % take out all possible solution
    [sectionAndFlipS,scoreS] = outputGoodConfig(testDexterity,iLocalPositionS,iLocalRotationS,iGlobalPositionS,options.dexteritySuggestPair);
    [sectionAndFlipE,scoreE] = outputGoodConfig(testDexterity,iLocalPositionE,iLocalRotationE,iGlobalPositionE,options.dexteritySuggestPair);
    StartAndEndPair = decideGoodCombination(squeeze(configSolColumn(:,iLocalPositionS,iLocalRotationS,iGlobalPositionS,:,:)), ...
                                            squeeze(configSolColumn(:,iLocalPositionE,iLocalRotationE,iGlobalPositionE,:,:)), ...
                                            sectionAndFlipS,scoreS,sectionAndFlipE,scoreE,options.dexteritySample);
    dexterityStartList = StartAndEndPair;

    for iDexterity = 1:options.dexteritySample
        progressbar();
        
        configSolStart = squeeze(configSolColumn(:,iLocalPositionS,iLocalRotationS,iGlobalPositionS,StartAndEndPair(iDexterity,1),StartAndEndPair(iDexterity,3)));
        configSolEnd = squeeze(configSolColumn(:,iLocalPositionE,iLocalRotationE,iGlobalPositionE,StartAndEndPair(iDexterity,2),StartAndEndPair(iDexterity,3)));
        successMarkStart = solutionFlagCheck22b(robotParameters,configSolStart);
        successMarkEnd = solutionFlagCheck22b(robotParameters,configSolEnd);
        if successMarkStart~=1 || successMarkEnd~=1
            if successMarkStart==2 || successMarkEnd==2
                dexterityConfigSuccess(iRandom) = 2;
            elseif successMarkStart==4 || successMarkEnd==4
                dexterityConfigSuccess(iRandom) = 4;
            else
                dexterityConfigSuccess(iRandom) = 3;
            end
            continue;
        end


        tic
        path = plan(rrt,configSolStart',configSolEnd');
        dexterityTimer(iDexterity) = toc;
        interpPath = interpolate(rrt,path,options.numInterp);
        successMark = checkPathValidity(robotParameters,interpPath);
%         if successMark ~=1
%             continue
%         end
%         [pathLength,cartLength] = pathAndCartDistance(robotParameters,{interpPath'});
%         dist = pathLength + cartLength;
%         if dist<distTmp
%             distTmp = dist;
%             pathTmp = interpPath;
%         end
        dexterityConfigSuccess(iDexterity) = successMark;
        dexterityJointWayPoints{iDexterity} = interpPath';

    end

    close(fig);

    % prepare the data
    markIn='twoPointRRTTest';
    dataListIn.dexterityNoFlipStartList = dexterityNoFlipStartList;
    dataListIn.dexterityStartList = dexterityStartList;
    dataListIn.randomConfigSuccess= randomConfigSuccess;
    dataListIn.randomJointWayPoints=randomJointWayPoints;
    dataListIn.dexterityNoFlipConfigSuccess = dexterityNoFlipConfigSuccess;
    dataListIn.dexterityNoFlipJointWayPoints = dexterityNoFlipJointWayPoints;
    dataListIn.dexterityConfigSuccess = dexterityConfigSuccess;
    dataListIn.dexterityJointWayPoints = dexterityJointWayPoints;
    metaListIn.randomTimer = randomTimer;
    metaListIn.dexterityNoFlipTimer = dexterityNoFlipTimer;
    metaListIn.dexterityTimer = dexterityTimer;

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);

end

% function sampleOut = randomSampleSorted(value,index,nSample)
%     cutIndex = 1;
%     while true
%         maxCurrent = max(value(cutIndex:end),[],'all');
%         maxCount = sum(value>=maxCurrent,'all');
%         cutIndex = maxCount+1;
%         if maxCount>=nSample
%             break;
%         end
%     end
%     sampleOut = index(randperm(cutIndex-1,nSample));
% end

function [configSol,successMark] = solveIK(ikHandle,robotParameters,targetTform,weightsIn,maxConstraintRepeat)
    for iRepeatCounter=1:maxConstraintRepeat
        initialguess = randomConfiguration(robotParameters.robotColumn);
        [configSol,solInfo] = ikHandle(robotParameters.endEffectorName,targetTform,weightsIn,initialguess);
        
        % if IK solver can't find Solution.
        if isequal(solInfo.Status,'best available')
            successMark = 2;
            break;

        elseif isequal(solInfo.Status,'success')
            % check the collision and joint Limit
            [solutionFlagOut,configSolOut] = solutionFlagCheck22b(robotParameters,configSol);
        
            if solutionFlagOut == 1
                configSol = configSolOut;
                successMark = 1;
                break;
            else
                successMark = solutionFlagOut;
                continue;
            end
        end
    end
end

function successMark = checkPathValidity(robotParameters,path)
    successMark = 1;
    for iPositionMark = 1:size(path,1)
        [solutionFlagOut,~] = solutionFlagCheck22b(robotParameters,path(iPositionMark,:)');
        
        if solutionFlagOut ~= 1
            successMark = solutionFlagOut;
            break;
        end
    end
end