function twoPointRRTStart(obj,iLocalPositionInS,iLocalRotationInS,iGlobalPositionInS,iLocalPositionInE,iLocalRotationInE,iGlobalPositionInE)
    arguments
        obj(1,1) twoPointRRTFixed
        iLocalPositionInS(1,1) double
        iLocalRotationInS(1,1) double
        iGlobalPositionInS(1,1) double
        iLocalPositionInE(1,1) double
        iLocalRotationInE(1,1) double
        iGlobalPositionInE(1,1) double
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('twoPointRRTStart');

    % prepare the data
    markIn='twoPointRRTStart';
    dataListIn.iLocalPositionS = iLocalPositionInS;
    dataListIn.iLocalRotationS = iLocalRotationInS;
    dataListIn.iGlobalPositionS = iGlobalPositionInS;
    dataListIn.iLocalPositionE = iLocalPositionInE;
    dataListIn.iLocalRotationE = iLocalRotationInE;
    dataListIn.iGlobalPositionE = iGlobalPositionInE;
    metaListIn = struct();

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

