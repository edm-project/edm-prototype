function [requireRetry,solutionFlagSlice,configSolColumnSlice,adjacentArray,previousArray] = motionNumeric(obj,iLocalPosition,iLocalRotation,iSectionNumber,iFlipSolution, ...
                                                                adjacentArray,previousArray, ...
                                                                maxIkIterationIn,maxNoSolutionRepeatIn,maxConstraintRepeatIn,startAngleTolerance,maxAngleTolerance, ...
                                                                maxGiveupDistance,options)
%GIKNUMERIC GIKMotion numeric method
    arguments
        obj(1,1) testDexterity
        iLocalPosition(1,1) double {mustBeInteger}
        iLocalRotation(1,1) double {mustBeInteger}
        iSectionNumber(1,1) double {mustBeInteger}
        iFlipSolution(1,1) double {mustBeInteger}
        adjacentArray(1,:) double {mustBeInteger}
        previousArray(:,:) double {mustBeInteger}
        maxIkIterationIn(1,1) double
        maxNoSolutionRepeatIn(1,1) double
        maxConstraintRepeatIn(1,1) double
        startAngleTolerance(1,1) double
        maxAngleTolerance(1,1) double
        maxGiveupDistance(1,1) double
        options.empirical(1,1) logical
        options.distanceChecking(1,1) logical
    end

    % extract the solutionFlag
    tformSlice =  squeeze(obj.generateMapHandle.dataList.allTform(:,:,iLocalPosition,iLocalRotation,:));
    solutionFlagSlice = squeeze(obj.dataList.solutionFlag(iLocalPosition,iLocalRotation,:,iSectionNumber,iFlipSolution));
    configSolColumnSlice = squeeze(obj.dataList.configSolColumn(:,iLocalPosition,iLocalRotation,:,iSectionNumber,iFlipSolution));
    solutionFlagReachabilitySlice = squeeze(obj.generateMapHandle.dataList.testReachability.dataList.solutionFlag(iLocalPosition,iLocalRotation,:));
    positionRow = obj.generateMapHandle.commonDataHandle.positionRow;
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);

    % check if the data matches what we need
    if solutionFlagSlice(adjacentArray(1))==2
        ME = MException('testDexterity:gikNumeric:sizeNotMatch', ...
                        'The starting index %d has solutionFlag 2', ...
                        adjacentArray(1));
        throw(ME)
    end
    
    % import the data that is not reachable
    if length(solutionFlagSlice)~=length(solutionFlagReachabilitySlice)
        ME = MException('testDexterity:gikNumeric:sizeNotMatch', ...
                        'Size of solutionflag not matched. Internal error');
        throw(ME)
    else
        solutionFlagSlice(solutionFlagReachabilitySlice==2)=solutionFlagReachabilitySlice(solutionFlagReachabilitySlice==2);
    end

    % import the solution previously calculated
    solutionFlagSection = squeeze(obj.dataList.solutionFlag(iLocalPosition,iLocalRotation,:,:,iFlipSolution));
    configSolColumnSection = squeeze(obj.dataList.configSolColumn(:,iLocalPosition,iLocalRotation,:,:,iFlipSolution));
    for iGlobalPosition = 1:nGlobalPosition
        if solutionFlagSlice(iGlobalPosition)~=1 && any(solutionFlagSection(iGlobalPosition,:)==1)
            solutionFlagSlice(iGlobalPosition)=1;
            if find(solutionFlagSection(iGlobalPosition,:)==1)==1
                configSolColumnSlice(:,iGlobalPosition)=configSolColumnSection(:,iGlobalPosition,solutionFlagSection(iGlobalPosition,:)==1);
            elseif solutionFlagSection(iGlobalPosition,iSectionNumber)==1
                configSolColumnSlice(:,iGlobalPosition)=configSolColumnSection(:,iGlobalPosition,iSectionNumber);
            else
                sectionTemp = obj.decideSection(iLocalPosition,iLocalRotation,iGlobalPosition,iFlipSolution);
                configSolColumnSlice(:,iGlobalPosition)=configSolColumnSection(:,iGlobalPosition,sectionTemp);
            end
        end
    end

    % inverse kineamatics
    robotParameters = obj.generateMapHandle.dataList.robotParameters;
    robotColumn = robotParameters.robotColumn;
    endEffectorName = robotParameters.endEffectorName;
    gik = generalizedInverseKinematics('RigidBodyTree',robotColumn,'ConstraintInputs',{'pose','jointbounds'});
    gik.SolverParameters.MaxIterations=maxIkIterationIn;

    % extract the parameters
    nGlobalPositionFiltered = length(adjacentArray);

    %progressbar
    fig = progressbar(['Dexterity: numeric gikMotion (',num2str(iLocalPosition),',', ...
                                                num2str(iLocalRotation),',:,', ...
                                                num2str(iSectionNumber),',', ...
                                                num2str(iFlipSolution),')'],nGlobalPositionFiltered);
    progressbar();

    % imperical counter
    empiricalCounter = 0;
    requireRetry = false;

    % calculate the output
    for iGlobalPosition=2:nGlobalPositionFiltered
        angleTolerance = startAngleTolerance;
        solvedFlag = solutionFlagSlice(adjacentArray(iGlobalPosition)) == 1;
        poseConst = constraintPoseTarget(endEffectorName,TargetTransform=tformSlice(:,:,adjacentArray(iGlobalPosition)));
        
        % imperical detection
        if iGlobalPosition == 6 && options.empirical==true  
            empiricalCondition5 = (empiricalCounter>=1);
        end
        if iGlobalPosition == 14 && options.empirical==true  
            empiricalCondition13 = (empiricalCounter>=3);
            if empiricalCondition5==true && empiricalCondition13==true
                disp(['Empirical decision: this gikMotion is probably useless C:[',num2str(empiricalCounter),']. Require Retry.']);
                requireRetry = true;
                delete(fig);
                return
            end
        end

        while solvedFlag == false
            % if the angleTolerance is bigger than the max, see the reason,
            % skip and replace it from the previous list.
            if angleTolerance >= maxAngleTolerance
                    disp(['[',num2str(adjacentArray(iGlobalPosition)),'] constraint tolerance exceeds [',num2str(rad2deg(maxAngleTolerance),3),'] reason [',num2str(solutionFlagSlice(adjacentArray(iGlobalPosition))),'].']);
                    % change the parent when no solution
                    if solutionFlagSlice(adjacentArray(iGlobalPosition)) == 2
                        previousArray(previousArray==adjacentArray(iGlobalPosition)) = previousArray(iGlobalPosition,1);
                        % delete the repeated column
                        for beforeCol=1:size(previousArray,2)-1
                            for afterCol=beforeCol+1:size(previousArray,2)
                                repeatMask = previousArray(:,beforeCol)==previousArray(:,afterCol);
                                previousArray(repeatMask,afterCol:end) = [previousArray(repeatMask,afterCol+1:end),zeros(sum(repeatMask,"all"),1)];
                            end
                        end
                        % empirical limit: if first 4 have more than one 1 exceeds [2]
                        % and first 12 have more than 3 exceeds [3], this
                        % gikMotion is then probably useless. Require for
                        % restart.
                        if iGlobalPosition <= 13 && options.empirical==true
                            empiricalCounter = empiricalCounter + 1;
                        end
                    end
                    break;

            % within the angleTolerance: check each element in previousArray
            else
                for iParentNode=1:size(previousArray,2)
                    if previousArray(iGlobalPosition,iParentNode)==0 || solvedFlag == true
                        break;
                    end

                    % skip if the distance bigger than expected
                    if norm(positionRow(previousArray(iGlobalPosition,iParentNode),:) - ...
                       positionRow(adjacentArray(iGlobalPosition),:)) > maxGiveupDistance
                        solutionFlagSlice(adjacentArray(iGlobalPosition)) = 2;
                        continue;
                    end

                    % make guess of the previous solution
                    iConstraintRepeat = 1;
                    iNoSolutionRepeat = 1;
                    previousSolution = configSolColumnSlice(:,previousArray(iGlobalPosition,iParentNode));
                    jointConst = jointBoundIntersect(robotParameters,previousSolution,angleTolerance);
                    initialguess = previousSolution;

                    % check if the process goes wrong
                    if ~any(previousSolution)
                        warning('initialguess is all zero. Probably mistake happens a:[%d] p:[%d]', ...
                        adjacentArray(iGlobalPosition),previousArray(iGlobalPosition));
                        continue;
                    end

                    % IK solution
                    while iNoSolutionRepeat <= maxNoSolutionRepeatIn && iConstraintRepeat <= maxConstraintRepeatIn
                        [configSol,solInfo] = gik(initialguess,poseConst,jointConst);

                        % if IK solver can't find Solution.
                        if isequal(solInfo.Status,'best available')
                            solutionFlagSlice(adjacentArray(iGlobalPosition)) = 2;
                            iNoSolutionRepeat = iNoSolutionRepeat + 1;

                        % if IK solver find Solution.
                        elseif isequal(solInfo.Status,'success')
                            % check the collision and joint Limit
                            [solutionFlagOut,configSolOut] = solutionFlagCheck(robotParameters,configSol);
                            
                            if solutionFlagOut == 1 || solutionFlagOut == solutionFlagReachabilitySlice(adjacentArray(iGlobalPosition))
                                solutionFlagSlice(adjacentArray(iGlobalPosition)) = solutionFlagOut;
                                configSolColumnSlice(:,adjacentArray(iGlobalPosition)) = configSolOut;
                                solvedFlag = true;
                                break;
                            else
                                solutionFlagSlice(adjacentArray(iGlobalPosition)) = solutionFlagOut;
                                if ~any(configSolColumnSlice(:,adjacentArray(iGlobalPosition)))
                                    configSolColumnSlice(:,adjacentArray(iGlobalPosition)) = configSolOut;
                                end
                                iConstraintRepeat = iConstraintRepeat + 1;
                            end
                        else
                            ME = MException('testDexterity:gikNumeric:elseError', ...
                                'if-else should not run into this branch');
                            throw(ME);
                        end

                        % if goes to here (some error happens), set joint bounds
                        initialguess = randomConfiguration(robotColumn);
                    end
                end

                % if a iteration for all previous nodes in one counter
                % failed, then lift up the angle tolerance
                angleTolerance = angleTolerance*2;
            end
        end

        % update the progressbar
        progressbar(); 
    end
    
    % close the progressbar
    close(fig);             
end

