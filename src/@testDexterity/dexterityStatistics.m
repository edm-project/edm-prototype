function dexterityStatistics(obj,options)
%DEXTERITYSTATISTICS calculate the statistics of the dexterity map
    arguments
        obj(1,1) testDexterity
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('dexterityStatistics');

    % import the point
    solutionFlag = obj.dataList.solutionFlag;
    configSolColumn = obj.dataList.configSolColumn;
    flipCounter = obj.dataList.flipCounter;
%     robotColumn = obj.generateMapHandle.commonDataHandle.robotParameters.robotColumn;
    
    % filter out the useless data
    solutionFlag = solutionFlag(:,:,:,:,1:flipCounter-1);

    if any(solutionFlag==0,'all')
        warning('There are points that are still not calculated.');
        disp('Continue?')
        pause on
        pause
    end

    % import the parameters
    nLocalPosition=size(obj.generateMapHandle.dataList.allTform,3);
    nLocalRotation=size(obj.generateMapHandle.dataList.allTform,4);
    nGlobalPosition=size(obj.generateMapHandle.dataList.allTform,5);
    nSection = obj.dataList.nSection;
    flipCounter = obj.dataList.flipCounter;
    robotParameters = obj.generateMapHandle.commonDataHandle.robotParameters;

    % generate the MEX file
    jointBoundHandle = MEXjointBoundDistanceGenerate(robotParameters, ...
        ignoreWarning=options.ignoreWarning, ...
        forceGenerate=options.forceGenerate);

    % calculate the dexterity Index
    rotationSumUp = any(solutionFlag==1,2);
    nSectionSumUp = sum(rotationSumUp,4);
    flippingSumUp = max(nSectionSumUp,[],5);
    directionSumUp = sum(flippingSumUp,1);
    dexterityValue = squeeze(directionSumUp);
    dexteritySlicedValue = squeeze(flippingSumUp);

    flipAndSectionReach = any(solutionFlag==1,[2 4 5]);
    directionSumUpReach = sum(flipAndSectionReach,1);
    reachabilityValue = squeeze(directionSumUpReach);

    dexterityIndex = dexterityValue/(nLocalPosition*nSection) *100;
    redudancyIndex = dexterityValue ./(reachabilityValue*nSection) *100;
    dexteritySlicedIndex = dexteritySlicedValue;

    fig = progressbar('Dexterity statistics: jointBoundDistance',nGlobalPosition*nLocalPosition);
    parallelQueue = parallel.pool.DataQueue; % use queue to async-update the progressbar
    afterEach(parallelQueue,@progressbar); 

    % calculate the joint Bound Index
    jointBoundDistanceMtxTmp = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,nSection,flipCounter-1);
    jointBoundSolvableDistanceMtxTmp = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,nSection,flipCounter-1);
    nFlipCounter = flipCounter-1;
    parfor iGlobalPosition=1:nGlobalPosition
        for iLocalPosition = 1:nLocalPosition
            for iLocalRotaion = 1:nLocalRotation
                for iSection = 1:nSection
                    for iFlip = 1:nFlipCounter
                        if solutionFlag(iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip)==1 
                            jointBoundTmp = jointBoundDistanceMEX(jointBoundHandle,configSolColumn(:,iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip));
                            jointBoundDistanceMtxTmp(iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip) = jointBoundTmp;
                            jointBoundSolvableDistanceMtxTmp(iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip) = jointBoundTmp;
                               
                        elseif solutionFlag(iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip)==2 ||...
                           solutionFlag(iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip)==3
                            jointBoundDistanceMtxTmp(iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip) = 0;
                            jointBoundSolvableDistanceMtxTmp(iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip) = 0;
                        else
                            jointBoundDistanceMtxTmp(iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip) = ...
                               jointBoundDistanceMEX(jointBoundHandle,configSolColumn(:,iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip));
                            jointBoundSolvableDistanceMtxTmp(iLocalPosition,iLocalRotaion,iGlobalPosition,iSection,iFlip) = 0;
                        end
                    end
                end
            end
            send(parallelQueue,'');
        end
    end
    jointBoundMaxSection = max(jointBoundDistanceMtxTmp,[],4);
    jointBoundSolvableMaxSection = max(jointBoundSolvableDistanceMtxTmp,[],4);
    jointBoundDistanceMtx = squeeze(jointBoundMaxSection);
    jointBoundSolvableDistanceMtx = squeeze(jointBoundSolvableMaxSection);
    close(fig);

    % calculate the flipDistance
    flipDistanceIndexMap=containers.Map('KeyType','char','ValueType','any');
    indexCounter = 1;
    for iStart = 1:flipCounter-1
        for iEnd = iStart+1:flipCounter-1
            if iStart==iEnd
                continue;
            end
            flipDistanceIndexMap([num2str(iStart),',',num2str(iEnd)]) = indexCounter;
            indexCounter = indexCounter + 1;
        end
    end
    fig = progressbar('Dexterity statistics: flipDistance',(indexCounter-1)*nGlobalPosition);
    
    flipDistance = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,indexCounter-1);
    flipDistanceSolvable = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,indexCounter-1);
    for iStart = 1:flipCounter-1
        for iEnd = iStart+1:flipCounter-1
            if iStart==iEnd
                continue;
            end
            flipIndex = flipDistanceIndexMap([num2str(iStart),',',num2str(iEnd)]);
            for iGlobalPosition=1:nGlobalPosition
                for iLocalPosition = 1:nLocalPosition
                    for iLocalRotaion = 1:nLocalRotation
                        if jointBoundMaxSection(iLocalPosition,iLocalRotaion,iGlobalPosition,iStart) == 0 ||...
                           jointBoundMaxSection(iLocalPosition,iLocalRotaion,iGlobalPosition,iEnd) == 0

                        else
                            solutionFlagMaskStart = squeeze(solutionFlag(iLocalPosition,iLocalRotaion,iGlobalPosition,:,iStart)~=2);
                            solutionFlagMaskEnd = squeeze(solutionFlag(iLocalPosition,iLocalRotaion,iGlobalPosition,:,iEnd)~=2);
                            flipDistance(iLocalPosition,iLocalRotaion,iGlobalPosition,flipIndex) = ...
                            sectionSectionDistance(squeeze(configSolColumn(:,iLocalPosition,iLocalRotaion,iGlobalPosition,solutionFlagMaskStart,iStart)), ...
                                                   squeeze(configSolColumn(:,iLocalPosition,iLocalRotaion,iGlobalPosition,solutionFlagMaskEnd,iEnd)));
                            
                            solutionFlagSolvableMaskStart = squeeze(solutionFlag(iLocalPosition,iLocalRotaion,iGlobalPosition,:,iStart)==1);
                            solutionFlagSolvableMaskEnd = squeeze(solutionFlag(iLocalPosition,iLocalRotaion,iGlobalPosition,:,iEnd)==1);
                            flipDistanceSolvable(iLocalPosition,iLocalRotaion,iGlobalPosition,flipIndex) = ...
                            sectionSectionDistance(squeeze(configSolColumn(:,iLocalPosition,iLocalRotaion,iGlobalPosition,solutionFlagSolvableMaskStart,iStart)), ...
                                                   squeeze(configSolColumn(:,iLocalPosition,iLocalRotaion,iGlobalPosition,solutionFlagSolvableMaskEnd,iEnd)));
                        end
                    end
                end
                progressbar();
            end
        end
    end
    close(fig);

    % prepare the data
    markIn='dexterityStatistics';
    dataListIn.dexterityIndex = dexterityIndex;
    dataListIn.dexteritySlicedIndex = dexteritySlicedIndex;
    dataListIn.redudancyIndex = redudancyIndex;
    dataListIn.jointBoundDistanceMtx = jointBoundDistanceMtx;
    dataListIn.jointBoundSolvableDistanceMtx = jointBoundSolvableDistanceMtx;
    dataListIn.flipDistanceIndexMap = flipDistanceIndexMap; 
    dataListIn.flipDistance = flipDistance;
    dataListIn.flipDistanceSolvable = flipDistanceSolvable;
    metaListIn = struct();

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

