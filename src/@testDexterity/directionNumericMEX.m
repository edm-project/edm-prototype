function [solutionFlagSlice,configSolColumnSlice] = directionNumericMEX(obj,iLocalRotationIn,iGlobalPositionIn,iSectionNumber,iFlipSolution, ...
                                                    maxIkIterationIn, maxNoSolutionRepeatIn,maxConstraintRepeatIn, ...
                                                    startAngleTolerance,maxAngleTolerance, ...
                                                    options)
%DIRECTIONNUMERICMEX numericMEX solution of the direction
    arguments
        obj(1,1) testDexterity
        iLocalRotationIn(1,1) double {mustBeInteger}
        iGlobalPositionIn(1,1) double {mustBeInteger}
        iSectionNumber(1,1) double {mustBeInteger}
        iFlipSolution(1,1) double {mustBeInteger}
        maxIkIterationIn(1,1) double
        maxNoSolutionRepeatIn(1,1) double
        maxConstraintRepeatIn(1,1) double
        startAngleTolerance(1,1) double
        maxAngleTolerance(1,1) double
        options.ignoreWarning(1,1) logical
        options.forceGenerate(1,1) logical
    end

    % extract the solutionFlag
    tformSlice =  squeeze(obj.generateMapHandle.dataList.allTform(:,:,:,iLocalRotationIn,iGlobalPositionIn));
    solutionFlagSlice = squeeze(obj.dataList.solutionFlag(:,iLocalRotationIn,iGlobalPositionIn,iSectionNumber,iFlipSolution));
    configSolColumnSlice = squeeze(obj.dataList.configSolColumn(:,:,iLocalRotationIn,iGlobalPositionIn,iSectionNumber,iFlipSolution));
    solutionFlagReachabilitySlice = squeeze(obj.generateMapHandle.dataList.testReachability.dataList.solutionFlag(:,iLocalRotationIn,iGlobalPositionIn));
    
    % calculate parameters
    nLocalPosition = size(obj.generateMapHandle.dataList.allTform,3);

    % check if the data matches what we need
    if any(solutionFlagReachabilitySlice~=1)
        ME = MException('testDexterity:directionNumeric:noSolution', ...
                        'The direction %s for position [%d] have no solution according to Reachability Map', ...
                        mat2str(find(solutionFlagReachabilitySlice~=1),2),iGlobalPositionIn);
        throw(ME)
    end

    % inverse kineamatics
    robotParameters = obj.generateMapHandle.dataList.robotParameters;
    robotColumn = robotParameters.robotColumn;
    endEffectorName = robotParameters.endEffectorName;
    
    % generate the gik MEX code
    gikHandle = MEXgikGenerate(robotParameters,maxIkIterationIn, ...
                   ignoreWarning=options.ignoreWarning, ...
                   forceGenerate=options.forceGenerate);
    solutionFlagCheckHandle = MEXsolutionFlagCheckGenerate(robotParameters, ...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);

    % calculate the output
    for iLocalPosition=2:nLocalPosition
        angleTolerance = startAngleTolerance;
        solvedFlag = solutionFlagSlice(iLocalPosition) == 1;
        
        while solvedFlag == false
            % if the angleTolerance is bigger than the max, see the reason,
            % skip and replace it from the previous list.
            if angleTolerance >= maxAngleTolerance
                disp(['The direction [',num2str(iLocalPosition),'] ' ...
                      'exceeds the tolerance [',num2str(angleTolerance),'] ' ...
                      'with reason [',num2str(solutionFlagSlice(iLocalPosition)),']. ' ...
                      'Adapt the Parameter.']);
                break;

            % within the angleTolerance: check each element in previousArray
            else
                for iParent = 1:(iLocalPosition-1)
                    % skip if the solution was found
                    if solvedFlag == true
                        break;
                    end

                    % make guess of the previous solution
                    iConstraintRepeat = 1;
                    iNoSolutionRepeat = 1;
                    previousSolution = configSolColumnSlice(:,iLocalPosition-iParent);
                    jointConst = jointBoundIntersect(robotParameters,previousSolution,angleTolerance);
                    initialguess = previousSolution;
    
                    % check if the process goes wrong
                    if ~any(previousSolution)
                        warning('initialguess is all zero. Probably mistake happens d:[%d]', ...
                        iLocalPosition);
                    end
    
                    % IK solution
                    while iNoSolutionRepeat <= maxNoSolutionRepeatIn && iConstraintRepeat <= maxConstraintRepeatIn
                        [configSol,solInfo] = gikMEX(gikHandle,initialguess,endEffectorName,tformSlice(:,:,iLocalPosition),jointConst.Bounds);
    
                        % if IK solver can't find Solution.
                        if isequal(solInfo.Status,'best available')
                            solutionFlagSlice(iLocalPosition) = 2;
                            iNoSolutionRepeat = iNoSolutionRepeat + 1;
    
                        % if IK solver find Solution.
                        elseif isequal(solInfo.Status,'success')
                            % check the collision and joint Limit
                            [solutionFlagOut,configSolOut] = solutionFlagCheckMEX(solutionFlagCheckHandle,configSol);
                            
                            if solutionFlagOut == 1
                                solutionFlagSlice(adjacentArray(iGlobalPosition)) = solutionFlagOut;
                                configSolColumnSlice(:,adjacentArray(iGlobalPosition)) = configSolOut;
                                solvedFlag = true;
                                break;
                            else
                                solutionFlagSlice(adjacentArray(iGlobalPosition)) = solutionFlagOut;
                                if ~any(configSolColumnSlice(:,adjacentArray(iGlobalPosition)))
                                    configSolColumnSlice(:,adjacentArray(iGlobalPosition)) = configSolOut;
                                end
                                iConstraintRepeat = iConstraintRepeat + 1;
                            end
                        else
                            ME = MException('testDexterity:directionNumeric:elseError', ...
                                'if-else should not run into this branch');
                            throw(ME);
                        end
    
                        % if goes to here (some error happens), set joint bounds
                        initialguess = randomConfiguration(robotColumn);
                    end
    
    
                    % if a iteration for all previous nodes in one counter
                    % failed, then lift up the angle tolerance
                    angleTolerance = angleTolerance*2;
                end
            end
        end
    end
end
