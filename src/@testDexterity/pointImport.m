function [sectionNumberOut,iFilpSolutionOut] = pointImport(obj,iLocalPosition,iLocalRotation,iGlobalPosition,options)
%POINTIMPORT import the point from testReachability
    arguments
        obj(1,1) testDexterity
        iLocalPosition(1,1) double {mustBeInteger}
        iLocalRotation(1,1) double {mustBeInteger}
        iGlobalPosition(1,1) double {mustBeInteger}
        options.ignoreWarning(1,1) logical = false
    end

    % check if allow call for efficiency
    obj.mustAllowCall('pointImport');

    % collect the data form testReachability
    solutionFlag=obj.generateMapHandle.dataList.testReachability.dataList.solutionFlag;
    configSolColumn=obj.generateMapHandle.dataList.testReachability.dataList.configSolColumn;
    positionRow=obj.generateMapHandle.dataList.positionRow;

    % import the specific point
    solutionFlagIn = solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition);
    configSolColumnIn = configSolColumn(:,iLocalPosition,iLocalRotation,iGlobalPosition);
    xIndex=positionRow(iGlobalPosition,1);
    yIndex=positionRow(iGlobalPosition,2);
    zIndex=positionRow(iGlobalPosition,3);
    [sectionNumberOut,iFilpSolutionOut] = obj.singlePointImport(solutionFlagIn, ...
                                             configSolColumnIn, ...
                                             iLocalPosition, ...
                                             iLocalRotation, ...
                                             iGlobalPosition, ...
                                             ignoreWarning=options.ignoreWarning);
    
    % prepare for update
    markIn='pointImport';
    dataListIn=struct();
    metaListIn.iLocalPosition=iLocalPosition;
    metaListIn.iLocalRotation=iLocalRotation;
    metaListIn.iGlobalPosition=iGlobalPosition;
    metaListIn.xIndex=xIndex;
    metaListIn.yIndex=yIndex;
    metaListIn.zIndex=zIndex;
    metaListIn.sectionNumberOut=sectionNumberOut;
    metaListIn.iFilpSolutionOut=iFilpSolutionOut;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

