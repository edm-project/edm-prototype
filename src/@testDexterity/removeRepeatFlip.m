function removeRepeatFlip(obj,iLocalPositionIn,iLocalRotationIn)
%REMOVEREPEATFLIP after nullspace motion, remove the data that is replicate
% to the previous flipped solution
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double {mustBeInteger}
        iLocalRotationIn(1,1) double {mustBeInteger}
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('removeRepeatFlip');

    % import the data
    solutionFlag = obj.dataList.solutionFlag;
    configSolColumn = obj.dataList.configSolColumn;
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);
    nSection = obj.dataList.nSection;
    
    solutionFlagSlice = squeeze(solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,:));
    configSolColumnSlice = squeeze(configSolColumn(:,iLocalPositionIn,iLocalRotationIn,:,:,:));
    
    if any(solutionFlagSlice==0)
        ME = MException('testDexterity:removeRepeatFlip:sizeNotMatch', ...
                        'This section (%d,%d,:,:,:) data is not fully calculated with nullspace motion.', ...
                        iLocalPositionIn,iLocalRotationIn);
        throw(ME)
    end

%     % handle
%     isReachableHandle = MEXisNullspaceReachableGenerate(robotParameters, ...
%         ignoreWarning=options.ignoreWarning, ...
%         forceGenerate=options.forceGenerate);
%     sectionHandle = MEXdexteritySectionGenerate(robotParameters, ...
%         ignoreWarning=options.ignoreWarning, ...
%         forceGenerate=options.forceGenerate);
%     % calculate with different mode
%     disp('This method is only implemented with numeric MEX mode.')
    
    for iGlobalPosition = 1:nGlobalPosition
        for iFlipSolutionNow = 3:4
            isRepeatFlag = false;
            for iFlipSolutionCompare = 1:2
                solutionFlagMaskNow = squeeze(solutionFlagSlice(iGlobalPosition,:,iFlipSolutionNow)~=2);
                solutionFlagMaskCompare = squeeze(solutionFlagSlice(iGlobalPosition,:,iFlipSolutionCompare)~=2);
                jointDist = sectionSectionDistance(squeeze(configSolColumnSlice(:,iGlobalPosition,solutionFlagMaskNow,iFlipSolutionNow)), ...
                                                   squeeze(configSolColumnSlice(:,iGlobalPosition,solutionFlagMaskCompare,iFlipSolutionCompare)));
                if jointDist<=deg2rad(360/nSection) && (sum(solutionFlagSlice(iGlobalPosition,:,iFlipSolutionNow)==1,"all")>=6)
                    isRepeatFlag = true;
                end
            end

            if isRepeatFlag == true
                disp(['flip [',num2str(iFlipSolutionNow),'] Position [',num2str(iGlobalPosition),'] in direction [',num2str(iLocalPositionIn),'] deleted.']);
                solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,:,iFlipSolutionNow) = 2;
                configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,:,iFlipSolutionNow) = 0;
            end
        end
    end

    % prepare the data
    markIn='removeRepeatFlip';
    dataListIn.solutionFlag = solutionFlag;
    dataListIn.configSolColumn = configSolColumn;
    metaListIn.iLocalPosition = iLocalPositionIn;
    metaListIn.iLocalRotation = iLocalRotationIn;

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

