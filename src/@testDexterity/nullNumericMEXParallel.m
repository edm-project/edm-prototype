function [solutionFlag,configSolColumn] = nullNumericMEXParallel(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution, ...
                                                        solutionFlag,configSolColumn, ...
                                                        timeStep,increaseStep,maxStep,options)
%NULLNUMERICPARALLEL numeric MEX parallel mode of the nullspace motion
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double {mustBeInteger}
        iLocalRotationIn(1,1) double {mustBeInteger}
        iFlipSolution(1,1) double {mustBeInteger}
        solutionFlag double
        configSolColumn double
        timeStep(1,1) double
        increaseStep(1,1) double
        maxStep(1,1) double
        options.ignoreWarning(1,1) logical
        options.forceGenerate(1,1) logical
    end

    % prepare variables for calculation and storage
    robotParameters = obj.generateMapHandle.commonDataHandle.robotParameters;
    nSection = obj.dataList.nSection;
    isRedudant = obj.dataList.isRedudant;
    adjacentMapFace = obj.dataList.adjacentMap;
    robotConfigSize = length(homeConfiguration(robotParameters.robotColumn));

    % generate the code
    nullspaceFixedHandle = MEXnullspaceFixedGenerate(robotParameters, ...
                                                    forceGenerate=options.forceGenerate, ...
                                                    ignoreWarning=options.ignoreWarning);

    solutionFlagCheckHandle = MEXsolutionFlagCheckGenerate(robotParameters, ...
        ignoreWarning=options.ignoreWarning, ...
        forceGenerate=options.forceGenerate);

    nullspaceFixedReverseHandle = MEXnullspaceFixedReverseGenerate(robotParameters,...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);

    sectionHandle = MEXdexteritySectionGenerate(robotParameters, ...
                  ignoreWarning=options.ignoreWarning, ...
                  forceGenerate=options.forceGenerate);

    % prepare parameters
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);

    % prepare the empty parameters
    sectionNumberArray = zeros(nGlobalPosition,1);
    solutionFlagParforInput = zeros(nGlobalPosition,1);
    configSolColumnParforInput = zeros(robotConfigSize,nGlobalPosition);

    % prepare progressbar
    %progressbar
    fig = progressbar(['Dexterity: numericMEXParallel nullspaceMotion (',num2str(iLocalPositionIn),',', ...
                                                num2str(iLocalRotationIn),',:,:,', ...
                                                num2str(iFlipSolution),')'],nGlobalPosition);
    parallelQueue = parallel.pool.DataQueue; % use queue to async-update the progressbar
    afterEach(parallelQueue,@progressbar); 

    % the code that can detect the correct sectionNumber for calculation
    for iGlobalPosition = 1:nGlobalPosition
        solutionFlagSlice = solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,:,iFlipSolution);
        
        % for data point that is already calculated
        if all(solutionFlagSlice==1|solutionFlagSlice==3|solutionFlagSlice==4) == true ||...
           all(solutionFlagSlice==2) == true
            sectionNumberArray(iGlobalPosition) = 0;

        % for a normal data point
        else
            % search for "the correct data point"
            if sum(solutionFlagSlice~=0,"all") == 1
                sectionIndex = solutionFlagSlice~=0;
            elseif sum(solutionFlagSlice==1,"all") >= 1
                sectionIndex = solutionFlagSlice==1;
            elseif sum(solutionFlagSlice==3,"all") >= 1
                sectionIndex = solutionFlagSlice==3;
            elseif sum(solutionFlagSlice==4,"all") >= 1
                sectionIndex = solutionFlagSlice==4;
            elseif sum(solutionFlagSlice==2,"all") >= 1
                sectionIndex = zeros(nSection,1,'logical');
                index = find(solutionFlagSlice==2, 1 );
                sectionIndex(index) = true;
            else
                solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,12,iFlipSolution) = 2;
                sectionIndex = zeros(nSection,1,'logical');
                sectionIndex(12) = true;
                warning('Global position [%d] has no data point. Automatic fill in 2.',iGlobalPosition);
            end

            % if the data above has two or more data point, check if they
            % belongs to the same nullspace motion
            if sum(sectionIndex,"all") > 1
                sectionIndex = find(sectionIndex);
                chooseIndex = 1;
                for iSection = 1:length(sectionIndex)-1

                    % if they don't belongs to the same nullspace motion,
                    % then try to 
                    if ~isNullspaceReachable(robotParameters, ...
                                            configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex(iSection),iFlipSolution), ...
                                            configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex(iSection+1),iFlipSolution))
                        warning('Global position [%d] has two data points [%d][%d] that cannot reach each other with program. Please check and decide.', ...
                            iGlobalPosition,sectionIndex(iSection),sectionIndex(iSection+1));
                        
                        [oneIndex,sectionNumber] = nearestOneIndex(solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution),iGlobalPosition,adjacentMapFace);
                        normDifference = zeros(length(length(sectionIndex)),1);
                        for iNormDifference = 1:length(sectionIndex)
                            normDifference(iNormDifference) = norm( ...
                             configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex(iNormDifference),iFlipSolution)- ...
                             configSolColumn(:,iLocalPositionIn,iLocalRotationIn,oneIndex,sectionNumber,iFlipSolution));
                        end
                        
                        [~,chooseIndex] = min(normDifference);
                        disp(['Automatic choose section ',num2str(sectionIndex(chooseIndex))]);
                    end
                end
                sectionIndex = sectionIndex(chooseIndex);
            else
                sectionIndex = find(sectionIndex);
            end

            sectionNumberArray(iGlobalPosition) = sectionIndex;
            configSolColumnParforInput(:,iGlobalPosition) = configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex,iFlipSolution);
            solutionFlagParforInput(iGlobalPosition) = solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex,iFlipSolution);
        end
    end

    % iterate through the GlobalPosition
    parfor iGlobalPosition = 1:nGlobalPosition
        % if the redudant robot have a solution here
        iSectionNumber = sectionNumberArray(iGlobalPosition);
        
        % skip if the SectionNumber == 0 (already calculated)
        if iSectionNumber == 0
        
        % if Redudant and we have a exact solution here
        elseif isRedudant == true && solutionFlagParforInput(iGlobalPosition) ~= 2
            configSol = configSolColumnParforInput(:,iGlobalPosition);
            [configSolCol,haveSolutionCol] = nullspaceRoundMEX(robotParameters,nullspaceFixedHandle,nullspaceFixedReverseHandle,sectionHandle, ...
                                                            configSol,nSection, ...
                                                            timeStep, ...
                                                            increaseStep, ...
                                                            maxStep);
            for iSection = 1:nSection
                if haveSolutionCol(iSection) == 0
                    solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iSection,iFlipSolution)=2;
                else
                    % check the collision and joint Limit
                    configSolSection = configSolCol(:,iSection);
                    [solutionFlagOut,configSolOut] = solutionFlagCheckMEX(solutionFlagCheckHandle,configSolSection);
                    solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iSection,iFlipSolution) = solutionFlagOut;
                    configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iSection,iFlipSolution) = configSolOut;
                end
            end
        
        else % if it is a none redudant robot or the input data point = 2 (no solution)
            for iSection = 1:nSection
                if solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iSection,iFlipSolution)==0
                    solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iSection,iFlipSolution)=2;
                end
            end
        end

        % update the progressbar(with Queue)
        send(parallelQueue,'');
    end

    % close the progressbar
    close(fig); 
end

function [oneIndex,sectionNumber] = nearestOneIndex(solutionFlagParted,indexNow,adjacentMapFace)
    queue = indexNow;
    queueIndex = 1;

    while queueIndex<=length(queue)
        % if the current solutionFlag is have one and only one = 1 point, then we use it as the index
        currentSolutionFlagSlice = squeeze(solutionFlagParted(1,1,queue(queueIndex),:,1));
        if any(currentSolutionFlagSlice==1) && length(find(currentSolutionFlagSlice==1))==1
            oneIndex = queue(queueIndex);
            sectionNumber = find(currentSolutionFlagSlice==1);
            break;
        end
        
        % if not, then we add it's adjacent point into it to see.
        adjacentArray = adjacentMapFace(queue(queueIndex));
        for iAdjacent=1:length(adjacentArray)
            if ~any(queue==adjacentArray(iAdjacent))
                queue = [queue,adjacentArray(iAdjacent)]; %#ok<AGROW> % because this is a queue, we cannot avoid reallocaltion
            end
        end

        queueIndex = queueIndex+1;
    end
end

