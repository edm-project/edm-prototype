function nullspaceMotionSinglePoint(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iFlipSolution,options)
%NULLSPACEMOTIONSINGLEPOINT Do the nullspace Motion for a single point
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double {mustBeInteger}
        iLocalRotationIn(1,1) double {mustBeInteger}
        iGlobalPositionIn(1,1) double {mustBeInteger}
        iFlipSolution(1,1) double {mustBeInteger}
        options.timeStep(1,1) double = 0.3
        options.increaseStep(1,1) double = 50
        options.maxStep(1,1) double = 200
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('nullspaceMotionSinglePoint');

    % check if the robot is redudant
    if obj.dataList.isRedudant==false
        ME = MException('testDexterity:nullspaceMotionSinglePoint:notRedudant', ...
                        'non-redudant robot cannot do nullspacemotion.');
        throw(ME); 
    end

    % prepare variables for calculation and storage
    mode = obj.dataList.mode;
    robotParameters = obj.generateMapHandle.commonDataHandle.robotParameters;
    solutionFlagIn = obj.dataList.solutionFlag; 
    configSolColumnIn = obj.dataList.configSolColumn;

    % prepare parameters
    nLocalPosition = size(obj.generateMapHandle.dataList.allTform,3);
    nLocalRotation = size(obj.generateMapHandle.dataList.allTform,4);
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);
    nSectionNumber = obj.dataList.nSection;
    nFlipSolution = size(obj.dataList.solutionFlag,5);
    robotConfigSize = length(homeConfiguration(robotParameters.robotColumn)); 

    % prepare empty output
    solutionFlag = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,nSectionNumber,nFlipSolution);
    configSolColumn = zeros(robotConfigSize,nLocalPosition,nLocalRotation,nGlobalPosition,nSectionNumber,nFlipSolution);

    % import the initial data
    solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlipSolution) = solutionFlagIn(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlipSolution); 
    configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlipSolution) = configSolColumnIn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlipSolution);

    % generalized ik calculation with different modes
    if isequal(mode,'numeric')
        [solutionFlag,configSolColumn] = obj.nullSinglePointNumeric(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iFlipSolution, ...
                                                    solutionFlag,configSolColumn, ...
                                                    options.timeStep,options.increaseStep,options.maxStep);
    elseif isequal(mode,'numericMEX') || isequal(mode,'numericMEXParallel')
        [solutionFlag,configSolColumn] = obj.nullSinglePointNumericMEX(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iFlipSolution, ...
                                                    solutionFlag,configSolColumn, ...
                                                    options.timeStep,options.increaseStep,options.maxStep, ...
                                                    forceGenerate=options.forceGenerate, ...
                                                    ignoreWarning=options.ignoreWarning);
    else
        ME = MException('testDexterity:nullspaceMotionSinglePoint:elseError', ...
                        'mode if-else should not run into this branch');
        throw(ME);  
    end

    % output
    % prepare the data
    markIn='nullspaceMotionSinglePoint';
    dataListIn.solutionFlag = solutionFlag;
    dataListIn.configSolColumn = configSolColumn;
    metaListIn.iLocalPosition = iLocalPositionIn;
    metaListIn.iLocalRotation = iLocalRotationIn;
    metaListIn.iGlobalPosition = iGlobalPositionIn;
    metaListIn.iFlipSolution = iFlipSolution;
    metaListIn.timeStep = options.timeStep;
    metaListIn.increaseStep = options.increaseStep;
    metaListIn.maxStep = options.maxStep;
    if isequal(mode,'numericMEX')
        tag = robotParameters.tag;
        backup_name = ['MEXnullspaceFixed_backup_',tag];
        metaListIn.numericMEXStr = fileread(backup_name);
    end

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

