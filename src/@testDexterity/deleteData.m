function deleteData(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution)
    % check if allow callable for efficiency
    obj.mustAllowCall('deleteData');

    % import the data
    solutionFlag = obj.dataList.solutionFlag;
    configSolColumn = obj.dataList.configSolColumn;
    
    solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution) = 0;
    configSolColumn(:,iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution) = 0;

    % prepare the data
    markIn='deleteData';
    dataListIn.solutionFlag = solutionFlag;
    dataListIn.configSolColumn = configSolColumn;
    metaListIn.iLocalPosition = iLocalPositionIn;
    metaListIn.iLocalRotation = iLocalRotationIn;
    metaListIn.iFlipSolution = iFlipSolution;

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

