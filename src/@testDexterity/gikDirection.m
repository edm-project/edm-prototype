function gikDirection(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iSectionNumber,iFlipSolution,options)
%GIKDIRECTION move the robot in one point with different directions (iLocaPosition)
%  to get a consistent iFlipSolution
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double {mustBeInteger}
        iLocalRotationIn(1,1) double {mustBeInteger}
        iGlobalPositionIn(1,1) double {mustBeInteger}
        iSectionNumber(1,1) double {mustBeInteger}
        iFlipSolution(1,1) double {mustBeInteger}
        options.maxIkIterationIn(1,1) double = 2000
        options.maxNoSolutionRepeatIn(1,1) double = 2
        options.maxConstraintRepeatIn(1,1) double = 5
        options.startAngleTolerance(1,1) double = deg2rad(30);
        options.maxAngleTolerance(1,1) double = deg2rad(135)
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('gikDirection');

    % check that the iLocalPositionIn must be 1
    if iLocalPositionIn~=1
        ME = MException('testDexterity:gikDirection:notImplemented', ...
                        'the gikDirection for iLocalPosition~=1 is not implemented.');
        throw(ME);
    end

    % start a time counter
    startTime= datetime('now');

    % prepare variables for calculation and storage
    mode = obj.dataList.mode;
    robotParameters = obj.generateMapHandle.commonDataHandle.robotParameters;

    % prepare parameters
    nLocalPosition = size(obj.generateMapHandle.dataList.allTform,3);
    nLocalRotation = size(obj.generateMapHandle.dataList.allTform,4);
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);
    nSectionNumber = obj.dataList.nSection;
    nFlipSolution = size(obj.dataList.solutionFlag,5);
    robotConfigSize = length(homeConfiguration(robotParameters.robotColumn)); 

    % prepare empty output
    solutionFlag = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,nSectionNumber,nFlipSolution);
    configSolColumn = zeros(robotConfigSize,nLocalPosition,nLocalRotation,nGlobalPosition,nSectionNumber,nFlipSolution);

    if isequal(mode,'numeric')
        [solutionFlagSlice,configSolColumnSlice]=obj.directionNumeric(iLocalPositionIn,iGlobalPositionIn,iSectionNumber,iFlipSolution, ...
                                                                  options.maxIkIterationIn, ...
                                                                  options.maxNoSolutionRepeatIn, ...
                                                                  options.maxConstraintRepeatIn, ...
                                                                  options.startAngleTolerance, ...
                                                                  options.maxAngleTolerance);
    elseif isequal(mode,'numericMEX') || isequal(mode,'numericMEXParallel')
        [solutionFlagSlice,configSolColumnSlice]=obj.directionNumericMEX(iLocalPositionIn,iGlobalPositionIn,iSectionNumber,iFlipSolution, ...
                                                                  options.maxIkIterationIn, ...
                                                                  options.maxNoSolutionRepeatIn, ...
                                                                  options.maxConstraintRepeatIn, ...
                                                                  options.startAngleTolerance, ...
                                                                  options.maxAngleTolerance, ...
                                                                  ignoreWarning=options.ignoreWarning, ...
                                                                  forceGenerate=options.forceGenerate);
    else
        ME = MException('testDexterity:gikDirection:elseError', ...
                        'mode if-else should not run into this branch');
        throw(ME);
    end

    % expand and calculate the reachability in different section and same iFlipSolution
    % (1 indices to 5 indices)
    for iLocalPosition=1:nLocalPosition
        [~,sectionNumber] = dexteritySection(robotParameters,configSolColumnSlice(:,iLocalPosition),nSectionNumber);
        solutionFlag(iLocalPosition,iLocalRotationIn,iGlobalPositionIn,sectionNumber,iFlipSolution) = ...
                     solutionFlagSlice(iLocalPosition);
        configSolColumn(:,iLocalPosition,iLocalRotationIn,iGlobalPositionIn,sectionNumber,iFlipSolution) = ...
                     configSolColumnSlice(:,iLocalPosition);
    end

    % prepare the data
    markIn='gikDirection';
    dataListIn.solutionFlag = solutionFlag;
    dataListIn.configSolColumn = configSolColumn;
    metaListIn.iLocalPosition = iLocalPositionIn;
    metaListIn.iLocalRotation = iLocalRotationIn;
    metaListIn.iGlobalPosition = iGlobalPositionIn;
    metaListIn.iSectionNumber = iSectionNumber;
    metaListIn.iFlipSolution = iFlipSolution;
    metaListIn.solutionFlagSlice = solutionFlagSlice;
    metaListIn.configSolColumnSlice = configSolColumnSlice;
    metaListIn.maxIkIterationIn = options.maxIkIterationIn;
    metaListIn.maxNoSolutionRepeatIn = options.maxNoSolutionRepeatIn;
    metaListIn.maxConstraintRepeatIn = options.maxConstraintRepeatIn;
    metaListIn.maxAngleTolerance = options.maxAngleTolerance;
    metaListIn.startTime=startTime;
    metaListIn.endTime=datetime('now');
    if isequal(mode,'numericMEX')
        metaListIn.numericMEXStr = fileread('MEXgik.m');
    end

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

