function pointBatchImportFD(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,sortList,options)
%POINTBATCHIMPORTFD 
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double {mustBeInteger}
        iLocalRotationIn(1,1) double {mustBeInteger}
        iGlobalPositionIn(1,1) double {mustBeInteger}
        sortList(1,:) double
        options.forceCrossing(1,1) logical = false
    end

    obj.mustAllowCall('pointBatchImportFD');

    % import the data needed
    solutionFlagFD = squeeze(obj.generateMapHandle.dataList.testFlippedDexterity.dataList.solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,:));
    configSolColumnFD = squeeze(obj.generateMapHandle.dataList.testFlippedDexterity.dataList.configSolColumn(:,iLocalPositionIn,iLocalRotationIn,:,:,:));
    solutionFlagReachability = obj.generateMapHandle.dataList.testReachability.dataList.solutionFlag;
    solutionFlag = obj.dataList.solutionFlag;
    configSolColumn = obj.dataList.configSolColumn;

    % calculate the parameters
    [adjacentArray, previousArray] = obj.adjacentListCalculation(iLocalPositionIn, ...
                                                                 iLocalRotationIn, ...
                                                                 iGlobalPositionIn, ...
                                                                 solutionFlagReachability, ...
                                                                 options.forceCrossing);
    nSection = obj.dataList.nSection;
    haveSolutionMask = solutionFlagFD == 1 | solutionFlagFD == 3 | solutionFlagFD == 4;
    existDataInSection = any(haveSolutionMask,2);
    existDataLength = squeeze(sum(existDataInSection,3));

    fig = progressbar(['Dexterity: numeric pointBatchImport (',num2str(iLocalPositionIn),',', ...
        num2str(iLocalRotationIn),',:,:,:)'],length(adjacentArray));
    progressbar();
    
    % import the first point
    % [2 3] => testD's 1Flip = testFD's 2Flip, testD 2 = testFD 3, etc... 
    for iFlip = 1:length(sortList)
        % check for repeat
        if isnan(sortList(iFlip))
            continue;
        end
        solutionFlagMask = squeeze(solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlip)==1|...
                                   solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlip)==3|...
                                   solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlip)==4);
        if any(solutionFlagMask)
            jointDist = sectionSectionDistance(squeeze(configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,solutionFlagMask,iFlip)), ...
                                              squeeze(configSolColumnFD(:,iGlobalPositionIn,:,sortList(iFlip))));
            if jointDist>deg2rad(360/nSection)
                error('The sort List for D[%d] FD[%d] is has large distance %d',iFlip,sortList(iFlip),jointDist);
            end
        end

        % check for bad data point
        solutionFlagFDMask = squeeze(solutionFlagFD(iGlobalPositionIn,:,sortList(iFlip))==2);
        if sum(solutionFlagFDMask,'all')>= nSection/3
            error('The import point is a bad point at flip %d. Please consider another.',sortList(iFlip))
        end

        % import the first data
        solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlip) = solutionFlagFD(iGlobalPositionIn,:,sortList(iFlip));
        configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,iFlip) = configSolColumnFD(:,iGlobalPositionIn,:,sortList(iFlip));
    end

    % import the rest of the data
    for iGlobalPosition = 2:length(adjacentArray)
        for iParentNode = 1:size(previousArray,2)
            % stop for this point when all parents are went through
            if previousArray(iGlobalPosition,iParentNode)==0
                break;
            end

            % change a parent if parent have no solution
            solutionFlagSlice = squeeze(solutionFlag(iLocalPositionIn,iLocalRotationIn,previousArray(iGlobalPosition,iParentNode),:,:));
            if ~any(solutionFlagSlice~=2 & solutionFlagSlice~=0)
                continue;
            end

            % check for the closest flip in original solutionFlag
            solutionFliplength = find(any(solutionFlagSlice,1), 1, 'last' );
            distance = zeros(solutionFliplength,existDataLength(adjacentArray(iGlobalPosition)));

            % go through flip in the data to be imported
            for iExist = 1:existDataLength(adjacentArray(iGlobalPosition))
                % check for bad data point
                solutionFlagFDMask = squeeze(solutionFlagFD(adjacentArray(iGlobalPosition),:,iExist)==2);
                if sum(solutionFlagFDMask,'all')>= nSection/3
%                     warning('The import point is a bad point at position [%d] flip [%d].',adjacentArray(iGlobalPosition),iExist)
                    continue;
                end

                for iFlip=1:solutionFliplength
                    solutionFlagMask = squeeze(solutionFlag(iLocalPositionIn,iLocalRotationIn,previousArray(iGlobalPosition,iParentNode),:,iFlip)==1 |...
                                               solutionFlag(iLocalPositionIn,iLocalRotationIn,previousArray(iGlobalPosition,iParentNode),:,iFlip)==3 |...
                                               solutionFlag(iLocalPositionIn,iLocalRotationIn,previousArray(iGlobalPosition,iParentNode),:,iFlip)==4);
                    solutionFlagFDMask = squeeze(solutionFlagFD(adjacentArray(iGlobalPosition),:,iFlip)~=2);
                    if ~any(solutionFlagMask)
                        distance(iFlip,iExist) = Inf;
                    else
                        distance(iFlip,iExist) = sectionSectionDistance(squeeze(configSolColumn(:,iLocalPositionIn,iLocalRotationIn,previousArray(iGlobalPosition,iParentNode),solutionFlagMask,iFlip)), ...
                                        squeeze(configSolColumnFD(:,adjacentArray(iGlobalPosition),solutionFlagFDMask,iExist)));
                    end
                end         
            end

            distanceSum = sum(distance<=deg2rad(360/nSection),2);
            if any(distanceSum>1)
                warning('The import point is has multiple projection at position [%d].',adjacentArray(iGlobalPosition));
                continue;
            end

            for iExist = 1:existDataLength(adjacentArray(iGlobalPosition))
                if length(find(distance(:,iExist)<=deg2rad(360/nSection))) == 1
                    solutionFlagMaskNow = squeeze(solutionFlag(iLocalPositionIn,iLocalRotationIn,adjacentArray(iGlobalPosition),:,distance(:,iExist)<=deg2rad(360/nSection))==1|...
                        solutionFlag(iLocalPositionIn,iLocalRotationIn,adjacentArray(iGlobalPosition),:,distance(:,iExist)<=deg2rad(360/nSection))==3|...
                        solutionFlag(iLocalPositionIn,iLocalRotationIn,adjacentArray(iGlobalPosition),:,distance(:,iExist)<=deg2rad(360/nSection))==4);
                    if ~any(solutionFlagMaskNow)
                        solutionFlag(iLocalPositionIn,iLocalRotationIn,adjacentArray(iGlobalPosition),:,distance(:,iExist)<=deg2rad(360/nSection)) = solutionFlagFD(adjacentArray(iGlobalPosition),:,iExist);
                        configSolColumn(:,iLocalPositionIn,iLocalRotationIn,adjacentArray(iGlobalPosition),:,distance(:,iExist)<=deg2rad(360/nSection)) = configSolColumnFD(:,adjacentArray(iGlobalPosition),:,iExist);
                    end
                end
            end
        end

        % take the unfilled part into zero
%         for iFlip = 1:length(sortList)
%             solutionFlagMaskNow = solutionFlag(iLocalPositionIn,iLocalRotationIn,adjacentArray(iGlobalPosition),:,distance<=deg2rad(360/nSection))==1|...
%                 solutionFlag(iLocalPositionIn,iLocalRotationIn,adjacentArray(iGlobalPosition),:,distance<=deg2rad(360/nSection))==3|...
%                 solutionFlag(iLocalPositionIn,iLocalRotationIn,adjacentArray(iGlobalPosition),:,distance<=deg2rad(360/nSection))==4;
%             if ~any(solutionFlagMaskNow)
%                 solutionFlag(iLocalPositionIn,iLocalRotationIn,adjacentArray(iGlobalPosition),:,iFlip) = 2;
%             end
%         end

        progressbar();
    end

    close(fig);

    % after calculation
    haveSolutionMaskAfter = solutionFlag == 1 | solutionFlag == 3 | solutionFlag == 4;
    existDataInSectionAfter = any(haveSolutionMaskAfter,4);
    existDataLengthAfter = squeeze(sum(existDataInSectionAfter,5));
    flipCounter = max(existDataLengthAfter,[],'all')+1;

    % prepare the data
    markIn='pointBatchImportFD';
    dataListIn.solutionFlag = solutionFlag;
    dataListIn.configSolColumn = configSolColumn;
    dataListIn.flipCounter = flipCounter;
    metaListIn.iLocalPosition = iLocalPositionIn;
    metaListIn.iLocalRotation = iLocalRotationIn;
    metaListIn.iGlobalPosition = iGlobalPositionIn;

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

