function assignSection(obj,options)
%ASSIGNADJACENT assign the section for the dexterity Map
    arguments
        obj(1,1) testDexterity
        options.section(1,1) {mustBeInteger,mustBePositive} = 24
        options.initialFlip(1,1) {mustBeInteger,mustBePositive} = 8
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('assignSection');

    % validate the input
    nAngles=length(homeConfiguration(obj.generateMapHandle.commonDataHandle.robotParameters.robotColumn));
    nLocalPosition=size(obj.generateMapHandle.dataList.allTform,3);
    nLocalRotation=size(obj.generateMapHandle.dataList.allTform,4);
    nGlobalRotation=size(obj.generateMapHandle.dataList.allTform,5);

    % assign the empty array
    solutionFlag=zeros(nLocalPosition,nLocalRotation,nGlobalRotation,options.section,options.initialFlip);
    configSolColumn=zeros(nAngles,nLocalPosition,nLocalRotation,nGlobalRotation,options.section,options.initialFlip);
    if nAngles>=7
        isRedudant=true;
    else
        isRedudant=false;
    end

    if nAngles~=7&&nAngles~=6
        warning('Degree of freedom [%d] is not 6 or 7, which may cause potential error.',nAngles)
    end

    % prepare for update
    markIn='assignSection';
    dataListIn.solutionFlag=solutionFlag;
    dataListIn.configSolColumn=configSolColumn;
    dataListIn.nSection=options.section;
    dataListIn.isRedudant=isRedudant;
    dataListIn.flipCounter=1;
    metaListIn=struct();

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end