function [solutionFlag,configSolColumn] = rotationNumeric(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,options)
%ROTATIONNUMERIC calculate the rotation with numeric way
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double
        iLocalRotationIn(1,1) double
        iFlipSolution(1,1) double
        options.extraCheck(1,1) logical = false
    end

    % prepare the data
    tform = obj.generateMapHandle.dataList.allTform;
    robotParameters = obj.generateMapHandle.commonDataHandle.robotParameters;
    robotColumn = robotParameters.robotColumn;
    robotConfigSize = length(homeConfiguration(robotColumn));
    endEffectorName = robotParameters.endEffectorName;
    baseName = robotParameters.baseName;

    % prepare the parameters
    nLocalPosition = size(tform,3);
    nLocalRotation = size(tform,4);
    nGlobalPosition = size(tform,5);
    nFlipSolution = size(obj.dataList.solutionFlag,5);
    nSectionNumber = obj.dataList.nSection;
    
    %progressbar
    fig = progressbar(['Dexterity: numeric eeRotation (',num2str(iLocalPositionIn),',', ...
                                                num2str(iLocalRotationIn),',:,:,', ...
                                                num2str(iFlipSolution),')'],(nLocalRotation-1)*nGlobalPosition);
    
    % prepare empty output
    solutionFlag = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,nSectionNumber,nFlipSolution);
    configSolColumn = zeros(robotConfigSize,nLocalPosition,nLocalRotation,nGlobalPosition,nSectionNumber,nFlipSolution);
    zeroConfigSol = zeros(robotConfigSize,1);

    % import the solutionFlag
    solutionFlagIn = obj.dataList.solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution);
    solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution) = solutionFlagIn;

    % import the configSolColumn
    configSolColumnIn = obj.dataList.configSolColumn(:,iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution);
    configSolColumn(:,iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution)=configSolColumnIn;

    % calculate the rotation
    for iGlobalPosition = 1:nGlobalPosition
        for iLocalRotation = 1:(nLocalRotation-1)
        iLocalRotationNow = mod(iLocalRotationIn+iLocalRotation-1,nLocalRotation)+1;
            for iSection = 1:nSectionNumber
                configSol = configSolColumnIn(:,1,1,iGlobalPosition,iSection,1);
                configSol(robotConfigSize) = configSol(robotConfigSize) +  2*pi*iLocalRotation/nLocalRotation;

                if solutionFlagIn(1,1,iGlobalPosition,iSection,1) == 2
                     solutionFlag(iLocalPositionIn,iLocalRotationNow,iGlobalPosition,iSection,iFlipSolution) = 2;
                     configSolColumn(:,iLocalPositionIn,iLocalRotationNow,iGlobalPosition,iSection,iFlipSolution) = zeroConfigSol;
                else
                    [solutionFlagOut,configSolOut] = solutionFlagCheck(robotParameters,configSol);

                    solutionFlag(iLocalPositionIn,iLocalRotationNow,iGlobalPosition,iSection,iFlipSolution) = solutionFlagOut;
                    configSolColumn(:,iLocalPositionIn,iLocalRotationNow,iGlobalPosition,iSection,iFlipSolution) = configSolOut;
                    
                    % check the difference
                    if options.extraCheck == true
                        tformNew = getTransform(robotColumn,configSol,endEffectorName,baseName);
                        tformShouldBe = tform(:,:,iLocalPositionIn,iLocalRotationNow,iGlobalPosition);
                        if norm(tformNew-tformShouldBe) > 1e-3
                            error('Difference too large. Mistake in algorithm')
                        end
                    end
                end
            end
            progressbar();
        end
    end

    close(fig);
end

