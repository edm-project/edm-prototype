function [sectionNumberOut,iFilpSolutionOut] = pointImportFD(obj,iLocalPosition,iLocalRotation,iGlobalPosition,iSection,iFlipSolution,options)
%POINTIMPORT import the point from testReachability
    arguments
        obj(1,1) testDexterity
        iLocalPosition(1,1) double {mustBeInteger}
        iLocalRotation(1,1) double {mustBeInteger}
        iGlobalPosition(1,1) double {mustBeInteger}
        iSection(1,1) double {mustBeInteger}
        iFlipSolution(1,1) double {mustBeInteger}
        options.ignoreWarning(1,1) logical = false
    end

    % check if allow call for efficiency
    obj.mustAllowCall('pointImportFD');

    % collect the data form testReachability
    solutionFlag=obj.generateMapHandle.dataList.testFlippedDexterity.dataList.solutionFlag;
    configSolColumn=obj.generateMapHandle.dataList.testFlippedDexterity.dataList.configSolColumn;
    positionRow=obj.generateMapHandle.dataList.positionRow;

    % import the specific point
    solutionFlagIn = solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition,iSection,iFlipSolution);
    configSolColumnIn = configSolColumn(:,iLocalPosition,iLocalRotation,iGlobalPosition,iSection,iFlipSolution);
    xIndex=positionRow(iGlobalPosition,1);
    yIndex=positionRow(iGlobalPosition,2);
    zIndex=positionRow(iGlobalPosition,3);
    [sectionNumberOut,iFilpSolutionOut] = obj.singlePointImport(solutionFlagIn, ...
                                             configSolColumnIn, ...
                                             iLocalPosition, ...
                                             iLocalRotation, ...
                                             iGlobalPosition, ...
                                             ignoreWarning=options.ignoreWarning);
    
    % prepare for update
    markIn='pointImportFD';
    dataListIn=struct();
    metaListIn.iLocalPosition=iLocalPosition;
    metaListIn.iLocalRotation=iLocalRotation;
    metaListIn.iGlobalPosition=iGlobalPosition;
    metaListIn.xIndex=xIndex;
    metaListIn.yIndex=yIndex;
    metaListIn.zIndex=zIndex;
    metaListIn.sectionNumberOut=sectionNumberOut;
    metaListIn.iFilpSolutionOut=iFilpSolutionOut;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end

