function pointBatchImportFDSign(obj)
%POINTBATCHIMPORTFDSIGN 
    arguments
        obj(1,1) testDexterity
    end

    obj.mustAllowCall('pointBatchImportFDSign');

    % import the data needed
    solutionFlagFD = obj.generateMapHandle.dataList.testFlippedDexterity.dataList.solutionFlag;
    configSolColumnFD = obj.generateMapHandle.dataList.testFlippedDexterity.dataList.configSolColumn;
    solutionFlag = obj.dataList.solutionFlag;
    configSolColumn = obj.dataList.configSolColumn;
    tform = obj.generateMapHandle.dataList.allTform;
    nLocalPosition = size(tform,3);
    nLocalRotation = size(tform,4);
    nGlobalPosition = size(tform,5);

    %initialize the data and progressbar
    minorWarningCounter = 0;
    majorWarningCounter = 0;
    rewriteCounter = 0;
    warningList = [0,0,0,0,0,0,0,0];
    fig = progressbar('Dexterity: pointBatchImportFDSign ',nLocalPosition*nLocalRotation*nGlobalPosition);

    % prepare the metaData
    nSection = obj.dataList.nSection;
    haveSolutionMask = solutionFlagFD == 1 | solutionFlagFD == 3 | solutionFlagFD == 4;
    existDataInSection = any(haveSolutionMask,4);
    existDataLength = squeeze(sum(existDataInSection,5));

    % the main iteration
    for iGlobalPosition=1:nGlobalPosition
        for iLocalPosition=1:nLocalPosition    % for each position(global and local)
            for iLocalRotation=1:nLocalRotation  % for each rotation on same position
                
                for iFlip = 1:existDataLength(iLocalPosition,iLocalRotation,iGlobalPosition)
                    FSUsolutionFlag = squeeze(solutionFlagFD(iLocalPosition,iLocalRotation,iGlobalPosition,:,iFlip));
                    FSUsolutionFlagMask = (FSUsolutionFlag==1)|(FSUsolutionFlag==3)|(FSUsolutionFlag==4);
                    FSUNoSolutionSum = sum((FSUsolutionFlag==0)|(FSUsolutionFlag==2),'all');
                    if sum(FSUsolutionFlagMask,'all') <= nSection /3
                        continue;
                    end
                    FSUcurrent = squeeze(configSolColumnFD(:,iLocalPosition,iLocalRotation,iGlobalPosition,:,iFlip));
                    signSum = sum(sign(FSUcurrent([2,4,6],:)),2);

                    % handle the warning case
                    if any(abs(signSum) < (nSection-FSUNoSolutionSum)/2) % major warning
                        majorWarningCounter = majorWarningCounter + 1;
                        warningList = [warningList;[1,iLocalPosition,iLocalRotation,iGlobalPosition,iFlip,signSum(1),signSum(2),signSum(3)]]; %#ok<*AGROW> 
                        warning('Major warning:(%d,%d,%d,:,%d) have signSum of [%d,%d,%d]',iLocalPosition,iLocalRotation,iGlobalPosition,iFlip,signSum(1),signSum(2),signSum(3));
                    elseif any(abs(signSum) ~= nSection-FSUNoSolutionSum) % minor warning
                        minorWarningCounter = minorWarningCounter + 1;
                        warningList = [warningList;[2,iLocalPosition,iLocalRotation,iGlobalPosition,iFlip,signSum(1),signSum(2),signSum(3)]];
                        warning('Minor warning:(%d,%d,%d,:,%d) have signSum of [%d,%d,%d]',iLocalPosition,iLocalRotation,iGlobalPosition,iFlip,signSum(1),signSum(2),signSum(3));
                    end

                    % encode the data
                    signOfSignSum = sign(signSum);
                    signOfSignSum(signOfSignSum == -1) = 0;
                    encodeValue = signOfSignSum;
                    nEncode = encodeValue(1)*4 + encodeValue(2)*2 + encodeValue(3) + 1;

                    % update the data and handle the rewrite warning
                    if any(solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition,:,nEncode) ~= 0)
                        rewriteCounter = rewriteCounter + 1;
                        equal1 = sum(solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition,:,nEncode) == 1,'all');
                        equal2 = sum(solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition,:,nEncode) == 2,'all');
                        equal34 = sum(solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition,:,nEncode) == 3 | solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition,:,nEncode) == 4,'all');
                        warningList = [warningList;[3,iLocalPosition,iLocalRotation,iGlobalPosition,nEncode,equal1,equal2,equal34]]; %#ok<*AGROW> 
                        warning('Rewrite warning: Rewriting (%d,%d,%d,:,%d) with solutionFlag of nonzero (%d,%d,%d)',iLocalPosition,iLocalRotation,iGlobalPosition,nEncode,equal1,equal2,equal34);
                    end
                    solutionFlag(iLocalPosition,iLocalRotation,iGlobalPosition,:,nEncode) = solutionFlagFD(iLocalPosition,iLocalRotation,iGlobalPosition,:,iFlip);
                    configSolColumn(:,iLocalPosition,iLocalRotation,iGlobalPosition,:,nEncode) = configSolColumnFD(:,iLocalPosition,iLocalRotation,iGlobalPosition,:,iFlip);
                end
                progressbar();
            end
        end
    end

    close(fig);


    % after calculation
    solutionFlag(solutionFlag==0) = 2;

    haveSolutionMaskAfter = solutionFlag == 1 | solutionFlag == 3 | solutionFlag == 4;
    existDataInSectionAfter = any(haveSolutionMaskAfter,4);
    existDataLengthAfter = squeeze(sum(existDataInSectionAfter,5));
    flipCounter = max(existDataLengthAfter,[],'all')+1;

    % prepare the data
    markIn='pointBatchImportFDSign';
    dataListIn.solutionFlag = solutionFlag;
    dataListIn.configSolColumn = configSolColumn;
    dataListIn.flipCounter = flipCounter;
    metaListIn.minorWarningCounter = minorWarningCounter;
    metaListIn.majorWarningCounter = majorWarningCounter;
    metaListIn.rewriteCounter = rewriteCounter;
    metaListIn.warningList = warningList;

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

