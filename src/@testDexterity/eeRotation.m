function eeRotation(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,options)
%EEROTATION rotate the solution (iLocalPosition,iLocalRotation,:,:,iFlipSolution)
%  the rotation will iterate through the GlobalPosition and Section (therefore requires full data point of it)
%  it will start from (iLocalPosition,iLocalRotation,1,1,iFlipSolution)
%  and try to directly get solution of (iLocalPosition,mod(iLocalRotation+1,nLocalRotation),1,1,iFlipSolution)
%  by adding some angle to the last joint.
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double
        iLocalRotationIn(1,1) double
        iFlipSolution(1,1) double
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
        options.extraCheck(1,1) logical = false
    end

    % check if allow call for efficiency
    obj.mustAllowCall('eeRotation');

    % start a time counter
    startTime= datetime('now');

    % get the information needed
    mode = obj.dataList.mode;

    % calculate the information
    if isequal(mode,'numeric')
        [solutionFlag,configSolColumn] = obj.rotationNumeric(iLocalPositionIn,iLocalRotationIn,iFlipSolution, ...
                                                            extraCheck=options.extraCheck);
    elseif isequal(mode,'numericMEX') 
        [solutionFlag,configSolColumn] = obj.rotationNumericMEX(iLocalPositionIn,iLocalRotationIn,iFlipSolution, ...
                                                            ignoreWarning=options.ignoreWarning,...
                                                            forceGenerate=options.forceGenerate,...
                                                            extraCheck=options.extraCheck);
    elseif isequal(mode,'numericMEXParallel')
        [solutionFlag,configSolColumn] = obj.rotationNumericMEXParallel(iLocalPositionIn,iLocalRotationIn,iFlipSolution, ...
                                                            ignoreWarning=options.ignoreWarning,...
                                                            forceGenerate=options.forceGenerate,...
                                                            extraCheck=options.extraCheck);
    else
        ME = MException('testDexterity:eeRotation:elseError', ...
                        'mode if-else should not run into this branch');
        throw(ME);
    end

    % prepare for update
    markIn='eeRotation';
    dataListIn.solutionFlag = solutionFlag;
    dataListIn.configSolColumn = configSolColumn;
    metaListIn.iLocalPosition = iLocalPositionIn;
    metaListIn.iLocalRotation = iLocalRotationIn;
    metaListIn.iFlipSolution = iFlipSolution;
    metaListIn.startTime=startTime;
    metaListIn.endTime=datetime('now');

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);
end
