function [sectionNumberOut,iFilpSolutionOut] = pointCalculate(obj,iLocalPosition,iLocalRotation,iGlobalPosition,options)
%CALCULATESINGLEPOINT calculate a single point's inverse kinematics and try to import it
    arguments
        obj(1,1) testDexterity
        iLocalPosition(1,1) double {mustBeInteger}
        iLocalRotation(1,1) double {mustBeInteger}
        iGlobalPosition(1,1) double {mustBeInteger}
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
        options.maxIkIterationIn(1,1) double = 1500
        options.weightsIn(1,6) double = [0.1 0.1 0.1 0.5 0.5 0.5]
        options.maxIKTrial(1,1) double = 25
    end

    % check if allow call for efficiency
    obj.mustAllowCall('pointCalculate');

    % import the data needed
    mode = obj.dataList.mode;
    tform = obj.generateMapHandle.dataList.allTform(:,:,iLocalPosition,iLocalRotation,iGlobalPosition);
    robotParameters = obj.generateMapHandle.commonDataHandle.robotParameters;
    robotColumn = robotParameters.robotColumn;
    endEffectorName = robotParameters.endEffectorName;
    positionRow=obj.generateMapHandle.dataList.positionRow;

    % prepare the inverse kinematics
    if isequal(mode,'numeric')
        ik = inverseKinematics('RigidBodyTree',robotColumn);
        ik.SolverParameters.MaxIterations=options.maxIkIterationIn;
    elseif isequal(mode,'numericMEX') || isequal(mode,'numericMEXParallel')
        ikHandle = MEXikGenerate(robotParameters, ...
                  options.maxIkIterationIn, ...
                  ignoreWarning=options.ignoreWarning, ...
                  forceGenerate=options.forceGenerate);
        solutionFlagCheckHandle = MEXsolutionFlagCheckGenerate(robotParameters, ...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);
    else
        ME = MException('testDexterity:pointCalculate:elseError', ...
                        'mode if-else should not run into this branch');
        throw(ME);
    end

    % inverse kinematics calculation and trial
    if isequal(mode,'numeric')
        for iIkIteration = 1:options.maxIKTrial
            initialguess = randomConfiguration(robotColumn);
            [configSol,solInfo] = ik(endEffectorName, ...
                                     tform, ...
                                     options.weightsIn, ...
                                     initialguess);

            % if IK solver can't find Solution.
            if isequal(solInfo.Status,'best available')
                error('The calculation of this point has no solution');

            % if IK solver find Solution.
            elseif isequal(solInfo.Status,'success')
                [solutionFlagOut,configSolOut] = solutionFlagCheck(robotParameters,configSol);

                if solutionFlagOut == 1 || iIkIteration==options.maxIKTrial
                    solutionFlagIn = solutionFlagOut;
                    configSolColumnIn = configSolOut;
                else
                    continue;
                end
                
                [sectionNumberOut,iFilpSolutionOut,isNewSolution] = obj.singlePointImport(solutionFlagIn, ...
                                     configSolColumnIn, ...
                                     iLocalPosition, ...
                                     iLocalRotation, ...
                                     iGlobalPosition, ...
                                     ignoreWarning=options.ignoreWarning);
                if isNewSolution == true
                    disp(['New solution at S:[',num2str(sectionNumberOut),'] F:[',num2str(iFilpSolutionOut),'] Found']);
                    break;
                end
            end
        end

    elseif isequal(mode,'numericMEX') || isequal(mode,'numericMEXParallel')
        for iIkIteration = 1:options.maxIKTrial
            initialguess = randomConfiguration(robotColumn);
            [configSol,solInfo] = ikMEX(ikHandle,endEffectorName, ...
                                     tform, ...
                                     options.weightsIn, ...
                                     initialguess);

            % if IK solver can't find Solution.
            if isequal(solInfo.Status,'best available')
                error('The calculation of this point has no solution');

            % if IK solver find Solution.
            elseif isequal(solInfo.Status,'success')
                [solutionFlagOut,configSolOut] = solutionFlagCheckMEX(solutionFlagCheckHandle, configSol);

                if solutionFlagOut == 1 || iIkIteration==options.maxIKTrial
                    solutionFlagIn = solutionFlagOut;
                    configSolColumnIn = configSolOut;
                else
                    continue;
                end

                [sectionNumberOut,iFilpSolutionOut,isNewSolution] = obj.singlePointImport(solutionFlagIn, ...
                                     configSolColumnIn, ...
                                     iLocalPosition, ...
                                     iLocalRotation, ...
                                     iGlobalPosition, ...
                                     ignoreWarning=options.ignoreWarning);
                if isNewSolution == true
                    disp(['New solution at S:[',num2str(sectionNumberOut),'] F:[',num2str(iFilpSolutionOut),'] Found']);
                    break;
                end
            end
        end
    else
        ME = MException('testDexterity:pointCalculate:elseError', ...
                        'mode if-else should not run into this branch');
        throw(ME);
    end

    % collect information for the point calculate
    xIndex=positionRow(iGlobalPosition,1);
    yIndex=positionRow(iGlobalPosition,2);
    zIndex=positionRow(iGlobalPosition,3);

    % prepare for update
    markIn='pointCalculate';
    dataListIn=struct();
    metaListIn.iLocalPosition=iLocalPosition;
    metaListIn.iLocalRotation=iLocalRotation;
    metaListIn.iGlobalPosition=iGlobalPosition;
    metaListIn.xIndex=xIndex;
    metaListIn.yIndex=yIndex;
    metaListIn.zIndex=zIndex;
    metaListIn.sectionNumberOut=sectionNumberOut;
    metaListIn.iFilpSolutionOut=iFilpSolutionOut;

    % update the Data
    obj.updateData(markIn,metaListIn,dataListIn);

end

