function nullspaceMotion(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,options)
%NULLSPACEMOTION do the nullspace motion 
% for a orientation and iFlipsolution in the whole map
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double {mustBeInteger}
        iLocalRotationIn(1,1) double {mustBeInteger}
        iFlipSolution(1,1) double {mustBeInteger}
        options.timeStep(1,1) double = 0.3
        options.increaseStep(1,1) double = 50
        options.maxStep(1,1) double = 200
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('nullspaceMotion');

    % check if the robot is redudant
    if obj.dataList.isRedudant==false
        ME = MException('testDexterity:nullspaceMotion:notRedudant', ...
                        'non-redudant robot cannot do nullspacemotion.');
        throw(ME); 
    end

    % start a time counter
    startTime= datetime('now');

    % prepare variables for calculation and storage
    mode = obj.dataList.mode;
    robotParameters = obj.generateMapHandle.commonDataHandle.robotParameters;
    solutionFlagIn = obj.dataList.solutionFlag; 
    configSolColumnIn = obj.dataList.configSolColumn;

    % prepare parameters
    nLocalPosition = size(obj.generateMapHandle.dataList.allTform,3);
    nLocalRotation = size(obj.generateMapHandle.dataList.allTform,4);
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);
    nSectionNumber = obj.dataList.nSection;
    nFlipSolution = size(obj.dataList.solutionFlag,5);
    robotConfigSize = length(homeConfiguration(robotParameters.robotColumn)); 

    % prepare empty output
    solutionFlag = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,nSectionNumber,nFlipSolution);
    configSolColumn = zeros(robotConfigSize,nLocalPosition,nLocalRotation,nGlobalPosition,nSectionNumber,nFlipSolution);

    % import the initial data
    solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution) = solutionFlagIn(iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution); 
    configSolColumn(:,iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution) = configSolColumnIn(:,iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution);

    % generalized ik calculation with different modes
    if isequal(mode,'numeric')
        [solutionFlag,configSolColumn] = obj.nullNumeric(iLocalPositionIn,iLocalRotationIn,iFlipSolution, ...
                                                    solutionFlag,configSolColumn, ...
                                                    options.timeStep,options.increaseStep,options.maxStep);
    elseif isequal(mode,'numericMEX')
        [solutionFlag,configSolColumn] = obj.nullNumericMEX(iLocalPositionIn,iLocalRotationIn,iFlipSolution, ...
                                                    solutionFlag,configSolColumn, ...
                                                    options.timeStep,options.increaseStep,options.maxStep, ...
                                                    forceGenerate=options.forceGenerate, ...
                                                    ignoreWarning=options.ignoreWarning);
    elseif isequal(mode,'numericMEXParallel')
        [solutionFlag,configSolColumn] = obj.nullNumericMEXParallel(iLocalPositionIn,iLocalRotationIn,iFlipSolution, ...
                                                    solutionFlag,configSolColumn, ...
                                                    options.timeStep,options.increaseStep,options.maxStep, ...
                                                    forceGenerate=options.forceGenerate, ...
                                                    ignoreWarning=options.ignoreWarning);
    else
        ME = MException('testDexterity:nullspaceMotion:elseError', ...
                        'mode if-else should not run into this branch');
        throw(ME);  
    end

    % output
    % prepare the data
    markIn='nullspaceMotion';
    dataListIn.solutionFlag = solutionFlag;
    dataListIn.configSolColumn = configSolColumn;
    metaListIn.iLocalPosition = iLocalPositionIn;
    metaListIn.iLocalRotation = iLocalRotationIn;
    metaListIn.iFlipSolution = iFlipSolution;
    metaListIn.timeStep = options.timeStep;
    metaListIn.increaseStep = options.increaseStep;
    metaListIn.maxStep = options.maxStep;
    metaListIn.startTime=startTime;
    metaListIn.endTime=datetime('now');
    if isequal(mode,'numericMEX')
        tag = robotParameters.tag;
        backup_name = ['MEXnullspaceFixed_backup_',tag];
        metaListIn.numericMEXStr = fileread(backup_name);
    end

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);

end

