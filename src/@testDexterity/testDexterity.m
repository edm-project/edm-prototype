classdef testDexterity  < handle
    %TESTREACHABILITY  A Class that calculate Reachability but stores more
    %information
    
    properties (SetAccess = immutable)
        version(1,1) double
    end 
    % end of immutable attributes
    
    properties (SetAccess=private)
        generateMapHandle(1,1) generateMap

        mark(1,:) cell {mustBeValidMarksNullPrepare} = {}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
    end 
    % end of private attributes

    methods
        function obj = testDexterity(generateMapHandleIn)
        %GENERATEGRID constructor
            arguments
                generateMapHandleIn(1,1) generateMap
            end
            obj.generateMapHandle=generateMapHandleIn;
            obj.version=1;
            obj.mark={'null'};
        end
    end
    % end of the constructor

    methods (Access=private)
        function mustAllowCall(obj,callMarkIn)
        % mustAllowCall: throw error if function current call is not allowed
            arguments
                obj(1,1) testDexterity
                callMarkIn(1,:) char {mustBeValidMarks}
            end

            % allow no repeat for {'assignMode','assignSection','assignAdjacent'}
            mustNoRepeatGroup(obj,callMarkIn,{'assignMode','assignSection','assignAdjacent'});

            % allow only call {'pointImport','pointCalculate'} after {'assignMode'} and {'assignSection'}
            mustInSequence(obj,callMarkIn,{'pointImport','pointCalculate','pointBatchImportFD','pointBatchImportFDSign'},{'assignMode'});
            mustInSequence(obj,callMarkIn,{'pointImport','pointCalculate','pointBatchImportFD','pointBatchImportFDSign'},{'assignSection'});

            % allow only call {'gikMotion'} after {'assignAdjacent'} 
            mustInSequence(obj,callMarkIn,{'gikMotion'},{'assignAdjacent'});

            % allow only call {'nullspaceMotion','nullspaceMotionSinglePoint','gikMotion'} after {'pointImport','pointCalculate'}
            mustInSequence(obj,callMarkIn,{'deleteData','nullspaceMotion','nullspaceMotionSinglePoint','gikMotion'},{'pointImport','pointCalculate','pointImportFD','pointBatchImportFD'});

            % allow only call {'eeRotation'} after {'nullspaceMotion'} and {'gikMotion'}
            % maybe improvement later
            mustInSequence(obj,callMarkIn,{'eeRotation'},{'nullspaceMotion'});
            mustInSequence(obj,callMarkIn,{'eeRotation'},{'gikMotion'});

            % allow only call {'removeRepeatFlip'} after {'nullspaceMotion'}
            mustInSequence(obj,callMarkIn,{'removeRepeatFlip'},{'nullspaceMotion'});
        end

        function updateData(obj,markIn,metaListIn,dataListIn)
        % update the data in this process, called in functional methods
            arguments
                obj(1,1) testDexterity
                markIn(1,:) char {mustBeValidMarks}
                metaListIn(1,1) struct
                dataListIn(1,1) struct {mustBeValidDataList}
            end
            % check if this call is allowed
            obj.mustAllowCall(markIn);

            % update mark
            if isequal(obj.mark,{'prepared'})
                obj.mark={markIn};
            else
                obj.mark=[obj.mark,{markIn}];
            end

            %update metaList and dataList
            if isequal(markIn,'gikMotion') || isequal(markIn,'gikDirection') || ... 
               isequal(markIn,'nullspaceMotion') || isequal(markIn,'nullspaceMotionSinglePoint') || ...
               isequal(markIn,'eeRotation')
                metaListReturn = obj.marginalUpdateData(dataListIn);    % function for marginal Update of some Data
                metaListIn = combineStruct(metaListIn,metaListReturn);
            elseif isequal(markIn,'deleteData') || isequal(markIn,'removeRepeatFlip') || ...
                isequal(markIn,'pointBatchImportFD') || isequal(markIn,'dexterityStatistics') || isequal(markIn,'pointBatchImportFDSign')
                obj.dataList = mergeUpdateStruct(obj.dataList,dataListIn);
            else
                obj.dataList = combineStruct(obj.dataList,dataListIn);
            end
            obj.metaList{length(obj.metaList)+1}=metaListIn;
        end

        function metaListReturn = marginalUpdateData(obj,dataListIn)
        % update the data with rules:
        % if old data ==0 and new data ~=0, then update the data
        % if old data ~=1 and new data ==1, then update the data
            if isfield(dataListIn,'solutionFlag')||isfield(dataListIn,'configSolColumn')
                if isfield(dataListIn,'solutionFlag')&&isfield(dataListIn,'configSolColumn')
                    nAngles=size(dataListIn.configSolColumn,1);

                    % if old data ==0 and new data ~=0, then update the data
                    oldZeroMask = (obj.dataList.solutionFlag == 0);
                    newNotZeroMask = (dataListIn.solutionFlag == 1)|(dataListIn.solutionFlag == 2)| ...
                                     (dataListIn.solutionFlag == 3)|(dataListIn.solutionFlag == 4);
                    solutionMaskOne = oldZeroMask & newNotZeroMask;
                    configSolMaskOne = permute(repmat(solutionMaskOne,1,1,1,1,1,nAngles),[6 1 2 3 4 5]);

                    % if old data ~=1 and new data ==1, then update the data
                    oldNotOneMask = (obj.dataList.solutionFlag == 2)|(obj.dataList.solutionFlag == 3)|(obj.dataList.solutionFlag == 4);
                    newOneMask = (dataListIn.solutionFlag == 1);
                    solutionMaskTwo = oldNotOneMask & newOneMask;
                    configSolMaskTwo = permute(repmat(solutionMaskTwo,1,1,1,1,1,nAngles),[6 1 2 3 4 5]);

                    % if old data == 2 and new data == 3 / 4 (==1 are already included by the above situation)
                    oldNoSolutionMask = (obj.dataList.solutionFlag == 2);
                    newConstraintMask = (dataListIn.solutionFlag == 3)|(dataListIn.solutionFlag == 4);
                    solutionMaskThree = oldNoSolutionMask & newConstraintMask;
                    configSolMaskThree = permute(repmat(solutionMaskThree,1,1,1,1,1,nAngles),[6 1 2 3 4 5]);

                    % update data with mask
                    obj.dataList.solutionFlag(solutionMaskOne)=dataListIn.solutionFlag(solutionMaskOne);
                    obj.dataList.configSolColumn(configSolMaskOne)=dataListIn.configSolColumn(configSolMaskOne);
                    obj.dataList.solutionFlag(solutionMaskTwo)=dataListIn.solutionFlag(solutionMaskTwo);
                    obj.dataList.configSolColumn(configSolMaskTwo)=dataListIn.configSolColumn(configSolMaskTwo);
                    obj.dataList.solutionFlag(solutionMaskThree)=dataListIn.solutionFlag(solutionMaskThree);
                    obj.dataList.configSolColumn(configSolMaskThree)=dataListIn.configSolColumn(configSolMaskThree);

                    % collect some metadata for later
                    metaListReturn.totalCalculation = sum(dataListIn.solutionFlag~=0,"all");
                    metaListReturn.newHasSolution = sum(dataListIn.solutionFlag==1,"all");
                    metaListReturn.newNoSolution = sum(dataListIn.solutionFlag==2,"all");
                    metaListReturn.newJointLimit = sum(dataListIn.solutionFlag==3,"all");
                    metaListReturn.newSelfCollision = sum(dataListIn.solutionFlag==4,"all");
                    metaListReturn.zero2OtherCounter = sum(solutionMaskOne,"all");
                    metaListReturn.other2OneCounter = sum(solutionMaskTwo,"all");
                    metaListReturn.two2ConstraintCounter = sum(solutionMaskThree,"all");
                else
                    ME = MException('testDexterity:marginalUpdateData:dataListNotComplete', ...
                        'dataList should contain solutionFlag [%d] configSolColumn [%d]', ...
                        isfield(dataListIn,'solutionFlag'),isfield(dataListIn,'configSolColumn'));
                    throw(ME)
                end
            else
                error('The dataList does not contain the solutionFlag and configSolColumn');
            end
        end
    end

    methods (Access = public)
        function initialize(obj,options)
        % initalize this process
            arguments
                obj(1,1) testDexterity
                options.ignoreWarning(1,1) logical = false
            end

            % warning when this class is not empty 
            if options.ignoreWarning==false && ~isequal(obj.dataList,struct())
                warning('This object testDexterity is not empty. Initialization will wipe out the old data.')
                disp('Continue Anyway?');
                pause on;
                pause;
            end

            % initialize(reset) the data section of this class
            obj.mark={'prepared'};
            obj.metaList={};
            obj.dataList=struct(); 
        end
    end
    % end of the robotMaps public methods

    methods (Access = private)
        [sectionNumberOut,iFilpSolutionOut,isNewSolution] = singlePointImport(obj,solutionFlagIn,configSolColumnIn,iLocalPosition,iLocalRotation,iGlobalPosition,options)
        [adjacentArray, previousArray] = adjacentListCalculation(obj,iLocalPosition,iLocalRotation,iGlobalPosition,solutionFlagReachability)
        [requireRetry,solutionFlagSlice,configSolColumnSlice,adjacentArray,previousArray] = motionNumeric(obj,iLocalPosition,iLocalRotation,iSectionNumber,iFlipSolution,adjacentArray,previousArray,maxIkIterationIn,maxConstraintRepeatIn,startAngleTolerance,maxAngleTolerance,maxGiveupDistance,options)
        [requireRetry,solutionFlagSlice,configSolColumnSlice,adjacentArray,previousArray] = motionNumericMEX(obj,iLocalPosition,iLocalRotation,iSectionNumber,iFlipSolution,adjacentArray,previousArray,maxIkIterationIn,maxConstraintRepeatIn,startAngleTolerance,maxAngleTolerance,maxGiveupDistance,options)
        [solutionFlag,configSolColumn] = nullNumeric(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,solutionFlag,configSolColumn,timeStep,increaseStep,maxStep)
        [solutionFlag,configSolColumn] = nullNumericMEX(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,solutionFlag,configSolColumn,timeStep,increaseStep,maxStep,options)
        [solutionFlag,configSolColumn] = nullNumericMEXParallel(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,solutionFlag,configSolColumn,timeStep,increaseStep,maxStep,options)
        [solutionFlag,configSolColumn] = nullSinglePointNumeric(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iFlipSolution,solutionFlag,configSolColumn,timeStep,increaseStep,maxStep)
        [solutionFlag,configSolColumn] = nullSinglePointNumericMEX(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iFlipSolution,solutionFlag,configSolColumn,timeStep,increaseStep,maxStep,options)
        [solutionFlagSlice,configSolColumnSlice] = directionNumeric(obj,iLocalRotationIn,iGlobalPositionIn,iSectionNumber,iFlipSolution,maxIkIterationIn, maxNoSolutionRepeatIn,maxConstraintRepeatIn,startAngleTolerance,maxAngleTolerance)
        [solutionFlagSlice,configSolColumnSlice] = directionNumericMEX(obj,iLocalRotationIn,iGlobalPositionIn,iSectionNumber,iFlipSolution,maxIkIterationIn, maxNoSolutionRepeatIn,maxConstraintRepeatIn,startAngleTolerance,maxAngleTolerance,options)
        [solutionFlag,configSolColumn] = rotationNumeric(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,options)
        [solutionFlag,configSolColumn] = rotationNumericMEX(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,options)
        [solutionFlag,configSolColumn] = rotationNumericMEXParallel(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,options)
    end
    % end of the private functional method (definition in same folder)

    methods (Access = public)
        assignMode(obj,mode)
        % Functional method. Assign the mode of the methods

        assignSection(obj,isRedudant,options)
        % Functional method. Do marginal update of reachability

        assignAdjacent(obj)
        % Functional method. Do marginal update of reachability

        [sectionNumberOut,iFilpSolutionOut] = pointImport(obj,iLocalPosition,iLocalRotation,iGlobalPosition,options)
        % Functional method, import the data form reachability map

        [sectionNumberOut,iFilpSolutionOut] = pointCalculate(obj,iLocalPosition,iLocalRotation,iGlobalPosition,options)
        % Functional method, calculate the data about the reachability

        [sectionNumberOut,iFilpSolutionOut] = pointImportFD(obj,iLocalPosition,iLocalRotation,iGlobalPosition,iSection,iFlipSolution,options)
        %  Functional method, import the data form flipped dexterity map

        requireRechoose = gikMotion(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iSectionNumber,iFlipSolution,options)
        % Functional method, generate the generalized ik solution in the cartesian space

        nullspaceMotion(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,options)
        % Functional method, do the nullspaceMotion for a specific Position

        nullspaceMotionSinglePoint(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iFlipSolution,options)
        % Functional method, do the nullspaceMotion for a specific Position
       
        eeRotation(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution)
        % Functional method, rotate the endeffector

        [shouldContinue,iGlobalPosition] = compareGikMotion(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,options)
        % helper method, hint the next nullmotion's parameter

        gikDirection(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iSectionNumber,iFlipSolution,options)
        % (not suggested to be used. try to find motion with same flipSolution)
        % (result not good)

        [calculationFinished,isDownGrade] = gikMotionEmpirical(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,options)
        % helper method, a more empirical gikMotion with better decision

        requireRechoose = gikMotionNull(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iFlipSolution,options)
        % Functional method, do gikMotion, but try with all possible gikMotion

        deleteData(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution)
        % Functional method, delete a section of data

        removeRepeatFlip(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolutionNow,options)
        % Functional method, remove a repeated flipped solution

        pointBatchImportFD(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,sortList)
        pointBatchImportFDSign(obj) 
        % Functional method, import point from Flipped Dexterity

        iSectionNumberList = decideSection(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositioIn,iFlipSolutionIn,options)
        % helper method, a better function for decide the best starting section

        dexterityStatistics(obj)
        % helper method, a function that calculate the information
    end
    % end of the public functional method (definition in same folder)
    
end

function mustBeValidMarks(marksIn)
% Check for Valid input as mark.
    mustBeMember(marksIn,{'assignMode','assignAdjacent','assignSection', ...
                          'pointImport','pointCalculate','pointImportFD', 'deleteData', 'pointBatchImportFD','pointBatchImportFDSign', ...
                          'nullspaceMotion','nullspaceMotionSinglePoint','gikMotion','gikDirection', ...
                          'eeRotation', 'removeRepeatFlip', ...
                          'dexterityStatistics'})
end

function mustBeValidMarksNullPrepare(marksIn)
% Check for Valid input as mark. Allow null and prepare.
    mustBeMember(marksIn,{'null','prepared','assignMode','assignAdjacent','assignSection', ...
                          'pointImport','pointCalculate','pointImportFD', 'deleteData', 'pointBatchImportFD','pointBatchImportFDSign',...
                          'nullspaceMotion','nullspaceMotionSinglePoint','gikMotion','gikDirection',...
                          'eeRotation', 'removeRepeatFlip',...
                          'dexterityStatistics'})
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{'solutionFlag','configSolColumn','dexterityIndex','redudancyIndex','dexteritySlicedIndex', ...
                                    'jointBoundDistanceMtx','jointBoundSolvableDistanceMtx','flipDistanceIndexMap','flipDistance','flipDistanceSolvable', ...
                                    'mode','nSection','isRedudant','adjacentMap','flipCounter'})
end

