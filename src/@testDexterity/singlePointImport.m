function [sectionNumberOut,iFilpSolutionOut,isNewSolution] = singlePointImport(obj,solutionFlagIn,configSolColumnIn,iLocalPosition,iLocalRotation,iGlobalPosition,options)
% import one pair of solutionFlag and configSolColumn without the dexterity dimension
    arguments
        obj(1,1) testDexterity
        solutionFlagIn(1,1) double
        configSolColumnIn(:,1) double
        iLocalPosition(1,1) double
        iLocalRotation(1,1) double
        iGlobalPosition(1,1) double
        options.ignoreWarning(1,1) logical = false
    end

    % check the input value
    if options.ignoreWarning== false && norm(abs(configSolColumnIn)) == 0
        warning('The configSolColumnIn normally is not an all zero value');
        disp('Continue Anyway?');
        pause on;
        pause;
    end

    if options.ignoreWarning== false && (solutionFlagIn == 0 || solutionFlagIn == 2)
        warning('The solutionFlag normally is not 0/2');
        disp('Continue Anyway?');
        pause on;
        pause;
    end
    
    % export the data needed
    robotParameters=obj.generateMapHandle.commonDataHandle.robotParameters;
    nSection=obj.dataList.nSection;
    flipCounter=obj.dataList.flipCounter;
    solutionFlagOriginal=obj.dataList.solutionFlag;
    configSolColumnOriginal=obj.dataList.configSolColumn;
    
    % assign the output data
    isNewSolution = false;

    % calculate the sectionNumber
    [~,sectionNumber] = dexteritySection(robotParameters,configSolColumnIn,nSection);

    % import the MEX file
    isReachableHandle = MEXisNullspaceReachableGenerate(robotParameters);

    sectionHandle = MEXdexteritySectionGenerate(robotParameters);

    % check and output if the robot is redudant
    if obj.dataList.isRedudant==true % if redudant
        for iFlipCounter=1:flipCounter
            if iFlipCounter==flipCounter 
                % if this filpCounter solutionFlag is empty, directly import the data
                % or if previous flipSolution are all skipped
                solutionFlagOriginal(iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,flipCounter)=solutionFlagIn;
                configSolColumnOriginal(:,iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,flipCounter)=configSolColumnIn;
                iFilpSolutionOut = flipCounter;
                flipCounter = flipCounter + 1;
                isNewSolution = true;
                break;
                
            elseif sum(obj.dataList.solutionFlag(iLocalPosition,iLocalRotation,:,:,iFlipCounter),"all") == 0 
                % if there are empty point
                solutionFlagOriginal(iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,iFlipCounter)=solutionFlagIn;
                configSolColumnOriginal(:,iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,iFlipCounter)=configSolColumnIn;
                iFilpSolutionOut = iFlipCounter;
                isNewSolution = true;
                break;

            else 
                % if this iFilpCounter's solutionFlag is not empty
                if solutionFlagOriginal(iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,iFlipCounter) == 0
                    % but if that point is empty, it means importing new points is not a good idea
                    warning('This sections for (%d,%d,:,:,%d) is not fully calculated.',iLocalPosition,iLocalRotation,iFlipCounter);
                    if options.ignoreWarning== false
                        disp('Continue?')
                        pause on
                        pause
                    end
                    solutionFlagOriginal(iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,iFlipCounter)=solutionFlagIn;
                    configSolColumnOriginal(:,iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,iFlipCounter)=configSolColumnIn;
                    iFilpSolutionOut = iFlipCounter;
                    isNewSolution = true;
                    break;
                
                elseif solutionFlagOriginal(iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,iFlipCounter) == 2
                    % if that point is marked as no solution, then we should assume that
                    % this point belongs to the another flipCounter
                    % Warning: could cause error if solutionFlagIn ~= 1
                    if options.ignoreWarning== false
                        warning('Skipping flipCounter %d because the solutionFlag is %d', ...
                            iFlipCounter, ...
                            solutionFlagOriginal(iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,iFlipCounter));
                    end
                    continue;
                
                else
                    % if that point is marked as a point with solution and now we have a new solution. 
                    % We have to campare if two solutions can reach each other with nullspace motion.
                    if jointSectionDistance(configSolColumnIn,squeeze(configSolColumnOriginal(:,iLocalPosition,iLocalRotation,iGlobalPosition,:,iFlipCounter))) < desg2rad(360/nSection) ...
                        || isNullspaceReachableMEX(isReachableHandle,sectionHandle,nSection, ...
                                                squeeze(configSolColumnOriginal(:,iLocalPosition,iLocalRotation,iGlobalPosition,:,iFlipCounter)), ...
                                                configSolColumnIn)
                        if options.ignoreWarning== false
                            disp(['The solution can be reach by existing point with nullspace motion. filpCounter=',num2str(iFlipCounter)]);
                        end
                        iFilpSolutionOut = iFlipCounter;
                        break;
                    else
                        continue;
                    end
                end
            end
        end

    else 
        % if non-redudant, then when 
        repeatMark = false;

        % search the data
        for iFlipCounter=1:(flipCounter-1)
            if norm(configSolColumnOriginal(:,iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,iFlipCounter)-configSolColumnIn)...
                < 1e-3
                repeatMark = true;
                iFilpSolutionOut = iFlipCounter;
            end
        end
                
        % If there are no repeat, merge the data to the original
        if repeatMark == true
            solutionFlagOriginal(iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,flipCounter)=solutionFlagIn;
            configSolColumnOriginal(:,iLocalPosition,iLocalRotation,iGlobalPosition,sectionNumber,flipCounter)=configSolColumnIn;
            iFilpSolutionOut = flipCounter;
            flipCounter = flipCounter + 1;
        end
    end

    % Output the flipCounter needed.
    sectionNumberOut = sectionNumber;

    % merge the data back to the point
    obj.dataList.solutionFlag=solutionFlagOriginal;
    obj.dataList.configSolColumn=configSolColumnOriginal;
    obj.dataList.flipCounter=flipCounter;
end
