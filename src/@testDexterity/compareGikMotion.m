function [shouldContinue,iGlobalPositionOut] = compareGikMotion(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution,options)
%COMPAREGIKMOTION compare the latest history gikMotion and the sliced
% Reachability to see the solution difference.
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double
        iLocalRotationIn(1,1) double
        iFlipSolution(1,1) double
        options.maxDifference(1,1) double = 40
        options.skipGraphDisplay(1,1) logical = false
        options.forceChoosing(1,1) logical = false
        options.logPath(1,:) char = './log/default/'
        options.suggestPointList(1,:) double = 0
    end

    % read the information needed
    reachabilitySlice = squeeze(obj.generateMapHandle.dataList.testReachability.dataList.solutionFlag(iLocalPositionIn,iLocalRotationIn,:));
    positionRow = obj.generateMapHandle.commonDataHandle.positionRow;
    gridMark=obj.generateMapHandle.robotMapsHandle.generateGridSlot.mark{1};
    gridMetaList=obj.generateMapHandle.robotMapsHandle.generateGridSlot.metaList{1};
    nSection = obj.dataList.nSection;
    solutionFlagFD = squeeze(obj.generateMapHandle.dataList.testFlippedDexterity.dataList.solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,:));
    solutionFlagFD(solutionFlagFD==0) = 2;
    configSolColumnFD = squeeze(obj.generateMapHandle.dataList.testFlippedDexterity.dataList.configSolColumn(:,iLocalPositionIn,iLocalRotationIn,:,:,:));
    flipCounter = obj.dataList.flipCounter;

    % get the parameters
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);

    % get the adjacentMap
    adjacentMapFace = obj.dataList.adjacentMap;
    adjacentMapFaceEdgeCorner = adjacentMapCalculation(gridMark,gridMetaList,positionRow,nGlobalPosition,'faceEdgeCorner');

                
    % look for the difference of the previous map the current Map
    solutionFlagNow = squeeze(obj.dataList.solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution));
    solutionFlagNow(solutionFlagNow == 0) = 2;
    configSolColumnNow = squeeze(obj.dataList.configSolColumn(:,iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution)); 
    solutionFlagSlice = zeros(nGlobalPosition,1);
    solutionFlagSlice(any(solutionFlagNow==2,2)) = 2;
    solutionFlagSlice(any(solutionFlagNow==3,2)) = 3;
    solutionFlagSlice(any(solutionFlagNow==4,2)) = 4;
    solutionFlagSlice(any(solutionFlagNow==1,2)) = 1;
    difference = (reachabilitySlice~=2) ~= (solutionFlagSlice~=2);

    % compare the difference
    if sum(difference,"all")<=options.maxDifference && ...
       options.forceChoosing == false
        % difference smaller than threshold
        shouldContinue = false;
        iGlobalPositionOut = 0;
        disp(['compareGikMotion(',num2str(iLocalPositionIn),',',num2str(iLocalRotationIn),',:,:,',num2str(iFlipSolution),') stopped. Difference ',num2str(sum(difference,"all")),' satisfied.']);
        return;

    else
        % assign some contants
        if isequal(gridMark,'cartesian2')
            maxAdjacent = 9;
            thresholdOut = 3;
        elseif isequal(gridMark,'cartesian3')
            maxAdjacent = 26;
            thresholdOut = 9;
        end

        % choose a point to continue
        adjacentDifferenceList = zeros(nGlobalPosition,1);
        adjacentDifferenceIndex = zeros(nGlobalPosition,maxAdjacent);
       
        for iGlobalPosition=1:nGlobalPosition
            if solutionFlagSlice(iGlobalPosition)==1 || solutionFlagSlice(iGlobalPosition)==3 || solutionFlagSlice(iGlobalPosition)==4
                adjacentIndex = adjacentMapFace(iGlobalPosition);
                for iMap = 1:length(adjacentIndex)
                    if solutionFlagSlice(adjacentIndex(iMap))==2 % check the edge of the solutionFlag = 1
                        intersectIndex = intersect(adjacentMapFaceEdgeCorner(iGlobalPosition),find(difference));

                        adjacentDifferenceList(iGlobalPosition) = length(intersectIndex);
                        adjacentDifferenceIndex(iGlobalPosition,1:length(intersectIndex)) = intersectIndex;
                    end
                end
            end
        end

        iGlobalPositionOutList = find(adjacentDifferenceList>=thresholdOut);
        iGlobalPositionOutMask = zeros(length(iGlobalPositionOutList),1,'logical');

        % detect if the neibouring point(section) has uncovered point that is close to the candidate points 
        for iGlobalList = 1:length(iGlobalPositionOutList)
            for iAdjacent = 1:adjacentDifferenceList(iGlobalPositionOutList(iGlobalList))
                for iFlip = 1:flipCounter-1
                    solutionFlagMask = squeeze(solutionFlagNow(iGlobalPositionOutList(iGlobalList),:)~=2);
                    solutionFlagFDMask = squeeze(solutionFlagFD(adjacentDifferenceIndex(iGlobalPositionOutList(iGlobalList),iAdjacent),:,iFlip)~=2);
            
                    if ~any(solutionFlagMask) || ~any(solutionFlagFDMask)
                        continue;
                    end
                    
                    jointDist = sectionSectionDistance(squeeze(configSolColumnNow(:,iGlobalPositionOutList(iGlobalList),solutionFlagMask)),...
                                           squeeze(configSolColumnFD(:,adjacentDifferenceIndex(iGlobalPositionOutList(iGlobalList),iAdjacent),solutionFlagFDMask,iFlip)));
                    if jointDist <= deg2rad(360/nSection)
                        iGlobalPositionOutMask(iGlobalList)=true;
                        break;
                    end
                end
            end
        end

        if isempty(iGlobalPositionOutList(iGlobalPositionOutMask)) 
            disp('No potential points found according to dex graph.')
            if options.forceChoosing == false
                shouldContinue = false;
                iGlobalPositionOut = 0;
                return
            end
        end

        % output to select an index
        shouldContinue = true;
        rng('shuffle');
        if any(options.suggestPointList~=0) && ~isempty(intersect(iGlobalPositionOutList(iGlobalPositionOutMask),options.suggestPointList))
            randomList = intersect(iGlobalPositionOutList(iGlobalPositionOutMask),options.suggestPointList); 
        elseif any(options.suggestPointList~=0) && options.forceChoosing==true
            randomList = options.suggestPointList;
        elseif isempty(iGlobalPositionOutList(iGlobalPositionOutMask)) || options.forceChoosing==true
            randomList = iGlobalPositionOutList;
        else
            randomList = iGlobalPositionOutList(iGlobalPositionOutMask);
        end

        % randomly choose one from the list
        iGlobalPositionOut = randomList(randi(length(randomList)));
        
        % display the information
        disp(['compareGikMotion(',num2str(iLocalPositionIn),',',num2str(iLocalRotationIn),',:,:,',num2str(iFlipSolution),') continues. Difference ',num2str(sum(difference,"all")),' not satisfied.']);
        
        % plot the result
        fig = figure;
        hold on;
        view(0,0);
        plotCompareGikResult(solutionFlagSlice,difference,positionRow,randomList,iGlobalPositionOut);
        if options.skipGraphDisplay==true
            figFileName = ['gikCompare(',num2str(iLocalPositionIn),',', ...
                num2str(iLocalRotationIn),',x,x,', ...
                num2str(iFlipSolution),')_', ...
                num2str(length(find(difference))),'_', ...
                num2str(iGlobalPositionOut),'.png'];
            saveas(fig,figFileName)
            movefile(figFileName,[options.logPath,figFileName]);
            hold off;
            close(fig);
        else
            disp('Please check the comparison graph.');
            disp('Continue?');
            pause on
            pause
            hold off;
            if isvalid(fig)
                close(fig);
            end
        end       
    end


end

