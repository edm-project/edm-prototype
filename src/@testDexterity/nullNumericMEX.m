function [solutionFlag,configSolColumn] = nullNumericMEX(obj,iLocalPositionIn,iLocalRotationIn,iFlipSolution, ...
                                                        solutionFlag,configSolColumn, ...
                                                        timeStep,increaseStep,maxStep,options)
%NULLNUMERIC numeric mode of the nullspace motion
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double {mustBeInteger}
        iLocalRotationIn(1,1) double {mustBeInteger}
        iFlipSolution(1,1) double {mustBeInteger}
        solutionFlag double
        configSolColumn double
        timeStep(1,1) double
        increaseStep(1,1) double
        maxStep(1,1) double
        options.ignoreWarning(1,1) logical
        options.forceGenerate(1,1) logical
    end

    % prepare variables for calculation and storage
    robotParameters = obj.generateMapHandle.commonDataHandle.robotParameters;
    nSection = obj.dataList.nSection;
    isRedudant = obj.dataList.isRedudant;
    adjacentMapFace = obj.dataList.adjacentMap;

    % generate the code
    nullspaceFixedHandle = MEXnullspaceFixedGenerate(robotParameters, ...
                                                    forceGenerate=options.forceGenerate, ...
                                                    ignoreWarning=options.ignoreWarning);

    solutionFlagCheckHandle = MEXsolutionFlagCheckGenerate(robotParameters, ...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);

    nullspaceFixedReverseHandle = MEXnullspaceFixedReverseGenerate(robotParameters,...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);

    sectionHandle = MEXdexteritySectionGenerate(robotParameters, ...
                  ignoreWarning=options.ignoreWarning, ...
                  forceGenerate=options.forceGenerate);

    % prepare parameters
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);

    % prepare progressbar
    %progressbar
    fig = progressbar(['Dexterity: numericMEX nullspaceMotion (',num2str(iLocalPositionIn),',', ...
                                                num2str(iLocalRotationIn),',:,:,', ...
                                                num2str(iFlipSolution),')'],nGlobalPosition);

    % the iteration of nullspace motion
    for iGlobalPosition = 1:nGlobalPosition
        solutionFlagSlice = solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,:,iFlipSolution);
        
        % for data point that is already calculated
        if all(solutionFlagSlice) == true

        % for a normal data point
        else
            % search for "the correct data point"
            if sum(solutionFlagSlice~=0,"all") == 1
                sectionIndex = solutionFlagSlice~=0;
            elseif sum(solutionFlagSlice==1,"all") >= 1
                sectionIndex = solutionFlagSlice==1;
            elseif sum(solutionFlagSlice==3,"all") >= 1
                sectionIndex = solutionFlagSlice==3;
            elseif sum(solutionFlagSlice==4,"all") >= 1
                sectionIndex = solutionFlagSlice==4;
            else
                error('Global position [%d] has no data point. This is not correct.',iGlobalPosition);
            end

            % if the data above has two or more data point, check if they
            % belongs to the same nullspace motion
            if sum(sectionIndex,"all") > 1
                sectionIndex = find(sectionIndex);
                chooseIndex = 1;
                for iSection = 1:length(sectionIndex)-1
                    if ~isNullspaceReachable(robotParameters, ...
                                            configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex(iSection),iFlipSolution), ...
                                            configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex(iSection+1),iFlipSolution))
                        warning('Global position [%d] has two data points [%d][%d] that cannot reach each other with program. Please check and decide.', ...
                            iGlobalPosition,sectionIndex(iSection),sectionIndex(iSection+1));
                        
                        [oneIndex,sectionNumber] = nearestOneIndex(solutionFlag(iLocalPositionIn,iLocalRotationIn,:,:,iFlipSolution),iGlobalPosition,adjacentMapFace);
                        normDifference = zeros(length(length(sectionIndex)),1);
                        for iNormDifference = 1:length(sectionIndex)
                            normDifference(iNormDifference) = norm( ...
                             configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex(iNormDifference),iFlipSolution)- ...
                             configSolColumn(:,iLocalPositionIn,iLocalRotationIn,oneIndex,sectionNumber,iFlipSolution));
                        end
                        
                        [~,chooseIndex] = min(normDifference);
                        disp(['Automatic choose section ',num2str(sectionIndex(chooseIndex))]);
                    end
                end
                sectionIndex = sectionIndex(chooseIndex);
            end

            % if the redudant robot have a solution here
            if isRedudant == true && solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex,iFlipSolution)~=2
                
                configSol=configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionIndex,iFlipSolution);
                [configSolCol,haveSolutionCol] = nullspaceRoundMEX(robotParameters,nullspaceFixedHandle,nullspaceFixedReverseHandle,sectionHandle, ...
                                                                configSol,nSection, ...
                                                                timeStep, ...
                                                                increaseStep, ...
                                                                maxStep);
                for iSection = 1:nSection
                    if haveSolutionCol(iSection) == 0
                        solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iSection,iFlipSolution)=2;
                    else
                        % check the collision and joint Limit
                        configSolSection = configSolCol(:,iSection);
                        [solutionFlagOut,configSolOut] = solutionFlagCheckMEX(solutionFlagCheckHandle,configSolSection);
                        solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iSection,iFlipSolution) = solutionFlagOut;
                        configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iSection,iFlipSolution) = configSolOut;
                    end
                end
            
            else % if it is a none redudant robot or the input data point = 2 (no solution)
                for iSection = 1:nSection
                    if solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iSection,iFlipSolution)==0
                        solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,iSection,iFlipSolution)=2;
                    end
                end
            end
        end

        % push the progressbar
        progressbar();
    end

    % close the progressbar
    close(fig); 
end

function [oneIndex,sectionNumber] = nearestOneIndex(solutionFlagParted,indexNow,adjacentMapFace)
    queue = indexNow;
    queueIndex = 1;

    while queueIndex>length(queue)
        % if the current solutionFlag is have one and only one = 1 point, then we use it as the index
        currentSolutionFlagSlice = squeeze(solutionFlagParted(1,1,queue(queueIndex),:,1));
        if any(currentSolutionFlagSlice==1) && length(find(currentSolutionFlagSlice==1))==1
            oneIndex = queue(queueIndex);
            sectionNumber = find(currentSolutionFlagSlice==1);
            break;
        end
        
        % if not, then we add it's adjacent point into it to see.
        adjacentArray = adjacentMapFace(queue(queueIndex));
        for iAdjacent=1:length(adjacentArray)
            if ~any(queue==adjacentArray(iAdjacent))
                queue = [queue,adjacentArray(iAdjacent)]; %#ok<AGROW> % because this is a queue, we cannot avoid reallocaltion
            end
        end

        queueIndex = queueIndex+1;
    end
end

