function [adjacentArray, previousArray] = adjacentListCalculation(obj,iLocalPosition,iLocalRotation,iGlobalPosition,solutionFlagReachability,forceCrossing)
%ADJACENTLISTCALCULATION Calculate the adjacentList
% in:
% [iLocalPosition]
% [iLocalRotation]
% [iGlobalPosition]: marks the starting
% out:
% [adjacentList]: sequence of the list
% [previousList]: the list of the previous node
    arguments
        obj(1,1) testDexterity
        iLocalPosition(1,1) double {mustBeInteger}
        iLocalRotation(1,1) double {mustBeInteger}
        iGlobalPosition(1,1) double {mustBeInteger}
        solutionFlagReachability double
        forceCrossing(1,1) logical 
    end

    % check if the import works
    if solutionFlagReachability(iLocalPosition,iLocalRotation,iGlobalPosition)== 2
        ME = MException('testDexterity:adjacentListCalculation', ...
            'the starting point is not reachable = 2. Change a start point');
        throw(ME)
    end

    % import the data
    adjacentMap = obj.dataList.adjacentMap;
    nGlobalPosition = size(obj.dataList.solutionFlag,3);
    solutionFlagArray = solutionFlagReachability(iLocalPosition,iLocalRotation,:);

    % preallocation of the List
    adjacentArray = zeros(nGlobalPosition,1);
    previousArray = zeros(nGlobalPosition,1);
    
    % helper array
    visitedList = zeros(nGlobalPosition,1);
    previousFixedList = zeros(nGlobalPosition,1);

    % help pointer and initialization
    readPointer = 1;
    writePointer = 2;
    adjacentArray(1) = iGlobalPosition;
    visitedList(iGlobalPosition) = 1;
    previousFixedList(iGlobalPosition) = 1;

    while readPointer <= length(adjacentArray) && readPointer ~= writePointer  % while all points are visited              
        currentReadPoint = adjacentArray(readPointer);
        currentAdjacentList = adjacentMap(currentReadPoint);             % read the adjacent point of the current point
        for iAdjacent = 1:length(currentAdjacentList)                    % for each adjacent point
            currentAdjacentPoint = currentAdjacentList(iAdjacent);
            if solutionFlagArray(currentReadPoint) ~= 2
                if visitedList(currentAdjacentPoint) == 0
                    adjacentArray(writePointer)=currentAdjacentPoint;
                    previousArray(writePointer)=currentReadPoint;
                    visitedList(currentAdjacentPoint)=1;
                    previousFixedList(currentAdjacentPoint)=1;
                    writePointer = writePointer+1;
                    
                elseif visitedList(currentAdjacentPoint) ~= 2 && ...
                       previousFixedList(currentAdjacentPoint) == 0 && ...
                       previousArray(readPointer) ~= currentAdjacentPoint
                    previousArray(adjacentArray==currentAdjacentPoint) = currentReadPoint;
                    previousFixedList(currentAdjacentPoint)=1;
                end
            else
                if forceCrossing == true && visitedList(currentAdjacentPoint) == 0
                    adjacentArray(writePointer)=currentAdjacentPoint;
                    previousArray(writePointer)=findPrevious(readPointer,adjacentArray,previousArray);
                    visitedList(currentAdjacentPoint)=1;
                    writePointer = writePointer+1;
                end
            end            
        end
        readPointer = readPointer + 1;
    end

    % filter the solutionFlag ~= 2 only:
    [~,ia,~]=intersect(adjacentArray,find(solutionFlagArray~=2),'stable');
    adjacentArray = adjacentArray(ia);
    previousArray = previousArray(ia);

    % expand all the possibilities of previousarray
    previousArraySeries = zeros(length(previousArray),2);
    previousArraySeries(:,1) = previousArray;
    for iPrevious = 2:length(previousArray)
        [C,~,~] = intersect(adjacentArray(1:(iPrevious-1)),adjacentMap(adjacentArray(iPrevious)),'stable');
        C = setdiff(C,[previousArray(iPrevious)]);
        previousArraySeries(iPrevious,2:1+length(C)) = C;
    end
    previousArray = previousArraySeries;

    function previousPoint = findPrevious(readPointer,adjacentArray,previousArray)
        if solutionFlagArray(adjacentArray(readPointer)) ~= 2
            previousPoint = adjacentArray(readPointer);
        else
            previousReadPointer = find(adjacentArray==previousArray(readPointer));
            previousPoint = findPrevious(previousReadPointer,adjacentArray,previousArray);
        end
    end
end

