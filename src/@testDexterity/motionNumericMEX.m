function [requireRetry,solutionFlagSlice,configSolColumnSlice,adjacentArray,previousArray] = motionNumericMEX(obj,iLocalPosition,iLocalRotation,iSectionNumber,iFlipSolution, ...
                                                                adjacentArray,previousArray, ...
                                                                maxIkIterationIn,maxNoSolutionRepeatIn,maxConstraintRepeatIn,startAngleTolerance,maxAngleTolerance,maxGiveupDistance, ...
                                                                options)
%GIKNUMERIC GIKMotion numeric method
    arguments
        obj(1,1) testDexterity
        iLocalPosition(1,1) double {mustBeInteger}
        iLocalRotation(1,1) double {mustBeInteger}
        iSectionNumber(1,1) double {mustBeInteger}
        iFlipSolution(1,1) double {mustBeInteger}
        adjacentArray(1,:) double {mustBeInteger}
        previousArray(:,:) double {mustBeInteger}
        maxIkIterationIn(1,1) double
        maxNoSolutionRepeatIn(1,1) double
        maxConstraintRepeatIn(1,1) double
        startAngleTolerance(1,1) double
        maxAngleTolerance(1,1) double
        maxGiveupDistance(1,1) double
        options.ignoreWarning(1,1) logical
        options.forceGenerate(1,1) logical
        options.empirical(1,1) logical
        options.distanceChecking(1,1) logical
    end

    % extract the solutionFlag
    tformSlice =  squeeze(obj.generateMapHandle.dataList.allTform(:,:,iLocalPosition,iLocalRotation,:));
    solutionFlagSlice = squeeze(obj.dataList.solutionFlag(iLocalPosition,iLocalRotation,:,iSectionNumber,iFlipSolution));
    configSolColumnSlice = squeeze(obj.dataList.configSolColumn(:,iLocalPosition,iLocalRotation,:,iSectionNumber,iFlipSolution));
    solutionFlagReachabilitySlice = squeeze(obj.generateMapHandle.dataList.testReachability.dataList.solutionFlag(iLocalPosition,iLocalRotation,:));
    positionRow = obj.generateMapHandle.commonDataHandle.positionRow;
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5); 
    solutionFlag = obj.dataList.solutionFlag;
    configSolColumn = obj.dataList.configSolColumn;
    flipCounter = obj.dataList.flipCounter-1;
    nSection = obj.dataList.nSection;

    % check if the data matches what we need
    if solutionFlagSlice(adjacentArray(1))==2
        ME = MException('testDexterity:gikNumeric:sizeNotMatch', ...
                        'The starting index %d has solutionFlag 2', ...
                        adjacentArray(1));
        throw(ME)
    end
    
    % import the data that is not reachable
    if length(solutionFlagSlice)~=length(solutionFlagReachabilitySlice)
        ME = MException('testDexterity:gikNumeric:sizeNotMatch', ...
                        'Size of solutionflag not matched. Internal error');
        throw(ME)
    else
        solutionFlagSlice(solutionFlagReachabilitySlice==2)=solutionFlagReachabilitySlice(solutionFlagReachabilitySlice==2);
    end

    % import the solution previously calculated
    solutionFlagSection = squeeze(obj.dataList.solutionFlag(iLocalPosition,iLocalRotation,:,:,iFlipSolution));
    configSolColumnSection = squeeze(obj.dataList.configSolColumn(:,iLocalPosition,iLocalRotation,:,:,iFlipSolution));
    for iGlobalPosition = 1:nGlobalPosition
        if solutionFlagSlice(iGlobalPosition)~=1 && any(solutionFlagSection(iGlobalPosition,:)==1)
            solutionFlagSlice(iGlobalPosition)=1;
            if find(solutionFlagSection(iGlobalPosition,:)==1)==1
                configSolColumnSlice(:,iGlobalPosition)=configSolColumnSection(:,iGlobalPosition,solutionFlagSection(iGlobalPosition,:)==1);
            elseif solutionFlagSection(iGlobalPosition,iSectionNumber)==1
                configSolColumnSlice(:,iGlobalPosition)=configSolColumnSection(:,iGlobalPosition,iSectionNumber);
            else 
                sectionTemp = obj.decideSection(iLocalPosition,iLocalRotation,iGlobalPosition,iFlipSolution);
                configSolColumnSlice(:,iGlobalPosition)=configSolColumnSection(:,iGlobalPosition,sectionTemp);
            end
        end
    end

    % inverse kineamatics
    robotParameters = obj.generateMapHandle.dataList.robotParameters;
    robotColumn = robotParameters.robotColumn;
    endEffectorName = robotParameters.endEffectorName;

    % generate the gik MEX code
    gikHandle = MEXgikGenerate(robotParameters,maxIkIterationIn, ...
                   ignoreWarning=options.ignoreWarning, ...
                   forceGenerate=options.forceGenerate);
    solutionFlagCheckHandle = MEXsolutionFlagCheckGenerate(robotParameters, ...
            ignoreWarning=options.ignoreWarning, ...
            forceGenerate=options.forceGenerate);
    sectionHandle = MEXdexteritySectionGenerate(robotParameters, ...
        ignoreWarning=options.ignoreWarning, ...
        forceGenerate=options.forceGenerate);
    isReachableHandle = MEXisNullspaceReachableGenerate(robotParameters, ...
        ignoreWarning=options.ignoreWarning, ...
        forceGenerate=options.forceGenerate);

    % extract the parameters
    nGlobalPositionFiltered = length(adjacentArray);

    %progressbar
    fig = progressbar(['Dexterity: numericMEX gikMotion (',num2str(iLocalPosition),',', ...
                                                num2str(iLocalRotation),',:,', ...
                                                num2str(iSectionNumber),',', ...
                                                num2str(iFlipSolution),')'],nGlobalPositionFiltered);
    progressbar();

    % imperical counter
    empiricalCounter = 0;
    requireRetry = false;
    zeroCounter = 0;
    for iGlobalPosition =2:13
        if solutionFlagSlice(adjacentArray(iGlobalPosition))==0||solutionFlagSlice(adjacentArray(iGlobalPosition))==2
            zeroCounter = zeroCounter+1;
        end
    end

    % calculate the output
    for iGlobalPosition=2:nGlobalPositionFiltered
        angleTolerance = startAngleTolerance;
        solvedFlag = solutionFlagSlice(adjacentArray(iGlobalPosition)) == 1;

        if iGlobalPosition == 14 && options.empirical==true  
            if empiricalCounter == zeroCounter
                disp(['Empirical decision: this gikMotion is probably useless C:[',num2str(empiricalCounter),']. Require Retry.']);
                requireRetry = true;
                delete(fig);
                return
            end
        end
        
        while solvedFlag == false
            % if the angleTolerance is bigger than the max, see the reason,
            % skip and replace it from the previous list.
            if angleTolerance >= maxAngleTolerance
                    disp(['[',num2str(adjacentArray(iGlobalPosition)),'] constraint tolerance exceeds [',num2str(rad2deg(maxAngleTolerance),3),'] reason [',num2str(solutionFlagSlice(adjacentArray(iGlobalPosition))),'].']);
                    % change the parent when no solution
                    if solutionFlagSlice(adjacentArray(iGlobalPosition)) == 2
                        previousArray(previousArray==adjacentArray(iGlobalPosition)) = previousArray(iGlobalPosition,1);
                        % delete the repeated column
                        for beforeCol=1:size(previousArray,2)-1
                            for afterCol=beforeCol+1:size(previousArray,2)
                                repeatMask = previousArray(:,beforeCol)==previousArray(:,afterCol);
                                previousArray(repeatMask,afterCol:end) = [previousArray(repeatMask,afterCol+1:end),zeros(sum(repeatMask,"all"),1)];
                            end
                        end
                        % empirical limit: if first 4 have more than one 1 exceeds [2]
                        % and first 12 have more than 3 exceeds [3], this
                        % gikMotion is then probably useless. Require for
                        % restart.
                        if iGlobalPosition <= 13 && options.empirical==true
                            empiricalCounter = empiricalCounter + 1;
                        end
                    end
                    break;

            % within the angleTolerance: check each element in previousArray
            else
                for iParentNode=1:size(previousArray,2)
                    if previousArray(iGlobalPosition,iParentNode)==0 || solvedFlag == true
                        break;
                    end

                    % skip if the distance bigger than expected
                    if norm(positionRow(previousArray(iGlobalPosition,iParentNode),:) - ...
                        positionRow(adjacentArray(iGlobalPosition),:)) > maxGiveupDistance
                        solutionFlagSlice(adjacentArray(iGlobalPosition)) = 2;
                        continue;
                    end

                    % make guess of the previous solution
                    iConstraintRepeat = 1;
                    iNoSolutionRepeat = 1;
                    previousSolution = configSolColumnSlice(:,previousArray(iGlobalPosition,iParentNode));
                    jointConst = jointBoundIntersect(robotParameters,previousSolution,angleTolerance);
                    initialguess = previousSolution;

                    % check if the process goes wrong
                    if ~any(previousSolution)
                        warning('initialguess is all zero. Probably mistake happens a:[%d] p:[%d]', ...
                        adjacentArray(iGlobalPosition),previousArray(iGlobalPosition));
                        continue;
                    end

                    % IK solution
                    while iNoSolutionRepeat <= maxNoSolutionRepeatIn && iConstraintRepeat <= maxConstraintRepeatIn
                        [configSol,solInfo] = gikMEX(gikHandle,initialguess,endEffectorName,tformSlice(:,:,adjacentArray(iGlobalPosition)),jointConst.Bounds);

                        % if IK solver can't find Solution.
                        if isequal(solInfo.Status,'best available')
                            solutionFlagSlice(adjacentArray(iGlobalPosition)) = 2;
                            iNoSolutionRepeat = iNoSolutionRepeat + 1;

                        % if IK solver find Solution.
                        elseif isequal(solInfo.Status,'success')
                            % check the collision and joint Limit
                            [solutionFlagOut,configSolOut] = solutionFlagCheckMEX(solutionFlagCheckHandle,configSol);
                            
                            % check if the other flipped solution already have this solution
                            % find the current exist data length. if the iFlip is not all zero, and if it is
                            % not the current flip, then decide the distance. If very near, then skip this point 
                            % WARNING: not implemented for non-redudant robot
                            iFlipBreak = false;
                            for iFlip=1:flipCounter
                                if iFlip==iFlipSolution
                                    continue;
                                end
                                solutionFlagMask = (solutionFlag(iLocalPosition,iLocalRotation,adjacentArray(iGlobalPosition),:,iFlip)==1|...
                                                    solutionFlag(iLocalPosition,iLocalRotation,adjacentArray(iGlobalPosition),:,iFlip)==3|...
                                                    solutionFlag(iLocalPosition,iLocalRotation,adjacentArray(iGlobalPosition),:,iFlip)==4);
                                if ~any(solutionFlagMask)
                                    continue;
                                end
                                jointDist = jointSectionDistance(configSolOut,squeeze(configSolColumn(:,iLocalPosition,iLocalRotation,adjacentArray(iGlobalPosition),:,iFlip)));
                                if jointDist<=deg2rad(360/nSection)
%                                     if isNullspaceReachableMEX(isReachableHandle,sectionHandle,nSection, ...
%                                             squeeze(configSolColumn(:,iLocalPosition,iLocalRotation,adjacentArray(iGlobalPosition),:,iFlip)),configSolOut)
                                        disp(['[',num2str(adjacentArray(iGlobalPosition)),'] find nullspace solution with flipSolution in [',num2str(iFlip),'].']);
                                        iFlipBreak = true;
%                                     end
                                end
                            end

                            if iFlipBreak == false
                                jointDist = jointJointDistance(configSolOut,previousSolution);
                                if jointDist>=maxAngleTolerance
                                    disp(['[',num2str(adjacentArray(iGlobalPosition)),'] find unusual long joint distance']);
                                    iFlipBreak = true;
                                end
                            end
                            
                            % check if there are previous section with this solution
                            if iFlipBreak == true
                                if iNoSolutionRepeat < maxNoSolutionRepeatIn
                                    solutionFlagSlice(adjacentArray(iGlobalPosition)) = 2;
                                    iNoSolutionRepeat = iNoSolutionRepeat + 1;
                                    continue;
                                else
                                    solutionFlagSlice(adjacentArray(iGlobalPosition)) = 2;
                                    angleTolerance = maxAngleTolerance + 1;
                                    break;
                                end
                            end
                            
                            % sort the distance
                            if solutionFlagOut == 1 || solutionFlagOut == solutionFlagReachabilitySlice(adjacentArray(iGlobalPosition))
                                solutionFlagSlice(adjacentArray(iGlobalPosition)) = solutionFlagOut;
                                configSolColumnSlice(:,adjacentArray(iGlobalPosition)) = configSolOut;
                                solvedFlag = true;
                                break;
                            else
                                solutionFlagSlice(adjacentArray(iGlobalPosition)) = solutionFlagOut;
                                if ~any(configSolColumnSlice(:,adjacentArray(iGlobalPosition)))
                                    configSolColumnSlice(:,adjacentArray(iGlobalPosition)) = configSolOut;
                                end
                                iConstraintRepeat = iConstraintRepeat + 1;
                            end
                        else
                            ME = MException('testDexterity:gikNumericMEX:elseError', ...
                                'if-else should not run into this branch');
                            throw(ME);
                        end

                        % if goes to here (some error happens), set joint bounds
                        initialguess = randomConfiguration(robotColumn);
                    end
                end

                % if a iteration for all previous nodes in one counter
                % failed, then lift up the angle tolerance
                angleTolerance = angleTolerance*2;
            end
        end

        % update the progressbar
        progressbar(); 
    end
    
    % close the progressbar
    close(fig);             
end

