function requireRechoose = gikMotion(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iSectionNumber,iFlipSolution,options)
%GIKMOTION generate the generalized ik solution in the cartesian space
%   all definitions are similar to the previous functions.
%   the point assigned here is the starting point of the generalized inverse Kinematics movement
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double {mustBeInteger}
        iLocalRotationIn(1,1) double {mustBeInteger}
        iGlobalPositionIn(1,1) double {mustBeInteger}
        iSectionNumber(1,1) double {mustBeInteger}
        iFlipSolution(1,1) double {mustBeInteger}
        options.maxIkIterationIn(1,1) double = 2000
        options.maxNoSolutionRepeatIn(1,1) double = 2
        options.maxConstraintRepeatIn(1,1) double = 3
        options.startAngleTolerance(1,1) double = deg2rad(10);
        options.maxAngleTolerance(1,1) double = deg2rad(45)
        options.maxGiveupDistanceCrossing(1,1) double = 0.4
        options.maxGiveupDistanceNormal(1,1) double = 0.2
        options.empiricalRestart(1,1) double = 1
        options.ignoreWarning(1,1) logical = false
        options.forceGenerate(1,1) logical = false
        options.forceCrossing(1,1) logical = false
        options.skipGraphDisplay(1,1) logical = false
        options.empirical(1,1) logical = false
        options.logPath(1,:) char = './log/default/'
    end

    % check if allow callable for efficiency
    obj.mustAllowCall('gikMotion');

    % start a time counter
    startTime= datetime('now');

    % prepare variables for calculation and storage
    mode = obj.dataList.mode;
    solutionFlagReachability = obj.generateMapHandle.dataList.testReachability.dataList.solutionFlag;
    positionRow = obj.generateMapHandle.commonDataHandle.positionRow;
    robotParameters = obj.generateMapHandle.commonDataHandle.robotParameters;

    % prepare parameters
    nLocalPosition = size(obj.generateMapHandle.dataList.allTform,3);
    nLocalRotation = size(obj.generateMapHandle.dataList.allTform,4);
    nGlobalPosition = size(obj.generateMapHandle.dataList.allTform,5);
    nSectionNumber = obj.dataList.nSection;
    nFlipSolution = size(obj.dataList.solutionFlag,5);
    robotConfigSize = length(homeConfiguration(robotParameters.robotColumn)); 

    % prepare empty output
    solutionFlag = zeros(nLocalPosition,nLocalRotation,nGlobalPosition,nSectionNumber,nFlipSolution);
    configSolColumn = zeros(robotConfigSize,nLocalPosition,nLocalRotation,nGlobalPosition,nSectionNumber,nFlipSolution);

    % calculate the adjacent linking List
    % [forceCrossing]: true when the solution of the reachability is not fully connected 
    % (e.g. in 2 seperate parts)
    [adjacentArray, previousArray] = obj.adjacentListCalculation(iLocalPositionIn, ...
                                                                 iLocalRotationIn, ...
                                                                 iGlobalPositionIn, ...
                                                                 solutionFlagReachability, ...
                                                                 options.forceCrossing);
    
    
%     fig = figure;
%     hold on;
%     view(1.15,1.5)
%     plotAdjacentAndPrevious(adjacentArray,previousArray,positionRow,solutionFlagReachability(iLocalPositionIn,iLocalRotationIn,:),false)
%     if options.skipGraphDisplay==true
%         figFileName = ['gikMotion(',num2str(iLocalPositionIn),',', ...
%             num2str(iLocalRotationIn),',', ...
%             num2str(iGlobalPositionIn),',', ...
%             num2str(iSectionNumber),',', ...
%             num2str(iFlipSolution),').png'];
%         saveas(fig,figFileName)
%         movefile(figFileName,[options.logPath,figFileName]);
%         close(fig);
%     else
%         disp('Please check the generated adjacent and previous Array graph. Only continue when no error spotted');
%         disp('Continue?');
%         pause on
%         pause
%         hold off;
%         if isvalid(fig)
%             close(fig);
%         end
%     end

    % prepare some extra parameters
    if options.forceCrossing==true
        maxGiveupDistance=options.maxGiveupDistanceCrossing;
    else
        maxGiveupDistance=options.maxGiveupDistanceNormal;
    end

    % generalized ik calculation with different modes
    for iRetry = 1:options.empiricalRestart
        if isequal(mode,'numeric')
            [requireRetry,solutionFlagSlice,configSolColumnSlice,adjacentArrayTemp,previousArrayTemp] = obj.motionNumeric(iLocalPositionIn,iLocalRotationIn,iSectionNumber,iFlipSolution, ...
                                                                      adjacentArray,previousArray, ...
                                                                      options.maxIkIterationIn, ...
                                                                      options.maxNoSolutionRepeatIn, ...
                                                                      options.maxConstraintRepeatIn, ...
                                                                      options.startAngleTolerance, ...
                                                                      options.maxAngleTolerance, ...
                                                                      maxGiveupDistance, ...
                                                                      empirical=options.empirical);
        
        elseif isequal(mode,'numericMEX') || isequal(mode,'numericMEXParallel')
            [requireRetry,solutionFlagSlice,configSolColumnSlice,adjacentArrayTemp,previousArrayTemp] = obj.motionNumericMEX(iLocalPositionIn,iLocalRotationIn,iSectionNumber,iFlipSolution, ...
                                                                      adjacentArray,previousArray, ...
                                                                      options.maxIkIterationIn, ...
                                                                      options.maxNoSolutionRepeatIn, ...
                                                                      options.maxConstraintRepeatIn, ...
                                                                      options.startAngleTolerance, ...
                                                                      options.maxAngleTolerance, ...
                                                                      maxGiveupDistance, ...
                                                                      ignoreWarning=options.ignoreWarning, ...
                                                                      forceGenerate=options.forceGenerate, ...
                                                                      empirical=options.empirical);
        else
            ME = MException('testDexterity:gikMotion:elseError', ...
                            'mode if-else should not run into this branch');
            throw(ME);
        end
    
        if requireRetry == false
            break;
        end
    end
    
    % return the function when all retry failed
    if requireRetry == true
        requireRechoose = true;
        return;
    else
        requireRechoose = false;
    end
    
    % update the temp variable
    adjacentArray = adjacentArrayTemp;
    previousArray = previousArrayTemp;

    % expand and calculate the reachability in different section and same iFlipSolution
    % (1 indices to 5 indices)
    for iGlobalPosition=1:nGlobalPosition
        [~,sectionNumber] = dexteritySection(robotParameters,configSolColumnSlice(:,iGlobalPosition),nSectionNumber);
        solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionNumber,iFlipSolution) = ...
                     solutionFlagSlice(iGlobalPosition);
        configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPosition,sectionNumber,iFlipSolution) = ...
                     configSolColumnSlice(:,iGlobalPosition);
    end

    % prepare the data
    markIn='gikMotion';
    dataListIn.solutionFlag = solutionFlag;
    dataListIn.configSolColumn = configSolColumn;
    metaListIn.iLocalPosition = iLocalPositionIn;
    metaListIn.iLocalRotation = iLocalRotationIn;
    metaListIn.iGlobalPosition = iGlobalPositionIn;
    metaListIn.iSectionNumber = iSectionNumber;
    metaListIn.iFlipSolution = iFlipSolution;
    metaListIn.solutionFlagSlice = solutionFlagSlice;
    metaListIn.configSolColumnSlice = configSolColumnSlice;
    metaListIn.adjacentArray = adjacentArray;
    metaListIn.previousArray = previousArray;
    metaListIn.maxGiveupDistance = maxGiveupDistance;
    metaListIn.maxIkIterationIn = options.maxIkIterationIn;
    metaListIn.maxNoSolutionRepeatIn = options.maxNoSolutionRepeatIn;
    metaListIn.maxConstraintRepeatIn = options.maxConstraintRepeatIn;
    metaListIn.maxAngleTolerance = options.maxAngleTolerance;
    metaListIn.startAngleTolerance = options.startAngleTolerance;
    metaListIn.startTime=startTime;
    metaListIn.endTime=datetime('now');
    if isequal(mode,'numericMEX')
        metaListIn.numericMEXStr = fileread('MEXgik.m');
    end

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

