function iSectionNumberList = decideSection(obj,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,iFlipSolutionIn,options)
    arguments
        obj(1,1) testDexterity
        iLocalPositionIn(1,1) double
        iLocalRotationIn(1,1) double
        iGlobalPositionIn(1,1) double
        iFlipSolutionIn(1,1) double
        options.outputLength(1,1) double = 1
        options.distanceCoefficient(1,1) double = 0.1
    end
    
    % import the data needed
    solutionFlagFD = squeeze(obj.generateMapHandle.dataList.testFlippedDexterity.dataList.solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,:));
    configSolColumnFD = squeeze(obj.generateMapHandle.dataList.testFlippedDexterity.dataList.configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,:));
    solutionFlag = squeeze(obj.dataList.solutionFlag(iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,:));
    configSolColumn = squeeze(obj.dataList.configSolColumn(:,iLocalPositionIn,iLocalRotationIn,iGlobalPositionIn,:,:));
    nSection = obj.dataList.nSection;
    robotColumn = obj.generateMapHandle.commonDataHandle.robotParameters.robotColumn;

    % check the nullspace motion result
    if any(solutionFlag(:,iFlipSolutionIn)==0)
        error('Please do the nullspace motion for this point and then use this method')
    end

    % do the projection and calculate the losest distance
    existDataLength =  find(any(solutionFlagFD,1), 1, 'last' );
    distance = zeros(existDataLength,1);

    for iFlip = 1:existDataLength
        solutionFlagMask = (solutionFlag(:,iFlipSolutionIn) ~= 2);
        solutionFlagFDMask  = (solutionFlagFD(:,iFlip) ~= 2);
        distance(iFlip) = sectionSectionDistance(squeeze(configSolColumn(:,solutionFlagMask,iFlipSolutionIn)), ...
                                        squeeze(configSolColumnFD(:,solutionFlagFDMask,iFlip)));
    end

    if length(find(distance<=deg2rad(360/nSection))) == 1
        nearPoint = find(distance<=deg2rad(360/nSection));
    else
        nearPoint = 0;
    end


    % calculate the joint Bound Distance
    jointBoundDist = zeros(nSection,1);
    for iSection = 1:nSection
        if solutionFlag(iSection,iFlipSolutionIn)~=2
            jointBoundDist(iSection) = jointBoundDistance(robotColumn,configSolColumn(:,iSection,iFlipSolutionIn));
        else
            jointBoundDist(iSection) = -Inf;
        end
    end

    jointBoundDist(jointBoundDist < 0) = -Inf;

    % calculate the minimum angle Distance
    adjacentDist = Inf(nSection,1);
    if nearPoint ~= 0 
        for iSection = 1:nSection
             for iFlip = 1:existDataLength
                 if iFlip==nearPoint
                    continue;
                 end
                 solutionFlagMask = (solutionFlag(:,iFlipSolutionIn) ~= 2);
                 solutionFlagFDMask  = (solutionFlagFD(:,iFlip) ~= 2);
                 
                 if ~any(solutionFlagMask) 
                    continue;
                 end
                 distance = jointSectionDistance(squeeze(configSolColumn(:,iSection,iFlipSolutionIn)), ...
                     squeeze(configSolColumnFD(:,solutionFlagFDMask,iFlip)));
                 adjacentDist(iSection)=min(adjacentDist(iSection),distance);
             end
        end
    end

    adjacentDist(adjacentDist==Inf) = 0;

    % get the maximum data
    generalDistance = jointBoundDist + options.distanceCoefficient * adjacentDist;

    % get the Global maximum and local maximum
    [~,globalMax] = sort(generalDistance,"descend");
    localMax = find(islocalmax(generalDistance));
    [~,localMaxIndex] = sort(generalDistance(localMax),"descend");
    localMax = localMax(localMaxIndex);

    % combine the output
    outputList = unique([localMax;globalMax],"stable");
    firstInfIndex = find(generalDistance(outputList)==-inf,1,"first");
    if ~isempty(firstInfIndex)
        outputList = outputList(1:firstInfIndex-1);
    end

    % output the suggested section List
    iSectionNumberList = outputList(1:min(length(outputList),options.outputLength));
end

