function figureHandle = progressbar(titleIn,totalNumIn)
%PROGRESSBAR % This argument block does not support the variable input length.
%     arguments
%         titleIn(1,:) char
%         totalNumIn(1,1) double
%     end
    persistent counter progresshandle totalNum;
    
    switch nargin
        case  2
            % initialization
            counter = 0;
            totalNum = totalNumIn;
            figureHandle = uifigure('Position',[524,342,560,202]);
            figureHandle.CloseRequestFcn = @(src,event)my_closereq(src);
            progresshandle = uiprogressdlg(figureHandle,'Title',titleIn,'Message','Initialzing');
            progresshandle.Indeterminate = 1;
            drawnow
        case  {0,1}
            % increment
            % should assert something because accepting 1 is a bad decision?
            progresshandle.Indeterminate = 0;
            counter = counter + 1 ;
            if counter<=totalNum
                progresshandle.Message=['Calculating',' ',num2str(counter),'/',num2str(totalNum)];
                progresshandle.Value=counter/totalNum;
            else
                warning('counter exceeds totalNum. Should not happen.')
            end
            drawnow
        otherwise
            ME = MException('progressbar:elseError', ...
                            'Progessbar should not run into this branch with param num [%d]',nargin);
            throw(ME);
    end

    function my_closereq(fig) 
        if exist('counter','var') == 1 && exist('totalNum','var') == 1 && counter<totalNum
            selection = uiconfirm(fig,'Calculation not finished. Close the figure window?',...
                'Confirmation');
        
            switch selection
                case 'OK'
                    delete(fig)
                case 'Cancel'
                    return
            end
        else
            delete(fig)
        end
    end
end

