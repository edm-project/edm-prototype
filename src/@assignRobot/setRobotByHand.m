function setRobotByHand(obj,robotNameIn,endEffectorNameIn,tagIn,robotColumnIn)
%SETROBOTBYHAND set the robot by directly giving rigidBodyTree
% robotNameIn: robotName input, constrained by mustBeValidRobot
% endEffectorNameIn: name of the endEffector (for Inverse Kinematics)
    arguments
        obj(1,1) assignRobot
        robotNameIn(1,:) char {mustBeValidRobot}
        endEffectorNameIn(1,:) char
        tagIn(1,:) char
        robotColumnIn(1,1) rigidBodyTree
    end
    
    % check if allow callable for efficiency
    obj.mustAllowCall('setRobotByHand');

    %preprate for updateData
    markIn='setRobotByHand';
    dataListIn.tag = tagIn;
    dataListIn.endEffectorName = endEffectorNameIn;
    dataListIn.robotColumn = robotColumnIn;
    metaListIn= struct();

    % special: update the robotname to the object
    obj.robotName=robotNameIn;
    % use standard update method
    obj.updateData(markIn,metaListIn,dataListIn);
end

