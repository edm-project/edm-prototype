classdef assignRobot < handle
    %ASSIGNROBOT Process assignRobot
    %  record robots used in research

    properties (SetAccess=immutable)
    end 
    % end of immutable attributes

    properties (SetAccess=private)
        isActive(1,1) logical
        commonDataHandle(1,1) commonData
        robotMapsHandle(1,1)

        mark(1,:) cell {mustBeValidMarksNullPrepare}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
        robotName(1,:) char {mustBeValidRobotNull} = 'null'
    end 
    % end of private attributes
    
    methods
        function obj = assignRobot()
            obj.isActive = false;
            obj.mark = {'null'};
            obj.robotName = 'null';
        end
    end 
    % end of the constructor

    methods (Access=private)
        function mustAllowCall(obj,callMarkIn)
        % mustAllowCall: throw error if function call is not allowed
            arguments
                obj(1,1) assignRobot
                callMarkIn(1,:) char {mustBeValidMarks}
            end
            
            % check if status Active
            mustIsActiveState(obj,true);

            % forbid Mark repeat
            mustNoRepeat(obj,callMarkIn);

            % allow only one of {'setRobotName','setRobotByHand'} 
            mustMutuallyExclude(obj,callMarkIn,{'setRobotName','setRobotByHand'});

            % check if {'setDexterityReference'} is after {'setRobotName','setRobotByHand'}
            mustInSequence(obj,callMarkIn,{'setDexterityReference'},{'setRobotName','setRobotByHand'});
        end

        function updateData(obj,markIn,metaListIn,dataListIn)
        % update the data in this procees, called in functional methods
            arguments
                obj(1,1) assignRobot
                markIn(1,:) char {mustBeValidMarks}
                metaListIn(1,1) struct
                dataListIn(1,1) struct
            end
            
            % check if this call is allowed
            obj.mustAllowCall(markIn);

            % update mark
            if isequal(obj.mark,{'prepared'})
                obj.mark={markIn};
            else
                obj.mark=[obj.mark,{markIn}];
            end

            %update metaList and dataList
            obj.metaList{length(obj.metaList)+1}=metaListIn;
            obj.dataList=combineStruct(obj.dataList,dataListIn);
        end
    end 
    % end of the private methods

    methods (Access={?robotMaps})
        function mustAllowInitialize(obj)
        % mustAllowInitialze: check conditions for Initialize
        
            % isActive must equal false, otherwise means already initialized
            mustIsActiveState(obj,false)

            % marks must be 'null', otherwise means already finalized
            mustBeMark(obj,'null',true);
        end

        function mustAllowFinalize(obj)
        % mustAllowFinalize: check conditions for Finalize
            
            % isActive must be true, otherwise already finalized or not initialized
            mustIsActiveState(obj,true)

            % marks must be not 'prepared', which means nothing was done
            mustBeMark(obj,'prepared',false);
        end

        function initialize(obj,commonDataHandleIn,robotMapsHandleIn)
        % initalize this process
            arguments
                obj(1,1) assignRobot
                commonDataHandleIn(1,1) commonData
                robotMapsHandleIn(1,1) robotMaps
            end

            % check if initialization valid
            obj.mustAllowInitialize();

            % set isActive and Mark for functional calls
            obj.isActive=true;
            obj.mark={'prepared'};

            % store the handle of commonData
            obj.commonDataHandle=commonDataHandleIn;
            obj.robotMapsHandle=robotMapsHandleIn;
        end

        function finalize(obj)
        % finialize this process

            % check if finalization valid
            obj.mustAllowFinalize();

            % set isActive state, lock write functions
            obj.isActive=false;

            % write data into commonData
            obj.dataList.robotName=obj.robotName;
            obj.dataList.mark=obj.mark;
            obj.commonDataHandle.updateDataAssignRobot(obj.dataList);
        end
    end 
    % end of the robotMaps friend methods

    methods (Access=public)
        setRobotName(obj,robotNameIn,endEffectorNameIn);  % Functional method: set the by default name
        setRobotByHand(obj,robotNameIn,endEffectorNameIn,tagIn,robotColumnIn) % Functional method: import the rigidBodyTree data by hand
        setDexterityReference(obj,baseNameIn,shoulderNameIn,elbowNameIn)
    end 
    % end of the public functional method (definition in same folder)
end

function mustBeValidMarks(marksIn)
    % Check for Valid input as mark.
    mustBeMember(marksIn,{'setRobotName','setRobotByHand','setDexterityReference'})
end
 
function mustBeValidMarksNullPrepare(marksIn)
    % Check for Valid input as mark. Allow null and prepare.
    mustBeMember(marksIn,{'null','prepared','setRobotName','setRobotByHand','setDexterityReference'})
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{'robotName','mark','tag','robotColumn', ...
                                    'endEffectorName','baseName','shoulderName','elbowName'});
end
