function setDexterityReference(obj,baseNameIn,shoulderNameIn,elbowNameIn)
%SETROBOTNAME set the extra dexterity reference linkName 
% robotNameIn: robotName input, constrained by mustBeValidRobot
% endEffectorNameIn: name of the endEffector (for Inverse Kinematics)
    arguments
        obj(1,1) assignRobot
        baseNameIn(1,:) char
        shoulderNameIn(1,:) char
        elbowNameIn(1,:) char
    end
    
    % check if allow callable for efficiency
    obj.mustAllowCall('setDexterityReference');

    %preprate for updateData
    markIn='setDexterityReference';
    dataListIn.baseName = baseNameIn;
    dataListIn.shoulderName = shoulderNameIn;
    dataListIn.elbowName = elbowNameIn;
    metaListIn= struct();

    % use standard update method
    obj.updateData(markIn,metaListIn,dataListIn);
end
