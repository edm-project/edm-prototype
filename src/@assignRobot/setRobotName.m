function setRobotName(obj,robotNameIn,endEffectorNameIn)
%SETROBOTNAME set the robotName to be researched, program automatically call loadrobot to get model. 
% robotNameIn: robotName input, constrained by mustBeValidRobot
% endEffectorNameIn: name of the endEffector (for Inverse Kinematics)
    arguments
        obj(1,1) assignRobot
        robotNameIn(1,:) char {mustBeValidRobot}
        endEffectorNameIn(1,:) char
    end
    
    % check if allow callable for efficiency
    obj.mustAllowCall('setRobotName');

    %preprate for updateData
    markIn='setRobotName';
    dataListIn.tag = [robotNameIn,'_default'];
    dataListIn.endEffectorName = endEffectorNameIn;
    dataListIn.robotColumn = loadrobot(robotNameIn,'DataFormat','column');
    metaListIn= struct();

    % special: update the robotname to the object
    obj.robotName=robotNameIn;
    % use standard update method
    obj.updateData(markIn,metaListIn,dataListIn);
end

