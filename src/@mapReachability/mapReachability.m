classdef mapReachability < handle
    %MAPREACHABILITY class container of reachability map
    
    properties (SetAccess = immutable)
        version(1,1) double
    end 
    % end of immutable attributes
    
    properties (SetAccess=private)
        mode(1,:) char {mustBeValidModeNull(mode)} = 'null'
        generateMapHandle(1,1) generateMap

        mark(1,:) cell = {}
        metaList(1,:) cell = {}
        dataList(1,1) struct {mustBeValidDataList} = struct()
    end 
    % end of private attributes

    methods
        function obj = mapReachability(generateMapHandleIn)
            %MAPREACHABILITY constructor
            arguments
                generateMapHandleIn(1,1) generateMap
            end
            obj.generateMapHandle=generateMapHandleIn;
            obj.version=2;
            obj.mode='null';
        end
    end
    % end of the constructor

    methods (Access=private)
        function assignMode(obj,modeIn)
        %ASSIGNMODE assign the mode to be used. Modes are options for different
        %calculation acceleration methods.
        %The accelerations will be prepared here.
            arguments
                obj(1,1) mapReachability
                modeIn(1,:) char {mustBeValidMode(modeIn)}
            end

            % prepare parallel pool
            if isequal(modeIn,'numeric_parallel') || isequal(modeIn,'numericMEX_parallel')
                p = gcp('nocreate'); % If no pool, do not create new one.
                if isempty(p)        % create pool when there is no existing pool
                    parpool;
                end
            end

            % record the mode and status
            obj.mode=modeIn;
        end
    
        function initialize(obj,options)
        % initialize the object. 
            arguments
                obj(1,1) mapReachability
                options.ignoreWarning(1,1) logical = false
            end

            % warning when this class is not empty 
            if options.ignoreWarning==false && ~isequal(obj.dataList,struct())
                warning('This object mapReachability is not empty. Initialization will wipe out the old data.')
                disp('Continue Anyway?');
                pause on;
                pause;
            end

            % initialize(reset) the data section of this class
            obj.mode='null';
            obj.mark={};
            obj.metaList={};
            obj.dataList=struct();           
        end
    
        function updateData(obj,markIn,metaListIn,dataListIn)
            arguments
                obj(1,1) mapReachability
                markIn(1,:) char
                metaListIn(1,1) struct
                dataListIn(1,1) struct {mustBeValidDataList}
            end

            % update mark
            obj.mark=[obj.mark,{markIn}];

            %update metaList and dataList
            obj.metaList{length(obj.metaList)+1}=metaListIn;
            obj.dataList=mergeUpdateStruct(obj.dataList,dataListIn);
        end
    end
    % end of the private methods

    methods (Access = {?mapReachability,?generateMap}, Static)
        function isConsistent = checkConsistency()
        % check the data consistency with the outside (generateMap and other maps)
            isConsistent=true;
        end
    end
    % end of the friend methods

    methods (Access = private)
        [solutionFlag,configSolColumn] = numeric(obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,options);
        [solutionFlag,configSolColumn] = numericMEX(obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,options);
        [solutionFlag,configSolColumn] = numericParallel(obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,options);
        [solutionFlag,configSolColumn] = numericMEXParallel(obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,options);
    end
    % end of the private functional method (definition in same folder)

    methods (Access = public)
        fullCalculation(obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,options)
        % Functional method. Do a full calculation of reachability

        marginalCalculation(obj,method,options)
        % Functional method. Do marginal update of reachability
    end 
    % end of the public functional method (definition in same folder)
end

function mustBeValidDataList(dataListIn)
% Check for dataList fielnames. Throw error when unknown fields exits
% (not for missing fields)
    % get fieldnames, and change to 1*n cells
    dataListFieldName = fieldnames(dataListIn)';
    mustBeMember(dataListFieldName,{'solutionFlag','configSolColumn','reachabilityIndex'})
end