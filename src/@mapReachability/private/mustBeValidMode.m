function mustBeValidMode(modeIn)
    % Check for Valid input as mode for calculation
    mustBeMember(modeIn,{'numeric','numericMEX','numeric_parallel','numericMEX_parallel'});
end

