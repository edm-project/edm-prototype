function mustBeValidModeNull(modeIn)
    % Check for Valid input as mode for calculation
    mustBeMember(modeIn,{'null','numeric','numericMEX','numeric_parallel','numericMEX_parallel'});
end
