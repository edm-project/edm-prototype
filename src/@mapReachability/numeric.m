function [solutionFlag,configSolColumn] = numeric(obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,options)
%NUMERIC calculation reachability with numeric mode
    arguments
        obj(1,1) mapReachability
        maxIkIterationIn(1,1) double {mustBePositive}
        maxConstraintRepeatIn(1,1) double {mustBePositive}
        weightsIn(1,6) double
        options.range(1,:) char {mustBeMember(options.range,{'full','targeted','null'})} = 'null'
        options.solutionFlagMask(:,:) logical = []
    end

    % check if the ranging method is not assigned
    if isequal(options.range,'null')
        ME = MException('mapReachability:numeric:modeNull', ...
                        'mode not assigned.');
        throw(ME)
    end

    % prepare variables for calculation
    % input tform and it's mask
    tform = obj.generateMapHandle.dataList.allTform;
    solutionFlagMask = options.solutionFlagMask;
    if isequal(options.range,'full')
        solutionFlagMask = true(size(tform,3),size(tform,5));
    end

    % check if the size is same
    if (size(tform,3)~=size(solutionFlagMask,1)) || (size(tform,5)~=size(solutionFlagMask,2))
        ME = MException('mapReachability:numeric:sizeNotMatch', ...
                        'tform and mask does not match in size [%s][%s]', ...
                         mat2str(size(tform)),mat2str(size(solutionFlagMask)));
        throw(ME)
    end
    
    % other inputs
    robotParameters=obj.generateMapHandle.dataList.robotParameters;
    robotColumn = obj.generateMapHandle.dataList.robotParameters.robotColumn;
    endEffectorName = obj.generateMapHandle.dataList.robotParameters.endEffectorName;
    ik = inverseKinematics('RigidBodyTree',robotColumn);
    ik.SolverParameters.MaxIterations=maxIkIterationIn;

    %parameters
    nLocalPosition = size(tform,3);
    nLocalRotation = size(tform,4);
    nGlobalPostition = size(tform,5);
    robotConfigSize = length(homeConfiguration(robotColumn)); 
    
    %progressbar
    fig = progressbar(['Reachability: ',obj.mode,' mode ',options.range,' range'],nLocalPosition*nGlobalPostition);

    %output set
    solutionFlag = zeros(nLocalPosition,nGlobalPostition);
    configSolColumn = zeros(robotConfigSize,nLocalPosition,nGlobalPostition);

    % data calculation
    for iGlobalPosition=1:nGlobalPostition
        for iLocalPosition=1:nLocalPosition % for each position(global and local)
            if solutionFlagMask(iLocalPosition,iGlobalPosition)==false
                % when the solution is not targeted, skip the calculation.
                progressbar();
                continue;
            end
            for iLocalRotation=1:nLocalRotation  % for each rotation on same position
                rotationSkipFlag = false; 
                for iRepeatCounter=1:maxConstraintRepeatIn % iteration for constrains
                    initialguess = randomConfiguration(robotColumn);
                    [configSol,solInfo] = ik(endEffectorName, ...
                                             tform(:,:,iLocalPosition,iLocalRotation,iGlobalPosition), ...
                                             weightsIn, ...
                                             initialguess);
                    
                    % if IK solver can't find Solution.
                    if isequal(solInfo.Status,'best available')
                        solutionFlag(iLocalPosition,iGlobalPosition) = 2;
                        break;
                    
                    % if IK solver find Solution.
                    elseif isequal(solInfo.Status,'success')
                        % check the collision and joint Limit
                        [solutionFlagOut,configSolOut] = solutionFlagCheck(robotParameters,configSol);
                        
                        if solutionFlagOut == 1
                            solutionFlag(iLocalPosition,iGlobalPosition) = solutionFlagOut;
                            configSolColumn(:,iLocalPosition,iGlobalPosition)=configSolOut;
                            rotationSkipFlag = true;
                            break;
                        else
                            solutionFlag(iLocalPosition,iGlobalPosition) = solutionFlagOut;
                            continue;
                        end
                        
                    else
                        ME = MException('generateMap:reachIkAll:elseError', ...
                                        'if-else should not run into this branch');
                        throw(ME);
                    end
                end
                if rotationSkipFlag==true
                    % skip constraint loop (4-th inner for) when solution is found 
                    break;
                end
            end
            % update the progressbar
            progressbar(); 
        end
    end

    % close the progressbar
    close(fig);
end

