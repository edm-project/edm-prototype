function marginalCalculation(obj,method,options)
%MARGINALCALCULATION Make marginal update of the reachability Index
% in:
% [method]: filtering method (decide what to recalculate)
%        - constraint: continue to calculate the pose with constraint unsatisfied
%        - index-edge: calculate the reachability index with uncontinous edge
%        - pose-edge: calculate for each position with with uncontinous edge
% [maxIkIterationIn,maxConstraintRepeatIn,weightsIn]: same as in fullCalculation
% [ignoreWarning]: the warning during the calculation(here consistency check)
    arguments
        obj(1,1) mapReachability
        method(1,:) char {mustBeMember(method,{'constraint'})}
        options.maxIkIterationIn(1,1) double {mustBePositive} = 1500
        options.maxConstraintRepeatIn(1,1) double {mustBePositive} = 10
        options.weightsIn(1,6) double = [0.1 0.1 0.1 0.5 0.5 0.5]
        options.ignoreWarning(1,1) logical = false
        options.extraCheck(1,1) logical = true
        options.forceGenerate(1,1) logical = false
    end

    % check if data exists
    if isfield(obj.dataList,'solutionFlag')==false
        ME = MException('mapReachability:marginalCalculation:noField', ...
                        'the class have no data for marginal update');
        throw(ME)
    end

    % check data consistency
    if options.ignoreWarning==false && obj.checkConsistency()==false
        warning(['Inconsistency between mapReachability and other classes detected.' ...
                 'Possibly dependent maps are updated after this map generated.'])
        disp('Continue Anyway?');
        pause on;
        pause;
    end

    % start a time counter
    startTime= datetime('now');

    % read in the original data
    allTform=obj.generateMapHandle.dataList.allTform;
    nLocalPosition=size(allTform,3);

    solutionFlagTemp=obj.dataList.solutionFlag;
    configSolColumnTemp=obj.dataList.configSolColumn;
    %reachabilityIndexTemp=obj.dataList.reachabilityIndex;

    % filter the targeted data 
    if isequal(method,'constraint')
        % recalculate all tform with solutionFlagTemp = 3
        solutionFlagMask = (solutionFlagTemp==3);

    elseif isequal(method,'index-edge')
        % recalculate the point in reach map that is different from it's surroundings

    elseif isequal(method,'pose-edge')
        % recalculate the pose in a point that is different from surroundings.
        
    else
        ME = MException('mapReachability:marginalCalculation:else_error', ...
                        'Method should not go into this else branch');
        throw(ME)
    end

    % recalculate the data
    if isequal(obj.mode,'numeric')
        [solutionFlagMasked,configSolColumnMasked] = obj.numeric(options.maxIkIterationIn, ...
                                                                options.maxConstraintRepeatIn, ...
                                                                options.weightsIn, ...
                                                                range='targeted', ...
                                                                solutionFlagMask=solutionFlagMask);
    elseif isequal(obj.mode,'numericMEX')
        [solutionFlagMasked,configSolColumnMasked] = obj.numericMEX(options.maxIkIterationIn, ...
                                                                   options.maxConstraintRepeatIn, ...
                                                                   options.weightsIn, ...
                                                                   range='targeted', ...
                                                                   solutionFlagMask=solutionFlagMask, ...
                                                                   ignoreWarning=options.ignoreWarning, ...
                                                                   forceGenerate=options.forceGenerate);
    elseif isequal(obj.mode,'numeric_parallel') 
        [solutionFlagMasked,configSolColumnMasked] = obj.numericParallel(options.maxIkIterationIn, ...
                                                                        options.maxConstraintRepeatIn, ...
                                                                        options.weightsIn, ...
                                                                        range='targeted', ...
                                                                        solutionFlagMask=solutionFlagMask);
    elseif isequal(obj.mode,'numericMEX_parallel')
        [solutionFlagMasked,configSolColumnMasked] = obj.numericMEXParallel(options.maxIkIterationIn, ...
                                                                options.maxConstraintRepeatIn, ...
                                                                options.weightsIn, ...
                                                                range='targeted', ...
                                                                solutionFlagMask=solutionFlagMask, ...
                                                                ignoreWarning=options.ignoreWarning, ...
                                                                forceGenerate=options.forceGenerate);
    else
        ME = MException('mapReachability:fullCalculation:elseError', ...
                        'mode if-else should not run into this branch');
        throw(ME);
    end

    % calculate the other logical matrix masks
    solutionFlagMaskAfter = (solutionFlagMasked==1);
    configSolColumnMask = repmat(reshape(solutionFlagMaskAfter,1,size(solutionFlagMaskAfter,1),size(solutionFlagMaskAfter,2)), ...
                                 size(configSolColumnTemp,1),1,1);

    % update the result data
    solutionFlagTemp(solutionFlagMaskAfter)=solutionFlagMasked(solutionFlagMaskAfter);
    configSolColumnTemp(configSolColumnMask)=configSolColumnMasked(configSolColumnMask);
    reachabilityIndexTemp = sum (solutionFlagTemp==1,1)*100/nLocalPosition;

    % calculate some data
    nTargeted = sum(solutionFlagMask,"all"); % number of tform to be recalculated
    nUpdated = sum(solutionFlagMasked==1,"all"); % number of tform updated
    nUnsolved = sum(solutionFlagMasked==2,"all"); % number of tform with unsolved
    nPending = sum(solutionFlagMasked==3,"all"); % number of tform still constrained

    % extra check mechanism to prevent error by reshape
    if options.extraCheck == true
        robotColumn = obj.generateMapHandle.dataList.robotParameters.robotColumn;
        [row,col] = find (solutionFlagMasked == 1);
        for iRow = 1:length(row)
            matrix_diff=getTransform(robotColumn,configSolColumnTemp(:,row(iRow),col(iRow)),'panda_link8') - ... 
                                     allTform(:,:,row(iRow),1,col(iRow));
            posnorm_diff=norm(matrix_diff(1:3,4));
            if posnorm_diff>= 1e-7
                ME = MException('mapReachability:marginalCalculation:match_error', ...
                                ['Mismatch by alltform occurs [%d][%d][%d][%d]. ' ...
                                'This is caused by logical matrix filtering.\n' ...
                                'This should not happen. But if it happens, please save example for debugging.'], ...
                                iRow,posnorm_diff,row(iRow),col(iRow));
                throw(ME)
            end
        end
    end

    % prepare the data for update
    markIn='mariginalUpdate';
    dataListIn.solutionFlag=solutionFlagTemp;
    dataListIn.configSolColumn=configSolColumnTemp;
    dataListIn.reachabilityIndex=reachabilityIndexTemp;

    metaListIn.maxIkInteration=options.maxIkIterationIn;
    metaListIn.maxConstraintRepeat=options.maxConstraintRepeatIn;
    metaListIn.weights=options.weightsIn;
    metaListIn.startTime=startTime;
    metaListIn.endTime=datetime('now');
    metaListIn.nTargetLength=nTargeted;
    metaListIn.nUpdatedLength=nUpdated;
    metaListIn.nUnsolvedLength=nUnsolved;
    metaListIn.nPendingLength=nPending;
    
    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

