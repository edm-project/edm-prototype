function fullCalculation(obj,maxIkIterationIn,maxConstraintRepeatIn,weightsIn,options)
%FULLCALCULATION Functional method. Do a full calculation of reachability
%  in:
%  [maxIkIterationIn]: maximum Itertion for numeric IK solver (defined by IK solver)
%  [maxConstraintRepeatIn]: maximum repeat when solution don't satisfy the
%  constraints (self collision or joint constaints)
%  [weightsIn]: weights Needed for the solution (defined by IK solver)
%
%  out(update): 
%  [solutionFlag]: mark up the solution status for each Tform:
%     0=undefined 1=haveSolution 2=noSolution 3=maxConstrainExceeds
%  [configSolColumn]: the joint configuration saved for each Tform
%  [reachabilityIndex]: the reachability Index for each Tform
    arguments
        obj(1,1) mapReachability
        maxIkIterationIn(1,1) double {mustBePositive}
        maxConstraintRepeatIn(1,1) double {mustBePositive}
        weightsIn(1,6) double
        options.ignoreWarning(1,1) logical = false
        options.mode(1,:) char {mustBeValidModeNull(options.mode)} = 'null'
        options.forceGenerate(1,1) logical = false
    end

    % initialize the class
    obj.initialize(ignoreWarning=options.ignoreWarning);

    % assign the mode
    obj.assignMode(options.mode);

    % start a time counter
    startTime= datetime('now');

    % prepare variables for calculation and storage
    nLocalPosition = size(obj.generateMapHandle.dataList.allTform,3);

    % ik calculation with different modes
    if isequal(obj.mode,'numeric')
        [solutionFlag,configSolColumn] = obj.numeric(maxIkIterationIn,maxConstraintRepeatIn,weightsIn, ...
                                                     range='full');
    elseif isequal(obj.mode,'numericMEX')
        [solutionFlag,configSolColumn] = obj.numericMEX(maxIkIterationIn,maxConstraintRepeatIn,weightsIn, ...
                                                        range='full', ...
                                                        ignoreWarning=options.ignoreWarning, ...
                                                        forceGenerate=options.forceGenerate);
    elseif isequal(obj.mode,'numeric_parallel') 
        [solutionFlag,configSolColumn] = obj.numericParallel(maxIkIterationIn,maxConstraintRepeatIn,weightsIn, ...
                                                             range='full');
    elseif isequal(obj.mode,'numericMEX_parallel')
        [solutionFlag,configSolColumn] = obj.numericMEXParallel(maxIkIterationIn,maxConstraintRepeatIn,weightsIn, ...
                                                                range='full', ...
                                                                ignoreWarning=options.ignoreWarning, ...
                                                                forceGenerate=options.forceGenerate);
    else
        ME = MException('mapReachability:fullCalculation:elseError', ...
                        'mode if-else should not run into this branch');
        throw(ME);
    end

    % reachability Index calculation
    reachabilityIndex=sum(solutionFlag==1,1)*100/nLocalPosition;

    % prepare the data
    markIn='fullCalculation';
    dataListIn.solutionFlag=solutionFlag;
    dataListIn.configSolColumn=configSolColumn;
    dataListIn.reachabilityIndex=reachabilityIndex;

    metaListIn.maxIkInteration=maxIkIterationIn;
    metaListIn.maxConstraintRepeat=maxConstraintRepeatIn;
    metaListIn.weights=weightsIn;
    metaListIn.startTime=startTime;
    metaListIn.endTime=datetime('now');
    if isequal(obj.mode,'numericMEX') || isequal(obj.mode,'numericMEX_parallel')
        metaListIn.numericMEXStr = fileread('MEXik.m');
    end

    % update the data
    obj.updateData(markIn,metaListIn,dataListIn);
end

