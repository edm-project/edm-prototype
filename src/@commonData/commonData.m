classdef commonData < handle
    %structure for dataSlot_class
    %   Contains commonly used data between process.

    properties (SetAccess=immutable)
        processName(1,:) cell {mustBeValidProcessNull(processName)}
    end 
    % end of immutable attributes

    properties (SetAccess=private) 
        steps(1,:) char {mustBeValidProcessNull(steps)} = 'null'
        isInitialized(1,1) logical = false;
        isFinalized(1,1) logical = false;

        robotParameters(1,1) struct = struct();
        positionRow(:,3) double 
        endEffectorTform(4,4,:,:) double 
    end 
    % end of private attributes
    
    methods (Access=public)
        function obj = commonData()
            obj.processName = {'null','assignRobot','generateGrid','filterGrid','generateDirection','generateMap'};
            obj.steps = 'null';
            obj.isInitialized = true;
            obj.isFinalized = true;
        end
    end 
    % end of the constructor
    
    methods (Access=private)
        function index = findProcessIndex(obj,processIn)
        %findProcessIndex: get the Index of a process name in processName{}
            arguments
                obj(1,1) commonData
                processIn(1,:) char {mustBeValidProcessNull(processIn)}
            end
            index = getIndexCell(obj.processName,processIn);
        end

        function mustAllowUpdateData(obj,processIn)
            arguments
                obj(1,1) commonData
                processIn(1,:) char {mustBeValidProcess(processIn)}
            end
            
            % If current step is not the process Input
            if ~isequal(obj.steps,processIn)
                ME = MException('commonData:mustAllowUpdateData:processError', ...
                                'current step [%s] is not input [%s]', ...
                                obj.steps,processIn);
                throw(ME)
            end

            % If the step now is in the finalized state
            if ~(obj.isInitialized == true && obj.isFinalized == true)
                ME = MException('commonData:mustAllowInitialize:markError', ...
                                'current marks not suitable for updateData [init:%s] [fin:%s]', ...
                                string(obj.isInitialized),string(obj.isFinalized));
                throw(ME)
            end
        end
    end 
    % end of the private method

    methods (Access={?robotMaps})
        function mustAllowInitialize(obj,processIn)
        %mustAllowInitialize: throw error if initialization of processIn is no allowed
            arguments
                obj(1,1) commonData
                processIn(1,:) char {mustBeValidProcess(processIn)}
            end

            % If the previous step is not finialized
            if ~(obj.isInitialized == true && obj.isFinalized == true)
                ME = MException('commonData:mustAllowInitialize:markError', ...
                                'predecessor step not finalized [init:%s] [fin:%s]', ...
                                string(obj.isInitialized),string(obj.isFinalized));
                throw(ME)
            end

            % If current step is not the previous step of process Input
            if ~isequal(obj.steps,obj.processName{obj.findProcessIndex(processIn)-1})
                ME = MException('commonData:mustAllowInitialize:processError', ...
                                'current step [%s] is not predecessor of the calling step [%s]', ...
                                obj.steps,processIn);
                throw(ME)
            end
        end

        function mustAllowFinalize(obj,processIn)
        %mustAllowFinalize: throw error if finalization of processIn no allowed
            arguments
                obj(1,1) commonData
                processIn(1,:) char {mustBeValidProcess(processIn)}
            end

            % If current step is not the process Input
            if ~isequal(obj.steps,processIn)
                ME = MException('commonData:mustAllowFinalize:processError', ...
                                'current step [%s] is not the calling step [%s]', ...
                                obj.steps,processIn);
                throw(ME)
            end

            % If the step now is ready for finalize state
            if ~(obj.isInitialized == true && obj.isFinalized == false)
                ME = MException('commonData:mustAllowFinalize:markError', ...
                                'current step mark not ready for finalize [init:%s] [fin:%s]', ...
                                string(obj.isInitialized),string(obj.isFinalized));
                throw(ME)
            end
        end

        function initializeProcess(obj,processIn)
            arguments
                obj(1,1) commonData
                processIn(1,:) char {mustBeValidProcess(processIn)}
            end

            % check if the initialization for process is Possible
            obj.mustAllowInitialize(processIn);

            % update the status
            obj.steps=processIn;
            obj.isInitialized=true;
            obj.isFinalized=false;
        end

        function finalizeProcess(obj,processIn)
            arguments
                obj(1,1) commonData
                processIn(1,:) char {mustBeValidProcess(processIn)}
            end

            % check if the finalization for process is Possible
            obj.mustAllowFinalize(processIn);
        
            % update the status
            obj.isFinalized=true;
        end
    end 
    % end of the robotMaps friend method

    methods (Access={?assignRobot})
        function updateDataAssignRobot(obj,robotParametersIn)
        % method called by assignRobot to update data after finalization
            arguments
                obj(1,1) commonData
                robotParametersIn(1,1) struct
            end

            % check if assignRobot updateData is allowed
            obj.mustAllowUpdateData('assignRobot');

            % write in the data
            obj.robotParameters=robotParametersIn;
        end
    end 
    % end of the assignRobot friend method

    methods (Access={?generateGrid})
        function updateDataGenerateGrid(obj,positionRowIn)
        % method called by generateGrid to update data after finalization
            arguments
                obj(1,1) commonData
                positionRowIn(:,3) double
            end

            % check if assignRobot updateData is allowed
            obj.mustAllowUpdateData('generateGrid');

            % write in the data
            obj.positionRow=positionRowIn;
        end
    end
    % end of the generateGrid friend method

    methods (Access={?filterGrid})
        function updateDataFilterGrid(obj,positionRowIn)
        % method called by filterGrid to update data after finalization
            arguments
                obj(1,1) commonData
                positionRowIn(:,3) double
            end

            % check if assignRobot updateData is allowed
            obj.mustAllowUpdateData('filterGrid');

            % write in the data
            obj.positionRow=positionRowIn;
        end
    end
    % end of the filterGrid friend method

    methods (Access={?generateDirection})
        function updateDataGenerateDirection(obj,endEffectorTform)
        % method called by generateDirection to update data after finalization
            arguments
                obj(1,1) commonData
                endEffectorTform(4,4,:,:) double
            end

            % check if assignRobot updateData is allowed
            obj.mustAllowUpdateData('generateDirection');

            % write in the data
            obj.endEffectorTform=endEffectorTform;
        end
    end
    % end of the generateDirection friend method
end

function mustBeValidProcess(processIn)
% Check for Valid input as mark
    mustBeMember(processIn,{'assignRobot','generateGrid','filterGrid','generateDirection','generateMap'});
end

function mustBeValidProcessNull(processIn)
% Check for Valid input as mark, allow null
    mustBeMember(processIn,{'null','assignRobot','generateGrid','filterGrid','generateDirection','generateMap'});
end
