% random set some points
rng('shuffle');
% xEnd = rand() - 0.5;
% zEnd = rand() / 2;
xEnd = rand() / 2 - 0.25;
zEnd = rand() / 4;
angleStart = rand(1,3)/0.5;
angleEnd = rand(1,3)/0.5;

startTform = eul2tform(angleStart);

endTform = eul2tform(angleEnd);
endTform(1,4) = xEnd; 
endTform(3,4) = zEnd;

% extract some data
robotColumn = robotMapsClass.commonDataSlot.robotParameters.robotColumn;

% plot the two Tform
plotTform(startTform,0.1);
plotTform(endTform,0.1);
grid on;
view(0,0);
xlim([-1 1])
zlim([-0.5 1.5])
ylim([-1 1])
pause on
pause

% calculate the two Point sample;
twoPointRRTHandle =  twoPointRRT(robotMapsClass);
twoPointRRTHandle.initialize();
twoPointRRTHandle.twoPointRRTStart(startTform,endTform,20);
twoPointRRTHandle.correlationCalculation();
twoPointRRTHandle.twoPointRRTTest();