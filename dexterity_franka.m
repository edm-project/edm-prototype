%% path helpers
% import data based on frankaEmikaPanda_dof7_reachabilityTest_flippedDexterityTest_202210260927
addpath(genpath('./log'));

%% constants
targetedFlipSolution = 4;
maxFailRepeat = 2;
maxRepeatLimit = 10;
iLocalRotation = 1;

% prepare for fileName
robotTag=robotMapsClass.commonDataSlot.robotParameters.tag;
startTime=datestr(datetime('now'),'yyyymmddHHMM');
dirName = ['DEX_',startTime,'_',robotTag];
logPath = ['./log/',dirName,'/'];

status = mkdir('./log',dirName);
addpath(genpath('./log'));
if status == 0
    ME = MException('robotMapsClass:fileFailed', ...
            'mkdir failed.', ...
            gridMark);
    throw(ME)
end

%% 1
% constants
iLocalPosition = 1;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,811,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,316,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,717,[1 4 2 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,285,[2 4 3 1])

gikAndNSWithDifferentStage;

% save
savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 2
% constants
iLocalPosition = 2;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,811,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,316,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,648,[2 NaN 1 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,221,[NaN 1 2 3])

gikAndNSWithDifferentStage;

%% 3
iLocalPosition = 3;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,811,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,159,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,349,[2 1 4 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,680,[2 1 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,648,[1 2 NaN 3])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 4
iLocalPosition = 4;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,877,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,221,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,680,[3 4 1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,449,[2 1 3 4])

gikAndNSWithDifferentStage;

%% 5
iLocalPosition = 5;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,350,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,895,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,902,[3 1 4 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,747,[3 2 4 1])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 6
iLocalPosition = 6;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,349,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,864,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,1015,[NaN 2 3 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,779,[4 1 2 3])

gikAndNSWithDifferentStage;

%% 7
iLocalPosition = 7;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,347,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,862,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,901,[1 2 3 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,777,[1 3 2 4])

gikAndNSWithDifferentStage;

%% 8
iLocalPosition = 8;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,811,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,283,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,610,[1 2 NaN 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,674,[2 1 3 4])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 9
iLocalPosition = 9;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,285,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,778,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,345,[3 2 1 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,337,[3 2 1 4])

gikAndNSWithDifferentStage;

%% 10
iLocalPosition = 10;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,254,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,647,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,251,[1 3 4 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,247,[1 2 4 3])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 11
iLocalPosition = 11;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,284,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,679,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,313,[1 2 3 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,209,[4 1 2 3])

gikAndNSWithDifferentStage;

%% 12
iLocalPosition = 12;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,777,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,315,[2 1])
% robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,,[])
% robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,,[])
% gikAndNSWithDifferentStage;

maxFailRepeat = 2;
for iFlip = 1:2
    % do gikMotion
    failCounter = 0;
    while failCounter < maxFailRepeat
        [calculationFinished,~] = robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotionEmpirical(iLocalPosition,iLocalRotation,iFlip,logPath=logPath, ...
               maxDifference=20);
        if calculationFinished==false
            failCounter=failCounter+1;
        else
            robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotion(iLocalPosition,iLocalRotation,iFlip);
            failCounter=0;
        end
    end
end

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 13
iLocalPosition = 13;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,345,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,776,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,901,[2 1 4 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,862,[2 1 3 4])

gikAndNSWithDifferentStage;

%% 14
iLocalPosition = 14;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,872,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,411,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,900,[1 2 3 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,1009,[3 4 1 2])

gikAndNSWithDifferentStage;

%% 15
iLocalPosition = 15;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,807,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,247,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,730,[1 4 3 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,496,[1 2 3 4])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 16
iLocalPosition = 16;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,708,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,216,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,309,[4 1 2 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,144,[2 3 1 4])

gikAndNSWithDifferentStage;

%% 17
iLocalPosition = 17;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,279,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,741,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,271,[1 3 2 4])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,632,[1 2 3 4])

gikAndNSWithDifferentStage;

%% 18
iLocalPosition = 18;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,870,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,343,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,896,[2 4 1 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,494,[2 1 4 3])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% 19
iLocalPosition = 19;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,805,[1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,277,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,301,[1 2 4 3])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,698,[2 4 3 1])

gikAndNSWithDifferentStage;

%% 20
iLocalPosition = 20;
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,803,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,275,[2 1])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,266,[3 4 1 2])
robotMapsClass.generateMapSlot.dataList.testDexterity.pointBatchImportFD(iLocalPosition,iLocalRotation,727,[3 1 4 2])

gikAndNSWithDifferentStage;

savePoint(robotMapsClass,'DEX',[num2str(iLocalPosition)],logPath)

%% flip swap and delete
for iLocalPosition=1:20
    if iLocalPosition==12
        continue;
    end
    robotMapsClass.generateMapSlot.dataList.testDexterity.removeRepeatFlip(iLocalPosition,iLocalRotation);
end

for iLocalPosition=[10 11 14 18]
    maxFailRepeat = 1;
    for iFlip = 3:4
        maxCounter = 0;
        % do gikMotion
        failCounter = 0;
        while failCounter < maxFailRepeat
            [calculationFinished,~] = robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotionEmpirical(iLocalPosition,iLocalRotation,iFlip,logPath=logPath);
            if calculationFinished==false
                failCounter=failCounter+1;
            else
                robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotion(iLocalPosition,iLocalRotation,iFlip);
                failCounter=0;
            end

            maxCounter = maxCounter+1;
            if maxCounter>=maxRepeatLimit
                break;
            end
        end
    end
end

%% final rotation
for iLocalPosition=1:20
    for iFlipSolution=1:4
    disp(['[Dexterity Main]: start eeRotation for (',num2str(iLocalPosition),',', ...
                                                        num2str(iLocalRotation),',:,:,', ...
                                                        num2str(iFlipSolution),').']);
    robotMapsClass.generateMapSlot.dataList.testDexterity.eeRotation(iLocalPosition,iLocalRotation,iFlipSolution);
    end
end