robotParameters=robotMapsClass.commonDataSlot.robotParameters;
configSol = randomConfiguration(robotParameters.robotColumn);
[configSolCol,angleCol,sectionCol] = nullspaceFixed(robotParameters,configSol,0.3,24,200);
configSol1 = configSolCol(:,1);
configSol2 = configSolCol(:,1+randi(3));
isReachableHandle = MEXisNullspaceReachableGenerate(robotParameters);
sectionHandle = MEXdexteritySectionGenerate(robotParameters);