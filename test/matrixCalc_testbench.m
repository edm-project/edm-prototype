% Testbench for local machine with different ways of code acceleration
% with: normal / parfor mode / gpuArray
% all missions are extracted from the real code enviroment

%% test - calculation with big matrix

% start parallel pool
p = gcp('nocreate'); % If no pool, do not create new one.
if isempty(p)
    parpool;
end

g=gpuArray();

% clear everything for the first time
clear
clc

%% alterative 1: normal code
% initialize
clear
% importdata
dataname='matrixCalc_data_20x5x1077.mat';
load(dataname)
% start
startClock=tic;
allPendingTform=repmat(endEffectorTform,1,1,1,1,size(positionRow,1));
for iGlobalPos=1:size(positionRow,1)
    allPendingTform(1:3,4,:,:,iGlobalPos)=allPendingTform(1:3,4,:,:,iGlobalPos)+positionRow(iGlobalPos,1:3)';
end
endtime=toc(startClock);
fprintf('Data:%s; Test:bigMatrixCalc; mode: normal; time=%f\n',dataname,endtime)

%% alterative 2: parfor code
% initialize
clear
% importdata
dataname='matrixCalc_data_20x5x1077.mat';
load(dataname)
% start
startClock=tic;
allPendingTform=repmat(endEffectorTform,1,1,1,1,size(positionRow,1));
nEffectorPos=size(endEffectorTform,3);
nEffectorRot=size(endEffectorTform,4);
parfor iGlobalPos=1:size(positionRow,1)
    for iEffectorPos=1:nEffectorPos
        for iEffectorRot=1:nEffectorRot
            for iNum=1:3
                allPendingTform(iNum,4,iEffectorPos,iEffectorRot,iGlobalPos)=allPendingTform(iNum,4,iEffectorPos,iEffectorRot,iGlobalPos)+positionRow(iGlobalPos,iNum)';
            end
        end
    end
end
endtime=toc(startClock);
fprintf('Data:%s; Test:bigMatrixCalc; mode: parfor; time=%f\n',dataname,endtime)

%% alterative 3: gpuArray code
% initialize
clear
% importdata
dataname='matrixCalc_data_20x5x1077.mat';
load(dataname)
startClock=tic;
endEffectorTform=gpuArray(endEffectorTform);
positionRow=gpuArray(positionRow);
endtime=toc(startClock);
allPendingTform=repmat(endEffectorTform,1,1,1,1,size(positionRow,1));
for iGlobalPos=1:size(positionRow,1)
    allPendingTform(1:3,4,:,:,iGlobalPos)=allPendingTform(1:3,4,:,:,iGlobalPos)+positionRow(iGlobalPos,1:3)';
end
endtime2=toc(startClock);
fprintf('Data:%s; Test:bigMatrixCalc; mode: normal; time=%f/%f\n',dataname,endtime,endtime)

%% result
% 202208021215
% after clear all / at the first start
% Data:matrixCalc_data_20x5x1077.mat; Test:bigMatrixCalc; mode: normal; time=0.031116
% Data:matrixCalc_data_20x5x1077.mat; Test:bigMatrixCalc; mode: parfor; time=1.304365
% Data:matrixCalc_data_20x5x1077.mat; Test:bigMatrixCalc; mode: normal; time=0.147525/0.147525

% after clear, with preloading of g=gpuArray() (line 13)
% Data:matrixCalc_data_20x5x1077.mat; Test:bigMatrixCalc; mode: normal; time=0.040176
% Data:matrixCalc_data_20x5x1077.mat; Test:bigMatrixCalc; mode: parfor; time=2.034847
% Data:matrixCalc_data_20x5x1077.mat; Test:bigMatrixCalc; mode: normal; time=0.009780/0.009780

% The original form is aleady very good.
% if gpu calculation is started, it would be quicker at matrix float point calculation

%% note
% repmat is much quicker than using allPendingTform=zero() and assign the postion later! 
% this implementation is already very good！
% BAD Example: (takes up more that 2min to do the same job)
% %             obj.dataList.allPendingTform = zeros(4,4, ...
% %                  size(obj.dataList.endEffectorTform,3),size(obj.dataList.endEffectorTform,4), ...
% %                  size(obj.dataList.positionRow,1));
% %             % initialize allPendingTform with data recieved
% %             for iLocalPos=1:size(obj.dataList.endEffectorTform,3)
% %                 for iLocalRot=1:size(obj.dataList.endEffectorTform,4)
% %                     for iGlobalPos=1:size(obj.dataList.positionRow,1)
% %                         tempTform=obj.dataList.endEffectorTform(:,:,iLocalPos,iLocalRot);
% %                         tempTform(1:3,4)=tempTform(1:3,4)+ obj.dataList.positionRow(iGlobalPos,:)';
% %                         obj.dataList.allPendingTform(:,:,iLocalPos,iLocalRot,iGlobalPos) = tempTform;
% %                     end
% %                 end
% %             end