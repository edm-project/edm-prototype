if checkCollision(robotColumn,configSol)==true
    solutionFlagTEMPTEST = 4;
elseif checkJointLimits(robotColumn,configSol)==true
    solutionFlagTEMPTEST = 3;
else
    solutionFlag(iLocalPosition,iGlobalPosition) = 1;
    configSolColumn(:,iLocalPosition,iGlobalPosition)=configSol;
end