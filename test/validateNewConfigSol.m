function [problemDataNew,successRateNew,problemDataOld,successRateOld] = validateNewConfigSol(robotMapsClass,validateLength,suggestList)
    arguments
        robotMapsClass
        validateLength
        suggestList = []
    end
    
    addpath('src/scripts/algorithmTest/');
    robotParameters=robotMapsClass.commonDataSlot.robotParameters;
    testDexterity=robotMapsClass.generateMapSlot.dataList.testDexterity;
    solutionFlag=testDexterity.dataList.solutionFlag;
    configSolColumn=testDexterity.dataList.configSolColumn;
    nLocalPosition=size(solutionFlag,1);
    nLocalRotation=size(solutionFlag,2);
    nGloalPosition=size(solutionFlag,3);
    nSection=size(solutionFlag,4);
    nFlip=testDexterity.dataList.flipCounter-1;

    if isempty(suggestList)
        rng('shuffle');
        LocalPositionList = randi(nLocalPosition,validateLength,1);
        LocalRotationList = randi(nLocalRotation,validateLength,1);
        GlobalPositionList = randi(nGloalPosition,validateLength,1);
        SectionList = randi(nSection,validateLength,1);
        FlipList = randi(nFlip,validateLength,1);
    else 
        LocalPositionList = suggestList(:,1);
        LocalRotationList = suggestList(:,2);
        GlobalPositionList = suggestList(:,3);
        SectionList = suggestList(:,4);
        FlipList = suggestList(:,5);
        validateLength = size(suggestList,1);
    end

    successNew = 0;
    successOld = 0;
    failNew = 0;
    failOld = 0;
    for iValidate=1:validateLength
        disp(['iValidate=',num2str(iValidate)]);
        while true
            disp(['select Point:(',num2str(LocalPositionList(iValidate)),',', ...
                                   num2str(LocalRotationList(iValidate)),',', ...
                                   num2str(GlobalPositionList(iValidate)),',' ...
                                   num2str(SectionList(iValidate)),',', ...
                                   num2str(FlipList(iValidate)),')']);
            disp(['Flag  = ',num2str(solutionFlag(LocalPositionList(iValidate),LocalRotationList(iValidate), ...
                            GlobalPositionList(iValidate),SectionList(iValidate),FlipList(iValidate)))]);

            if solutionFlag(LocalPositionList(iValidate),LocalRotationList(iValidate), ...
                            GlobalPositionList(iValidate),SectionList(iValidate),FlipList(iValidate))==2
                LocalPositionList(iValidate) = randi(nLocalPosition,1,1);
                LocalRotationList(iValidate) = randi(nLocalRotation,1,1);
                GlobalPositionList(iValidate) = randi(nGloalPosition,1,1);
                SectionList(iValidate) = randi(nSection,1,1);
                FlipList(iValidate) = randi(nFlip,1,1);
                continue;
            end
            configSol=configSolColumn(:,LocalPositionList(iValidate),LocalRotationList(iValidate), ...
                                     GlobalPositionList(iValidate),SectionList(iValidate),FlipList(iValidate));
            break;
        end

        disp('Start New Calculation...');
        t1 = tic;
        [configSolColumnNew,haveSolutionColNew] = algotest_NullDirRound(robotParameters,configSol,nSection, ...
                                                                                                  "increaseStep",50, ...
                                                                                                  "maxStep",250, ...
                                                                                                  "timeStep",0.2);
        timeNew = toc(t1);
        problemDataNew.time(iValidate) = timeNew;

        disp('Start Old Calculation...');
        t2 = tic;
        [configSolColumnOld,haveSolutionColOld] = nullspaceRound(robotParameters,configSol,nSection, ...
                                                                                          "increaseStep",50, ...
                                                                                          "maxStep",250, ...
                                                                                       "timeStep",0.2);
        timeOld = toc(t2);
        problemDataOld.time(iValidate) = timeOld;

        if any(haveSolutionColNew==0,'all')
            failNew = failNew+1;
            problemDataNew.index(failNew,:) = [LocalPositionList(iValidate),LocalRotationList(iValidate), ...
                                               GlobalPositionList(iValidate),SectionList(iValidate),FlipList(iValidate)];
            problemDataNew.configSolColumn(failNew,:,:) = configSolColumnNew;
            problemDataNew.haveSolution(failNew,:) = haveSolutionColNew;
        else
            successNew = successNew+1;
        end

        if any(haveSolutionColOld==0,'all')
            failOld = failOld+1;
            problemDataOld.index(failOld,:) = [LocalPositionList(iValidate),LocalRotationList(iValidate), ...
                                               GlobalPositionList(iValidate),SectionList(iValidate),FlipList(iValidate)];
            problemDataOld.configSolColumn(failOld,:,:) = configSolColumnOld;
            problemDataOld.haveSolution(failOld,:) = haveSolutionColOld;
        else
            successOld = successOld+1;
        end
    end

    successRateNew = successNew/validateLength;
    successRateOld = successOld/validateLength;
end

