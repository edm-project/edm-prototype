% Main function for producing data and drawing map.
% It would be better to start up the parallel pool before running the code.
%% Preparation
addpath(genpath('./src'));
codePrepare;

%% Initialization
% define an empty robotMaps
robotMapsClass=robotMaps();

%% robotName assign
% prepare data
% robotColumnIn=loadrobot('kukaIiwa14','DataFormat','column');

robotMapsClass.initialize('assignRobot');
robotMapsClass.assignRobotSlot.setRobotName('kukaIiwa14','iiwa_link_ee_kuka');
robotMapsClass.assignRobotSlot.setDexterityReference('world','iiwa_link_2','iiwa_link_4'); 
robotMapsClass.finalize('assignRobot');

%% grid generation
robotMapsClass.initialize('generateGrid');
robotMapsClass.generateGridSlot.cartesian2(1.4,0.025);
robotMapsClass.finalize('generateGrid');

%% filter Grid
robotMapsClass.initialize('filterGrid');
robotMapsClass.filterGridSlot.rangeZ(-0.355,1.4);
robotMapsClass.filterGridSlot.sphereIn([0 0 0.36],1.0);
% robotMapsClass.filterGridSlot.sphereIn([0 0 0.8],0.1);
robotMapsClass.finalize('filterGrid');

%% direction generation
robotMapsClass.initialize('generateDirection');
robotMapsClass.generateDirectionSlot.saffAndKuijlaars(0.025,20);
% robotMapsClass.generateDirectionSlot.nullRandomRotation(5);
robotMapsClass.generateDirectionSlot.alwaysNorthRotation(5);
robotMapsClass.finalize('generateDirection');

%% map Generation
robotMapsClass.initialize('generateMap');
robotMapsClass.generateMapSlot.assignAllTform();

%% reachability map
% robotMapsClass.generateMapSlot.reachability('numericMEX_parallel');
robotMapsClass.generateMapSlot.reachabilityTest('hardSkip',maxConstraintRepeatIn=15);

%% flipped dexterity map
% robotMapsClass.generateMapSlot.flippedDexterityTest();
% robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.assignSection(section=24);
% robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.assignAdjacent();
% robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.fullCalculation();
% robotMapsClass.generateMapSlot.dataList.testFlippedDexterity.marginalCalculation();

%% dexterity map
% robotMapsClass.generateMapSlot.dexterityTest();
% robotMapsClass.generateMapSlot.dataList.testDexterity.assignMode('numericMEXParallel');
% robotMapsClass.generateMapSlot.dataList.testDexterity.assignSection(section=24);
% robotMapsClass.generateMapSlot.dataList.testDexterity.assignAdjacent();

% rest of the things see these scripts 
% dexterity_gik_franka
% dexterity_nullAndRotation_franka

% example area
% [sectionNumberOut,iFlipSolutionOut] = robotMapsClass.generateMapSlot.dataList.testDexterity.pointImport(20,1,870);
% [sectionNumberOut,iFlipSolutionOut] = robotMapsClass.generateMapSlot.dataList.testDexterity.pointCalculate(20,1,870);
% robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotionSinglePoint(20,1,870,iFlipSolutionOut);
% robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotion(20,1,870,sectionNumberOut,iFlipSolutionOut);
% robotMapsClass.generateMapSlot.dataList.testDexterity.nullspaceMotion(20,1,iFlipSolutionOut);
% robotMapsClass.generateMapSlot.dataList.testDexterity.eeRotation(20,1,iFlipSolutionOut);
% [shouldContinue,iGlobalPositionOut,iSectionNumber] = robotMapsClass.generateMapSlot.dataList.testDexterity.compareGikMotion(2,1,2)
% finished = robotMapsClass.generateMapSlot.dataList.testDexterity.gikMotionEmperical(2,1,2);

%% spatial connectivity map
% index = robotMapsClass.generateMapSlot.spatialConnectivityTest();
% robotMapsClass.generateMapSlot.dataList.testSpatialConnectivity.initialize();
% robotMapsClass.generateMapSlot.dataList.testSpatialConnectivity.assignMode('numeric');
% robotMapsClass.generateMapSlot.dataList.testSpatialConnectivity.assignAdjacent('faceEdge',2,1);
% robotMapsClass.generateMapSlot.dataList.testSpatialConnectivity.fullCalculation('cubicpoly');
% robotMapsClass.generateMapSlot.dataList.testSpatialConnectivity.spatialConnectivityCount();

%% Save file
% fileSave(robotMapsClass);